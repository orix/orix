package jp.co.orix.dao.imp;



import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.GetControlNumberDao;


public class GetControlNumberDaoImpl implements GetControlNumberDao {

	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	private String controlNumber;

	@Override
	public String getControlNumber(Map<String, String> condition) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		GetControlNumberDao getControlNumberDao = session.getMapper(GetControlNumberDao.class);

		try {
			controlNumber = getControlNumberDao.getControlNumber(condition);
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}

		return controlNumber;
	}

}
