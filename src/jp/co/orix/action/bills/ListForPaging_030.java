package jp.co.orix.action.bills;

import jp.co.orix.model.Paging_030;


public class ListForPaging_030  {
	
	public static final int BlockCount = 12;
	Paging_030 paging_030 = new Paging_030();
	
	public Paging_030 getPageInfo(int currentPage) {
		paging_030.setStartRow((currentPage * BlockCount) - (BlockCount - 1));;
		paging_030.setEndRow(currentPage * BlockCount);
		return paging_030;
	}
}
