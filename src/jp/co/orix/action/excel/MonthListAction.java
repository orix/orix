package jp.co.orix.action.excel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import jp.co.orix.dao.DateTimeDao;
import jp.co.orix.dao.GetIMAML000DataListDao;
import jp.co.orix.dao.MonthListDao;
import jp.co.orix.model.Excel;
import jp.co.orix.model.MonthList;
import jp.co.orix.util.CommApi_32;
import jp.co.orix.util.Util;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

public class MonthListAction extends ActionSupport implements ServletRequestAware  {

	private MonthList monthList;
	private MonthListDao monthListDao;
	private List<MonthList> listSearch;
	private List<MonthList> listMonth;
	private int month;
	private String usePlaceCombobox;
	private String groupCombobox;
	private ArrayList<String> sixMonth;
	private String useDate;
	
	private DateTimeDao dateTimeDao;
	private GetIMAML000DataListDao getIMAML000DataListDao;
	
	public HttpServletRequest request;
	private String parameter;
	
	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;

	}

	public String formView() throws Exception {
//		String dateTime = dateTimeDao.dateTime();
//		System.out.println("DateTime : " + dateTime);
//		String getMail = getIMAML000DataListDao.getIMAML000DataList();
//		System.out.println(getMail);
		Date date = new Date();
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		sixMonth = Util.yearList(now, 5);
		
		if(useDate != null){
		String year = useDate.substring(0,4);
		String month = useDate.substring(5,7);
		String newdate = year + month;
		String m0402 = usePlaceCombobox;
		int searchDate = Integer.parseInt(newdate);
		System.out.println("받아오는값" + searchDate + m0402);
		listSearch = monthListDao.selectEx(searchDate, m0402);
		
		try {
			CommApi_32 api = new CommApi_32();
			api.setFormParamator(request, "checkContractInfo");
			HashMap<String, Object> param = api.getFormParamator("checkContractInfo");
			parameter = (String)param.get("param");
		} catch(Exception e) {
			
		}
			return SUCCESS;
		}else{
			try {
				CommApi_32 api = new CommApi_32();
				api.setFormParamator(request, "checkContractInfo");
				HashMap<String, Object> param = api.getFormParamator("checkContractInfo");
				parameter = (String)param.get("param");
			} catch(Exception e) {
				
			}

			return SUCCESS;
		}
		 
		
	}
	

	


	

	public String getUsePlaceCombobox() {		return usePlaceCombobox;	}
	public void setUsePlaceCombobox(String usePlaceCombobox) {		this.usePlaceCombobox = usePlaceCombobox;	}
	public String getGroupCombobox() {		return groupCombobox;	}
	public void setGroupCombobox(String groupCombobox) {		this.groupCombobox = groupCombobox;	}
	public HttpServletRequest getRequest() {		return request;	}
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;		
	}
	public void setRequest(HttpServletRequest request) {		this.request = request;	}
	public String getParameter() {		return parameter;	}
	public void setParameter(String parameter) {		this.parameter = parameter;	}
	public DateTimeDao getDateTimeDao() {		return dateTimeDao;	}
	public GetIMAML000DataListDao getGetIMAML000DataListDao() {		return getIMAML000DataListDao;	}
	public void setGetIMAML000DataListDao(			GetIMAML000DataListDao getIMAML000DataListDao) {		this.getIMAML000DataListDao = getIMAML000DataListDao;	}
	public void setDateTimeDao(DateTimeDao dateTimeDao) {		this.dateTimeDao = dateTimeDao;	}
	public List<MonthList> getListSearch() {		return listSearch;	}
	public int getMonth() {		return month;	}
	public void setMonth(int month) {		this.month = month;	}
	public MonthList getMonthList() {		return monthList;	}
	public void setMonthList(MonthList monthList) {		this.monthList = monthList;	}
	public void setListSearch(List<MonthList> listSearch) {		this.listSearch = listSearch;	}
	public MonthListDao getMonthListDao() {		return monthListDao;	}
	public void setMonthListDao(MonthListDao monthListDao) {		this.monthListDao = monthListDao;	}
	public List<MonthList> getListMonth() {		return listMonth;	}
	public void setListMonth(List<MonthList> listMonth) {		this.listMonth = listMonth;	}
	public ArrayList<String> getSixMonth() {		return sixMonth;	}
	public void setSixMonth(ArrayList<String> sixMonth) {		this.sixMonth = sixMonth;	}
	public String getUseDate() {		return useDate;	}
	public void setUseDate(String useDate) {		this.useDate = useDate;	}

}
