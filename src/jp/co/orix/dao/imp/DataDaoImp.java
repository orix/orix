package jp.co.orix.dao.imp;

import java.util.List;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.MonthListDao;
import jp.co.orix.model.MonthList;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class DataDaoImp implements MonthListDao{
	private MonthList monthlist = new MonthList();
	private List<MonthList> listSearch;
	private List<MonthList> listMonth;
	private List<String> excelDown;
	@Autowired
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	@Override
	public List<MonthList> selectEx(int searchDate, String m0402) {

		SqlSession session = sqlSession.openSession();
		MonthListDao dao = session.getMapper(MonthListDao.class);
		
		try {
			listSearch = dao.selectEx(searchDate, m0402);
		
		
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		return listSearch;
	}
	
	@Override
	public List<MonthList> getMonth(int getMonth) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		MonthListDao dao = session.getMapper(MonthListDao.class);
		
		try {
			listMonth = dao.getMonth(getMonth);
		
		
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		return listMonth;
	}

//	@Override
//	public List<String> excelDown(String type) {
//		SqlSession session = sqlSession.openSession();
//		DataDao dao = session.getMapper(DataDao.class);
//		try {
//			excelDown = dao.excelDown(type);
//		} catch (Exception e) {
//			// TODO: handle exception
//			System.err.println(e.getMessage());
//		}finally{
//			session.close();
//		}
//		return excelDown;
//	}
	
	
}
