<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script type="text/javascript">
window.onload = function check() {
	if(${check} == 99){
		alert("");
	}	
}

</script>
<div class="modify_confirm">
	<div class="input">
		
		<form action="changeAddressViewForm" method="post">
			ご使用場所<input type="text" value="${sinseiInfo.usePlace}"
				readonly="readonly" />
		</form>

		<s:if test="%{sinseiInfo.placeOption ==0 }">
			<input type="radio" checked="checked" value="0" disabled="disabled">該当ご使用場所のみ反映する&nbsp;&nbsp;&nbsp;
			<input type="radio" value="1" disabled="disabled">全ご使用場所に反映する
			</s:if>
		<s:else>
			<input type="radio" value="0">該当ご使用場所のみ反映する&nbsp;&nbsp;&nbsp;
			<input type="radio" checked="checked" value="1">全ご使用場所に反映する
			</s:else>


		<form action="changeAddressConfirmForm" method="get">

			<input type="hidden" name="idValue" value="${idValue }" />
			<table>
				<tr>
					<th>住所</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.shiyojusho" value="${sinseiInfo.shiyojusho}" /></td>
				</tr>
				<tr>
					<th>ご使用場所</th>
					<td><input type="text" id="read" readonly="readonly" id="read"
						name="sinseiInfo.usePlace" value="${sinseiInfo.usePlace }" /></td>
				</tr>
				<tr>
					<th>契約番号</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.keiyakuNo" value="${sinseiInfo.keiyakuNo }" /></td>
				</tr>
				<tr>
					<th>契約者名義人</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.keiyakuMeibinin"
						value="${sinseiInfo.keiyakuMeibinin }" /></td>
				</tr>
			</table>
			<br /> <br />
			<table>
				<tr>
					<th colspan="2" align="left">（１）請求書送付先情報</th>
				</tr>

				<tr>
					<th>郵便番号</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_YubinBango"
						value="${sinseiInfo.seikyushoSofuinfo_YubinBango }" /></td>
				</tr>

				<tr>
					<th>都道府県名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_Jusho1"
						value="${sinseiInfo.seikyushoSofuinfo_Jusho1 }" /></td>
				</tr>
				<tr>
					<th>市区町村名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_Jusho2"
						value="${sinseiInfo.seikyushoSofuinfo_Jusho2 }" /></td>
				</tr>
				<tr>
					<th>町名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_Jusho3"
						value="${sinseiInfo.seikyushoSofuinfo_Jusho3 }" /></td>
				</tr>
				<tr>
					<th>番地</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_Jusho4"
						value="${sinseiInfo.seikyushoSofuinfo_Jusho4 }" /></td>
				</tr>
				<tr>
					<th>ビルㆍメンション名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_BiruName"
						value="${sinseiInfo.seikyushoSofuinfo_BiruName }" /></td>
				</tr>
				<tr>
					<th>部屋番号</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_RoomNo"
						value="${sinseiInfo.seikyushoSofuinfo_RoomNo }" /></td>
				</tr>
				<tr>
					<th>屋号</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_Yago"
						value="${sinseiInfo.seikyushoSofuinfo_Yago }" /></td>
				</tr>
				<tr>
					<th>法人名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_HojinName"
						value="${sinseiInfo.seikyushoSofuinfo_HojinName }" /></td>
				</tr>
				<tr>
					<th>部署名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_BushoName"
						value="${sinseiInfo.seikyushoSofuinfo_BushoName }" /></td>
				</tr>
				<tr>
					<th>担当者１</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_Tanto1"
						value="${sinseiInfo.seikyushoSofuinfo_Tanto1 }" /></td>
				</tr>
				<tr>
					<th>担当者２</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_Tanto2"
						value="${sinseiInfo.seikyushoSofuinfo_Tanto2 }" /></td>
				</tr>
				<tr>
					<th>担当者メールアドレス１</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_MailAddress"
						value="${sinseiInfo.seikyushoSofuinfo_MailAddress }" /></td>
				</tr>
				<tr>
					<th>担当者メールアドレス２</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo.seikyushoSofuinfo_MailAddress2"
						value="${sinseiInfo.seikyushoSofuinfo_MailAddress2 }" /></td>
				</tr>
			</table>

			<br />
			<table>
				<tr>
					<th colspan="3" align="left">（２）需給管理情報</th>
				</tr>


				<tr>
					<th>法人名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo2.jukyuKanriInfo_HojinName"
						value="${sinseiInfo2.jukyuKanriInfo_HojinName }" /></td>
				</tr>

				<tr>
					<th>部署名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo2.jukyuKanriInfo_BushoName"
						value="${sinseiInfo2.jukyuKanriInfo_BushoName }" /></td>
				</tr>

				<tr>
					<th>担当者１</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo2.jukyuKanriInfo_Tanto1"
						value="${sinseiInfo2.jukyuKanriInfo_Tanto1 }" /></td>
				</tr>

				<tr>
					<th>担当者２</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo2.jukyuKanriInfo_Tanto2"
						value="${sinseiInfo2.jukyuKanriInfo_Tanto2 }" /></td>
				</tr>

				<tr>
					<th>電話番号</th>
					<td><input type="text" id="read-mini"
						name="sinseiInfo2.jukyuKanriInfo_Tel1"
						value="${sinseiInfo2.jukyuKanriInfo_Tel1}" /> <input type="text"
						id="read-mini" name="sinseiInfo2.jukyuKanriInfo_Tel2"
						value="${sinseiInfo2.jukyuKanriInfo_Tel2}" /> <input type="text"
						id="read-mini" name="sinseiInfo2.jukyuKanriInfo_Tel3"
						value="${sinseiInfo2.jukyuKanriInfo_Tel3}" /></td>
				</tr>

				<tr>
					<th>F A X番号</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo2.jukyuKanriInfo_Fax"
						value="${sinseiInfo2.jukyuKanriInfo_Fax }" /></td>
				</tr>

				<tr>
					<th>メールアドレス１</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo2.jukyuKanriInfo_MailAddress1"
						value="${sinseiInfo2.jukyuKanriInfo_MailAddress1 }" /></td>
				</tr>

				<tr>
					<th>メールアドレス２</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo2.jukyuKanriInfo_MailAddress2"
						value="${sinseiInfo2.jukyuKanriInfo_MailAddress2 }" /></td>
				</tr>
			</table>
			<br /> <br />
			<table>
				<tr>
					<th colspan="3" align="left">（３）請求問合わせ窓口情報</th>
				</tr>


				<tr>
					<th>法人名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_HojinName"
						value="${sinseiInfo3.seikyuInfo_HojinName }" /></td>
				</tr>

				<tr>
					<th>部署名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_BushoName"
						value="${sinseiInfo3.seikyuInfo_BushoName }" /></td>
				</tr>

				<tr>
					<th>担当者１</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_Tanto1"
						value="${sinseiInfo3.seikyuInfo_Tanto1 }" /></td>
				</tr>

				<tr>
					<th>担当者２</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_Tanto2"
						value="${sinseiInfo3.seikyuInfo_Tanto2 }" /></td>
				</tr>

				<tr>
					<th>電話番号</th>
					<td><input type="text" id="read-mini" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_Tel1"
						value="${sinseiInfo3.seikyuInfo_Tel1}" />-<input type="text"
						id="read-mini" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_Tel2"
						value="${sinseiInfo3.seikyuInfo_Tel2}" />-<input type="text"
						id="read-mini" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_Tel3"
						value="${sinseiInfo3.seikyuInfo_Tel3}" /></td>
				</tr>

				<tr>
					<th>F A X番号</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_Fax"
						value="${sinseiInfo3.seikyuInfo_Fax }" /></td>
				</tr>

				<tr>
					<th>メールアドレス１</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_MailAddress1"
						value="${sinseiInfo3.seikyuInfo_MailAddress1 }" /></td>
				</tr>

				<tr>
					<th>メールアドレス２</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo3.seikyuInfo_MailAddress2"
						value="${sinseiInfo3.seikyuInfo_MailAddress2 }" /></td>
				</tr>
			</table>
			<br /> <br />
			<table>
				<tr>
					<th colspan="3" align="left">（４）契約条件問合せ窓口情報</th>
				</tr>


				<tr>
					<th>法人名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo4.keiyakuInfo_HojinName"
						value="${sinseiInfo4.keiyakuInfo_HojinName }" /></td>
				</tr>

				<tr>
					<th>部署名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo4.keiyakuInfo_BushoName"
						value="${sinseiInfo4.keiyakuInfo_BushoName }" /></td>
				</tr>

				<tr>
					<th>担当者１</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo4.keiyakuInfo_Tanto1"
						value="${sinseiInfo4.keiyakuInfo_Tanto1 }" /></td>
				</tr>

				<tr>
					<th>担当者２</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo4.keiyakuInfo_Tanto2"
						value="${sinseiInfo4.keiyakuInfo_Tanto2 }" /></td>
				</tr>

				<tr>
					<th>電話番号</th>
					<td><input type="text" id="read-mini" readonly="readonly"
						name="sinseiInfo4.keiyakuInfo_Tel1"
						value="${sinseiInfo4.keiyakuInfo_Tel1}" style="width: 150px"  />  -<input type="text"
						id="read-mini" readonly="readonly" 
						name="sinseiInfo4.keiyakuInfo_Tel2"
						value="${sinseiInfo4.keiyakuInfo_Tel2}" style="width: 150px" />-<input type="text"
						id="read-mini" readonly="readonly"
						name="sinseiInfo4.keiyakuInfo_Tel3"
						value="${sinseiInfo4.keiyakuInfo_Tel3}" style="width: 150px"  /></td>
				</tr>

				<tr>
					<th>F A X番号</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo4.keiyakuInfo_Fax"
						value="${sinseiInfo4.keiyakuInfo_Fax }" /></td>
				</tr>

				<tr>
					<th>メールアドレス１</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo4.keiyakuInfo_MailAddress1"
						value="${sinseiInfo4.keiyakuInfo_MailAddress1 }" /></td>
				</tr>

				<tr>
					<th>メールアドレス２</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo4.keiyakuInfo_MailAddress2"
						value="${sinseiInfo4.keiyakuInfo_MailAddress2 }" /></td>
				</tr>
			</table>
			<br /> <br />
			<table>
				<tr>
					<th colspan="3" align="left">（５）申込担当者情報</th>
				</tr>
				<tr>
					<th>郵便番号</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo5.tantoInfo_YubinBango"
						value="${sinseiInfo5.tantoInfo_YubinBango}" /></td>
				</tr>
				<tr>
					<th>住所</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo5.tantoInfo_Jusho"
						value="${sinseiInfo5.tantoInfo_Jusho }" /></td>
				</tr>

				<tr>
					<th>会社名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo5.tantoInfo_HojinName"
						value="${sinseiInfo5.tantoInfo_HojinName }" /></td>
				</tr>

				<tr>
					<th>部署名</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo5.tantoInfo_BushoName"
						value="${sinseiInfo5.tantoInfo_BushoName }" /></td>
				</tr>

				<tr>
					<th>ご担当者</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo5.tantoInfo_Tanto1"
						value="${sinseiInfo5.tantoInfo_Tanto1 }" /></td>
				</tr>


				<tr>
					<th>電話番号</th>
					<td><input type="text" id="read-mini" readonly="readonly"
						name="sinseiInfo5.tantoInfo_Tel1"
						value="${sinseiInfo5.tantoInfo_Tel1}" /> <input type="text"
						id="read-mini" readonly="readonly"
						name="sinseiInfo5.tantoInfo_Tel2"
						value="${sinseiInfo5.tantoInfo_Tel2}" /> <input type="text"
						id="read-mini" readonly="readonly"
						name="sinseiInfo5.tantoInfo_Tel3"
						value="${sinseiInfo5.tantoInfo_Tel3}" /></td>
				</tr>


				<tr>
					<th>メールアドレス</th>
					<td><input type="text" id="read" readonly="readonly"
						name="sinseiInfo5.tantoInfo_MailAddress1"
						value="${sinseiInfo5.tantoInfo_MailAddress1 }" />${sinseiInfo5.tantoInfo_MailAddress1 }</td>
				</tr>

			</table>
			<table>
				<tr>
					<th width="20%">（６）自由記入欄</th>
					<td><textarea rows="4" cols="83"
							name="sinseiInfo5.tantoInfo_Memo" readonly="readonly"
							style="background-color: #CACACA">${sinseiInfo5.tantoInfo_Memo}</textarea></td>
				</tr>

			</table>
			<br /> <input type="button" value="戻る"
				onclick="javascript:location.href='changeAddressViewForm.action?useAddress=<s:property value="sinseiInfo.usePlace"/>';">
			<input type="submit" value="申込">
		</form>
	</div>
</div>