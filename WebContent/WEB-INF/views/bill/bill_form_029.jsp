<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@	taglib prefix="s" uri="/struts-tags" %>
<link type="text/css" rel="stylesheet" href="/resource/css/checkInformationOfContract_023_024.css">
<script type="text/javascript">
	/*  
		탭에서 할때에는 그룹과 이용장소 lock (type = 1)
		이전화면에서는 활성화  (type = 2)
	*/
	var parameter = '${parameter}'; 
	
	$(function(){
		
		if(GetQueryStringParams("type") == 1){
			$('#groupCombobox').attr("disabled","disabled");
			$('#usePlaceCombobox').attr("disabled","disabled");
		}
		
		$.ajax({url:"/getUsePlaceComboboxList",
			   dataType:'json',
			   success: function(data) {
					var json = JSON.parse(JSON.stringify(data));
					var select = $('#groupCombobox');
					var group = GetQueryStringParams("group");
					select.find('option').remove();
					$('<option>').val("").text("全件").appendTo(select);
					$.each(json.groupComboboxMap, function(key, value) {
						if (typeof(group) != "undefined" && group == key){
							$('<option>').val(key).text(value).appendTo(select).attr('selected','selected');
						}else
							$('<option>').val(key).text(value).appendTo(select);
					});
					placeComboboxList();
				}});
		
		$('#groupCombobox').change(function(event) {
			placeComboboxList();
		});
		
		function placeComboboxList(){
			var groupComboboxCode = $("select#groupCombobox").val();
			var select = $('#usePlaceCombobox');
			var place = GetQueryStringParams("place");
			if (groupComboboxCode != 0)
			{
				$.getJSON('/getUsePlaceComboboxList', {
					groupComboboxCode : groupComboboxCode
					}, function(jsonResponse) {
						select.find('option').remove();
						$.each(jsonResponse.usePlaceMap, function(key, value) {
							if (typeof(place) != "undefined" && place == key){
								$('<option>').val(key).text(value).appendTo(select).attr('selected','selected');
							}else
								$('<option>').val(key).text(value).appendTo(select);
						});
					});
			}else{
				select.find('option').remove();
				$('<option>').val("").text("全件").appendTo(select);
			}
			
			
		}
		
		$("#downLoadPDF").click(function(){
			
			
			$.ajax({url:"/download/pdfDownload?"+parameter,
			   dataType:'json',
			   success: function(data) {
					var json = JSON.parse(JSON.stringify(data));
					if(json.data.success){	
						/* 파일생성이 성공하면 다운로드 실행 */
						location.href = "/electricRates/pdfDownload?fileName="+json.data.fileName;;
					}else{
						alert("pdf file create fail");
					}
				}});
		});
		
		$("#downLoadCSV").click(function(){
			$.ajax({url:"/download/csvDownload?"+parameter,
				   dataType:'json',
				   success: function(data) {
						var json = JSON.parse(JSON.stringify(data));
						console.log(json);
						if(json.data.success){	
							/* 파일생성이 성공하면 다운로드 실행 */
							location.href = "/electricRates/csvDownload?fileName="+json.data.fileName;
						}else{
							alert("csv file create fail");
						}
					}});
		});
		
		function toggleMenu(obj) {
			if (obj.style.display == 'none')
				obj.style.display = '';
			else
				obj.style.display = 'none';
		}

		/* 검색버튼 */		
		$("#goSearchBill").click(function(){
			var type = GetQueryStringParams("type");
			var path = "/electricRates/bill?type="+ type;
					
			var group = $("#groupCombobox").val();
			if(group.length!=0)
				path += "&group=" + group;
			var place = $("#usePlaceCombobox").val();
			if(place.length!=0)
				path += "&place=" + place;
			
			var useDate = $("#useDate").val();
			path += "&useDate=" + useDate;
			window.location = path;
		});
	});
	
</script>
<div class="search-bar">
	<p class="pTag1">
		<!-- 그룹 -->
		グループ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
		<select id="groupCombobox" name="groupCombobox" class="select1"></select>
		<!-- 사용장소 -->
		&nbsp;&nbsp;&nbsp;&nbsp;ご使用場所&nbsp;&nbsp;&nbsp;&nbsp;
		<select id="usePlaceCombobox" name="usePlaceCombobox" class="select2"></select>
		&nbsp;&nbsp;&nbsp;&nbsp; 
		<!-- <input type="button" value="検索" onclick="goSearchBill();" class="submit"> -->
		<button id="goSearchBill" class="submit" value="">検索</button>
	</p>
	<div style="height: 2px; background-color:#89ABD3;">
	</div>
</div>
 <div class="content">

	<div class="date-box">
		<label class="pTag1">表示範囲</label>
		<select class="select1" id="useDate">
			<s:iterator value="twoYear" status="st">
				<option>${twoYear[st.index]}</option>
			</s:iterator>
		</select>
	</div>
	<div class="title">
		<h2>ご 請 求 内 容</h2>
	</div>
	<div class="cost-box">
		<table>
			<tr>
				<td width="50%">
					<div class="value-blue">${japanYear}</div>
				</td>
				<td width="50%">
					<div class="value-white">
						<s:property value="getText('{0,number,#,##0}',{bill.h0604})"/>円
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="use-box">
		<table>
			<tr>
			 	<td colspan="2">
					<div class="value-blue-title">1. ご使用場所</div>
				</td>
			</tr>
			<tr>
				<td width="40%">
					<div class="value-blue">住所</div>
				</td>
				<td width="60%">
					<div class="value-white" style="text-align: left; text-indent: 10px;">${bill.h0605}&nbsp;</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="value-blue">名称</div>
				</td>
				<td>
					<div class="value-white" style="text-align: left; text-indent: 10px;">
						${bill.h0606}&nbsp;
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="use-box">
		<table>
			<tr>
				<td width="40%">
					<div class="value-blue-title">2. ご使用期間</div>
				</td>
				<td width="60%">
					<div class="value-white" style="text-align: center;">
						<s:if test='type == 1'>
							${japanYMD}
						</s:if>
						<s:else>
							&nbsp;
						</s:else>
						
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="use-box">
		<table>
			<tr>
			 	<td colspan="2">
					<div class="value-blue-title">
						<s:if test='type == 1'>
							&nbsp;
						</s:if>
						<s:else>
							3. お引き落とし
						</s:else>
					</div>
				</td>
			</tr>
			<tr>
				<td width="40%">
					<div class="value-blue">銀行</div>
				</td>
				<td width="60%">
					<div class="value-white" style="text-align: left; text-indent: 10px;">
						<s:if test='type != 1'>
							${bill.h0609}&nbsp;
						</s:if>
						<s:else>
							&nbsp;
						</s:else>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="value-blue">口座番号</div>
				</td>
				<td>
					<div class="value-white" style="text-align: left; text-indent: 10px;">
						<s:if test='type != 1'>
							${bill.h0610}&nbsp;
						</s:if>
						<s:else>
							&nbsp;
						</s:else>
					</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="value-blue">口座名義</div>
				</td>
				<td>
					<div class="value-white" style="text-align: left; text-indent: 10px;">
						<s:if test='type != 1'>
							${bill.h0611}&nbsp;
						</s:if>
						<s:else>
							&nbsp;
						</s:else>
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="use-box">
		<table>
			<tr>
				<td width="40%">
					<div class="value-blue-title" >
						<s:if test='type == 1'>
							&nbsp;
						</s:if>
						<s:else>
							4. お引き落とし予定日
						</s:else>
					</div>
				</td>
				<td width="60%">
					<div class="value-white" style="text-align: center;">
						<s:date name="bill.h0612" format="yyyy年MM月dd日" />&nbsp;
					</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="info1">
		<table>
			<tr>
				<td colspan="6">
					<div class="value-blue-title">5. 電力量等内駅</div>
				</td>
			</tr>
			<tr>
				<td width="16%">
					<div class="value-glay">使用電力量</div>
				</td>
				<td width="16%">
					<div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0613})"/>&nbsp;kWh</div>
				</td>
				<td width="16%">
					<div class="value-glay">当月最大需要電力</div>
				</td>
				<td width="16%">
					<div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0616})"/>&nbsp;kW</div>
				</td>
				<td width="16%">
					
				</td>
				<td width="16%">
					
				</td>
				
			</tr>
			<tr>
				<td>
					<div class="value-glay">乗率</div>
				</td>
				<td>
					<div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0614})"/>&nbsp;</div>
				</td>
				<td> 
					<div class="value-glay">有効電力量当月分</div>
				</td>
				<td>
					<div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0633})"/>&nbsp;kWh</div>
				</td>
				<td>
					<div class="value-glay">有効電力量指示数当月分</div>
				</td>
				<td>
					<div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0617})"/>&nbsp;kWh</div>
				</td>
			</tr>
			<tr>
				<td>
					<div class="value-glay">当月力率</div>
				</td>
				<td>
					<div class="value-white">${bill.h0615}&nbsp;%</div>
				</td>
				<td>
					<div class="value-glay">無効電力量当月分</div>
				</td>
				<td>
					<div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0634})"/>&nbsp;kVarh</div>
				</td>
				<td>
					<div class="value-glay">無効電力量指示数当月分</div>
				</td>
				<td>
					<div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0618})"/>&nbsp;kVarh</div>
				</td>
			</tr>
		</table>
	</div>
	<div class="info1">
		<table>
			<tr>
				<td colspan="6">
					<div class="value-blue-title">6. オリックス株式会社　主契約料金</div>
				</td>
			</tr>
			<tr>
				<td rowspan="2" colspan="2" width="25%">
					<div class="value-glay" style="padding: 23px 0">基本料金</div>
				</td>
				<td width="17%"><div class="value-glay">i.契約電力(本体kW)</div></td>
				<td width="17%"><div class="value-glay">ii.単価(円/kW)</div></td>
				<td width="17%"><div class="value-glay">iii.力率(%)</div></td>
				<td width="24%"><div class="value-glay">基本料金</div></td>
			</tr>
			<tr>
				<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0619})"/>&nbsp;kWh</div></td>
				<s:if test='type == 1'>
					<td><div class="value-white" style="text-align: center;">-</div></td>
					<td><div class="value-white" style="text-align: center;">-</div></td>
				</s:if>
				<s:else>
					<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0620})"/>&nbsp;kW/円</div></td>
					<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0621})"/>&nbsp;</div></td>
				</s:else>
				<td><div class="value-white"><s:property value="getText('{0,number,#,##0.00}',{bill.h0622})"/>&nbsp;円</div></td>
			</tr>
			
			<s:if test='type == 1'>
				<tr>
					<td rowspan="2" colspan="2"><div class="value-glay" style="padding: 24px 0">電力量料金</div></td>
					<td><div class="value-glay">使用電力量</div></td>
					<td><div class="value-glay">単価</div></td>
					<td><div class="value-glay">燃料費調整</div></td>
					<td><div class="value-glay">電力量料金</div></td>
				</tr>
				<tr>
					<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0623})"/>&nbsp;kW</div></td>
					<td><div class="value-white" style="text-align: center;">-</div></td>
					<td><div class="value-white" style="text-align: center;">-</div></td>
					<td><div class="value-white"><s:property value="getText('{0,number,#,##0.00}',{bill.h0626})"/>&nbsp;円</div></td>
				</tr>
			</s:if>
			<s:else>
				<tr>
					<td colspan="2"><div class="value-glay">電力量料金</div></td>
					<td><div class="value-glay">使用電力量</div></td>
					<td><div class="value-glay">単価</div></td>
					<td><div class="value-glay">燃料費調整</div></td>
					<td><div class="value-glay">電力量料金</div></td>
				</tr>
				<!-- 夏季 -->
				
				<s:if test='isSummer'>
					<tr>
						<td rowspan="2"><div class="value-glay" style="padding: 24px 0">単価</div></td>
						<!-- 하계 -->
						<td><div class="value-glay" >夏季</div></td>
						<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0623})"/>&nbsp;kWh</div></td>
						<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0624})"/>&nbsp;円/kWh</div></td>
						<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0625})"/>&nbsp;円/kWh</div></td>
						<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0626})"/>&nbsp;円</div></td>
					</tr>
					<tr style="text-align: center;">
						<!--다른계절 -->
						<td><div class="value-glay">他季</div></td>
						<td><div class="value-white align-c">-</div></td>
						<td><div class="value-white align-c">-</div></td>
						<td><div class="value-white align-c">-</div></td>
						<td><div class="value-white align-c">-</div></td>
					</tr>
				</s:if>
				<s:else>
					<tr>
						<td rowspan="2"><div class="value-glay" style="padding: 24px 0">単価</div></td>
						<!-- 하계 -->
						<td><div class="value-glay" >夏季</div></td>
						<td><div class="value-white align-c">-</div></td>
						<td><div class="value-white align-c">-</div></td>
						<td><div class="value-white align-c">-</div></td>
						<td><div class="value-white align-c">-</div></td>
					</tr>
					<tr>
						<!--다른계절 -->
						<td><div class="value-glay">他季</div></td>
						<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0623})"/>&nbsp;kWh</div></td>
						<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0624})"/>&nbsp;円/kWh</div></td>
						<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0625})"/>&nbsp;円/kWh</div></td>
						<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0626})"/>&nbsp;円</div></td>
					</tr>
				</s:else>
			</s:else>
			
			<tr>
				<td colspan="2">
					<div class="value-glay">再生可能エネルギー発電促進賦課金等<br>＠東電</div>
				</td>
				<td><div class="value-white" style="padding: 14px 0"><s:property value="getText('{0,number,#,##0}',{bill.h0627})"/>&nbsp;kWh</div></td>
				<td>
				<s:if test='type == 1'>
					<div class="value-white" style="padding: 14px 0; text-align: center;">
					-
					</div>
				</s:if>
				<s:else>
					<div class="value-white" style="padding: 14px 0">
					<s:property value="getText('{0,number,#,##0}',{bill.h0628})"/>&nbsp;円/kWh
					</div>
				</s:else>
				</td>
				<td><div class="value-light-glay" style="padding: 14px 0"></div></td>
				<td><div class="value-white" style="padding: 14px 0"><s:property value="getText('{0,number,#,##0}',{bill.h0629})"/>&nbsp;円</div></td>
			</tr>
			<tr>
				<td colspan="2"><div class="value-glay">合　　　計</div></td>
				<td colspan="3"><div class="value-light-glay"></div></td>
				<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0631})"/>&nbsp;円</div></td>
			</tr>
			<tr>
				<td colspan="2"><div class="value-glay">他季1</div></td>
				<td colspan="3"><div class="value-light-glay"></div></td>
				<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0632})"/>&nbsp;円</div></td>
			</tr>
			<tr>
				<td colspan="2"><div class="value-glay">(うち消費税相当額)</div></td>
				<td colspan="3"><div class="value-light-glay" ></div></td>
				<td><div class="value-white"><s:property value="getText('{0,number,#,##0}',{bill.h0633})"/>&nbsp;円</div></td>
			</tr>
		</table>
	</div>
	<div class="info1">
		<table>
			<tr>
				<td colspan="6">
					<div class="value-blue-title">7. 最大需要電力の履歴<br></div>
				</td>
			</tr>
			<tr>
				<td><div class="value-glay">${billMonths[11].month}</div></td>
				<td><div class="value-glay">${billMonths[10].month}</div></td>
				<td><div class="value-glay">${billMonths[9].month}</div></td>
				<td><div class="value-glay">${billMonths[8].month}</div></td>
				<td><div class="value-glay">${billMonths[7].month}</div></td>
				<td><div class="value-glay">${billMonths[6].month}</div></td>
			</tr>
			<tr>
				<td><div class="value-white">${billMonths[11].kw}kW</div></td>
				<td><div class="value-white">${billMonths[10].kw}kW</div></td>
				<td><div class="value-white">${billMonths[9].kw}kW</div></td>
				<td><div class="value-white">${billMonths[8].kw}kW</div></td>
				<td><div class="value-white">${billMonths[7].kw}kW</div></td>
				<td><div class="value-white">${billMonths[6].kw}kW</div></td>
			</tr>
			<tr>
				<td><div class="value-glay">${billMonths[5].month}</div></td>
				<td><div class="value-glay">${billMonths[4].month}</div></td>
				<td><div class="value-glay">${billMonths[3].month}</div></td>
				<td><div class="value-glay">${billMonths[2].month}</div></td>
				<td><div class="value-glay">${billMonths[1].month}</div></td>
				<td><div class="value-glay">${billMonths[0].month}</div></td>
			</tr>
			<tr>
				<td><div class="value-white">${billMonths[5].kw}kW</div></td>
				<td><div class="value-white">${billMonths[4].kw}kW</div></td>
				<td><div class="value-white">${billMonths[3].kw}kW</div></td>
				<td><div class="value-white">${billMonths[2].kw}kW</div></td>
				<td><div class="value-white">${billMonths[1].kw}kW</div></td>
				<td><div class="value-white">${billMonths[0].kw}kW</div></td>
			</tr>
		</table>
	</div>
	<s:if test="type == 1">
		<div>
			
		</div>
	</s:if>
	<div class="btn-gp-box">
		<button class="btn" id="downLoadCSV">CSVダウンロード</button>
		<button class="btn" id="downLoadPDF">PDFダウンロード</button>
	</div>
</div>
<script>
	var useDate = GetQueryStringParams("useDate");
	if (typeof(useDate) != "undefined"){
		$("#useDate").val(useDate);
	}
</script>