package jp.co.orix.model;

import java.sql.Date;

public class User {
	//ユーザー識別番号
	private int M0100;
	//ユーザーID
	private String M0201;
	//ユーザー名称
	private String M0202;
	//ユーザー種別
	private String M0301;
	//ユーザー種別値
	private String M0302;
	//ユーザー状態
	private String M0303;
	//ロック状態
	private String M0305;
	//ロックステータス
	private String M0306;
	//ロック理由
	private String M0307;
	//低圧
	private String M0351;
	//高圧
	private String M0352;
	//代理店
	private String M0353;
	//最終ログイントライ日時
	private Date M0400;
	//連続ログイン失敗回数
	private int M0500;
	//本登録ユーザーＩＤ
	private String M0701;
	//本登録日時
	private Date M0702;
	//登録日時
	private Date M9901;
	//登録セッションＩＤ
	private String M9902;
	//最終更新日時
	private Date M9951;
	//最終更新セッションＩＤ
	private String M9952;
	//外部Ｉ／Ｆ日時
	private Date M9971;
	//外部Ｉ／ＦセッションＩＤ
	private String M9972;
	//外部Ｉ／ＦトランザクションＩＤ
	private int M9973;
	//削除日時
	private Date M9991;
	//削除セッションＩＤ
	private String M9992;
	
	private UserMail mail;
	
	private UserDetail detail;
	
	public User() {
		super();
		// TODO Auto-generated constructor stub
	}

	public User(int m0100, String m0201, String m0202, String m0301,
			String m0302, String m0303, String m0305, String m0306,
			String m0307, String m0351, String m0352, String m0353, Date m0400,
			int m0500, String m0701, Date m0702, Date m9901, String m9902,
			Date m9951, String m9952, Date m9971, String m9972, int m9973,
			Date m9991, String m9992) {
		M0100 = m0100;
		M0201 = m0201;
		M0202 = m0202;
		M0301 = m0301;
		M0302 = m0302;
		M0303 = m0303;
		M0305 = m0305;
		M0306 = m0306;
		M0307 = m0307;
		M0351 = m0351;
		M0352 = m0352;
		M0353 = m0353;
		M0400 = m0400;
		M0500 = m0500;
		M0701 = m0701;
		M0702 = m0702;
		M9901 = m9901;
		M9902 = m9902;
		M9951 = m9951;
		M9952 = m9952;
		M9971 = m9971;
		M9972 = m9972;
		M9973 = m9973;
		M9991 = m9991;
		M9992 = m9992;
	}

	
	public int getM0100() {
		return M0100;
	}

	public void setM0100(int m0100) {
		M0100 = m0100;
	}

	public String getM0201() {
		return M0201;
	}

	public void setM0201(String m0201) {
		M0201 = m0201;
	}

	public String getM0202() {
		return M0202;
	}

	public void setM0202(String m0202) {
		M0202 = m0202;
	}

	public String getM0301() {
		return M0301;
	}

	public void setM0301(String m0301) {
		M0301 = m0301;
	}

	public String getM0302() {
		return M0302;
	}

	public void setM0302(String m0302) {
		M0302 = m0302;
	}

	public String getM0303() {
		return M0303;
	}

	public void setM0303(String m0303) {
		M0303 = m0303;
	}

	public String getM0305() {
		return M0305;
	}

	public void setM0305(String m0305) {
		M0305 = m0305;
	}

	public String getM0306() {
		return M0306;
	}

	public void setM0306(String m0306) {
		M0306 = m0306;
	}

	public String getM0307() {
		return M0307;
	}

	public void setM0307(String m0307) {
		M0307 = m0307;
	}

	public String getM0351() {
		return M0351;
	}

	public void setM0351(String m0351) {
		M0351 = m0351;
	}

	public String getM0352() {
		return M0352;
	}

	public void setM0352(String m0352) {
		M0352 = m0352;
	}

	public String getM0353() {
		return M0353;
	}

	public void setM0353(String m0353) {
		M0353 = m0353;
	}

	public Date getM0400() {
		return M0400;
	}

	public void setM0400(Date m0400) {
		M0400 = m0400;
	}

	public int getM0500() {
		return M0500;
	}

	public void setM0500(int m0500) {
		M0500 = m0500;
	}

	public String getM0701() {
		return M0701;
	}

	public void setM0701(String m0701) {
		M0701 = m0701;
	}

	public Date getM0702() {
		return M0702;
	}

	public void setM0702(Date m0702) {
		M0702 = m0702;
	}

	public Date getM9901() {
		return M9901;
	}

	public void setM9901(Date m9901) {
		M9901 = m9901;
	}

	public String getM9902() {
		return M9902;
	}

	public void setM9902(String m9902) {
		M9902 = m9902;
	}

	public Date getM9951() {
		return M9951;
	}

	public void setM9951(Date m9951) {
		M9951 = m9951;
	}

	public String getM9952() {
		return M9952;
	}

	public void setM9952(String m9952) {
		M9952 = m9952;
	}

	public Date getM9971() {
		return M9971;
	}

	public void setM9971(Date m9971) {
		M9971 = m9971;
	}

	public String getM9972() {
		return M9972;
	}

	public void setM9972(String m9972) {
		M9972 = m9972;
	}

	public int getM9973() {
		return M9973;
	}

	public void setM9973(int m9973) {
		M9973 = m9973;
	}

	public Date getM9991() {
		return M9991;
	}

	public void setM9991(Date m9991) {
		M9991 = m9991;
	}

	public String getM9992() {
		return M9992;
	}

	public void setM9992(String m9992) {
		M9992 = m9992;
	}

	public UserMail getMail() {
		return mail;
	}

	public void setMail(UserMail mail) {
		this.mail = mail;
	}

	public UserDetail getDetail() {
		return detail;
	}

	public void setDetail(UserDetail detail) {
		this.detail = detail;
	}

	@Override
	public String toString() {
		return "User [M0100=" + M0100 + ", M0201=" + M0201 + ", M0202=" + M0202
				+ ", M0301=" + M0301 + ", M0302=" + M0302 + ", M0303=" + M0303
				+ ", M0305=" + M0305 + ", M0306=" + M0306 + ", M0307=" + M0307
				+ ", M0351=" + M0351 + ", M0352=" + M0352 + ", M0353=" + M0353
				+ ", M0400=" + M0400 + ", M0500=" + M0500 + ", M0701=" + M0701
				+ ", M0702=" + M0702 + ", M9901=" + M9901 + ", M9902=" + M9902
				+ ", M9951=" + M9951 + ", M9952=" + M9952 + ", M9971=" + M9971
				+ ", M9972=" + M9972 + ", M9973=" + M9973 + ", M9991=" + M9991
				+ ", M9992=" + M9992 + ", mail=" + mail + "]";
	}

	
	
}
