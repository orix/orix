<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<link type="text/css" rel="stylesheet" href="/resource/css/download_030.css">

<script>

	var parameter = '${parameter}' 

	$(function(){
		
		$.ajax({url:"/getUsePlaceComboboxList",
			   dataType:'json',
			   success: function(data) {
					var json = JSON.parse(JSON.stringify(data));
					var select = $('#groupCombobox');
					var group = GetQueryStringParams("group");
					select.find('option').remove();
					$('<option>').val("").text("全件").appendTo(select);
					$.each(json.groupComboboxMap, function(key, value) {
						if (typeof(group) != "undefined" && group == key){
							$('<option>').val(key).text(value).appendTo(select).attr('selected','selected');
						}else
							$('<option>').val(key).text(value).appendTo(select);
					});
					placeComboboxList();
				}});
		$('#groupCombobox').change(function(event) {
			placeComboboxList();
		});
		
		function placeComboboxList(){
			var groupComboboxCode = $("select#groupCombobox").val();
			var select = $('#usePlaceCombobox');
			var place = GetQueryStringParams("place");
			if (groupComboboxCode != 0)
			{
				$.getJSON('/getUsePlaceComboboxList', {
					groupComboboxCode : groupComboboxCode
					}, function(jsonResponse) {
						select.find('option').remove();
						$.each(jsonResponse.usePlaceMap, function(key, value) {
							if (typeof(place) != "undefined" && place == key){
								$('<option>').val(key).text(value).appendTo(select).attr('selected','selected');
							}else
								$('<option>').val(key).text(value).appendTo(select);
						});
					});
			}else{
				select.find('option').remove();
				$('<option>').val("").text("全件").appendTo(select);
			}
			
			
		}
		
		
		$("#downLoadPDF").click(function(){
			
			var downChk = document.getElementById("radiochk").elements["pkNumber"].value
			
			
			
			//테이블에서 선택한 PK의 값을 가져오기
			
			  $.ajax({
				   url:"/download/pdfDownload?type=2&"+parameter+"&num="+ downChk,
				   dataType:'json',
				   success: function(data) {
						var json = JSON.parse(JSON.stringify(data));
						if(json.data.success){	
							//파일생성이 성공하면 다운로드 실행 
							location.href = "/electricRates/pdfDownload?fileName="+json.data.fileName;;
						}else{
							alert("pdf file create fail");
						}
					}});   
		});
		
		
	})

	function myfunction1(value){
		alert(value)	
	}
	
	function groupFunction() {
	    var x = document.getElementById("groupSelect").value;
	    document.getElementById("demo").innerHTML = "You selected: " + x;
	}
	function groupFunction1() {
	    var x = document.getElementById("groupSelect1").value;
	    document.getElementById("demo1").innerHTML = "You selected: " + x;
	}
	function groupFunction2() {
	    var x = document.getElementById("groupSelect2").value;
	    document.getElementById("demo2").innerHTML = "You selected: " + x;
	}
	
	function search() {
		
		var path = "/electricRates/billDownload";
		
		var group = $("#groupCombobox").val();
		if(group.length!=0)
			path += "?group=" + group;
		var place = $("#usePlaceCombobox").val();
		if(place.length!=0)
			path += "&place=" + place;
		
		var useDate = $("#useDate").val();
		path += "&useDate=" + useDate;
		window.location = path;
	};
</script>
<br>

<div class="download" align="center">

		

	<div class="download_form">			

				グループ <select id="groupCombobox" name="searchModel_030.h0602"
					onchange="groupFunction()" >
					
					<s:iterator value="groupList">
						<option value="<s:property value="h0602" />"><s:property
								value="h0602" /></option>
					</s:iterator>
				</select>&nbsp;

				<!-- <p id="demo"></p> -->

				ご使用場所 <select id="usePlaceCombobox" name="searchModel_030.h0603"
					onchange="groupFunction1()">
					<option value="0">全件</option>
					<s:iterator value="groupList">
						<option value="<s:property value="h0603" />"><s:property
								value="h0603" /></option>
					</s:iterator>
				</select>&nbsp;

				<!-- <p id="demo1"></p> -->

				出力年月 検索 <select id="useDate" name="tempDate"
					onchange="groupFunction2()">
					<s:iterator value="dateList" status="statusVar">
						<option>${dateList[statusVar.index]}</option>
					</s:iterator>
				</select>&nbsp;

				<!-- <p id="demo2"></p> -->

				<button class="" onclick="search()">検索</button>
		</div>

	</div>

<hr>

	<div class="download_table">
		<form id="radiochk">
		<table>
		
			<tr>
				<th width="10%">No</th>
				<th width="15%">選択</th>
				<th>ご使用場所</th>
				<th>電気料金(円)</th>
				<th>使用量(kwh)</th>
			</tr>
			
			<s:iterator value="searchedList" status="a">
			<tr>
				<td>${a.count}</td>
				<td><input type="radio" name="pkNumber" value="<s:property value="H0601"/>"
						onclick="myfunction(this.value)">
				</td>
				<td><s:property value="H0602" /></td>
				<td><s:property value="H0613" /></td>
				<td><s:property value="H0631" /></td>
			</tr>
			</s:iterator>
			
	</table></form>
<br>			
<br>			
<br>

	<h1>페이지처리 </h1>
	<c:forEach var="i" begin="1" end="${totalPage}" step="1">
		<a href='/electricRates/billDownload?group=${param.group}&place=${param.place}&useDate=${param.useDate}&currentPage=${i}'>${i}</a>
	</c:forEach>
	
	<h2>전체 글 개수 : <s:property value="rowCount"/></h2>
	<h3>총페이지 : <s:property value="totalPage"/></h3>			
	<input class="download_button" id="downLoadPDF" type="button" value="ダウンロード">
</div>

<script>
	var useDate = GetQueryStringParams("useDate");
	if (typeof(useDate) != "undefined"){
		$("#useDate").val(useDate);
	}
</script>