<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<title>登録情報変更</title>

<style>
table {
	border-collapse: collapse;
	align: center;
	width: 40%;
}

th {
	background-color: #4F81BD;
	padding: 10px;
	color: white;
	border-radius: 8px;
	border-color: #000000;
	border: 2px solid white;
	font-size: 20px;
}

td {
	background-color: #FFFFFF;
	color: black;
	border: 2px white;
	border-radius: 5px;
	font-size: 20px;
}

input[type=text] {
	width: 100%;
	padding: 10px;
	margin: 0 0;
	box-sizing: border-box;
	border: 2px solid #1E4F8B;
	border-radius: 8px;
	font-size: 20px;
}

input[type=button] {
   background-color: #4F81BD;
    box-shadow: 0 4px 16px 0 rgba(0,0,0,0.2), 0 4px 10px 0 rgba(0,0,0,0.19);
    border: none;
    color: white;
    padding: 12px 60px;
    text-decoration: none;
    margin: 4px 8px;
    cursor: pointer;
    text-align: center;
    border-radius: 8px;
    font-size: 20px;
    display: inline-block;
    
}

.div{
	align:center;
}

input[type=submit] {
   background-color: #4F81BD;
    box-shadow: 0 4px 16px 0 rgba(0,0,0,0.2), 0 4px 10px 0 rgba(0,0,0,0.19);
    border: none;
    color: white;
    padding: 12px 60px;
    text-decoration: none;
    margin: 4px 8px;
    cursor: pointer;
    text-align: center;
    border-radius: 8px;
    font-size: 20px;
}

</style>
<script>
function goPrev() {
	window.location = "User";
}
</script>
</head>
<body>

	<div align="center" style="color: #245A82">
		<h2>申込内容確認</h2>
		<br />
	</div>

	<div align="center">
		<form method="post" name="checkUser" action="updateUser">
			<table>
				<tr>
				
					<th colspan="3" align="left">ユーザーID情報</th>
				</tr>

				<tr>
					<th align="center">郵便番号</th>

					<td>
					<input type="hidden" name="H1801" value="${sessionScope.M0100}">
					<input type="hidden" name="H1802" value="${H1802}">
					<input type="hidden" name="H1807" value="${H1807}">
					<input type="text" value="${H1807}" disabled></td>
				</tr>

				<tr>
					<th align="center">住所</th>
					<td><input type="hidden" name="H1809" value="${H1809}">
					<input type="text" value="${H1809}" disabled></td>
				</tr>
				
				<tr>
					<th align="center">ビル・マンション名</th>
					<td><input type="hidden" name="H1811" value="${H1811}">
					<input type="text" value="${H1811}" disabled></td>
				</tr>
				
				<tr>
					<th align="center">会社名</th>
					<td><input type="hidden" name="H1813" value="${H1813}">
					<input type="text" value="${H1813}" disabled></td>
				</tr>
				
				<tr>
					<th align="center">部署名</th>
					<td><input type="hidden" name="H1815" value="${H1815}">
					<input type="text" value="${H1815}" disabled></td>
				</tr>
				
				<tr>
					<th align="center">ご担当者</th>
					<td><input type="hidden" name="H1817" value="${H1817}">
					<input type="text" value="${H1817}" disabled></td>
				</tr>
				
				<tr>
					<td><br/></td>
				</tr>

				<tr>
					<th align="center">メールアドレス</th>
					<td><input type="hidden" name="newM0200"  value="${newM0200}" >
					<input type="text" value="${newM0200}" disabled></td>
				</tr>
				
				<tr>
				<td colspan="2"><br/>
				<div align="center" style="color: #245A82">
				<h3>上記内容に間違いがない場合、「申込」ボタンを押してください。</h3>
				<h3>内容を変更する場合は、「戻る」ボタンを押してください。</h3>
				</div>
				</td>
				</tr>
			</table>
			<div class="div">
						<input type="button" value="戻る" onClick="goPrev()"/>	
						<input type="submit" value="申込"/>
			</div>
		</form>
	</div>