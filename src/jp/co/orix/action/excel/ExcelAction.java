package jp.co.orix.action.excel;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URLEncoder;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.orix.dao.ExcelDao;

import com.opensymphony.xwork2.ActionSupport;

public class ExcelAction extends ActionSupport {
	public HttpServletRequest request;
	public HttpServletResponse response;
	private String[] M0621;
	private ExcelDao excelDao;
	private ExcelMerge excelmerge;
	private static final long serialVersionUID = -5920613370450411237L;
//	private String fileUploadPath = "E:/ExcelTest/拡張子名.zip";
	private String fileUploadPath = "//192.168.0.53/Share/ExcelTest/計量日誌.zip";
//	\\192.168.0.53\Share\ExcelTest
	private long contentLength;
	private FileInputStream inputStream;
	private FileOutputStream outputStream;
	private String contentDisposition;
	private String filename = "計量日誌.zip";
	File file = new File(fileUploadPath);
	
	public String excelAction() throws Exception{
		System.out.println(M0621.toString());
		ExcelMerge.ExcelMerge(M0621);
		
		setContentLength(file.length());
		setContentDisposition("attachment;filename=" + URLEncoder.encode(filename, "UTF-8"));
		inputStream = new FileInputStream(fileUploadPath);
		
		return SUCCESS;
	}



	public String down() throws Exception {

		
		return SUCCESS;
	}

	public String getFileUploadPath() {
		return fileUploadPath;
	}
	public void setFileUploadPath(String fileUploadPath) {
		this.fileUploadPath = fileUploadPath;
	}
	public long getContentLength() {
		return contentLength;
	}
	public void setContentLength(long contentLength) {
		this.contentLength = contentLength;
	}
	public InputStream getInputStream() {
		return inputStream;
	}
//	public void setInputStream(InputStream inputStream) {
//		this.inputStream = inputStream;
//	}
	
	public String getContentDisposition() {
		return contentDisposition;
	}
	public void setInputStream(FileInputStream inputStream) {
		this.inputStream = inputStream;
	}



	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}
	public String getFilename() {
		return filename;
	}
	public void setFileName(String filename) {
		this.filename = filename;
	}
	public static long getSerialversionuid() {
		return serialVersionUID;
	}
	public void setFilename(String filename) {
		this.filename = filename;
	}
	
	public HttpServletRequest getRequest() {
		return request;
	}
	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}
	public HttpServletResponse getResponse() {
		return response;
	}



	public void setResponse(HttpServletResponse response) {
		this.response = response;
	}



	public FileOutputStream getOutputStream() {
		return outputStream;
	}



	public void setOutputStream(FileOutputStream outputStream) {
		this.outputStream = outputStream;
	}



	public File getFile() {
		return file;
	}



	public void setFile(File file) {
		this.file = file;
	}



	public String[] getM0621() {		return M0621;	}
	public void setM0621(String[] m0621) {		M0621 = m0621;	}
	public ExcelMerge getExcelmerge() {		return excelmerge;	}
	public ExcelDao getExcelDao() {		return excelDao;	}
	public void setExcelDao(ExcelDao excelDao) {		this.excelDao = excelDao;	}
	public void setExcelmerge(ExcelMerge excelmerge) {		this.excelmerge = excelmerge;	}


	
	
}
