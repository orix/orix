package jp.co.orix.model;

import java.util.Date;

public class IMACP000 {
	
	private String acceptNumber;   // 접수 번호
	private String changeDivision;   // 변경 내용 구분
	private String requestDate;   // 신청날짜
	private String potalName;   // 포털 구분
	private String p1;   // 유저ID
	private String p1_name;   // 사용자 이름
	private String p1_Number;   // 유저ID 식별 번호
	private String groupName;   // 그룹
	private String location;   // 사용장소
	private Date registrationDate;   // 등록날짜
	private String registrationSessionID;   // 등록세션ID
	
	
	public String getAcceptNumber() {
		return acceptNumber;
	}
	public void setAcceptNumber(String acceptNumber) {
		this.acceptNumber = acceptNumber;
	}
	public String getChangeDivision() {
		return changeDivision;
	}
	
	public void setChangeDivision(String changeDivision) {
		this.changeDivision = changeDivision;
	}
	public String getRequestDate() {
		return requestDate;
	}
	public void setRequestDate(String requestDate) {
		this.requestDate = requestDate;
	}
	public String getPotalName() {
		return potalName;
	}
	public void setPotalName(String potalName) {
		this.potalName = potalName;
	}
	public String getP1() {
		return p1;
	}
	public void setP1(String p1) {
		this.p1 = p1;
	}
	public String getP1_name() {
		return p1_name;
	}
	public void setP1_name(String p1_name) {
		this.p1_name = p1_name;
	}
	public String getP1_Number() {
		return p1_Number;
	}
	public void setP1_Number(String p1_Number) {
		this.p1_Number = p1_Number;
	}
	public String getGroupName() {
		return groupName;
	}
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public Date getRegistrationDate() {
		return registrationDate;
	}
	public void setRegistrationDate(Date registrationDate) {
		this.registrationDate = registrationDate;
	}
	public String getRegistrationSessionID() {
		return registrationSessionID;
	}
	public void setRegistrationSessionID(String registrationSessionID) {
		this.registrationSessionID = registrationSessionID;
	}
	@Override
	public String toString() {
		return "IMACP000 [acceptNumber=" + acceptNumber + ", changeDivision="
				+ changeDivision + ", requestDate=" + requestDate
				+ ", potalName=" + potalName + ", p1=" + p1 + ", p1_name="
				+ p1_name + ", p1_Number=" + p1_Number + ", groupName="
				+ groupName + ", location=" + location + ", registrationDate="
				+ registrationDate + ", registrationSessionID="
				+ registrationSessionID + "]";
	}
	

}
