package jp.co.orix.dao;

import java.util.ArrayList;
import java.util.Date;

import org.apache.ibatis.annotations.Param;

import jp.co.orix.model.Bill;
import jp.co.orix.model.BillMonth;

public interface BillDao {
	public ArrayList<Bill> findAll();
	
	//@Param("billINumber") Integer billINumber,
	public Bill findByBillInfo(@Param("group") String group, 
									 @Param("area") String area,
									 @Param("uesDate") Date uesDate,
									 @Param("num") Integer num);
	
	public ArrayList<BillMonth>findByBillYearList(@Param("endDate") String endDate);
	
	
	
	public Bill findByBillAll(@Param("uesDate") Date uesDate);
	
}
