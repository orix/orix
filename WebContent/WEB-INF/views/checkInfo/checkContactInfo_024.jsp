<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script>
	/* 내용 보여주기 */
	function toggleMenu(obj) {
		if (obj.style.display == 'none')
			obj.style.display = '';
		else
			obj.style.display = 'none';
	}

	<!--
	onclick = "document.getElementById('table2').style.display='block'"
	-->
	/* 검색버튼 */
	function goSearch() {
		var groupCombobox = document.getElementsByName("groupCombobox")[0].value;
		var usePlaceCombobox = document.getElementsByName("usePlaceCombobox")[0].value;

		if (groupCombobox == '') {
			alert('グループを選択して下さい。');
			return false;
		}

		if (usePlaceCombobox == '') {
			alert('ご使用場所を選択して下さい。');
			return false;
		}

		window.location = "checkContactInfo?groupCombobox=" + groupCombobox
				+ "&usePlaceCombobox=" + usePlaceCombobox
	}

	function goRenewal() {
		var groupCombobox = document.getElementsByName("groupCombobox")[0].value;
		var usePlaceCombobox = document.getElementsByName("usePlaceCombobox")[0].value;

		window.location = "/changeAddressViewForm?groupCombobox="
				+ groupCombobox + "&useAddress=" + usePlaceCombobox

	}

	function loadInformationOnfailure() {
		alert('対象データが存在しません。');
	}
</script>
<link type="text/css" rel="stylesheet"
	href="/resource/css/checkInformationOfContract_023_024.css">
<script type="text/javascript"
	src="/resource/js/selectBox/getGroupAndUsePlaceComboboxList1.js?ver=1.1"></script>
<form>
	<input type="hidden" name="parameter" id="parameter"
		value="${parameter }">

	<!-- 高圧連絡先照会データが取得できなかった場合、『共通メッセージ（E0005）』を出力する。 -->
	<c:if test="${contactInfoList.size() == 0 }">
		<script>
			loadInformationOnfailure()
		</script>
	</c:if>

	<p class="pTag1">
		<!-- 그룹 -->
		グループ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <select
			id="groupCombobox" name="groupCombobox" class="select1">
			<option value=""></option>
			<%-- <c:forEach var="groupComboboxInfo" items="${universalInfoList }">
				<option value="${groupComboboxInfo.t0200 }">${groupComboboxInfo.t0401 }</option>
			</c:forEach> --%>
		</select>
		<!-- 사용장소 -->
		&nbsp;&nbsp;&nbsp;&nbsp;ご使用場所&nbsp;&nbsp;&nbsp;&nbsp;<select
			id="usePlaceCombobox" name="usePlaceCombobox" class="select2"><option
				value=""></option></select>&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
			value="検索" onclick="goSearch();" class="submit">
	</p>

	<hr class="hr_top">
	<br>
	<!-- (1)請求書 ・ その他の文書の送付先 -->
	<div class="table024">
		<table>
			<tr>
				<th><a onclick="toggleMenu(table1);">(1) 請求書 ・ その他の文書の送付先</a></th>
				<td><input type="button" value="更新" onclick="goRenewal()"
					class="button"></td>
			</tr>
		</table>
	</div>
	<div class="table" id="table1" style="display: none">
		<br>
		<table>
			<c:forEach items="${contactInfoList}" var="item" varStatus="row">
				<c:if test="${row.index == 0}">
					<tr>
						<td>郵便番号</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1304}" readonly="readonly"></td>
					</tr>
					<tr>
						<td>住所</td>
						<td><input type="text" name="" value="${item.h0305}"
							readonly="readonly"></td>
					</tr>
					<tr>
						<td>ビル名称</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1312 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>法人名</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1318 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>部署名</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1320 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者１</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1322 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者２</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1324 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者メールアドレス１</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1326 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者メールアドレス１</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1328 }" readonly="readonly"></td>
					</tr>
				</c:if>
			</c:forEach>
		</table>
	</div>
	<!-- （２）　停電管理項目　（停電連絡等を含む） -->
	<div class="table024">
		<br>
		<table>
			<tr>
				<th><a onclick="toggleMenu(table2);">(2) 停電管理項目 （停電連絡等を含む）</a></th>
				<td><input type="button" value="更新" onclick="goRenewal()"
					class="button"></td>
			</tr>
		</table>
	</div>
	<div class="table" id="table2" style="display: none">
		<br>
		<table>
			<c:forEach items="${contactInfoList}" var="item" varStatus="row">
				<c:if test="${row.index == 1}">
					<tr>
						<td>法人名</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1318 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>部署名</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1320 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者１</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1322 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者２</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1324 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>電話番号</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1332 }-${item.contactDetailInfo.h1333 }-${item.contactDetailInfo.h1334 }"
							readonly="readonly"></td>
					</tr>
					<tr>
						<td>FAX番号</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1336 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>メールアドレス１</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1326 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>メールアドレス2</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1328 }" readonly="readonly"></td>
					</tr>
				</c:if>
			</c:forEach>
		</table>
	</div>
	<!-- （３）　請求に関する問い合わせ窓口 -->
	<div class="table024">
		<br>
		<table>
			<tr>
				<th><a onclick="toggleMenu(table3);">(3) 請求に関する問い合わせ窓口 </a></th>
				<td><input type="button" value="更新" onclick="goRenewal()"
					class="button"></td>
			</tr>
		</table>
	</div>
	<div class="table" id="table3" style="display: none">
		<br>
		<table>
			<c:forEach items="${contactInfoList}" var="item" varStatus="row">
				<c:if test="${row.index == 2}">
					<tr>
						<td>法人名</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1318 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>部署名</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1320 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者１</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1322 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者２</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1324 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>電話番号</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1332 }-${item.contactDetailInfo.h1333 }-${item.contactDetailInfo.h1334 }"
							readonly="readonly"></td>
					</tr>
					<tr>
						<td>FAX番号</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1336 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>メールアドレス１</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1326 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>メールアドレス2</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1328 }" readonly="readonly"></td>
					</tr>
				</c:if>
			</c:forEach>
		</table>
	</div>
	<!-- （４）　契約案件の協議窓口 -->
	<div class="table024">
		<br>
		<table>
			<tr>
				<th><a onclick="toggleMenu(table4);">(4) 契約案件の協議窓口</a></th>
				<td><input type="button" value="更新" onclick="goRenewal()"
					class="button"></td>
			</tr>
		</table>
	</div>
	<div class="table" id="table4" style="display: none">
		<br>
		<table>
			<c:forEach items="${contactInfoList}" var="item" varStatus="row">
				<c:if test="${row.index == 3}">
					<tr>
						<td>法人名</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1318 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>部署名</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1320 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者１</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1322 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>担当者２</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1324 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>電話番号</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1332 }-${item.contactDetailInfo.h1333 }-${item.contactDetailInfo.h1334 }"
							readonly="readonly"></td>
					</tr>
					<tr>
						<td>FAX番号</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1336 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>メールアドレス１</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1326 }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>メールアドレス2</td>
						<td><input type="text" name=""
							value="${item.contactDetailInfo.h1328 }" readonly="readonly"></td>
					</tr>
				</c:if>
			</c:forEach>
		</table>
	</div>
	<div class="table">
		<br> <br> <br>
		<p class="bottom">
			高圧は受け処理当月の月末に反映のため、現在のご契約内容を表示しております。<br>
			更新のご依頼をいただいてから、Web表示反映にお時間をいただくことがあります<br>ので、あらかじめご了承ください。
		</p>
	</div>

</form>