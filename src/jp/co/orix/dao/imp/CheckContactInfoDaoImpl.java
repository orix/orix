package jp.co.orix.dao.imp;

import java.util.List;
import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.CheckContactInfoDao;
import jp.co.orix.model.ContactInfoHV003;

public class CheckContactInfoDaoImpl implements CheckContactInfoDao {

	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	private List<ContactInfoHV003> contactInfoList;

	@Override
	public List<ContactInfoHV003> getDestinationOfBillBy(Map<String, String> condition) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		CheckContactInfoDao checkContactInfoDao = session.getMapper(CheckContactInfoDao.class);

		try {
			contactInfoList = checkContactInfoDao.getDestinationOfBillBy(condition);
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}

		return contactInfoList;
	}

	@Override
	public List<ContactInfoHV003> getPowerManagementEntriesBy(
			Map<String, String> condition) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		CheckContactInfoDao checkContactInfoDao = session.getMapper(CheckContactInfoDao.class);

		try {
			contactInfoList = checkContactInfoDao.getPowerManagementEntriesBy(condition);
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}

		return contactInfoList;
	}

	@Override
	public List<ContactInfoHV003> getInfoOfEnquiriesBy(
			Map<String, String> condition) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		CheckContactInfoDao checkContactInfoDao = session.getMapper(CheckContactInfoDao.class);

		try {
			contactInfoList = checkContactInfoDao.getInfoOfEnquiriesBy(condition);
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}

		return contactInfoList;
	}

	@Override
	public List<ContactInfoHV003> getInfoOfCooperationBy(
			Map<String, String> condition) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		CheckContactInfoDao checkContactInfoDao = session.getMapper(CheckContactInfoDao.class);

		try {
			contactInfoList = checkContactInfoDao.getInfoOfCooperationBy(condition);
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}

		return contactInfoList;
	}


}
