package jp.co.orix.model;

import java.sql.Date;

public class UserMailIndex {
	//メール内容識別番号
	private int M0101;
	//メール内容種類
	private String M0102;
	//メール内容種類名
	private String M0103;
	//メール内容適用開始日時
	private Date M0104;
	//追加宛先メールアドレス
	private String M0301;
	//追加ＣＣ
	private String M0302;
	//追加ＢＣＣ
	private String M0303;
	//件名
	private String M0400;
	//本文
	private String M0500;
	//メール内容条件有効
	private String M0601;
	//メール内容条件有効値
	private String M0602;
	//メール内容条件需要場所番号開始
	private String M0603;
	//メール内容条件需要場所番号終了
	private String M0604;
	//登録日時
	private Date M9901;
	//登録セッションＩＤ
	private String M9902;
	//最終更新日時
	private Date M9951;
	//最終更新セッションＩＤ
	private String M9952;
	//削除日時
	private Date M9991;
	//削除セッションＩＤ
	private String M9992;
	//ユーザー識別番号
	private int M9993;

	public UserMailIndex() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserMailIndex(int m0101, String m0102, String m0103, Date m0104,
			String m0301, String m0302, String m0303, String m0400,
			String m0500, String m0601, String m0602, String m0603,
			String m0604, Date m9901, String m9902, Date m9951, String m9952,
			Date m9991, String m9992, int m9993) {
		super();
		M0101 = m0101;
		M0102 = m0102;
		M0103 = m0103;
		M0104 = m0104;
		M0301 = m0301;
		M0302 = m0302;
		M0303 = m0303;
		M0400 = m0400;
		M0500 = m0500;
		M0601 = m0601;
		M0602 = m0602;
		M0603 = m0603;
		M0604 = m0604;
		M9901 = m9901;
		M9902 = m9902;
		M9951 = m9951;
		M9952 = m9952;
		M9991 = m9991;
		M9992 = m9992;
		M9993 = m9993;
	}

	public int getM0101() {
		return M0101;
	}

	public void setM0101(int m0101) {
		M0101 = m0101;
	}

	public String getM0102() {
		return M0102;
	}

	public void setM0102(String m0102) {
		M0102 = m0102;
	}

	public String getM0103() {
		return M0103;
	}

	public void setM0103(String m0103) {
		M0103 = m0103;
	}

	public Date getM0104() {
		return M0104;
	}

	public void setM0104(Date m0104) {
		M0104 = m0104;
	}

	public String getM0301() {
		return M0301;
	}

	public void setM0301(String m0301) {
		M0301 = m0301;
	}

	public String getM0302() {
		return M0302;
	}

	public void setM0302(String m0302) {
		M0302 = m0302;
	}

	public String getM0303() {
		return M0303;
	}

	public void setM0303(String m0303) {
		M0303 = m0303;
	}

	public String getM0400() {
		return M0400;
	}

	public void setM0400(String m0400) {
		M0400 = m0400;
	}

	public String getM0500() {
		return M0500;
	}

	public void setM0500(String m0500) {
		M0500 = m0500;
	}

	public String getM0601() {
		return M0601;
	}

	public void setM0601(String m0601) {
		M0601 = m0601;
	}

	public String getM0602() {
		return M0602;
	}

	public void setM0602(String m0602) {
		M0602 = m0602;
	}

	public String getM0603() {
		return M0603;
	}

	public void setM0603(String m0603) {
		M0603 = m0603;
	}

	public String getM0604() {
		return M0604;
	}

	public void setM0604(String m0604) {
		M0604 = m0604;
	}

	public Date getM9901() {
		return M9901;
	}

	public void setM9901(Date m9901) {
		M9901 = m9901;
	}

	public String getM9902() {
		return M9902;
	}

	public void setM9902(String m9902) {
		M9902 = m9902;
	}

	public Date getM9951() {
		return M9951;
	}

	public void setM9951(Date m9951) {
		M9951 = m9951;
	}

	public String getM9952() {
		return M9952;
	}

	public void setM9952(String m9952) {
		M9952 = m9952;
	}

	public Date getM9991() {
		return M9991;
	}

	public void setM9991(Date m9991) {
		M9991 = m9991;
	}

	public String getM9992() {
		return M9992;
	}

	public void setM9992(String m9992) {
		M9992 = m9992;
	}

	public int getM9993() {
		return M9993;
	}

	public void setM9993(int m9993) {
		M9993 = m9993;
	}

}
