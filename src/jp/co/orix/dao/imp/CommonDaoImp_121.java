package jp.co.orix.dao.imp;

import java.util.Map;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.CommonDao_121;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class CommonDaoImp_121 implements CommonDao_121 {

	@Autowired
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();

	@Override
	public int insertIMACP000(Map map) {

		SqlSession session = sqlSession.openSession();
		CommonDao_121 dao = session.getMapper(CommonDao_121.class);

		try {
			int check = dao.insertIMACP000(map);

			return check;
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.commit();
			session.close();
		}

		return 0;
	}

	@Override
	public int maxSeqIDUMS000() {

		SqlSession session = sqlSession.openSession();
		CommonDao_121 dao = session.getMapper(CommonDao_121.class);

		try {
			int value = dao.maxSeqIDUMS000();

			return value;
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.commit();
			session.close();
		}

		return 0;
	}

	@Override
	public void insertSendMail(Map map) {

		SqlSession session = sqlSession.openSession();
		CommonDao_121 dao = session.getMapper(CommonDao_121.class);

		try {
			dao.insertSendMail(map);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.commit();
			session.close();
		}

	}

}
