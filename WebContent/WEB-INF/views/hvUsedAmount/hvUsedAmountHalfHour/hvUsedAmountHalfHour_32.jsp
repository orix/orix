 <%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
  <script src="<%=request.getContextPath()%>/resource/js/chart/Chart.js"></script>
  <script src="<%=request.getContextPath()%>/resource/js/common_32.js"></script>
  <link type="text/css" rel="stylesheet" href="<%= request.getContextPath() %>/resource/css/common_32.css"/>
  <link type="text/css" rel="stylesheet"
	href="<%=request.getContextPath()%>/resource/css/checkInformationOfContract_023_024.css">
  <script type="text/javascript">
  	function send(){
  		if(validation()){
			var form = document.forms[0];
			
			var groupCombobox = document.getElementsByName("groupCombobox")[0].value;
			var usePlaceCombobox = document.getElementsByName("usePlaceCombobox")[0].value;
			
			form.setAttribute("method", "post");
			form.setAttribute("action", "/hvUsedAmount/hvUsedAmountHalfHour.action?groupCombobox="
					+ groupCombobox + "&usePlaceCombobox=" + usePlaceCombobox);
			
			var chkBox = document.getElementsByName("compChk")[0];
			
			if(chkBox.checked == false){
				chkBox.checked = true
				chkBox.value="off";
			}else{
				chkBox.value="on";
			}
			
			form.submit();
		}
	}
  	
  	function csvDownload(){
  		if(!confirm("ファイルのダウンロードを行います。よろしいですか？"))
  			return;
  		var form = document.forms[0];
  		form.setAttribute("method", "post");
  		form.setAttribute("action", "/hvUsedAmount/hvUsedAmountHalfHourCsvDownload.action");
  		
  		var chkBox = document.getElementsByName("compChk")[0];
  		
  		if(chkBox.checked == false){
  			chkBox.checked = true
  			chkBox.value="off";
  		}else{
  			chkBox.value="on";
  		}
  		
  		form.submit();
  	}
  	
	/* 내용 보여주기 */
	function toggleMenu(obj) {
		if (obj.style.display == 'none')
			obj.style.display = '';
		else
			obj.style.display = 'none';
	}

	/* 검색버튼 */
	function goSearch() {
		var groupCombobox = document.getElementsByName("groupCombobox")[0].value;
		var usePlaceCombobox = document.getElementsByName("usePlaceCombobox")[0].value;

		window.location = "/hvUsedAmount/hvUsedAmountHalfHour.action?groupCombobox="
				+ groupCombobox + "&usePlaceCombobox=" + usePlaceCombobox;
	}
	
	function validation(){
		//１)	グループチェック
		var groupCombobox = document.getElementsByName('groupCombobox')[0];
		if(!IsValidCombobox(groupCombobox)){
			//共通メッセージ(E0029)
			alert("グループに無効な値が選択されています。");
			return false;
		}
	//	2)	ご使用場所チェック

		var usePlaceCombobox = document.getElementsByName('usePlaceCombobox')[0];
		if(!IsValidCombobox(usePlaceCombobox)){
			//共通メッセージ(E0029)
			alert("ご使用場所に無効な値が選択されています。");
			return false;
		}
		
		//3)指定日チェック
		var specDateStr = document.getElementsByName('specDate')[0].value;
		
		if(!(IsValidateStringValue(specDateStr)&&IsDate(specDateStr))){
			alert('指定日' + 'に無効な値が入力されています。');
			return false;
		}
		
		//4)	比較日チェック
		if(document.getElementsByName("compChk")[0].checked){
			var compDateStr = document.getElementsByName('compDate')[0].value;
			if(!(IsValidateStringValue(compDateStr)&&IsDate(compDateStr))){
				alert('比較日' + 'に無効な値が入力されています。');
				return false;
			}
		}
		return true;
	}
	
  </script>

<script type="text/javascript"
	src="<%=request.getContextPath()%>/resource/js/selectBox/getGroupAndUsePlaceComboboxList2.js?ver=1.1"></script>

	<p class="pTag1">
		<!-- 그룹 -->
		グループ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <select
			id="groupCombobox" name="groupCombobox" class="select1">
			<option value=""></option>
		</select>
		<!-- 사용장소 -->
		&nbsp;&nbsp;&nbsp;&nbsp;ご使用場所&nbsp;&nbsp;&nbsp;&nbsp;<select
			id="usePlaceCombobox" name="usePlaceCombobox" class="select2"><option
				value=""></option></select>&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
			value="検索" onclick="goSearch();" class="submit">
	</p>
	<hr class="hr_top">
	<br>
	<form>
		<input type="hidden" name="parameter" value="${parameter}">
		<input type="hidden" name="customerContractNumber" value="${customerContractNumber}">
	<div class="content">
		<!-- 案内文書 -->
		<div class="UIdesign1">
			<span>ご指定の１日の電力ご使用を３０分単位でご確認いただけます</span>
		</div>
		<!--　指定日と比較日  -->
		<div class="UIdesign1">
			<!-- 指定日　３番 -->
			<span>指定日</span><input class="UIdesign1"type="text" name="specDate" value="${specDate}">
			<!-- 比較日　４番 -->
			<span>比較日</span><input class="UIdesign1" type="text" name="compDate" value="${compDate}">
			<!-- 比較表示　13番 -->
			<input type="checkbox" name="compChk" value="${compChk }"> <span>比較表示</span>
			<!-- 表示　9番 -->
			<input class="UIdesign1" id="compare" type="button" value="表示" onClick="send();">
		</div>
		<!--　使用量グラフ  ８番-->
			<div class="UIdesign1" id="graph">
	       	 	<canvas id="canvas"></canvas>
	    	</div>
		<!-- 管理者コメント	１０番 -->
		<div class="UIdesign1" id="adminMessage">
			<span>
				${adminMessage}
			</span>
			
		</div>
		<!-- 11	使用量リスト　-->
		<div class="UIdesign1">
			<table id="useList">
				<tr>
					<th>時間</th>
					<th>指定日使用量</th>
					<th>比較日使用量</th>
					<th>差分使用量</th>
					<th>時間</th>
					<th>指定日使用量</th>
					<th>比較日使用量</th>
					<th>差分使用量</th>
				</tr>
				<c:forEach begin="0" end="23" varStatus="status">
					<tr>
						<th>${usePowerTableMap.columnNames[status.index]}</th>
						<td>${usePowerTableMap.usePowers[status.index]}</td>
						<td>${usePowerTableMap.useCompPowers[status.index]}</td>
						<td>${usePowerTableMap.useDifPowers[status.index]}</td>
						<th>${usePowerTableMap.columnNames[status.index+24]}</th>
						<td>${usePowerTableMap.usePowers[status.index+24]}</td>
						<td>${usePowerTableMap.useCompPowers[status.index+24]}</td>
						<td>${usePowerTableMap.useDifPowers[status.index+24]}</td>
					</tr>
				</c:forEach>				
			</table>
		</div>
		<!-- 12 ダウンロード	-->
		<div class="UIdesign1" id="download">
			<input class="UIdesign1" id="downloadBtn" type="button" value="ダウンロード" onClick="csvDownload();"/>
		</div>
	</div>
</form>
<script>
	window.onload = function() {
		//set checkbox on or off using the comparing flag 
		var compChkFlag = "${compChk }";
		if(compChkFlag.length != 0){
			document.getElementsByName("compChk")[0].checked = true;
		}
		var errorCode = "${parameters.errorCode[0]}";
		if(errorCode == "E0004"){
			alert("初期処理中にエラーが発生しました。。");
		}else if(errorCode == "E0032"){
			alert("ダウンロードファイルの作成に失敗しました。");
		}
		
		var customerContractNumber ='${customerContractNumber}';
		if(customerContractNumber == ''){
			//E0005
			alert("対象データが存在しません。");
		}
		
		//draw a graph (the name of canvas is setted of "canvas")
		GetGraphUsedPower(${graphCreateInfo}, ${usePowerGraphMappingModel});
	};
</script>