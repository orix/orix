package jp.co.orix.model;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.logging.SimpleFormatter;

public class BillMonth {
	private String month;
	private String kw;
	public String getMonth() {
		return month;
	}
	public void setMonth(String month) throws ParseException {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy年M月");
		
		this.month = format2.format(format.parse(month));
	}
	public String getKw() {
		return kw;
	}
	public void setKw(String kw) {
		this.kw =  kw;
	}
	
	@Override
	public String toString() {
		return "BillMonth [month=" + month + ", kw=" + kw + "]";
	}
	
	
	
}
