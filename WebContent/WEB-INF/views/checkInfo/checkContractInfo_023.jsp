<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	function goSearch() {
		var groupCombobox = document.getElementsByName("groupCombobox")[0].value;
		var usePlaceCombobox = document.getElementsByName("usePlaceCombobox")[0].value;

		if (groupCombobox == '') {
			alert('グループを選択して下さい。');
			return false;
		}

		if (usePlaceCombobox == '') {
			alert('ご使用場所を選択して下さい。');
			return false;
		}

		window.location = "checkContractInfo?groupCombobox=" + groupCombobox
				+ "&usePlaceCombobox=" + usePlaceCombobox
	}

	function loadInformationOnfailure() {
		alert('対象データが存在しません。');
	}

	function goRenewal() {
		
		alert('コさんのページに');
		
		var groupCombobox = document.getElementsByName("groupCombobox")[0].value;
		var usePlaceCombobox = document.getElementsByName("usePlaceCombobox")[0].value;

		window.location = "//?groupCombobox=" + groupCombobox + "&useAddress="
				+ usePlaceCombobox
	}
</script>
<link type="text/css" rel="stylesheet"
	href="/resource/css/checkInformationOfContract_023_024.css">
<script type="text/javascript"
	src="/resource/js/selectBox/getGroupAndUsePlaceComboboxList1.js?ver=1.1"></script>

<form>
	<input type="hidden" name="parameter" id="parameter"
		value="${parameter }">

	<!-- 高圧契約情報が取得できなかった場合、『共通メッセージ（E0005）』を出力する -->
	<c:if test="${checkContractInfoHV002 == null }">
		<script>
			loadInformationOnfailure()
		</script>
	</c:if>

	<p class="pTag1">
		<!-- 그룹 -->
		グループ&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <select
			id="groupCombobox" name="groupCombobox" class="select1">
			<option value=""></option>
		</select>
		<!-- 사용장소 -->
		&nbsp;&nbsp;&nbsp;&nbsp;ご使用場所&nbsp;&nbsp;&nbsp;&nbsp;<select
			id="usePlaceCombobox" name="usePlaceCombobox" class="select2"><option
				value=""></option></select>&nbsp;&nbsp;&nbsp;&nbsp; <input type="button"
			value="検索" onclick="return goSearch();" class="submit">
	</p>
	<hr class="hr_top">
	<br>
	<!--  -->
	<div class="table">
		<table>
			<tr>
				<td>住所</td>
				<td><input type="text" name="checkContractInfoHV002.address"
					value="${checkContractInfoHV002.address }" readonly="readonly"></td>
			</tr>
			<tr>
				<td>ご使用場所</td>
				<td><input type="text" name="" value="" readonly="readonly"></td>
			</tr>
			<tr>
				<td>契約番号</td>
				<td><input type="text"
					name="checkContractInfoHV002.contractNumber"
					value="${checkContractInfoHV002.contractNumber }"
					readonly="readonly"></td>
			</tr>
			<tr>
				<td>契約名議員</td>
				<td><input type="text"
					name="checkContractInfoHV002.contractName"
					value="${checkContractInfoHV002.contractName}" readonly="readonly"></td>
			</tr>
		</table>
		<br>
	</div>
	<!--  -->
	<div class="table1">
		<table>
			<tr>
				<th>&nbsp;お支払情報</th>
				<!-- 下記の「更新」ボタンはユーザーIDが「04需要家別IDと読み取り専用なら非表示」 -->
				<td><input type="button" value="更新" onclick="goRenewal()"
					class="button"></td>
		</table>
	</div>
	<!--  -->
	<div class="table">
		<table>
			<c:choose>
				<c:when test="${checkContractInfoHV002.payment == '04' }">
					<tr>
						<td>お支払方法</td>
						<td><input type="text"
							name="checkContractInfoHV002.universalInfo.t0401"
							value="${checkContractInfoHV002.universalInfo.t0401 }"
							readonly="readonly"></td>
					</tr>
				</c:when>
				<c:otherwise>
					<tr>
						<td>お支払方法</td>
						<td><input type="text"
							name="checkContractInfoHV002.universalInfo.t0401"
							value="${checkContractInfoHV002.universalInfo.t0401 }"
							readonly="readonly"></td>
					</tr>
					<tr>
						<td>金融機関</td>
						<td><input type="text"
							name="checkContractInfoHV002.financialInsitutionName"
							value="${checkContractInfoHV002.financialInsitutionName}"
							readonly="readonly"></td>
					</tr>
					<tr>
						<td>支店</td>
						<td><input type="text" name="checkContractInfoHV002.branch"
							value="${checkContractInfoHV002.branch }" readonly="readonly"></td>
					</tr>
					<tr>
						<td>口座科目</td>
						<td><input type="text"
							name="checkContractInfoHV002.accountName"
							value="${checkContractInfoHV002.accountName }"
							readonly="readonly"></td>
					</tr>
					<tr>
						<td>口座番号</td>
						<td><input type="text"
							name="checkContractInfoHV002.accountNumber"
							value="${checkContractInfoHV002.accountNumber }"
							readonly="readonly"></td>
					</tr>
					<tr>
						<td>口座名義人</td>
						<td><input type="text"
							name="checkContractInfoHV002.accountHolder"
							value="${checkContractInfoHV002.accountHolder }"
							readonly="readonly"></td>
					</tr>
				</c:otherwise>
			</c:choose>
		</table>
	</div>
	<br>
	<!--  -->
	<div class="table">
		<table>
			<tr>
				<th colspan="2">&nbsp;契約種別</th>
			</tr>
			<tr>
				<td>主契約</td>
				<td><input type="text"
					name="checkContractInfoHV002.mainContract"
					value="${checkContractInfoHV002.mainContract }"></td>
			</tr>
			<tr>
				<td>予備電源契約</td>
				<td><input type="text"
					name="checkContractInfoHV002.standbyPowerContact"
					value="${checkContractInfoHV002.standbyPowerContact }"></td>
			</tr>
			<tr>
				<td>予備選契約</td>
				<td><input type="text"
					name="checkContractInfoHV002.standbyLineContact"
					value="${checkContractInfoHV002.standbyLineContact }"></td>
			</tr>
		</table>
		<br>
		<p class="bottom">
			高圧は受け処理当月の月末に反映のため、現在のご契約内容を表示しております。<br>
			更新のご依頼をいただいてから、Web表示反映にお時間をいただくことがあります<br>ので、あらかじめご了承ください。
		</p>
	</div>
</form>