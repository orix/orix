package jp.co.orix.dao.imp;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.ExcelDao;
import jp.co.orix.model.Excel;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ExcelDaoImp implements ExcelDao {
	private Excel excele;
//	private List<Excel> excelDown;
	@Autowired
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	@Override
//	public List<Excel> excelDown(String excel) {
	public Excel excelDown(String excel) {
		SqlSession session = sqlSession.openSession();
		ExcelDao excelDao = session.getMapper(ExcelDao.class);
		try {
//			excelDown = excelDao.excelDown(excel);
			excele = excelDao.excelDown(excel);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
//		return excelDown;
		return excele;
	}
	

}
