package jp.co.orix.action.bill;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.orix.dao.imp.BillDaoImp;
import jp.co.orix.model.Bill;
import jp.co.orix.model.BillMonth;
import jp.co.orix.util.CommApi_32;
import jp.co.orix.util.Util;
import oracle.net.aso.b;
import oracle.net.aso.l;

import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class BliiAction_029  extends  ActionSupport  implements ServletRequestAware{
	
	private InputStream fileInputStream;
	private BillDaoImp billDaoImp = new BillDaoImp();
	public HttpServletRequest request;
	private String contentDisposition;
	
	private ArrayList<BillMonth> billMonths;
	private ArrayList<String> twoYear;
	
	private String useDate;	
	private Boolean isSummer = false;
	private String japanYear;
	private String japanYMD;
	private String fileName;
	private String parameter;
	private Bill bill;
	private Integer type;
	
	public String form() throws ParseException {
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyy年M月");
		SimpleDateFormat format3 = new SimpleDateFormat("yyyyMM");
		SimpleDateFormat format4 = new SimpleDateFormat("MM");
		
		//참고
		CommApi_32 api = new CommApi_32();
		api.setFormParamator(request, "bill");
		HashMap<String, Object> param = api.getFormParamator("bill");
		
//		parameter = pa
//		String str = "ｺ";
//		if (Util.isValidateAlphanumericJisLevel2(str)) 
//			System.out.println("반각문자 O");
//		else
//			System.out.println("반각문자 X");
		
		Date date  = new Date();
		Calendar now = Calendar.getInstance();
		now.setTime(date);
		if (useDate == null){
			now.add(Calendar.MONTH, -1);
			date = now.getTime();
		}
		else date = format.parse(useDate);
		
		//2년 ArrayList
		twoYear = Util.yearList(now, 23);
		
		String group = "";
		String place = "";
		String useDate = "";
		
		if (param != null) {
			parameter = (String)param.get("param");
			group  =  (String)param.get("group");
			place  =  (String)param.get("place");
			useDate  =  (String)param.get("useDate");
			if (useDate == null) 
				useDate = twoYear.get(0);
		}
		
		Integer setMonth = Integer.parseInt(format4.format(date));
		
		if( setMonth >= 5 && setMonth <= 8) isSummer = true;
		
		if (type == 1) {
			bill =billDaoImp.findByBillAll(format.parse(useDate));
		}else{
			bill = billDaoImp.findByBillInfo(group , place, date, null);
		}
		
		japanYear = Util.japanYearMonthCircle(date);
		japanYMD = Util.japanYearMonthDayCircle(date);
		
		
		
		//最大需要電力の履歴 기간 구하기
		Calendar end = Calendar.getInstance();
		end.setTime(date);
		end.add(Calendar.MONTH, +1);
		String endDate = end.get(Calendar.YEAR)+ "/"+end.get(Calendar.MONTH);
		endDate = format.format(format.parse(endDate));
		
		ArrayList<BillMonth> list = billDaoImp.findByBillYearList(endDate);
		for (int i = 0; i < list.size(); i++) {
			BillMonth billMonth1 = list.get(i);
			BillMonth billMonth2 = list.get(i+1);
			if (i == list.size() - 2) {
				break;
			}else{
				if (billMonth1.getMonth().equals(billMonth2.getMonth()) && !billMonth1.getKw().equals(billMonth2.getKw())) {
					list.remove(i);
					--i;
				}
			}
		}
		billMonths = list;
		
		return SUCCESS;
	}

	
	
	
	public String pdfDownload() throws FileNotFoundException, UnsupportedEncodingException {
		System.out.println("test pdfDownload");
		String savePath  = request.getSession().getServletContext().getRealPath("/").concat("resource/pdf/");
		setContentDisposition("attachment;filename="+URLEncoder.encode(fileName, "UTF-8"));
		setFileInputStream(new FileInputStream(new File(savePath+fileName)));
		return SUCCESS;
	}
	
	public String csvDownload() throws FileNotFoundException, UnsupportedEncodingException {
		String savePath  = request.getSession().getServletContext().getRealPath("/").concat("resource/csv/");
		setContentDisposition("attachment;filename="+URLEncoder.encode(fileName, "UTF-8"));
		setFileInputStream(new FileInputStream(new File(savePath+fileName)));
		return SUCCESS;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}


	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getJapanYear() {
		return japanYear;
	}


	public void setJapanYear(String japanYear) {
		this.japanYear = japanYear;
	}

	public Bill getBill() {
		return bill;
	}

	public void setBill(Bill bill) {
		this.bill = bill;
	}

	public String getUseDate() {
		return useDate;
	}

	public void setUseDate(String useDate) {
		this.useDate = useDate;
	}

	public ArrayList<BillMonth> getBillMonths() {
		return billMonths;
	}

	public void setBillMonths(ArrayList<BillMonth> billMonths) {
		this.billMonths = billMonths;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentDisposition() {
		return contentDisposition;
	}

	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}

	public Boolean getIsSummer() {
		return isSummer;
	}

	public void setIsSummer(Boolean isSummer) {
		this.isSummer = isSummer;
	}
	
	public ArrayList<String> getTwoYear() {
		return twoYear;
	}

	public void setTwoYear(ArrayList<String> twoYear) {
		this.twoYear = twoYear;
	}

	public String getJapanYMD() {
		return japanYMD;
	}

	public void setJapanYMD(String japanYMD) {
		this.japanYMD = japanYMD;
	}

	public String getParameter() {
		return parameter;
	}
	
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	
	public Integer getType() {
		return type;
	}
	
	public void setType(Integer type) {
		this.type = type;
	}
}
