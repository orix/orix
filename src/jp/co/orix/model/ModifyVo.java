package jp.co.orix.model;

public class ModifyVo {
	
	private int KeiyakuNo;
	private String Keiyakumeibinin;
	private String ShiyoJusho;
		
	private String Equipment1;
	private String Equipment2;
	private String EquipmentKubun1;
	private String EquipmentKubun2;
	private String ChangeKubun;
	private String Supplement;
	private String WorkCompanyName;
	private String WorkTantoName;
	private String WorkTel1;
	private String WorkTel2;
	private String WorkTel3;
	
	private String MainEngineerCompanyName;
	private String MainEngineerName;
	private String MainEngineerTel1;
	private String MainEngineerTel2;
	private String MainEngineerTel3;
	
	private String YubinBango;
	private String Jusho;
	private String HojinName;
	private String BushoName;
	private String TantoName;
	private String Tel1;
	private String Tel2;
	private String Tel3;
	
	private String MailAddress;
	private String Memo;
	
	public int getKeiyakuNo() {
		return KeiyakuNo;
	}
	public void setKeiyakuNo(int keiyakuNo) {
		KeiyakuNo = keiyakuNo;
	}
	public String getKeiyakumeibinin() {
		return Keiyakumeibinin;
	}
	public void setKeiyakumeibinin(String keiyakumeibinin) {
		Keiyakumeibinin = keiyakumeibinin;
	}
	public String getShiyoJusho() {
		return ShiyoJusho;
	}
	public void setShiyoJusho(String shiyoJusho) {
		ShiyoJusho = shiyoJusho;
	}
	public String getEquipment1() {
		return Equipment1;
	}
	public void setEquipment1(String equipment1) {
		Equipment1 = equipment1;
	}
	public String getEquipment2() {
		return Equipment2;
	}
	public void setEquipment2(String equipment2) {
		Equipment2 = equipment2;
	}
	public String getEquipmentKubun1() {
		return EquipmentKubun1;
	}
	public void setEquipmentKubun1(String equipmentKubun1) {
		EquipmentKubun1 = equipmentKubun1;
	}
	public String getEquipmentKubun2() {
		return EquipmentKubun2;
	}
	public void setEquipmentKubun2(String equipmentKubun2) {
		EquipmentKubun2 = equipmentKubun2;
	}
	public String getChangeKubun() {
		return ChangeKubun;
	}
	public void setChangeKubun(String changeKubun) {
		ChangeKubun = changeKubun;
	}
	public String getSupplement() {
		return Supplement;
	}
	public void setSupplement(String supplement) {
		Supplement = supplement;
	}
	public String getWorkCompanyName() {
		return WorkCompanyName;
	}
	public void setWorkCompanyName(String workCompanyName) {
		WorkCompanyName = workCompanyName;
	}
	public String getWorkTantoName() {
		return WorkTantoName;
	}
	public void setWorkTantoName(String workTantoName) {
		WorkTantoName = workTantoName;
	}
	public String getWorkTel1() {
		return WorkTel1;
	}
	public void setWorkTel1(String workTel1) {
		WorkTel1 = workTel1;
	}
	public String getWorkTel2() {
		return WorkTel2;
	}
	public void setWorkTel2(String workTel2) {
		WorkTel2 = workTel2;
	}
	public String getWorkTel3() {
		return WorkTel3;
	}
	public void setWorkTel3(String workTel3) {
		WorkTel3 = workTel3;
	}
	public String getMainEngineerCompanyName() {
		return MainEngineerCompanyName;
	}
	public void setMainEngineerCompanyName(String mainEngineerCompanyName) {
		MainEngineerCompanyName = mainEngineerCompanyName;
	}
	public String getMainEngineerName() {
		return MainEngineerName;
	}
	public void setMainEngineerName(String mainEngineerName) {
		MainEngineerName = mainEngineerName;
	}
	public String getMainEngineerTel1() {
		return MainEngineerTel1;
	}
	public void setMainEngineerTel1(String mainEngineerTel1) {
		MainEngineerTel1 = mainEngineerTel1;
	}
	public String getMainEngineerTel2() {
		return MainEngineerTel2;
	}
	public void setMainEngineerTel2(String mainEngineerTel2) {
		MainEngineerTel2 = mainEngineerTel2;
	}
	public String getMainEngineerTel3() {
		return MainEngineerTel3;
	}
	public void setMainEngineerTel3(String mainEngineerTel3) {
		MainEngineerTel3 = mainEngineerTel3;
	}
	public String getYubinBango() {
		return YubinBango;
	}
	public void setYubinBango(String yubinBango) {
		YubinBango = yubinBango;
	}
	public String getJusho() {
		return Jusho;
	}
	public void setJusho(String jusho) {
		Jusho = jusho;
	}
	public String getHojinName() {
		return HojinName;
	}
	public void setHojinName(String hojinName) {
		HojinName = hojinName;
	}
	public String getBushoName() {
		return BushoName;
	}
	public void setBushoName(String bushoName) {
		BushoName = bushoName;
	}
	public String getTantoName() {
		return TantoName;
	}
	public void setTantoName(String tantoName) {
		TantoName = tantoName;
	}
	public String getTel1() {
		return Tel1;
	}
	public void setTel1(String tel1) {
		Tel1 = tel1;
	}
	public String getTel2() {
		return Tel2;
	}
	public void setTel2(String tel2) {
		Tel2 = tel2;
	}
	public String getTel3() {
		return Tel3;
	}
	public void setTel3(String tel3) {
		Tel3 = tel3;
	}
	public String getMailAddress() {
		return MailAddress;
	}
	public void setMailAddress(String mailAddress) {
		MailAddress = mailAddress;
	}
	public String getMemo() {
		return Memo;
	}
	public void setMemo(String memo) {
		Memo = memo;
	}
	@Override
	public String toString() {
		return "ModifyVo [KeiyakuNo=" + KeiyakuNo + ", Keiyakumeibinin="
				+ Keiyakumeibinin + ", ShiyoJusho=" + ShiyoJusho
				+ ", Equipment1=" + Equipment1 + ", Equipment2=" + Equipment2
				+ ", EquipmentKubun1=" + EquipmentKubun1 + ", EquipmentKubun2="
				+ EquipmentKubun2 + ", ChangeKubun=" + ChangeKubun
				+ ", Supplement=" + Supplement + ", WorkCompanyName="
				+ WorkCompanyName + ", WorkTantoName=" + WorkTantoName
				+ ", WorkTel1=" + WorkTel1 + ", WorkTel2=" + WorkTel2
				+ ", WorkTel3=" + WorkTel3 + ", MainEngineerCompanyName="
				+ MainEngineerCompanyName + ", MainEngineerName="
				+ MainEngineerName + ", MainEngineerTel1=" + MainEngineerTel1
				+ ", MainEngineerTel2=" + MainEngineerTel2
				+ ", MainEngineerTel3=" + MainEngineerTel3 + ", YubinBango="
				+ YubinBango + ", Jusho=" + Jusho + ", HojinName=" + HojinName
				+ ", BushoName=" + BushoName + ", TantoName=" + TantoName
				+ ", Tel1=" + Tel1 + ", Tel2=" + Tel2 + ", Tel3=" + Tel3
				+ ", MailAddress=" + MailAddress + ", Memo=" + Memo + "]";
	}
	public ModifyVo() {
		// TODO Auto-generated constructor stub
	}
	public ModifyVo(int keiyakuNo, String keiyakumeibinin, String shiyoJusho,
			String equipment1, String equipment2, String equipmentKubun1,
			String equipmentKubun2, String changeKubun, String supplement,
			String workCompanyName, String workTantoName, String workTel1,
			String workTel2, String workTel3, String mainEngineerCompanyName,
			String mainEngineerName, String mainEngineerTel1,
			String mainEngineerTel2, String mainEngineerTel3,
			String yubinBango, String jusho, String hojinName,
			String bushoName, String tantoName, String tel1, String tel2,
			String tel3, String mailAddress, String memo) {
		KeiyakuNo = keiyakuNo;
		Keiyakumeibinin = keiyakumeibinin;
		ShiyoJusho = shiyoJusho;
		Equipment1 = equipment1;
		Equipment2 = equipment2;
		EquipmentKubun1 = equipmentKubun1;
		EquipmentKubun2 = equipmentKubun2;
		ChangeKubun = changeKubun;
		Supplement = supplement;
		WorkCompanyName = workCompanyName;
		WorkTantoName = workTantoName;
		WorkTel1 = workTel1;
		WorkTel2 = workTel2;
		WorkTel3 = workTel3;
		MainEngineerCompanyName = mainEngineerCompanyName;
		MainEngineerName = mainEngineerName;
		MainEngineerTel1 = mainEngineerTel1;
		MainEngineerTel2 = mainEngineerTel2;
		MainEngineerTel3 = mainEngineerTel3;
		YubinBango = yubinBango;
		Jusho = jusho;
		HojinName = hojinName;
		BushoName = bushoName;
		TantoName = tantoName;
		Tel1 = tel1;
		Tel2 = tel2;
		Tel3 = tel3;
		MailAddress = mailAddress;
		Memo = memo;
	}
	
	
	
}
