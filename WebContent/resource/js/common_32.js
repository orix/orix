/**
 * 
 * 
 */

function GetGraphUsedPower(graphCreateInfo, usePowerGraphMappingModel) {
	var ctx = document.getElementById("canvas").getContext("2d");
	ctx.textAlign = 'center';
	var datasetIndex = 0;
	
	var chartData = {
			labels : graphCreateInfo.labels
		,
			datasets : graphCreateInfo.datasets
		};
	
	//dataset
	if( usePowerGraphMappingModel.useCompPowers != null && usePowerGraphMappingModel.useCompPowers.size != 0){
		chartData.datasets[datasetIndex].data = usePowerGraphMappingModel.useCompPowers;
			datasetIndex++;
	}
	
	if( usePowerGraphMappingModel.useDayTimePowers != null && usePowerGraphMappingModel.useDayTimePowers.size != 0){
		chartData.datasets[datasetIndex].data = usePowerGraphMappingModel.useDayTimePowers;
		datasetIndex++;
	}
	if( usePowerGraphMappingModel.useNightTimePowers != null && usePowerGraphMappingModel.useNightTimePowers.size != 0){
		chartData.datasets[datasetIndex].data = usePowerGraphMappingModel.useNightTimePowers;
		datasetIndex++;
	}
		//dataset processing for other time unit.	
	
	window.myMixedChart = new Chart(ctx, {
		type : 'bar',
		data : chartData,
		options : {
			responsive : true,
			bezierCurve : false,
			tooltips : {
				mode : 'index',
				intersect : true
			},
			legend : {
				display : true
			},
			scales : {
				xAxes: [{
		              stacked: true,
		              scaleLabel : {
		            	  display : true,
		            	  labelString : graphCreateInfo.xScaleLabel
		              }
		            }],
				yAxes : [ {
					stacked: true,
					scaleLabel : {
						display : true,
						labelString : graphCreateInfo.yScaleLabel
					}
				} ]
			}
		}
	});
}

function IsValidateStringValue(str){
		for (var i = 0; i < str.length; i++) {
			 var c = str.charCodeAt(i);
			 if (!((c >= 0x0 && c < 0x81) || (c == 0xf8f0) || 
					 (c >= 0xff61 && c < 0xffa0) || 
					 (c >= 0xf8f1 && c < 0xf8f4))){
				 
				 return false;
			 }
		}
		return true;
}

function IsDate(str){
	var d = new Date(str);
	return !isNaN(d.valueOf());
}

function IsValidCombobox(comboBox){
	if(comboBox.options[comboBox.selectedIndex].value=="00000"){
		return false;
	}
	return true;
}