<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">

	function goBack() {

		var HV011_paymentMethodID = document.getElementById("userPaymentInfo.HV011_paymentMethodID").value;
		var HV011_location = document.getElementById("userPaymentInfo.HV011_location").value;

		if (HV011_paymentMethodID == 1 || HV011_paymentMethodID == 2 || HV011_paymentMethodID == 3) {
			window.location = "accountInfoChange.action?userPaymentInfo.HV011_location=" + HV011_location;
		} else {
			window.location = "changePaymentMethod.action";
		}
	}
</script>

<form action="toHvChangePaymentConfirm" method="post">

<br>
		<s:set name="changeScope" value="changeScope"/>
		<div align="center">
		
		<s:if test="%{#changeScope.equalsIgnoreCase('0')}">
			<input type="radio" name="changeScope" value="0" checked="checked">該当ご使用場所のみ反映する 
			<input type="radio" name="changeScope" value="1">全ご使用場所に反映する 
		</s:if>
		
		<s:else>
		<input type="radio" name="changeScope" value="0">該当ご使用場所のみ反映する 
			<input type="radio" name="changeScope" value="1" checked="checked">全ご使用場所に反映する 
		</s:else>
		
		</div>

		<br>
		
		<hr class="hr_top">
	
		<br>
	
		<div class="toChangePaymentMethodTable">
			<table>
				<tr>
				<th colspan="2">口座情報</th>
				</tr>
				<tr>
					<td id="title">金融機関</td>
					<td><input type="text" value="${userPaymentInfo.CM004_financialInstitutionName}" readonly="readonly" name="userPaymentInfo.CM004_financialInstitutionName" class="outputTextField2"></td>
				</tr>
				<tr>
					<td id="title">口座名義人</td>
					<td><input type="text" value="${userPaymentInfo.CM004_accountNameAfter}" readonly="readonly" name="userPaymentInfo.CM004_accountNameAfter" class="outputTextField2"></td>
				</tr>
			</table>
		</div>

		<br>

		<div class="toChangePaymentMethodTable">
			<table><tr>
				<th colspan="2">申請担当者情報</th>
				</tr><tr>
					<td id="title">郵便番号</td>
					<td><input type="text" value="${userPaymentInfo.HV011_zipCode}" readonly="readonly" name="userPaymentInfo.HV011_zipCode" class="outputTextField2"></td>
				</tr>
				<tr>
					<td id="title">住所</td>
					<td><input type="text" value="${userPaymentInfo.HV011_address}" readonly="readonly" name="userPaymentInfo.HV011_address" class="outputTextField2"></td>
				</tr>
				<tr>
					<td id="title">会社名</td>
					<td><input type="text" value="${userPaymentInfo.HV011_companyName}" readonly="readonly" name="userPaymentInfo.HV011_companyName" class="outputTextField2"></td>
				</tr>
				<tr>
					<td id="title">部署名</td>
					<td><input type="text" value="${userPaymentInfo.HV011_departmentName}" readonly="readonly" name="userPaymentInfo.HV011_departmentName" class="outputTextField2"></td>
				</tr>
				<tr>
					<td id="title">ご担当者</td>
					<td><input type="text" value="${userPaymentInfo.HV011_personInChargeName}" readonly="readonly" name="userPaymentInfo.HV011_personInChargeName" class="outputTextField2"></td>
				</tr>
				<tr>
					<td id="title">電話番号</td>
					<td><input type="text" value="${userPaymentInfo.HV011_areaCode}-${userPaymentInfo.HV011_downtownCode}-${userPaymentInfo.HV011_memberCode}" readonly="readonly" class="outputTextField2"></td>
				</tr>
				<tr>
					<td id="title">メールアドレス</td>
					<td><input type="text" value="${userPaymentInfo.IDUMS000_mailAddress}" readonly="readonly" name="userPaymentInfo.IDUMS000_mailAddress" class="outputTextField2"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td id="title">自由記入欄</td>
					<td><textarea rows="3" cols="80" readonly="readonly" name="userPaymentInfo.HV011_entry" class="outputTextarea">${userPaymentInfo.HV011_entry}</textarea></td>
				</tr>
			</table>

			<input type="hidden" value="${inputUserPaymentInfo.HV011_paymentMethodID}" name="inputUserPaymentInfo.HV011_paymentMethodID" id="userPaymentInfo.HV011_paymentMethodID">
			<input type="hidden" value="${inputUserPaymentInfo.HV011_location}" name="inputUserPaymentInfo.HV011_location" id="userPaymentInfo.HV011_location">
			<input type="hidden" value="${userPaymentInfo.HV011_areaCode}" name="userPaymentInfo.HV011_areaCode">
			<input type="hidden" value="${userPaymentInfo.HV011_downtownCode}" name="userPaymentInfo.HV011_downtownCode"> 
		</div>
		<br><br>
		<div align="center">	
			<input type="button" value="戻る" onclick="goBack()" class="button">&nbsp;
			<input type="submit" value="更新" class="button">
		</div>
		
	<br>
	<br>

	<div align="center">
		上記内容に間違えがない場合は、「更新」ボタンを押してください。 <br> 内容を変更する場合は、「戻す」ボタンを押してください。
	</div>
</form>
