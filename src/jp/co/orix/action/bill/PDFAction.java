package jp.co.orix.action.bill;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.StringReader;
import java.nio.charset.Charset;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import jp.co.orix.dao.imp.BillDaoImp;
import jp.co.orix.model.Bill;
import jp.co.orix.model.BillMonth;
import jp.co.orix.util.CommApi_32;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.util.ServletContextAware;

import com.itextpdf.text.Document;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorker;
import com.itextpdf.tool.xml.XMLWorkerFontProvider;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.itextpdf.tool.xml.css.CssFile;
import com.itextpdf.tool.xml.css.StyleAttrCSSResolver;
import com.itextpdf.tool.xml.html.CssAppliers;
import com.itextpdf.tool.xml.html.CssAppliersImpl;
import com.itextpdf.tool.xml.html.Tags;
import com.itextpdf.tool.xml.parser.XMLParser;
import com.itextpdf.tool.xml.pipeline.css.CSSResolver;
import com.itextpdf.tool.xml.pipeline.css.CssResolverPipeline;
import com.itextpdf.tool.xml.pipeline.end.PdfWriterPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipeline;
import com.itextpdf.tool.xml.pipeline.html.HtmlPipelineContext;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PDFAction extends ActionSupport  implements ServletContextAware{
	
	private HashMap<String, Object> data = new HashMap<String, Object>();
	
	private String billNumber;
	private String useDate;
	private String date;
	private String invoiceNumber;
	private BillDaoImp daoImp = new BillDaoImp();
	
//	public HttpServletRequest request;
//	public HttpServletResponse response;
	private ServletContext context;

	public String pdfDownload() throws ParseException {

//		System.out.println("pdfDownload");
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy/MM");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyyMM");
		HttpServletResponse response = ServletActionContext.getResponse();
		HttpServletRequest request = ServletActionContext.getRequest();
		
		CommApi_32 api = new CommApi_32();
		api.setFormParamator(request, "bill");
		HashMap<String, Object> param = api.getFormParamator("bill");
		
		System.out.println(param);
				
//		Integer billNumber = Integer.parseInt((String)hashmap.get("billNumber"));
		String group= (String)param.get("group");
		String place =  (String)param.get("place");
		Integer type = Integer.parseInt((String)param.get("type"));
		Integer num = 0;
		System.out.println("11 : " + param.get("num"));
		if (param.get("num") != null) {
			num = Integer.parseInt((String)param.get("num"));
		}
		
		System.out.println("22 : " + num);
		 
		date = (String)param.get("useDate");
		
		//파일 확장자 정보  : 계약번호_yyyyMM_정구서정보.pdf
		
		//css, font setting
		String scss = context.getRealPath("/resource/css/pdfForm.css");
		String font = context.getRealPath("/resource/font/bokutachi.otf");
		String savePath  = context.getRealPath("/").concat("resource/pdf/");
		
		
		File dir = new File(context.getRealPath("/").concat("resource/pdf"));
		if (!dir.exists()) 
			dir.mkdir();
		
		Bill bill = null;
		
		
		ArrayList<BillMonth> list = daoImp.findByBillYearList(date);
		for (int i = 0; i < list.size(); i++) {
			BillMonth billMonth1 = list.get(i);
			BillMonth billMonth2 = list.get(i+1);
			if (i == list.size() - 2) {
				break;
			}else{
				if (billMonth1.getMonth().equals(billMonth2.getMonth()) && !billMonth1.getKw().equals(billMonth2.getKw())) {
					list.remove(i);
					--i;
				}
			}
		}		
		
		if (type == 1) {
			bill = daoImp.findByBillAll(format1.parse(date));
		}else{
			bill = daoImp.findByBillInfo(group, place, format1.parse(date), num);
		}
		
		if (bill != null) {
			String fileName = bill.getH0601() + "_" +format2.format(format1.parse(date))+"_" +  "請求書情報.pdf";
			File file = new File(fileName);
			
	
			try {
				//용지사이즈
				Document document = new Document(PageSize.A4, 15, 15, 30, 15); // 용지 및 여백 설정(L, R, T, B)
				
			     
				// PdfWriter 생성
				//PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream("d:/test.pdf")); // 바로 다운로드.
				PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(savePath+file));
				writer.setInitialLeading(12.5f);
				 
				// 파일 다운로드 설정
				response.setContentType("application/pdf");
				response.setHeader("Content-Transper-Encoding", "binary");
				response.setHeader("Content-Disposition", "inline; filename=" + fileName + ".pdf");
				 
				// Document 오픈
				document.open();
				XMLWorkerHelper helper = XMLWorkerHelper.getInstance();
				     
				// CSS
				CSSResolver cssResolver = new StyleAttrCSSResolver();
				CssFile cssFile = helper.getCSS(new FileInputStream(scss));
				cssResolver.addCss(cssFile);
				     
				// HTML, 폰트 설정
				XMLWorkerFontProvider fontProvider = new XMLWorkerFontProvider(XMLWorkerFontProvider.DONTLOOKFORFONTS);
				fontProvider.register(font,"bokutachi"); // MalgunGothic은 alias,
				CssAppliers cssAppliers = new CssAppliersImpl(fontProvider);
				 
				HtmlPipelineContext htmlContext = new HtmlPipelineContext(cssAppliers);
				htmlContext.setTagFactory(Tags.getHtmlTagProcessorFactory());
				 
				// Pipelines
				PdfWriterPipeline pdf = new PdfWriterPipeline(document, writer);
				HtmlPipeline html = new HtmlPipeline(htmlContext, pdf);
				CssResolverPipeline css = new CssResolverPipeline(cssResolver, html);
				 
				XMLWorker worker = new XMLWorker(css, true);
				XMLParser xmlParser = new XMLParser(worker, Charset.forName("UTF-8"));
				 
				// 폰트 설정에서 별칭으로 줬던 "MalgunGothic"을 html 안에 폰트로 지정한다.
				
				String htmlBody = BillHtmlForm.htmlForm(bill, type, date, list);
				
				String htmlStr  = "<html><head></head><body style='font-family: bokutachi'><div style=\"width: 100%; height: 100%\">";
				htmlStr += htmlBody;
				htmlStr+="</div></body></html>";
				 
				StringReader strReader = new StringReader(htmlStr);
				xmlParser.parse(strReader);
				 
				document.close();
				writer.close();
				
				data.put("fileName", fileName);
				data.put("success", true);
			} catch (Exception e) {
				// TODO: handle exception
				System.err.println("pdf Download Error\n" + e.getMessage());
				data.put("success", false);
			}
		}else{
			data.put("success", false);
			data.put("message", "");
		}
		
		return SUCCESS;
	}

	public HashMap<String, Object> getData() {
		return data;
	}


	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}
	
	public String getBillNumber() {
		return billNumber;
	}

	public void setBillNumber(String billNumber) {
		this.billNumber = billNumber;
	}


	public String getUseDate() {
		return useDate;
	}


	public void setUseDate(String useDate) {
		this.useDate = useDate;
	}


	public String getInvoiceNumber() {
		return invoiceNumber;
	}


	public void setInvoiceNumber(String invoiceNumber) {
		this.invoiceNumber = invoiceNumber;
	}


	@Override
	public void setServletContext(ServletContext context) {
		// TODO Auto-generated method stub
		this.context = context;
		
	}
}
