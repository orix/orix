<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<title>登録情報変更</title>

<style>
table {
	border-collapse: collapse;
	align: center;
	width: 40%;
}

th {
	background-color: #4F81BD;
	padding: 10px;
	color: white;
	border-radius: 8px;
	border-color: #000000;
	border: 2px solid white;
	font-size: 20px;
}

td {
	background-color: #FFFFFF;
	color: black;
	border: 2px white;
	border-radius: 5px;
	font-size: 20px;
}

input[type=text] {
	width: 100%;
	padding: 10px;
	margin: 0 0;
	box-sizing: border-box;
	border: 2px solid #1E4F8B;
	border-radius: 8px;
	font-size: 20px;
}

input[type=button] {
   background-color: #4F81BD;
    box-shadow: 0 4px 16px 0 rgba(0,0,0,0.2), 0 4px 10px 0 rgba(0,0,0,0.19);
    border: none;
    color: white;
    padding: 12px 60px;
    text-decoration: none;
    margin: 4px 8px;
    cursor: pointer;
    text-align: center;
    border-radius: 8px;
    font-size: 20px;
    display: inline-block;
    
}

.div{
	align:center;
	margin: 4px 1050px;
}

</style>

<script>
function goPrev() {
	if(${sessionScope.M0201.equals("test")}) {
		window.location = "User";		
	 } else if (${sessionScope.M0201.equals("admin")}) {
		 window.location = "admin";
	 }
	
}
</script>
</head>
<body>
	<div align="center" style="color: #245A82">
		<h2>変更申込が完了しました。</h2>
		<br/>
		<h2>メールアドレス変更については、変更後のメールアドレスに確認メールをお送りします。</h2>
		<h2>メールに記載されているURLにアクセスして確認を行ってください。</h2>
		<h2>確認後、メールアドレスが変更されます。</h2>
		<br/>
		<h2>※${user.mail.m0400}までにURLにアクセスいただけない場合、</h2>
		<h2 style="text-indent: 50px;">お申し込みいただいたメールアドレスの変更は無効となりますので、ご注意ください。</h2>
		<br/>
	</div>

	<div class="div">
		<input type="button" value="先頭へ戻る" onClick="goPrev()"/>
	</div>

</body>
</html>