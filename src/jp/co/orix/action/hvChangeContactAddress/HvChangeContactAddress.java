package jp.co.orix.action.hvChangeContactAddress;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.opensymphony.xwork2.ActionSupport;

import jp.co.orix.commons.CommonAPIDaoImp_030;
import jp.co.orix.dao.ChangeAddressDao_121;
import jp.co.orix.dao.CommonDao_121;
import jp.co.orix.dao.GetGroupAndPlaceDao;
import jp.co.orix.model.Info;
import jp.co.orix.model.Info2;
import jp.co.orix.model.Info3;
import jp.co.orix.model.Info4;
import jp.co.orix.model.SendMailVo;
import jp.co.orix.model.SinseiInfo;
import jp.co.orix.model.SinseiInfo2;
import jp.co.orix.model.SinseiInfo3;
import jp.co.orix.model.SinseiInfo4;
import jp.co.orix.model.SinseiInfo5;
import jp.co.orix.model.UniversalInfoITHNT000;
import jp.co.orix.util.SendMail;

public class HvChangeContactAddress extends ActionSupport {

	private List<UniversalInfoITHNT000> list;
	private ChangeAddressDao_121 changeAddressDaoImp_121;
	private GetGroupAndPlaceDao getGroupAndPlaceDao;
	private CommonAPIDaoImp_030 commonAPIDao_030;
	private CommonDao_121 commonDaoImp_121;
	private SendMail sendMail;
	private SendMailVo sendMailVo;
	private int idValue;
	private int check;
	private String keiyakubango;

	private Info info;
	private Info2 info2;
	private Info3 info3;
	private Info4 info4;
	private SinseiInfo sinseiInfo;
	private SinseiInfo2 sinseiInfo2;
	private SinseiInfo3 sinseiInfo3;
	private SinseiInfo4 sinseiInfo4;
	private SinseiInfo5 sinseiInfo5;

	private Map map;
	private Map imacp000;
	private Map insertMail;
	private String useAddress;
	private String managementPhone;
	private String billingPhone;
	private String contactPhone;

	@Override
	public String execute() throws Exception {
		System.out.println("load test");
		map = new HashMap<String, String>();
		map.put("T0200", "50003");
		map.put("T0100", "50002");
		map.put("groupComboboxCode", "c0002");
		list = getGroupAndPlaceDao.getUsePlaceComboboxList(map);
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getT0200());
			System.out.println(list.get(i).getT0401());

		}
		return SUCCESS;

	}

	public String formView() throws Exception {
		int count = commonAPIDao_030.getLocationData();

		map = new HashMap<String, String>();
		map.put("T0200", "50003");
		map.put("T0100", "50002");
		map.put("groupComboboxCode", "c0002");

		list = getGroupAndPlaceDao.getUsePlaceComboboxList(map);
		for (int i = 0; i < list.size(); i++) {
			System.out.println(list.get(i).getT0200());
			System.out.println(list.get(i).getT0100());
		}
		info = changeAddressDaoImp_121.infoSearch01(useAddress);
		info2 = changeAddressDaoImp_121.infoSearch02(useAddress);
		managementPhone = info2.getManagementPhone1() + "-" + info2.getManagementPhone2() + "-"
				+ info2.getManagementPhone3();
		info3 = changeAddressDaoImp_121.infoSearch03(useAddress);
		billingPhone = info3.getBillingPhone1() + "-" + info3.getBillingPhone2() + "-" + info3.getBillingPhone3();
		info4 = changeAddressDaoImp_121.infoSearch04(useAddress);
		contactPhone = info4.getContactPhone1() + "-" + info4.getContactPhone2() + "-" + info4.getContactPhone3();

		return SUCCESS;
	}

	public String formView2() throws Exception {
		map = new HashMap<String, Object>();
		System.out.println(sinseiInfo);
		System.out.println(sinseiInfo.getUsePlace());
		System.out.println("장소" + sinseiInfo.getPlaceOption());
		System.out.println(sinseiInfo4.getKeiyakuInfo_Tel1());
		System.out.println(sinseiInfo5.getTantoInfo_Tel2());

		return SUCCESS;
	}

	public String formView3() throws Exception {
		map = new HashMap<String, Object>();
		map.put("sinseiInfo", sinseiInfo);
		map.put("sinseiInfo2", sinseiInfo2);
		map.put("sinseiInfo3", sinseiInfo3);
		map.put("sinseiInfo4", sinseiInfo4);
		map.put("sinseiInfo5", sinseiInfo5);
		map.put("id", idValue);
		int maxSeq = changeAddressDaoImp_121.maxValueHV013();
		map.put("maxSeq", maxSeq + 1);
		// if(!(sinseiInfo.getPlaceOption() == 0)){

		changeAddressDaoImp_121.updateInfo01(map);

		changeAddressDaoImp_121.updateInfo02(map);
		changeAddressDaoImp_121.updateInfo03(map);
		changeAddressDaoImp_121.updateInfo04(map);
		changeAddressDaoImp_121.updateInfo05(map);
		// update가 성공했을때 다음을 진행하게 ...

		// InsertIMACP000 check가 1이하일경우 경고창 띄워야함
		String seq = idValue + "1";
		System.out.println("teste" + seq);
		imacp000 = new HashMap();
		imacp000.put("seq", seq);
		imacp000.put("infokubun", "01");

		check = commonDaoImp_121.insertIMACP000(imacp000);

		if (check == 0) {
			check = 99;
			return "input";
		}
		keiyakubango = commonAPIDao_030.getContractNumber(useAddress);

		
		
		 sendMailVo.setRecipient(sinseiInfo5.getTantoInfo_MailAddress1());
		 sendMailVo.setSubject(keiyakubango);
		 sendMailVo.setBody("変更申込完了");
		 sendMail.sendMail(sendMailVo);
		 int value = commonDaoImp_121.maxSeqIDUMS000();
		 
		 insertMail = new HashMap();
		 insertMail.put("seq", value+1);
		 
		 commonDaoImp_121.insertSendMail(insertMail);
		 		
		
		// getIMAL000DataList 사용 관리자 메일주소 가져옴
//		 sendMailVo.setRecipient();
//		 sendMailVo.setSubject(subject);
//		 sendMailVo.setBody(body);
//		 sendMail.sendMail();
//		 insertMail = new HashMap();
//		 insertMail.put("seq", value+1);
//		 
//		 commonDaoImp_121.insertSendMail(insertMail);
		 
		return SUCCESS;
	}

	public ChangeAddressDao_121 getChangeAddressDaoImp_121() {
		return changeAddressDaoImp_121;
	}

	public void setChangeAddressDaoImp_121(ChangeAddressDao_121 changeAddressDaoImp_121) {
		this.changeAddressDaoImp_121 = changeAddressDaoImp_121;
	}

	public Info getInfo() {
		return info;
	}

	public void setInfo(Info info) {
		this.info = info;
	}

	public Info2 getInfo2() {
		return info2;
	}

	public void setInfo2(Info2 info2) {
		this.info2 = info2;
	}

	public Info3 getInfo3() {
		return info3;
	}

	public void setInfo3(Info3 info3) {
		this.info3 = info3;
	}

	public Info4 getInfo4() {
		return info4;
	}

	public void setInfo4(Info4 info4) {
		this.info4 = info4;
	}

	public SinseiInfo getSinseiInfo() {
		return sinseiInfo;
	}

	public void setSinseiInfo(SinseiInfo sinseiInfo) {
		this.sinseiInfo = sinseiInfo;
	}

	public String getUseAddress() {
		return useAddress;
	}

	public void setUseAddress(String useAddress) {
		this.useAddress = useAddress;
	}

	public String getManagementPhone() {
		return managementPhone;
	}

	public void setManagementPhone(String managementPhone) {
		this.managementPhone = managementPhone;
	}

	public String getBillingPhone() {
		return billingPhone;
	}

	public void setBillingPhone(String billingPhone) {
		this.billingPhone = billingPhone;
	}

	public String getContactPhone() {
		return contactPhone;
	}

	public void setContactPhone(String contactPhone) {
		this.contactPhone = contactPhone;
	}

	public SinseiInfo2 getSinseiInfo2() {
		return sinseiInfo2;
	}

	public void setSinseiInfo2(SinseiInfo2 sinseiInfo2) {
		this.sinseiInfo2 = sinseiInfo2;
	}

	public SinseiInfo3 getSinseiInfo3() {
		return sinseiInfo3;
	}

	public void setSinseiInfo3(SinseiInfo3 sinseiInfo3) {
		this.sinseiInfo3 = sinseiInfo3;
	}

	public SinseiInfo4 getSinseiInfo4() {
		return sinseiInfo4;
	}

	public void setSinseiInfo4(SinseiInfo4 sinseiInfo4) {
		this.sinseiInfo4 = sinseiInfo4;
	}

	public SinseiInfo5 getSinseiInfo5() {
		return sinseiInfo5;
	}

	public void setSinseiInfo5(SinseiInfo5 sinseiInfo5) {
		this.sinseiInfo5 = sinseiInfo5;
	}

	public List<UniversalInfoITHNT000> getList() {
		return list;
	}

	public void setList(List<UniversalInfoITHNT000> list) {
		this.list = list;
	}

	public Map getImacp000() {
		return imacp000;
	}

	public void setImacp000(Map imacp000) {
		this.imacp000 = imacp000;
	}

	public GetGroupAndPlaceDao getGetGroupAndPlaceDao() {
		return getGroupAndPlaceDao;
	}

	public void setGetGroupAndPlaceDao(GetGroupAndPlaceDao getGroupAndPlaceDao) {
		this.getGroupAndPlaceDao = getGroupAndPlaceDao;
	}

	public int getIdValue() {
		return idValue;
	}

	public void setIdValue(int idValue) {
		this.idValue = idValue;
	}

	public CommonDao_121 getCommonDaoImp_121() {
		return commonDaoImp_121;
	}

	public void setCommonDaoImp_121(CommonDao_121 commonDaoImp_121) {
		this.commonDaoImp_121 = commonDaoImp_121;
	}

	public SendMail getSendMail() {
		return sendMail;
	}

	public void setSendMail(SendMail sendMail) {
		this.sendMail = sendMail;
	}

	public SendMailVo getSendMailVo() {
		return sendMailVo;
	}

	public void setSendMailVo(SendMailVo sendMailVo) {
		this.sendMailVo = sendMailVo;
	}

	public int getCheck() {
		return check;
	}

	public void setCheck(int check) {
		this.check = check;
	}

	public String getKeiyakubango() {
		return keiyakubango;
	}

	public void setKeiyakubango(String keiyakubango) {
		this.keiyakubango = keiyakubango;
	}

	public CommonAPIDaoImp_030 getCommonAPIDao_030() {
		return commonAPIDao_030;
	}

	public void setCommonAPIDao_030(CommonAPIDaoImp_030 commonAPIDao_030) {
		this.commonAPIDao_030 = commonAPIDao_030;
	}


}
