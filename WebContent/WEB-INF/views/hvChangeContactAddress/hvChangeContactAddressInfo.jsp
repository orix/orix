<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script type="text/javascript">
	function IsValidateStringValue(str) {
		var letters = /^[A-Za-z]+$/;
		if (!str.value.match(letters)) {
			return false;
		} else {
			return true;
		}
	}

	function IsValidateWideStringValue(str){

		for(var i=0;i<str.length;i++){
		 var c = str.charCodeAt(i);
		 if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
			 	return false;
			}else{
				return true;
			}
		}
	}

	function IsMailAddress(str){
		var regex = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;

	    if(!regex.test(str)) {
	        return false;
	        }
	    else{
	    	return true;
	    }
	}

	function IsPostalNo(str) {
			var regPostcode = /^([0-9]){7}$/;
			if(!regPostcode.test(str)){
				return false;
			}
			else{
				return true;
			}
		}

	function IsBlank(str){
		if(str.length == 0 || !str.trim()){
			return true;
		}else{
			return false;
		}

	}

	function IsNumber(n) {
		  return !isNaN(parseFloat(n)) && isFinite(n);
	}

	function IsBango1(n){
		return n.length<2 || n.length>=6;
	}

	function IsBango2(n){
		return n.length>=5;
	}

	function IsBango3(n){
		return n.length<4 || n.length>=5;
	}

	function IsBangoLength(n1,n2,n3){
		return n1.length + n2.length + n3.length < 10 || n1.length + n2.length + n3.length >= 12;
	}

	function Validation() {

		//(1)
		var yubinBango = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_YubinBango")[0].value;
		var jusho3 = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_Jusho3")[0].value;
		var Jusho4 = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_Jusho4")[0].value;
		var BiruName = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_BiruName")[0].value;
		var RoomNo = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_RoomNo")[0].value;
		var Yago = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_Yago")[0].value;
		var HojinName = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_HojinName")[0].value;
		var BushoName = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_BushoName")[0].value;
		var Tanto1 = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_Tanto1")[0].value;
		var Tanto2 = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_Tanto2")[0].value;
		var tantoMail = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_MailAddress")[0].value;
		var tantoMail2 = document.getElementsByName("sinseiInfo.seikyushoSofuinfo_MailAddress2")[0].value;

		if(!IsBlank(yubinBango) && !IsPostalNo(yubinBango)){
			alert("(1)郵便番号に無効な値が入力されています。");
			return false;
		}

	// 	if(!IsValidateStringValue(yubinBango)){
	// 		alert("郵便番号は半角文字を入力してください。");
	// 	}

		if(!IsBlank(jusho3) && !IsValidateWideStringValue(jusho3)){
			alert("(1)町名は全角文字を入力してください。");
			return false;
		}

		 if(!IsBlank(Jusho4) && !IsValidateWideStringValue(Jusho4)){
			alert("(1)番地は全角文字を入力してください。");
			return false;
		}
		 if(!IsBlank(BiruName) && !IsValidateWideStringValue(BiruName)){
				alert("(1)ビルㆍメンション名は全角文字を入力してください。");
				return false;
			}

		 if(!IsBlank(RoomNo) && !IsValidateWideStringValue(RoomNo)){
				alert("(1)部屋番号は全角文字を入力してください。");
				return false;
			}
		 if(!IsBlank(Yago) && !IsValidateWideStringValue(Yago)){
				alert("(1)屋号は全角文字を入力してください。");
				return false;
			}

		 if(!IsBlank(HojinName) && !IsValidateWideStringValue(HojinName)){
				alert("(1)法人名は全角文字を入力してください。");
				return false;
			}

		 if(!IsBlank(BushoName) && !IsValidateWideStringValue(BushoName)){
				alert("(1)部署名は全角文字を入力してください。");
				return false;
			}
		 if(!IsBlank(Tanto1) && !IsValidateWideStringValue(Tanto1)){
				alert("(1)担当者１は全角文字を入力してください。")
				return false;
			}

		 if(!IsBlank(Tanto2) && !IsValidateWideStringValue(Tanto2)){
				alert("(1)担当者2は全角文字を入力してください。")
				return false;
			}

	// 	 if(!IsValidateStringValue(tantoMail)){
	// 			alert("担当者メールアドレス１は半角文字を入力してください。");
	// 			return false;
	// 		}

		 if(!IsBlank(tantoMail) && !IsMailAddress(tantoMail)){
				alert("(1)担当者メールアドレス１に無効な値が入力されています。");
				return false;
			}

	// 	 if(!IsValidateStringValue(tantoMail2)){
	// 			alert("郵便番号は半角文字を入力してください。");
	// 			return false;
	// 		}

		if(!IsBlank(tantoMail) && !IsMailAddress(tantoMail)){
				alert("(1)担当者メールアドレス１に無効な値が入力されています。");
				return false;
			}

		//(2)
		var jukyuKanriInfo_HojinName = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_HojinName")[0].value;
		var jukyuKanriInfo_BushoName = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_BushoName")[0].value;
		var jukyuKanriInfo_Tanto1 = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_Tanto1")[0].value;
		var jukyuKanriInfo_Tanto2 = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_Tanto2")[0].value;
		var jukyuKanriInfo_Tel1 = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_Tel1")[0].value;
		var jukyuKanriInfo_Tel2 = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_Tel2")[0].value;
		var jukyuKanriInfo_Tel3 = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_Tel3")[0].value;
		var jukyuKanriInfo_Fax = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_Fax")[0].value;
		var jukyuKanriInfo_MailAddress1 = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_MailAddress1")[0].value;
		var jukyuKanriInfo_MailAddress2 = document.getElementsByName("sinseiInfo2.jukyuKanriInfo_MailAddress2")[0].value;
		if(!IsBlank(jukyuKanriInfo_HojinName) && !IsValidateWideStringValue(jukyuKanriInfo_HojinName)){
			alert("(2)法人名は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(jukyuKanriInfo_BushoName) && !IsValidateWideStringValue(jukyuKanriInfo_BushoName)){
			alert("(2)部署名は全角文字を入力してください。");
			return false;

		}
		if(!IsBlank(jukyuKanriInfo_Tanto1) && !IsValidateWideStringValue(jukyuKanriInfo_Tanto1)){
			alert("(2)担当者１は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(jukyuKanriInfo_Tanto2) && !IsValidateWideStringValue(jukyuKanriInfo_Tanto2)){
			alert("(2)担当者2は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(jukyuKanriInfo_Fax) && !IsValidateWideStringValue(jukyuKanriInfo_Fax)){
			alert("(2)F　A　X番号は全角文字を入力してください。");
			return false;

		}

		if(IsBlank(jukyuKanriInfo_Tel1) || !IsNumber(jukyuKanriInfo_Tel1) || IsBango1(jukyuKanriInfo_Tel1)){
			alert("(2)市外局番を整数2桁以上、5桁以内で入力してください。");
			return false;
		}

		if(IsBlank(jukyuKanriInfo_Tel2) || !IsNumber(jukyuKanriInfo_Tel2) || IsBango1(jukyuKanriInfo_Tel2)){
			alert("(2)市内局番を整1桁以上、4桁以内で入力してください。");
			return false;
		}

		if(IsBlank(jukyuKanriInfo_Tel3) || !IsNumber(jukyuKanriInfo_Tel3) || IsBango1(jukyuKanriInfo_Tel3)){
			alert("(2)加入者番号を整数4桁で入力してください。");
			return false;
		}

		//	 if(!IsValidateStringValue(jukyuKanriInfo_MailAddress1)){
		//		alert("担当者メールアドレス１は半角文字を入力してください。");
		//		return false;
		//	}

		if(!IsBlank(jukyuKanriInfo_MailAddress1) && !IsMailAddress(jukyuKanriInfo_MailAddress1)){
			alert("(2)担当者メールアドレス１に無効な値が入力されています。");
		return false;
		}

		//if(!IsValidateStringValue(jukyuKanriInfo_MailAddress1)){
			//alert("担当者メールアドレス１は半角文字を入力してください。");
		//return false;
		//}

		if(!IsBlank(jukyuKanriInfo_MailAddress2) && !IsMailAddress(jukyuKanriInfo_MailAddress2)){
			alert("(2)担当者メールアドレス2に無効な値が入力されています。");
		return false;
		}

		//(3)
		var seikyuInfo_HojinName = document.getElementsByName("sinseiInfo3.seikyuInfo_HojinName")[0].value;
		var seikyuInfo_BushoName = document.getElementsByName("sinseiInfo3.seikyuInfo_BushoName")[0].value;
		var seikyuInfo_Tanto1 = document.getElementsByName("sinseiInfo3.seikyuInfo_Tanto1")[0].value;
		var seikyuInfo_Tanto2 = document.getElementsByName("sinseiInfo3.seikyuInfo_Tanto2")[0].value;	
		var seikyuInfo_Tel1 = document.getElementsByName("sinseiInfo3.seikyuInfo_Tel1")[0].value;
		var seikyuInfo_Tel2 = document.getElementsByName("sinseiInfo3.seikyuInfo_Tel2")[0].value;
		var seikyuInfo_Tel3 = document.getElementsByName("sinseiInfo3.seikyuInfo_Tel3")[0].value;
		var seikyuInfo_Fax = document.getElementsByName("sinseiInfo3.seikyuInfo_Fax")[0].value;
		var seikyuInfo_MailAddress1 = document.getElementsByName("sinseiInfo3.seikyuInfo_MailAddress1")[0].value;
		var seikyuInfo_MailAddress2 = document.getElementsByName("sinseiInfo3.seikyuInfo_MailAddress2")[0].value;

		if(!IsBlank(seikyuInfo_HojinName) && !IsValidateWideStringValue(seikyuInfo_HojinName)){
			alert("(3)法人名は全角文字を入力してください。");
			return false;

		}
		if(!IsBlank(seikyuInfo_BushoName) && !IsValidateWideStringValue(seikyuInfo_BushoName)){
			alert("(3)部署名は全角文字を入力してください。");
			return false;

		}
		if(!IsBlank(seikyuInfo_Tanto1) && !IsValidateWideStringValue(seikyuInfo_Tanto1)){
			alert("(3)担当者１は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(seikyuInfo_Tanto2) && !IsValidateWideStringValue(seikyuInfo_Tanto2)){
			alert("(3)担当者2は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(seikyuInfo_Fax) && !IsValidateWideStringValue(seikyuInfo_Fax)){
			alert("(3)F　A　X番号は全角文字を入力してください。");
			return false;

		}

		if(IsBlank(seikyuInfo_Tel1) || !IsNumber(seikyuInfo_Tel1) || IsBango1(seikyuInfo_Tel1)){
			alert("(3)市外局番を整数2桁以上、5桁以内で入力してください。");
			return false;
		}

		if(IsBlank(seikyuInfo_Tel2) || !IsNumber(seikyuInfo_Tel2) || IsBango1(seikyuInfo_Tel2)){
			alert("(3)市内局番を整1桁以上、4桁以内で入力してください。");
			return false;
		}

		if(IsBlank(seikyuInfo_Tel3) || !IsNumber(seikyuInfo_Tel3) || IsBango1(seikyuInfo_Tel3)){
			alert("(3)加入者番号を整数4桁で入力してください。");
			return false;
		}

		//	 if(!IsValidateStringValue(jukyuKanriInfo_MailAddress1)){
		//		alert("担当者メールアドレス１は半角文字を入力してください。");
		//		return false;
		//	}

		if(!IsBlank(seikyuInfo_MailAddress1) && !IsMailAddress(seikyuInfo_MailAddress1)){
			alert("(3)担当者メールアドレス１に無効な値が入力されています。");
		return false;
		}

		//if(!IsValidateStringValue(jukyuKanriInfo_MailAddress1)){
			//alert("担当者メールアドレス１は半角文字を入力してください。");
		//return false;
		//}

		if(!IsBlank(seikyuInfo_MailAddress2) && !IsMailAddress(seikyuInfo_MailAddress2)){
			alert("(3)担当者メールアドレス2に無効な値が入力されています。");
		return false;
		}

		//(4)

		var keiyakuInfo_HojinName = document.getElementsByName("sinseiInfo4.keiyakuInfo_HojinName")[0].value;
		var keiyakuInfo_BushoName = document.getElementsByName("sinseiInfo4.keiyakuInfo_BushoName")[0].value;
		var keiyakuInfo_Tanto1 = document.getElementsByName("sinseiInfo4.keiyakuInfo_Tanto1")[0].value;
		var keiyakuInfo_Tanto2 = document.getElementsByName("sinseiInfo4.keiyakuInfo_Tanto2")[0].value;
		var keiyakuInfo_Tel1 = document.getElementsByName("sinseiInfo4.keiyakuInfo_Tel1")[0].value;
		var keiyakuInfo_Tel2 = document.getElementsByName("sinseiInfo4.keiyakuInfo_Tel2")[0].value;
		var keiyakuInfo_Tel3 = document.getElementsByName("sinseiInfo4.keiyakuInfo_Tel3")[0].value;
		var keiyakuInfo_Fax = document.getElementsByName("sinseiInfo4.keiyakuInfo_Fax")[0].value;
		var keiyakuInfo_MailAddress1 = document.getElementsByName("sinseiInfo4.keiyakuInfo_MailAddress1")[0].value;
		var keiyakuInfo_MailAddress2 = document.getElementsByName("sinseiInfo4.keiyakuInfo_MailAddress2")[0].value;

		if(!IsBlank(keiyakuInfo_HojinName) && !IsValidateWideStringValue(keiyakuInfo_HojinName)){
			alert("(4)法人名は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(keiyakuInfo_BushoName) && !IsValidateWideStringValue(keiyakuInfo_BushoName)){
			alert("(4)部署名は全角文字を入力してください。");
			return false;

		}
		if(!IsBlank(keiyakuInfo_Tanto1) && !IsValidateWideStringValue(keiyakuInfo_Tanto1)){
			alert("(4)担当者１は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(keiyakuInfo_Tanto2) && !IsValidateWideStringValue(keiyakuInfo_Tanto2)){
			alert("(4)担当者2は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(keiyakuInfo_Fax) && !IsValidateWideStringValue(seikyuInfo_Fax)){
			alert("(4)F　A　X番号は全角文字を入力してください。");
			return false;

		}

		if(IsBlank(keiyakuInfo_Tel1) || !IsNumber(keiyakuInfo_Tel1) || IsBango1(keiyakuInfo_Tel1)){
			alert("(4)市外局番を整数2桁以上、5桁以内で入力してください。");
			return false;
		}

		if(IsBlank(keiyakuInfo_Tel2) || !IsNumber(keiyakuInfo_Tel2) || IsBango1(keiyakuInfo_Tel2)){
			alert("(4)市内局番を整1桁以上、4桁以内で入力してください。");
			return false;
		}

		if(IsBlank(keiyakuInfo_Tel3) || !IsNumber(keiyakuInfo_Tel3) || IsBango1(keiyakuInfo_Tel3)){
			alert("(4)加入者番号を整数4桁で入力してください。");
			return false;
		}

		//	 if(!IsValidateStringValue(jukyuKanriInfo_MailAddress1)){
		//		alert("担当者メールアドレス１は半角文字を入力してください。");
		//		return false;
		//	}

		if(!IsBlank(keiyakuInfo_MailAddress1) && !IsMailAddress(keiyakuInfo_MailAddress1)){
			alert("(4)担当者メールアドレス１に無効な値が入力されています。");
		return false;
		}

		//if(!IsValidateStringValue(jukyuKanriInfo_MailAddress1)){
			//alert("担当者メールアドレス１は半角文字を入力してください。");
		//return false;
		//}

		if(!IsBlank(keiyakuInfo_MailAddress2) && !IsMailAddress(keiyakuInfo_MailAddress2)){
			alert("(4)担当者メールアドレス2に無効な値が入力されています。");
		return false;
		}

		//(5)
		var tantoInfo_YubinBango = document.getElementsByName("sinseiInfo5.tantoInfo_YubinBango")[0].value;
		var tantoInfo_Jusho = document.getElementsByName("sinseiInfo5.tantoInfo_Jusho")[0].value;
		var tantoInfo_HojinName = document.getElementsByName("sinseiInfo5.tantoInfo_HojinName")[0].value;
		var tantoInfo_BushoName = document.getElementsByName("sinseiInfo5.tantoInfo_BushoName")[0].value;
		var tantoInfo_Tanto1 = document.getElementsByName("sinseiInfo5.tantoInfo_Tanto1")[0].value;
		var tantoInfo_Tel1 = document.getElementsByName("sinseiInfo5.tantoInfo_Tel1")[0].value;
		var tantoInfo_Tel2 = document.getElementsByName("sinseiInfo5.tantoInfo_Tel2")[0].value;
		var tantoInfo_Tel3 = document.getElementsByName("sinseiInfo5.tantoInfo_Tel3")[0].value;
		var tantoInfo_MailAddress1 = document.getElementsByName("sinseiInfo5.tantoInfo_MailAddress1")[0].value;
		var tantoInfo_Memo = document.getElementsByName("sinseiInfo5.tantoInfo_Memo")[0].value;

		if(!IsBlank(tantoInfo_YubinBango) && !IsPostalNo(tantoInfo_YubinBango)){
			alert("(5)郵便番号に無効な値が入力されています。");
			return false;
		}

		if(!IsBlank(tantoInfo_Jusho) && !IsValidateWideStringValue(tantoInfo_Jusho)){
			alert("(5)法人名は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(tantoInfo_HojinName) && !IsValidateWideStringValue(tantoInfo_HojinName)){
			alert("(5)法人名は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(tantoInfo_BushoName) && !IsValidateWideStringValue(tantoInfo_BushoName)){
			alert("(5)法人名は全角文字を入力してください。");
			return false;

		}

		if(!IsBlank(tantoInfo_Memo) && !IsValidateWideStringValue(tantoInfo_Memo)){
			alert("(5)全角文字を入力してください。");
			return false;

		}

		if(IsBlank(tantoInfo_Tel1) || !IsNumber(tantoInfo_Tel1) || IsBango1(tantoInfo_Tel1)){
			alert("(5)市外局番を整数2桁以上、5桁以内で入力してください。");
			return false;
		}

		if(IsBlank(tantoInfo_Tel2) || !IsNumber(tantoInfo_Tel2) || IsBango1(tantoInfo_Tel2)){
			alert("(5)市内局番を整1桁以上、4桁以内で入力してください。");
			return false;
		}

		if(IsBlank(tantoInfo_Tel3) || !IsNumber(tantoInfo_Tel3) || IsBango1(tantoInfo_Tel3)){
			alert("(5)加入者番号を整数4桁で入力してください。");
			return false;
		}

		//	 if(!IsValidateStringValue(tantoMail2)){
		//		alert("郵便番号は半角文字を入力してください。");
		//		return false;
		//	}

		if(!IsBlank(tantoInfo_MailAddress1) && !IsMailAddress(tantoInfo_MailAddress1)){
			alert("(5)担当者メールアドレス１に無効な値が入力されています。");
			return false;
		}

		//자유기입란
		if(!IsBlank(keiyakuInfo_HojinName) && !IsValidateWideStringValue(keiyakuInfo_HojinName)){
			alert("(6)自由記入欄は全角文字を入力してください。");
			return false;

		}
		 alert("오류없");
		 return true;

	}
</script>



<div class="modify">
	<div class="search">
		<form action="changeAddressViewForm" method="post">
			ご使用場所<input type="text" list="usePlaceComboboxList" name="useAddress" />
			<datalist id="usePlaceComboboxList">
				<s:iterator value="list"> --%>
				<option value="${t0200}">${t0401 }</option>

				</s:iterator>
			</datalist>

			<%-- 		ご使用場所<select name="useAddress" id="test1"> --%>
			<%-- 			<s:iterator value="list"> --%>
			<%-- 				<option value="${t0200}">${t0401 }</option> --%>

			<%-- 			</s:iterator> --%>

			<!-- 			<option value="i0001">test2</option> -->
			<!-- 			<option value="f0001">test3</option> -->
			<!-- 			<option value="c0002">test4</option> -->
			<%-- 		</select> --%>
			<input type="submit" value="検索" />
		</form>
	</div>

	<form action="changeAddressCheckForm" method="get">
		<center>
		<input type="radio" checked="checked" name="sinseiInfo.placeOption"
			value="0">該当ご使用場所のみ反映する&nbsp;&nbsp;&nbsp; <input type="radio"
			name="sinseiInfo.placeOption" value="1"> 全ご使用場所に反映する
		</center>
		<!-- 	onclick="javascript:location.href='action.action?selectAll=1'"> -->
		<div class="modify_confirm">
			<div class="input">
				<input type="hidden" name="idValue" value="${info.id }" />
				<table>
					<tr>
						<th>住所</th>
						<td>
							<%-- 					<s:property value="%{info.address}" /> --%> <input
							type="text" value="${info.address}" name="sinseiInfo.shiyojusho"
							id="read" readonly="readonly">
						</td>
					</tr>
					<tr>
						<th>ご使用場所</th>
						<td>
							<%-- 					<s:property value="%{info.location}" /> --%> <input
							type="text" value="${info.location}" name="sinseiInfo.usePlace"
							id="read" readonly="readonly">
						</td>
					</tr>
					<tr>
						<th>契約番号</th>
						<td>
							<%-- 					<s:property value="%{info.contractNumber}" /> --%> <input
							type="text" value="${info.contractNumber}"
							name="sinseiInfo.keiyakuNo" id="read" readonly="readonly">
						</td>
					</tr>
					<tr>
						<th>契約者名義人</th>
						<td>
							<%-- 					${info.contractName} --%> <input type="text"
							value="${info.contractName}" id="read"
							name="sinseiInfo.keiyakuMeibinin" readonly="readonly">
						</td>
					</tr>
				</table>
			</div>
		</div>

		<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

		<div class="input">
			<table>
				<tr>
					<td colspan="3">※「＊」の付いた項目は必ず入力してください。</td>
				</tr>
				<tr>
					<th id="th_left" colspan="3">（１）請求書送付先情報</th>
					<td></td>
				</tr>
				<tr>
					<td style="width: 15%"></td>
					<th style="width: 35%">変更前</th>
					<th style="width: 35%">変更後</th>
					<td style="width: 15%"></td>
				<tr>
					<th>*郵便番号</th>
					<td><input type="text" id="read" value="${info.postNumber}" /></td>
					<td class="white"><input type="text"
						value="${info.postNumber}" id="zip2"
						name="sinseiInfo.seikyushoSofuinfo_YubinBango" /></td>
					<td class="white"><input type="button" value="郵便番号"
						onClick="getSearchPostalNo2()"></td>


				</tr>



				<tr>
					<th>都道府県名</th>
					<td><input type="text" id="read" value="${info.address1}" /></td>
					<td><input type="text" value="${info.address1}"
						readonly="readonly" id="address1"
						name="sinseiInfo.seikyushoSofuinfo_Jusho1"></td>
				</tr>

				<tr>
					<th>市区町村名</th>
					<td><input type="text" id="read" value="${info.address2}" /></td>
					<td><input type="text" value="${info.address2}"
						readonly="readonly" id="address2"
						name="sinseiInfo.seikyushoSofuinfo_Jusho2"></td>
				</tr>

				<tr>
					<th>＊町名</th>
					<td><input type="text" id="read" value="${info.address3}" /></td>
					<td class="white"><input type="text" value="${info.address3}"
						id="address3" name="sinseiInfo.seikyushoSofuinfo_Jusho3" /></td>
				</tr>

				<tr>
					<th>＊番地</th>
					<td><input type="text" id="read" value="${info.address4}" /></td>
					<td class="white"><input type="text" value="${info.address4}"
						name="sinseiInfo.seikyushoSofuinfo_Jusho4" /></td>
				</tr>

				<tr>
					<th>ビルㆍメンション名</th>
					<td><input type="text" id="read" value="${info.buildingName}" /></td>
					<td class="white"><input type="text"
						value="${info.buildingName}"
						name="sinseiInfo.seikyushoSofuinfo_BiruName" /></td>
				</tr>


				<tr>
					<th>部屋番号</th>
					<td><input type="text" id="read" value="${info.roomNo}" /></td>
					<td class="white"><input type="text" value="${info.roomNo}"
						name="sinseiInfo.seikyushoSofuinfo_RoomNo" /></td>
				</tr>

				<tr>
					<th>屋号</th>
					<td><input type="text" id="read" value="${info.houseNo}" /></td>
					<td class="white"><input type="text" value="${info.houseNo}"
						name="sinseiInfo.seikyushoSofuinfo_Yago" /></td>
				</tr>

				<tr>
					<th>*法人名</th>
					<td><input type="text" id="read"
						value="${info.destinationCorporateName}" /></td>
					<td class="white"><input type="text"
						value="${info.destinationCorporateName}"
						name="sinseiInfo.seikyushoSofuinfo_HojinName" /></td>
				</tr>

				<tr>
					<th>部署名</th>
					<td><input type="text" id="read"
						value="${info.destinationDivisionName}" /></td>
					<td class="white"><input type="text"
						value="${info.destinationDivisionName}"
						name="sinseiInfo.seikyushoSofuinfo_BushoName" /></td>
				</tr>

				<tr>
					<th>担当者１</th>
					<td><input type="text" id="read"
						value="${info.destinationName1}" /></td>
					<td class="white"><input type="text"
						value="${info.destinationName1}"
						name="sinseiInfo.seikyushoSofuinfo_Tanto1" /></td>
				</tr>

				<tr>
					<th>担当者２</th>
					<td><input type="text" id="read"
						value="${info.destinationName2}" /></td>
					<td class="white"><input type="text"
						value="${info.destinationName2}"
						name="sinseiInfo.seikyushoSofuinfo_Tanto2" /></td>
				</tr>

				<tr>
					<th>担当者メールアドレス１</th>
					<td><input type="text" id="read"
						value="${info.destinationMail1}" /></td>
					<td class="white"><input type="text"
						value="${info.destinationMail1}"
						name="sinseiInfo.seikyushoSofuinfo_MailAddress" /></td>
				</tr>

				<tr>
					<th>担当者メールアドレス２</th>
					<td><input type="text" id="read"
						value="${info.destinationMail2}" /></td>
					<td class="white"><input type="text"
						value="${info.destinationMail2}"
						name="sinseiInfo.seikyushoSofuinfo_MailAddress2" /></td>
				</tr>

			</table>
			<br />
			<table>
				<tr>
					<th id="th_left" colspan="3">（２）需給管理情報</th>
				</tr>

				<tr>
					<td></td>
					<th>変更前</th>
					<th>変更後</th>

				</tr>
				<tr>
					<th>＊法人名</th>
					<td><input type="text" id="read"
						value="${info2.managementCorporateName }" /></td>
					<td><input type="text"
						value="${info2.managementCorporateName }"
						name="sinseiInfo2.jukyuKanriInfo_HojinName" /></td>
				</tr>

				<tr>
					<th>部署名</th>
					<td><input type="text" id="read"
						value="${info2.managementDivisionName }" /></td>
					<td><input type="text"
						value="${info2.managementDivisionName }"
						name="sinseiInfo2.jukyuKanriInfo_BushoName" /></td>
				</tr>

				<tr>
					<th>担当者１</th>
					<td><input type="text" id="read"
						value="${info2.managementName1 }" /></td>
					<td><input type="text" value="${info2.managementName1 }"
						name="sinseiInfo2.jukyuKanriInfo_Tanto1" /></td>
				</tr>

				<tr>
					<th>担当者２</th>
					<td><input type="text" id="read"
						value="${info2.managementName2 }" /></td>
					<td><input type="text" value="${info2.managementName2 }"
						name="sinseiInfo2.jukyuKanriInfo_Tanto2" /></td>
				</tr>

				<tr>
					<th>＊電話番号</th>
					<%-- 				<td>${info2.managementPhone1}-${info2.managementPhone2}-${info2.managementPhone3 }</td> --%>
					<td><input type="text" id="read" value="${managementPhone}" /></td>
					<td><input type="text" name="sinseiInfo2.jukyuKanriInfo_Tel1"
						value="${info2.managementPhone1}" id="mini">-<input
						type="text" name="sinseiInfo2.jukyuKanriInfo_Tel2" id="mini"
						value="${info2.managementPhone2}">-<input type="text"
						name="sinseiInfo2.jukyuKanriInfo_Tel3" id="mini"
						value="${info2.managementPhone3}"></td>
				</tr>

				<tr>
					<th>F A X番号</th>
					<td><input type="text" id="read"
						value="${info2.managementFax }" /></td>
					<td><input type="text" value="${info2.managementFax }"
						name="sinseiInfo2.jukyuKanriInfo_Fax" /></td>
				</tr>

				<tr>
					<th>メールアドレス１</th>
					<td><input type="text" id="read"
						value="${info2.managementMail1 }" /></td>
					<td><input type="text" value="${info2.managementMail1 }"
						name="sinseiInfo2.jukyuKanriInfo_MailAddress1" /></td>
				</tr>

				<tr>
					<th>メールアドレス２</th>
					<td><input type="text" id="read"
						value="${info2.managementMail2 }" /></td>
					<td><input type="text" value="${info2.managementMail2 }"
						name="sinseiInfo2.jukyuKanriInfo_MailAddress2" /></td>
				</tr>
			</table>

			<table>
				<tr>
					<th id="th_left" colspan="3">（３）請求問合わせ窓口情報</th>

				</tr>

				<tr>
					<td></td>
					<th>変更前</th>
					<th>変更後</th>
				</tr>

				<tr>
					<th>*法人名</th>
					<td><input type="text" id="read"
						value="${info3.billingCompanyName }" /></td>
					<td><input type="text" value="${info3.billingCompanyName }"
						name="sinseiInfo3.seikyuInfo_HojinName" /></td>
				</tr>

				<tr>
					<th>部署名</th>
					<td><input type="text" id="read"
						value="${info3.billingDivisionName }" /></td>
					<td><input type="text" value="${info3.billingDivisionName }"
						name="sinseiInfo3.seikyuInfo_BushoName" /></td>
				</tr>

				<tr>
					<th>担当者１</th>
					<td><input type="text" id="read"
						value="${info3.billingName1 }" /></td>
					<td><input type="text" value="${info3.billingName1 }"
						name="sinseiInfo3.seikyuInfo_Tanto1" /></td>
				</tr>

				<tr>
					<th>担当者２</th>
					<td><input type="text" id="read"
						value="${info3.billingName2 }" /></td>
					<td><input type="text" value="${info3.billingName2 }"
						name="sinseiInfo3.seikyuInfo_Tanto2" /></td>
				</tr>

				<tr>
					<th>＊電話番号</th>
					<%-- 				<td>${info3.billingPhone1}-${info3.billingPhone2}-${info3.billingPhone3}</td> --%>
					<td><input type="text" id="read" value="${billingPhone }" /></td>
					<td><input type="text" name="sinseiInfo3.seikyuInfo_Tel1"
						id="mini" value="${info3.billingPhone1}">-<input
						type="text" name="sinseiInfo3.seikyuInfo_Tel2" id="mini"
						value="${info3.billingPhone2}">-<input type="text"
						name="sinseiInfo3.seikyuInfo_Tel3" id="mini"
						value="${info3.billingPhone3}"></td>
				</tr>

				<tr>
					<th>F A X番号</th>
					<td><input type="text" id="read" value="${info3.billingFax }" /></td>
					<td><input type="text" value="${info3.billingFax }"
						name="sinseiInfo3.seikyuInfo_Fax" /></td>
				</tr>

				<tr>
					<th>メールアドレス１</th>
					<td><input type="text" id="read"
						value="${info3.billingMail1 }" /></td>
					<td><input type="text" value="${info3.billingMail1 }"
						name="sinseiInfo3.seikyuInfo_MailAddress1" /></td>
				</tr>

				<tr>
					<th>メールアドレス２</th>
					<td><input type="text" id="read"
						value="${info3.billingMail2 }" /></td>
					<td><input type="text" value="${info3.billingMail2 }"
						name="sinseiInfo3.seikyuInfo_MailAddress2" /></td>
				</tr>


			</table>

			<table>
				<tr>
					<th id="th_left" colspan="3">（４）契約条件問合せ窓口情報</th>

				</tr>

				<tr>
					<td></td>
					<th>変更前</th>
					<th>変更後</th>
				</tr>

				<tr>
					<th>＊法人名</th>
					<td><input type="text" id="read"
						value="${info4.contactCorporateName }" /></td>
					<td><input type="text" value="${info4.contactCorporateName }"
						name="sinseiInfo4.keiyakuInfo_HojinName" /></td>
				</tr>

				<tr>
					<th>部署名</th>
					<td><input type="text" id="read"
						value="${info4.contactDivisionName }" /></td>
					<td><input type="text" value="${info4.contactDivisionName }"
						name="sinseiInfo4.keiyakuInfo_BushoName" /></td>
				</tr>

				<tr>
					<th>担当者１</th>
					<td><input type="text" id="read"
						value="${info4.contactName1 }" /></td>
					<td><input type="text" value="${info4.contactName1 }"
						name="sinseiInfo4.keiyakuInfo_Tanto1" /></td>
				</tr>

				<tr>
					<th>担当者２</th>
					<td><input type="text" id="read"
						value="${info4.contactName2 }" /></td>
					<td><input type="text" value="${info4.contactName2 }"
						name="sinseiInfo4.keiyakuInfo_Tanto2" /></td>
				</tr>

				<tr>
					<th>＊電話番号</th>
					<%-- 				<td>${info4.contactPhone1 }-${info4.contactPhone2 }-${info4.contactPhone3 }</td> --%>
					<td><input type="text" id="read" value="${contactPhone }" /></td>
					<td><input type="text" name="sinseiInfo4.keiyakuInfo_Tel1"
						id="mini" value="${info4.contactPhone1 }">-<input
						type="text" name="sinseiInfo4.keiyakuInfo_Tel2" id="mini"
						value="${info4.contactPhone2 }">-<input type="text"
						name="sinseiInfo4.keiyakuInfo_Tel3" id="mini"
						value="${info4.contactPhone3 }"></td>
				</tr>

				<tr>
					<th>F A X番号</th>
					<td><input type="text" id="read" value="${info4.contactFax }" /></td>
					<td><input type="text" value="${info4.contactFax }"
						name="sinseiInfo4.keiyakuInfo_Fax" /></td>
				</tr>

				<tr>
					<th>メールアドレス１</th>
					<td><input type="text" id="read"
						value="${info4.contactMail1 }" /></td>
					<td><input type="text" value="${info4.contactMail1 }"
						name="sinseiInfo4.keiyakuInfo_MailAddress1" /></td>
				</tr>

				<tr>
					<th>メールアドレス２</th>
					<td><input type="text" id="read"
						value="${info4.contactMail2 }" /></td>
					<td><input type="text" value="${info4.contactMail2 }"
						name="sinseiInfo4.keiyakuInfo_MailAddress2" /></td>
				</tr>

			</table>

			<table>
				<tr>
					<th id="th_left" colspan="3">（５）申込担当者情報</th>

				</tr>


				<tr>
					<th id="id_fixed">＊郵便番号</th>
					<td id="td_fixed"><input type="text"
						name="sinseiInfo5.tantoInfo_YubinBango" id="zip" /></td>
					<td align="left"><input type="button" value="郵便番号検索"
						onclick="getSearchPostalNo()" /></td>
				</tr>

				<tr>
					<th>＊住所</th>
					<td><input type="text" name="sinseiInfo5.tantoInfo_Jusho"
						id="address" /></td>
				</tr>

				<tr>
					<th>＊会社名</th>
					<td><input type="text" name="sinseiInfo5.tantoInfo_HojinName" /></td>
				</tr>

				<tr>
					<th>部署名</th>
					<td><input type="text" name="sinseiInfo5.tantoInfo_BushoName" /></td>
				</tr>

				<tr>
					<th>＊ご担当者</th>
					<td><input type="text" name="sinseiInfo5.tantoInfo_Tanto1" /></td>
				</tr>

				<tr>
					<th>＊電話番号</th>
					<td><input type="text" name="sinseiInfo5.tantoInfo_Tel1"
						id="mini">-<input type="text"
						name="sinseiInfo5.tantoInfo_Tel2" id="mini">-<input
						type="text" name="sinseiInfo5.tantoInfo_Tel3" id="mini"></td>
				</tr>

				<tr>
					<th>＊メールアドレス</th>
					<td><input type="text"
						name="sinseiInfo5.tantoInfo_MailAddress1" /></td>
				</tr>


			</table>
			<br />

			<table>
				<tr>
					<th width="20%">（６）自由記入欄</th>
					<td><textarea rows="4" cols="83"
							name="sinseiInfo5.tantoInfo_Memo"></textarea></td>
				</tr>

			</table>
			<br /> <input type="submit" value="内容確認"
				onclick="return Validation()">
		</div>
	</form>
</div>

<script src="https://code.jquery.com/jquery-2.2.0.min.js"></script>



<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />
<br />