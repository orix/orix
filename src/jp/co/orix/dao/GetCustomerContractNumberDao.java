package jp.co.orix.dao;

import java.util.Map;

public interface GetCustomerContractNumberDao {

	public String getCustomerContractNumber(Map<String, String> condition);

}
