package jp.co.orix.dao;

import java.util.Map;

import jp.co.orix.model.IMACP000;
import jp.co.orix.model.UserPaymentInfo;

public interface PaymentInfoDao {

	public UserPaymentInfo getUserInfo(String HV011_location);
	public Map getKubunName(Map paymentMethodInfoSet);
	
	public void changeUserPaymentInfo_HV011(UserPaymentInfo userPaymentInfo);
	public void changeUserPaymentInfo_CM004(UserPaymentInfo userPaymentInfo);
	
	public int countRecordOfIDUMS000();
	public void changeUserPaymentInfo_IDUMS000(UserPaymentInfo userPaymentInfo);
	
	public int countRecordOfIMACP000();
	public void getInsertIMACP000SQL(IMACP000 imacp000);
	
	public void changeUserPaymentInfo_All_HV011(UserPaymentInfo userPaymentInfo);
	public void changeUserPaymentInfo_All_CM004(UserPaymentInfo userPaymentInfo);
	public void changeUserPaymentInfo_All_IDUMS000(UserPaymentInfo userPaymentInfo);
	public void getInsertIMACP000SQL_All(IMACP000 imacp000);
}
