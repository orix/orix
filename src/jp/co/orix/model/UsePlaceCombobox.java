package jp.co.orix.model;

public class UsePlaceCombobox {
	private int H0803;
	private String H0804;
	
	public int getH0803() {
		return H0803;
	}

	public void setH0803(int h0803) {
		H0803 = h0803;
	}

	public String getH0804() {
		return H0804;
	}

	public void setH0804(String h0804) {
		H0804 = h0804;
	}

	public UsePlaceCombobox() {
		// TODO Auto-generated constructor stub
	}

	public UsePlaceCombobox(int h0803, String h0804) {
		H0803 = h0803;
		H0804 = h0804;
	}

	@Override
	public String toString() {
		return "UsePlaceCombobox [H0803=" + H0803 + ", H0804=" + H0804 + "]";
	}
	
}
