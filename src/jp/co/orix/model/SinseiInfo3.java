package jp.co.orix.model;

public class SinseiInfo3 {

	private String seikyuInfo_HojinName;
	private String seikyuInfo_BushoName;
	private String seikyuInfo_Tanto1;
	private String seikyuInfo_Tanto2;
	private String seikyuInfo_Tel1;
	private String seikyuInfo_Tel2;
	private String seikyuInfo_Tel3;
	private String seikyuInfo_Fax;
	private String seikyuInfo_MailAddress1;
	private String seikyuInfo_MailAddress2;
	public String getSeikyuInfo_HojinName() {
		return seikyuInfo_HojinName;
	}
	public void setSeikyuInfo_HojinName(String seikyuInfo_HojinName) {
		this.seikyuInfo_HojinName = seikyuInfo_HojinName;
	}
	public String getSeikyuInfo_BushoName() {
		return seikyuInfo_BushoName;
	}
	public void setSeikyuInfo_BushoName(String seikyuInfo_BushoName) {
		this.seikyuInfo_BushoName = seikyuInfo_BushoName;
	}
	public String getSeikyuInfo_Tanto1() {
		return seikyuInfo_Tanto1;
	}
	public void setSeikyuInfo_Tanto1(String seikyuInfo_Tanto1) {
		this.seikyuInfo_Tanto1 = seikyuInfo_Tanto1;
	}
	public String getSeikyuInfo_Tanto2() {
		return seikyuInfo_Tanto2;
	}
	public void setSeikyuInfo_Tanto2(String seikyuInfo_Tanto2) {
		this.seikyuInfo_Tanto2 = seikyuInfo_Tanto2;
	}
	public String getSeikyuInfo_Tel1() {
		return seikyuInfo_Tel1;
	}
	public void setSeikyuInfo_Tel1(String seikyuInfo_Tel1) {
		this.seikyuInfo_Tel1 = seikyuInfo_Tel1;
	}
	public String getSeikyuInfo_Tel2() {
		return seikyuInfo_Tel2;
	}
	public void setSeikyuInfo_Tel2(String seikyuInfo_Tel2) {
		this.seikyuInfo_Tel2 = seikyuInfo_Tel2;
	}
	public String getSeikyuInfo_Tel3() {
		return seikyuInfo_Tel3;
	}
	public void setSeikyuInfo_Tel3(String seikyuInfo_Tel3) {
		this.seikyuInfo_Tel3 = seikyuInfo_Tel3;
	}
	public String getSeikyuInfo_Fax() {
		return seikyuInfo_Fax;
	}
	public void setSeikyuInfo_Fax(String seikyuInfo_Fax) {
		this.seikyuInfo_Fax = seikyuInfo_Fax;
	}
	public String getSeikyuInfo_MailAddress1() {
		return seikyuInfo_MailAddress1;
	}
	public void setSeikyuInfo_MailAddress1(String seikyuInfo_MailAddress1) {
		this.seikyuInfo_MailAddress1 = seikyuInfo_MailAddress1;
	}
	public String getSeikyuInfo_MailAddress2() {
		return seikyuInfo_MailAddress2;
	}
	public void setSeikyuInfo_MailAddress2(String seikyuInfo_MailAddress2) {
		this.seikyuInfo_MailAddress2 = seikyuInfo_MailAddress2;
	}
	
	
}
