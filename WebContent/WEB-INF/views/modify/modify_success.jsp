<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
    <style>
<!--

-->

.modify_success {
	align:center;
	text-align:center;
	margin:auto;
}

.modify_success button{
background-color: #4F81BD;
	box-shadow: 0 1px 4px 0 rgba(0, 0, 0, 0.2), 0 1px 3px 0
		rgba(0, 0, 0, 0.19);
	width: 80px;
	height: 26px;
	border: none;
	color: white;
	cursor: pointer;
	text-align: center;
	border-radius: 2px;
	font-size: 12px;
}
</style>
<div class="modify_success">
<br/>
  変更申込が完了しました。
<br/>
<br/>
    <button onclick="javascript:location.href='./View.action'">先頭へ戻る</button>
    </div>