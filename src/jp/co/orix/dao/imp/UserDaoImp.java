package jp.co.orix.dao.imp;


import java.util.Map;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.DateTimeDao;
import jp.co.orix.dao.UserDao;
import jp.co.orix.model.User;
import jp.co.orix.model.UserDetail;
import jp.co.orix.model.UserMail;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class UserDaoImp implements UserDao {
	
	private User user = new User();
	private UserMail mail = new UserMail();
	private UserDetail detail = new UserDetail();
	
	@Autowired
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	
	

	@Override
	public User getUserData(String identificationNumber) throws Exception {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		UserDao dao = session.getMapper(UserDao.class);
		
		User user = null;
		System.out.println("1");
		try {
			user = dao.getUserData(identificationNumber);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
		return user;
	}
	
	//유저의 선택정보를 구해오는 부분.
	@Override
	public User select(String id) {
		// TODO Auto-generated method stub , Integer type , type
		User user = null;
		
		SqlSession session = sqlSession.openSession();
		System.out.println("++++++++++: sqlSession : +++++++ : " + sqlSession);
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			user = dao.select(id);
			System.out.println("DaoImpl Result 1 = " + user);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return user;
	}

	@Override
	public User userSelect(String id) {
		// TODO Auto-generated method stub , Integer type , type
		User user = null;
		
		SqlSession session = sqlSession.openSession();
		System.out.println("++++++++++: UsersqlSession : +++++++ : " + sqlSession);
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			user = dao.userSelect(id);
			System.out.println("UserDetailDaoResult 1 = " + user);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return user;
	}
	
	//유저명 변경 부분
	@Override
	public int nameUpdate(Integer type, String newM0201) {
		// TODO Auto-generated method stub
		int nameUpdate = 0;
		SqlSession session = sqlSession.openSession();
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			nameUpdate = dao.nameUpdate(type, newM0201);
			session.commit();
			System.out.println("NameUpdate : " + nameUpdate);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return nameUpdate;
	}
	
	@Override
	public int mailUpdate(Integer type, String newM0200) {
		// TODO Auto-generated method stub
		int mailUpdate = 0;
		SqlSession session = sqlSession.openSession();
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			mailUpdate = dao.mailUpdate(type, newM0200);
			session.commit();
			System.out.println("MailUpdate : " + mailUpdate);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return mailUpdate;
	}
	
	@Override
	public int afterUpdate(Integer type,  UserDetail detail) {
		// TODO Auto-generated method stub
		int detailUpdate = 0;
		SqlSession session = sqlSession.openSession();
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			detailUpdate = dao.afterUpdate(type, detail);
			System.out.println("detail : " + detail);
			session.commit();
			System.out.println("detailUpdate : "+detailUpdate);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return detailUpdate;
	}
	
	@Override
	public User afterCheck(String id) {
		// TODO Auto-generated method stub
		User detail = null;
		
		SqlSession session = sqlSession.openSession();
		System.out.println("++++++++++: UsersqlSession : +++++++ : " + sqlSession);
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			detail = dao.afterCheck(id);
			System.out.println("UserDetailDaoResult 1 = " + detail);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return detail;
	}
	
	@Override
	public int detailUpdate(Integer type, UserDetail detail) {
		// TODO Auto-generated method stub
		int detailUpdate = 0;
		SqlSession session = sqlSession.openSession();
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			detailUpdate = dao.detailUpdate(type, detail);
			System.out.println("DAO detail 1 : " + detail);
			session.commit();
			System.out.println("DAO detailUpdate 2  : "+detailUpdate);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return detailUpdate;
		
	}

	@Override
	public int getUrlAccessLimitDays(Integer type) {
		// TODO Auto-generated method stub
		int ualdUpdate = 0;
		SqlSession session = sqlSession.openSession();
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			ualdUpdate = dao.getUrlAccessLimitDays(type);
			session.commit();
			System.out.println("UALDUpdate : " + ualdUpdate);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return ualdUpdate;
	}
	
	@Override
	public User limitDaysCheck(Integer type) {
		// TODO Auto-generated method stub
		User user = null;
		
		SqlSession session = sqlSession.openSession();
		System.out.println("++++++++++: sqlSession : +++++++ : " + sqlSession);
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			user = dao.limitDaysCheck(type);
			System.out.println("limitDaysCheck Result 1 = " + user);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return user;
	}
	
	@Override
	public User userlimitDaysCheck(Integer type) {
		// TODO Auto-generated method stub
User user = null;
		
		SqlSession session = sqlSession.openSession();
		System.out.println("++++++++++: sqlSession : +++++++ : " + sqlSession);
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			user = dao.userlimitDaysCheck(type);
			System.out.println("userlimitDaysCheck Result 1 = " + user);
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			session.close();
		}
		return user;
	}

	@Override
	public String dateTime() {
		// TODO Auto-generated method stub
		String dateTime = "";
		SqlSession session = sqlSession.openSession();
		System.out.println(session);
		UserDao dao = session.getMapper(UserDao.class);
		
		try {
			dateTime = dao.dateTime();
			System.out.println(dateTime);
		
		
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		return dateTime;
	}

	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public SqlSessionFactory getSqlSession() {
		return sqlSession;
	}
	public void setSqlSession(SqlSessionFactory sqlSession) {
		this.sqlSession = sqlSession;
	}
	public UserMail getMail() {
		return mail;
	}
	public void setMail(UserMail mail) {
		this.mail = mail;
	}
	public UserDetail getDetail() {
		return detail;
	}
	public void setDetail(UserDetail detail) {
		this.detail = detail;
	}
}
