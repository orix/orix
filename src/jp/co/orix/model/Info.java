package jp.co.orix.model;

public class Info {

	private String address;
	private String location;
	private String contractNumber;
	private String contractName;
	
	
	private String postNumber;
	private String address1;
	private String address2;
	private String address3;
	private String address4;
	private String buildingName;
	private String roomNo;
	private String houseNo;
	private String destinationCorporateName;
	private String destinationDivisionName;
	private String destinationName1;
	private String destinationName2;
	private String destinationMail1;
	private String destinationMail2;
	private String id;

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public String getPostNumber() {
		return postNumber;
	}

	public void setPostNumber(String postNumber) {
		this.postNumber = postNumber;
	}

	public String getAddress1() {
		return address1;
	}

	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	public String getAddress2() {
		return address2;
	}

	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	public String getAddress3() {
		return address3;
	}

	public void setAddress3(String address3) {
		this.address3 = address3;
	}

	public String getAddress4() {
		return address4;
	}

	public void setAddress4(String address4) {
		this.address4 = address4;
	}

	public String getBuildingName() {
		return buildingName;
	}

	public void setBuildingName(String buildingName) {
		this.buildingName = buildingName;
	}

	public String getRoomNo() {
		return roomNo;
	}

	public void setRoomNo(String roomNo) {
		this.roomNo = roomNo;
	}

	public String getHouseNo() {
		return houseNo;
	}

	public void setHouseNo(String houseNo) {
		this.houseNo = houseNo;
	}

	public String getDestinationCorporateName() {
		return destinationCorporateName;
	}

	public void setDestinationCorporateName(String destinationCorporateName) {
		this.destinationCorporateName = destinationCorporateName;
	}

	public String getDestinationDivisionName() {
		return destinationDivisionName;
	}

	public void setDestinationDivisionName(String destinationDivisionName) {
		this.destinationDivisionName = destinationDivisionName;
	}

	public String getDestinationName1() {
		return destinationName1;
	}

	public void setDestinationName1(String destinationName1) {
		this.destinationName1 = destinationName1;
	}

	public String getDestinationName2() {
		return destinationName2;
	}

	public void setDestinationName2(String destinationName2) {
		this.destinationName2 = destinationName2;
	}

	public String getDestinationMail1() {
		return destinationMail1;
	}

	public void setDestinationMail1(String destinationMail1) {
		this.destinationMail1 = destinationMail1;
	}

	public String getDestinationMail2() {
		return destinationMail2;
	}

	public void setDestinationMail2(String destinationMail2) {
		this.destinationMail2 = destinationMail2;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	
}
