package jp.co.orix.model;

public class MonthList {

	private int M0100;		//식별번호
	private String M0101;	//문서분류
	private int M0401;		// 년월
	private String M0402;	//사용장소
	private int M0601;		//파일관리용ID
	private String M0620;	//파일명
	private String M0621;	//파일경로
	private String M0801;	//문서표시상태
	private String M0803;	//문서 표시
	
	public MonthList(){}
	
	public int getM0100() {		return M0100;	}
	public void setM0100(int m0100) {		M0100 = m0100;	}
	public String getM0101() {		return M0101;	}
	public void setM0101(String m0101) {		M0101 = m0101;	}
	public int getM0401() {		return M0401;	}
	public void setM0401(int m0401) {		M0401 = m0401;	}
	public String getM0402() {		return M0402;	}
	public void setM0402(String m0402) {		M0402 = m0402;	}
	public int getM0601() {		return M0601;	}
	public void setM0601(int m0601) {		M0601 = m0601;	}
	public String getM0620() {		return M0620;	}
	public void setM0620(String m0620) {		M0620 = m0620;	}
	public String getM0621() {		return M0621;	}
	public void setM0621(String m0621) {		M0621 = m0621;	}
	public String getM0801() {		return M0801;	}
	public void setM0801(String m0801) {		M0801 = m0801;	}
	public String getM0803() {		return M0803;	}
	public void setM0803(String m0803) {		M0803 = m0803;	}

	@Override
	public String toString() {
		return "Test [M0100=" + M0100 + ", M0101=" + M0101 + ", M0401=" + M0401
				+ ", M0402=" + M0402 + ", M0601=" + M0601 + ", M0620=" + M0620
				+ ", M0621=" + M0621 + ", M0801=" + M0801 + ", M0803=" + M0803
				+ "]";
	}
	
	
}
