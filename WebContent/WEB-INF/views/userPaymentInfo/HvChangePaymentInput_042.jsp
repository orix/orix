<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<link rel="stylesheet" type="text/css" href="./resource/css/common.css" />

<script type="text/javascript">
	 
	  function validate() {

		var financialInstitutionName = document.forms["paymentMethodChange"]["userPaymentInfo.CM004_financialInstitutionName"].value;
		var accountName = document.forms["paymentMethodChange"]["userPaymentInfo.CM004_accountNameAfter"].value;

		var postalNo = document.forms["paymentMethodChange"]["userPaymentInfo.HV011_zipCode"].value;
		var address = document.forms["paymentMethodChange"]["userPaymentInfo.HV011_address"].value;
		var companyName = document.forms["paymentMethodChange"]["userPaymentInfo.HV011_companyName"].value;
		var personInCharge = document.forms["paymentMethodChange"]["userPaymentInfo.HV011_personInChargeName"].value;
		var areaCode = document.forms["paymentMethodChange"]["userPaymentInfo.HV011_areaCode"].value;
		var downtownCode = document.forms["paymentMethodChange"]["userPaymentInfo.HV011_downtownCode"].value;
		var memberCode = document.forms["paymentMethodChange"]["userPaymentInfo.HV011_memberCode"].value;
		var mailAddress = document.forms["paymentMethodChange"]["userPaymentInfo.IDUMS000_mailAddress"].value;
		var entry = document.forms["paymentMethodChange"]["userPaymentInfo.HV011_entry"].value;


		if (financialInstitutionName == '') {
			alert("金融機関を入力してください。");
			return false;
		}
		if (accountName == '') {
			alert("口座名義人を入力してください。");
			return false;
		}
		if (postalNo == '') {
			alert("郵便番号を入力してください。");
			return false;
		}
		if (address == '') {
			alert("住所を入力してください。");
			return false;
		}
		if (companyName == '') {
			alert("会社名を入力してください。");
			return false;
		}
		if (personInCharge == '') {
			alert("ご担当者を入力してください。");
			return false;
		}
		
		if (areaCode == '') {
			alert("市外局番を入力してください。");
			return false;
		}
		
		if (downtownCode == '') {
			alert("市内局番を整1桁以上、4桁以内で入力してください。");
			return false;
		}
		
		if (memberCode == '') {
			alert("加入者番号を整数4桁で入力してください。");
			return false;
		}
		
		if (mailAddress == '') {
			alert("メールアドレスを入力してください。");
			return false;
		}
		
/* 		if (areaCode.length < 2 || areaCode.length > 5) {
			alert("市外局番を整数2桁以上、5桁以内で入力してください。");
			return false;
		}
		
		if (downtownCode.length < 1 || downtownCode.length > 4) {
			alert("市外局番を整数2桁以上、5桁以内で入力してください。");
			return false;
		} */
			
		 if(!IsValidateWideStringValue(accountName)) {
		 alert('口座名義人は全角文字を入力してください。')
		 return false;
		 }
		 else if(!IsValidateWideStringValue(address)) {
		 alert('住所は全角文字を入力してください。')
		 return false;
		 }
		 else if(!IsValidateWideStringValue(companyName)) {
		 alert('会社名は全角文字を入力してください。')
		 return false;
		 }
		 else if(!IsValidateWideStringValue(departmentName)) {
		 alert('部署名は全角文字を入力してください。')
		 return false;
		 }
		 else if(!IsValidateWideStringValue(personInCharge)) {
		 alert('ご担当者は全角文字を入力してください。')
		 return false;
		 }
		 else {
		 return false;
		 } 

		return true;

	}

	function IsValidateWideStringValue(str) {
	 for (var i = 0; i < str.length; ++i) {
	 var charOfStr = str.charCodeAt(i);	
	 if (charOfStr < 256 || (charOfStr >= 0xff61 && charOfStr <= 0xff9f)) return false;
	 }
	 return true;	
	 } 
</script>

<form action="checkOfContent" method="post" name="paymentMethodChange"
	onsubmit="return validate()">
<br>
	<div align="center">

		<input type="radio" name="changeScope" value="0"
			checked="checked">該当ご使用場所のみ反映する <input type="radio"
			name="changeScope" value="1">全ご使用場所に反映する

		<br>
		<br>
		<hr class="hr_top">

	</div>

	<br>

	<div align="center">
		※「*」の付いた項目は必ず入力してください。 <br> <br> 口座振替の申込用紙を送りします。
	</div>

	<br>
	<br>

	<div class="toChangePaymentMethodTable">
		<table>
			<tr>
				<th colspan="3">口座情報</th>
			</tr>
			<tr>
				<td id="title">*金融機関</td>
				<td><input type="text"
					name="userPaymentInfo.CM004_financialInstitutionName"
					class="inputTextField"></td>
				<td></td>
			</tr>
			<tr>
				<td id="title">*口座名義人</td>
				<td><input type="text"
					name="userPaymentInfo.CM004_accountNameAfter"
					class="inputTextField"></td>
				<td></td>
			</tr>
		</table>
	</div>

	<br>
	<br>

	<div class="toChangePaymentMethodTable">

		<table>
			<tr>
				<th colspan="3">申込担当者情報</th>
			</tr>

			<tr>
				<td id="title">*郵便番号</td>
				<td><input type="text" name="userPaymentInfo.HV011_zipCode" id="zip"
					class="inputTextField"></td>
				<td><input type="button" value="郵便番号検索" onclick="getSearchPostalNo()"
					class="button"></td>
			</tr>
			<tr>
				<td id="title">*住所</td>
				<td><input type="text" name="userPaymentInfo.HV011_address" id="address"
					class="inputTextField"></td>
				<td></td>
			</tr>
			<tr>
				<td id="title">*会社名</td>
				<td><input type="text" name="userPaymentInfo.HV011_companyName"
					class="inputTextField"></td>
				<td></td>
			</tr>
			<tr>
				<td id="title">部署名</td>
				<td><input type="text"
					name="userPaymentInfo.HV011_departmentName" class="inputTextField"></td>
				<td></td>
			</tr>
			<tr>
				<td id="title">*ご担当者</td>
				<td><input type="text"
					name="userPaymentInfo.HV011_personInChargeName"
					class="inputTextField"></td>
				<td></td>
			</tr>
			<tr>
				<td id="title">*電話番号</td>
				<td><input type="text" name="userPaymentInfo.HV011_areaCode"
					id="code"> &nbsp;-&nbsp; <input type="text"
					name="userPaymentInfo.HV011_downtownCode" id="code">
					&nbsp;-&nbsp; <input type="text"
					name="userPaymentInfo.HV011_memberCode" id="code"></td>
				<td></td>
			</tr>
			<tr>
				<td id="title">*メールアドレス</td>
				<td><input type="text"
					name="userPaymentInfo.IDUMS000_mailAddress" class="inputTextField"></td>
				<td></td>
			</tr>
		</table>

	</div>
	<br>
	<div align="center">
		<table>
			<tr>
				<td id="title">自由記入欄</td>
				<td><textarea rows="3" cols="80"
						name="userPaymentInfo.HV011_entry" class="inputTextarea"></textarea></td>
			</tr>
		</table>
	</div>

	<br>
	<br>

	<div align="center">
		<input type="submit" value="内容確認" class="button">
	</div>

	<br>

</form>

<br>

<div align="center">
	お申し込みされた内容が 「契約情報照会画面」 に反映される前は、<br>お申し込み前の情報が表示されますのでご了承ください。
</div>
<br>

