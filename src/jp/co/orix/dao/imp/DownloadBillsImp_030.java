package jp.co.orix.dao.imp;

import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.DownloadBillsDao_030;
import jp.co.orix.model.GroupListModel_030;
import jp.co.orix.model.SearchModel_030;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class DownloadBillsImp_030 implements DownloadBillsDao_030{

	@Autowired
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();

	private ArrayList<GroupListModel_030> groupListModel_030;
	private ArrayList<SearchModel_030> searchModel_030;
	private ArrayList<String> arrayList;
	private int rowCount;
	private ArrayList<GroupListModel_030> list;
	
	
	@Override
	public ArrayList<GroupListModel_030> getPageInfo(Map<String, Object> condition) {
		SqlSession session = sqlSession.openSession();
		DownloadBillsDao_030 dao = session.getMapper(DownloadBillsDao_030.class);
		try {
			list = dao.getPageInfo(condition);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		
		return list;
	}
	
	
	@Override
	public int getRowCount(Map condition) {
		
		SqlSession session = sqlSession.openSession();
		DownloadBillsDao_030 dao = session.getMapper(DownloadBillsDao_030.class);
		
		try {
			rowCount = dao.getRowCount(condition);
			
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		
		return rowCount;
	}
	
	@Override
	public ArrayList<String> getNengetsuComboboxList() {

		SqlSession session = sqlSession.openSession();
		DownloadBillsDao_030 dao = session.getMapper(DownloadBillsDao_030.class);
		
		try {
			arrayList = dao.getNengetsuComboboxList();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		System.out.println(arrayList);
		return arrayList;		
	}
	
	@Override
	public ArrayList<GroupListModel_030> getSearchList(String groupId,
			String placeId, Date selectedDate) {
		
		SqlSession session = sqlSession.openSession();
		DownloadBillsDao_030 dao = session.getMapper(DownloadBillsDao_030.class);
		
		try {
			groupListModel_030 = dao.getSearchList(groupId, placeId, selectedDate);
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		return groupListModel_030;
	}
	
	@Override
	public ArrayList<GroupListModel_030> getGroupComboboxList(){
		
		SqlSession session = sqlSession.openSession();
		DownloadBillsDao_030 dao = session.getMapper(DownloadBillsDao_030.class);
		
		try {
			groupListModel_030 = dao.getGroupComboboxList();
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally{
			session.close();
		}
		
		return groupListModel_030;
	}

	

	public SqlSessionFactory getSqlSession() {
		return sqlSession;
	}

	public void setSqlSession(SqlSessionFactory sqlSession) {
		this.sqlSession = sqlSession;
	}

	public ArrayList<GroupListModel_030> getGropuListModel_030() {
		return groupListModel_030;
	}

	public void setGropuListModel_030(
			ArrayList<GroupListModel_030> gropuListModel_030) {
		this.groupListModel_030 = gropuListModel_030;
	}

	public ArrayList<SearchModel_030> getSearchModel_030() {
		return searchModel_030;
	}

	public void setSearchModel_030(ArrayList<SearchModel_030> searchModel_030) {
		this.searchModel_030 = searchModel_030;
	}

	public ArrayList<GroupListModel_030> getGroupListModel_030() {
		return groupListModel_030;
	}

	public void setGroupListModel_030(
			ArrayList<GroupListModel_030> groupListModel_030) {
		this.groupListModel_030 = groupListModel_030;
	}

	public ArrayList<String> getArrayList() {
		return arrayList;
	}

	public void setArrayList(ArrayList<String> arrayList) {
		this.arrayList = arrayList;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	

	

	
}
