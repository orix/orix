package jp.co.orix.dao;

import java.util.ArrayList;
import java.util.Map;

import jp.co.orix.model.KubunCombobox;
import jp.co.orix.model.MailMessageData;
import jp.co.orix.model.ModifyVo;
import jp.co.orix.model.UsePlace;
import jp.co.orix.model.UsePlaceCombobox;

public interface ModifyDao_040 {
	public ArrayList<UsePlaceCombobox> GetUsePlaceComboboxList() throws Exception;
	public UsePlace GetUsePlaceUsingH0804(String H0804) throws Exception;
	public ArrayList<KubunCombobox> GetKubunComboboxList(Map<String,Integer> condition) throws Exception;
	public void ModifyPlace(ModifyVo modifyvo) throws Exception;
	//유저 타입에 따라서 종류 값은 스트링 3글자.
	public MailMessageData GetMailMessageData(String Type) throws Exception;
}
