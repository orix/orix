package jp.co.orix.action.bills;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.orix.dao.DownloadBillsDao_030;
import jp.co.orix.model.GroupListModel_030;
import jp.co.orix.model.Paging_030;
import jp.co.orix.model.SearchModel_030;
import jp.co.orix.util.CommApi_32;
import jp.co.orix.util.Util;

import org.apache.struts2.interceptor.ServletRequestAware;

import com.opensymphony.xwork2.ActionSupport;

public class DownloadBills_030 extends ActionSupport implements ServletRequestAware{

	private DownloadBillsDao_030 downloadBillsDao_030;
	public List<GroupListModel_030> groupList = new ArrayList<GroupListModel_030>();
	public List<GroupListModel_030> groupList1 = new ArrayList<GroupListModel_030>();
	public List<String> dateList;
	public SearchModel_030 searchModel_030 = new SearchModel_030();
	public String tempDate;
	public List<GroupListModel_030> searchedList = new ArrayList<GroupListModel_030>();
	public HttpServletRequest request;
	private String parameter;
	private int rowCount;
	ListForPaging_030 listForPaging_030 = new ListForPaging_030();
	private int totalPage;
	private int currentPage;
	Paging_030 paging_030 = new Paging_030();
	
	@Override
	public void setServletRequest(HttpServletRequest request) {
		this.request = request;
	}
	
	@Override
	public String execute() throws Exception {
		
		SimpleDateFormat transFormat = new SimpleDateFormat("yyyy/MM");
		
		CommApi_32 api = new CommApi_32();
		api.setFormParamator(request, "bill");
		System.out.println(api.getParameterMap());
		HashMap<String, Object> param = api.getFormParamator("bill");
		System.out.println("par : " + param);
		
		Calendar now = Calendar.getInstance();
		now.setTime(new Date());
		dateList = Util.yearList(now, 23);
		
		String useDate = dateList.get(0);
		String group = "";
		String place = "";
		if (param != null) {
			
			parameter = (String)param.get("param");
			
			if (param.get("useDate")!= null) 
				useDate = (String)param.get("useDate");
			
				group = (String)param.get("group");
				place = (String)param.get("place");
			
				searchedList = downloadBillsDao_030.getSearchList(
						group, place, transFormat.parse(useDate));

//				groupList1 = searchedList;
				
				// 전체 페이지 수
				Map<String, Object> condition1 = new HashMap<String, Object>();
				
				condition1.put("groupId", group);
				condition1.put("placeId", place);
				condition1.put("selectedDate", transFormat.parse(useDate));
				
				rowCount = downloadBillsDao_030.getRowCount(condition1);
				totalPage = (rowCount % 12 != 0)?(rowCount / 12) + 1 : rowCount / 12;
				
				// 페이징
				
				if(currentPage == 0) {
					currentPage = 1;
				}
				paging_030 = listForPaging_030.getPageInfo(currentPage);
				
				System.out.println(currentPage);
				
				Map<String, Object> condition = new HashMap<String, Object>();
				
				condition.put("startRow", paging_030.getStartRow());
				condition.put("endRow", paging_030.getEndRow());
				condition.put("groupId", group);
				condition.put("placeId", place);
				condition.put("selectedDate", transFormat.parse(useDate));
				
				searchedList = downloadBillsDao_030.getPageInfo(condition);
				
				System.out.println(searchedList);
		}
		
		
		
		return SUCCESS;
	}

	public DownloadBillsDao_030 getDownloadBillsDao_030() {
		return downloadBillsDao_030;
	}

	public void setDownloadBillsDao_030(DownloadBillsDao_030 downloadBillsDao_030) {
		this.downloadBillsDao_030 = downloadBillsDao_030;
	}

	public List<GroupListModel_030> getGroupList() {
		return groupList;
	}

	public void setGroupList(List<GroupListModel_030> groupList) {
		this.groupList = groupList;
	}

	public List<String> getDateList() {
		return dateList;
	}

	public void setDateList(List<String> dateList) {
		this.dateList = dateList;
	}

	public SearchModel_030 getSearchModel_030() {
		return searchModel_030;
	}

	public void setSearchModel_030(SearchModel_030 searchModel_030) {
		this.searchModel_030 = searchModel_030;
	}

	public String getTempDate() {
		return tempDate;
	}

	public void setTempDate(String tempDate) {
		this.tempDate = tempDate;
	}

	public List<GroupListModel_030> getSearchedList() {
		return searchedList;
	}

	public void setSearchedList(List<GroupListModel_030> searchedList) {
		this.searchedList = searchedList;
	}

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public List<GroupListModel_030> getGroupList1() {
		return groupList1;
	}

	public void setGroupList1(List<GroupListModel_030> groupList1) {
		this.groupList1 = groupList1;
	}

	public void setRowCount(int rowCount) {
		this.rowCount = rowCount;
	}

	public int getRowCount() {
		return rowCount;
	}

	public ListForPaging_030 getListForPaging() {
		return listForPaging_030;
	}

	public void setListForPaging(ListForPaging_030 listForPaging) {
		this.listForPaging_030 = listForPaging;
	}

	public ListForPaging_030 getListForPaging_030() {
		return listForPaging_030;
	}

	public void setListForPaging_030(ListForPaging_030 listForPaging_030) {
		this.listForPaging_030 = listForPaging_030;
	}

	public int getTotalPage() {
		return totalPage;
	}

	public void setTotalPage(int totalPage) {
		this.totalPage = totalPage;
	}

	public int getCurrentPage() {
		return currentPage;
	}

	public void setCurrentPage(int currentPage) {
		this.currentPage = currentPage;
	}
	
	/*C:\Users\bds\workspace\.metadata\.plugins\org.eclipse.wst.server.core\tmp0\wtpwebapps\Orix_koautu\resource\pdf*/
}
