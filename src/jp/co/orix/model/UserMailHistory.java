package jp.co.orix.model;

import java.sql.Date;

public class UserMailHistory {
	//�r�d�p
	private int D0100;
	//���[�����M����
	private Date D0200;
	//���o�l
	private String D0300;
	//���惆�[�U�[���ʔԍ�
	private int D0401;
	//���惁�[���A�h���X
	private String D0402;
	//�b�b
	private String D0500;
	//�a�b�b
	private String D0600;
	//����
	private String D0700;
	//�{��
	private String D0800;
	//�����_��ԍ�
	private String D0901;
	//�����\�_��l
	private String D0902;
	//�����\�_��l
	private String D0903;
	//�������l
	private int D0904;
	//�O���h�^�e����
	private Date D9971;
	//�O���h�^�e�Z�b�V�����h�c
	private String D9972;

	public UserMailHistory() {
		super();
		// TODO Auto-generated constructor stub
	}

	public UserMailHistory(int d0100, Date d0200, String d0300, int d0401,
			String d0402, String d0500, String d0600, String d0700,
			String d0800, String d0901, String d0902, String d0903, int d0904,
			Date d9971, String d9972) {
		super();
		D0100 = d0100;
		D0200 = d0200;
		D0300 = d0300;
		D0401 = d0401;
		D0402 = d0402;
		D0500 = d0500;
		D0600 = d0600;
		D0700 = d0700;
		D0800 = d0800;
		D0901 = d0901;
		D0902 = d0902;
		D0903 = d0903;
		D0904 = d0904;
		D9971 = d9971;
		D9972 = d9972;
	}

	public int getD0100() {
		return D0100;
	}

	public void setD0100(int d0100) {
		D0100 = d0100;
	}

	public Date getD0200() {
		return D0200;
	}

	public void setD0200(Date d0200) {
		D0200 = d0200;
	}

	public String getD0300() {
		return D0300;
	}

	public void setD0300(String d0300) {
		D0300 = d0300;
	}

	public int getD0401() {
		return D0401;
	}

	public void setD0401(int d0401) {
		D0401 = d0401;
	}

	public String getD0402() {
		return D0402;
	}

	public void setD0402(String d0402) {
		D0402 = d0402;
	}

	public String getD0500() {
		return D0500;
	}

	public void setD0500(String d0500) {
		D0500 = d0500;
	}

	public String getD0600() {
		return D0600;
	}

	public void setD0600(String d0600) {
		D0600 = d0600;
	}

	public String getD0700() {
		return D0700;
	}

	public void setD0700(String d0700) {
		D0700 = d0700;
	}

	public String getD0800() {
		return D0800;
	}

	public void setD0800(String d0800) {
		D0800 = d0800;
	}

	public String getD0901() {
		return D0901;
	}

	public void setD0901(String d0901) {
		D0901 = d0901;
	}

	public String getD0902() {
		return D0902;
	}

	public void setD0902(String d0902) {
		D0902 = d0902;
	}

	public String getD0903() {
		return D0903;
	}

	public void setD0903(String d0903) {
		D0903 = d0903;
	}

	public int getD0904() {
		return D0904;
	}

	public void setD0904(int d0904) {
		D0904 = d0904;
	}

	public Date getD9971() {
		return D9971;
	}

	public void setD9971(Date d9971) {
		D9971 = d9971;
	}

	public String getD9972() {
		return D9972;
	}

	public void setD9972(String d9972) {
		D9972 = d9972;
	}

}
