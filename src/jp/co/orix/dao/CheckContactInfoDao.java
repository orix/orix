package jp.co.orix.dao;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import jp.co.orix.model.ContactInfoHV003;

public interface CheckContactInfoDao {

	public List<ContactInfoHV003> getDestinationOfBillBy(Map<String, String> condition);

	public List<ContactInfoHV003> getPowerManagementEntriesBy(
			Map<String, String> condition);

	public List<ContactInfoHV003> getInfoOfEnquiriesBy(
			Map<String, String> condition);

	public List<ContactInfoHV003> getInfoOfCooperationBy(
			Map<String, String> condition);

}
