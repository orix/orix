package jp.co.orix.action.user;

import jp.co.orix.dao.CommonDao_131;
import jp.co.orix.dao.UserDao;
import jp.co.orix.model.UniversalInfoITHNT000;
import jp.co.orix.model.User;
import jp.co.orix.model.UserDetail;
import jp.co.orix.model.UserMail;

import com.opensymphony.xwork2.ActionSupport;

public class CommApi_131 extends ActionSupport {

	private User user;
	private UserMail mail;
	private UserDetail detail;
	private UserDao userDao;
	private CommonDao_131 commApi;
	private UniversalInfoITHNT000 portal;
	
	private String M0201;
	
	private int M0100;
	private String T0100;
	private String T0200;
	private String kubun;
	
	
	public String getUrlAccessLimitDays() throws Exception {
		int ualdUpdate;
		M0100 = 10001;
		
		ualdUpdate = userDao.getUrlAccessLimitDays(M0100);
		System.out.println("ualdUpdate = "+ualdUpdate);
		return SUCCESS;
	}
	
	public String getSystemKubun() throws Exception {
		kubun = "高圧";
		return SUCCESS;
	}
	
//	public String getLoginInfo() throws Exception {
//		sessionAction으로 대체.
//		
//		return SUCCESS;
//	}
	
	public String getSystemPortalClass() throws Exception {
		T0100 = "10010";
		T0200 = "1";
		if(T0200.equals("1")) {
			portal = (UniversalInfoITHNT000) commApi.getSystemPortalClass(T0100, T0200);
		} else { 
			return ERROR;
		}
		
		return SUCCESS;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserMail getMail() {
		return mail;
	}

	public void setMail(UserMail mail) {
		this.mail = mail;
	}

	public UserDetail getDetail() {
		return detail;
	}

	public void setDetail(UserDetail detail) {
		this.detail = detail;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public int getM0100() {
		return M0100;
	}

	public void setM0100(int m0100) {
		M0100 = m0100;
	}

	public CommonDao_131 getCommApi() {
		return commApi;
	}

	public void setCommApi(CommonDao_131 commApi) {
		this.commApi = commApi;
	}

	public UniversalInfoITHNT000 getPortal() {
		return portal;
	}

	public void setPortal(UniversalInfoITHNT000 portal) {
		this.portal = portal;
	}

	public String getM0201() {
		return M0201;
	}

	public void setM0201(String m0201) {
		M0201 = m0201;
	}

	public String getT0100() {
		return T0100;
	}

	public void setT0100(String t0100) {
		T0100 = t0100;
	}

	public String getT0200() {
		return T0200;
	}

	public void setT0200(String t0200) {
		T0200 = t0200;
	}

	public String getKubun() {
		return kubun;
	}

	public void setKubun(String kubun) {
		this.kubun = kubun;
	}
	
	
}
