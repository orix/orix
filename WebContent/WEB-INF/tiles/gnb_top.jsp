<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<script>
	$(document)
			.ready(
					function() {
						//탭(ul) onoff
						$('.jq_tabonoff>.jq_cont').children().css('display',
								'none');
						$('.jq_tabonoff>.jq_cont div:first-child').css(
								'display', 'block');
						$('.jq_tabonoff')
								.delegate(
										'.jq_tab>li',
										'click',
										function() {
											var index = $(this).parent()
													.children().index(this);
											$(this).siblings().removeClass();
											$(this).addClass('on');
											$(this).parent().next('.jq_cont')
													.children().hide()
													.eq(index).show();
										});
					});
</script>
<!-- 메뉴를 위한  CSS, JQuery 임포트 (head 태그 내 위치) 끝 -->


<!-- 선택된 메뉴 -->
<script>
	$(document).ready(
			function() {
				var url = window.location.href.substr(window.location.href
						.lastIndexOf("/") + 1);
				// 자식 active 하이라이트
				$('[href$="' + url + '"]').parent().addClass("on");
				// 부모 active 하이라이트
				$('[vvv$="' + url + '"]').parent().addClass("on");
				var index2 = $('[vvv$="' + url + '"]').parent().index();
				$('.jq_tabonoff>.jq_cont').children().each(function(i) {
					$(this).attr('tabindex', i);
					if (i == index2) {
						$(this).show();
					}
				});
			});
</script>
<div id="top">
	<!-- 메뉴 시작 -->
	<div class="header">
		<div>
			<img
				src="<%=request.getContextPath()%>/resource/images/logo/header_logo.gif">
			<img
				src="<%=request.getContextPath()%>/resource/images/logo/header_catchcopy.png">
		</div>

		<div class="menu_title">
			<!-- 		타이틀을 입력합니다. -->
			月次電力量（計量日誌）ダウンロード
		</div>
		<div class="jq_tabonoff comm_tab1">
			<ul class="jq_tab tab_menu" id="menu">
				<li><a href="#" class="tit">お知らせ</a></li>
				<li><a href="javascript:;" class="tit">契約内容照会</a></li>
				<li><a href="javascript:;" class="tit">電気料金照会</a></li>
				<li><a href="javascript:;" class="tit"
					vvv="<%=request.getContextPath()%>/viewForm">ご使用実績照会</a></li>
				<li><a href="javascript:;" class="tit">変更申込</a></li>
				<li><a href="javascript:;" class="tit">Q&A</a></li>
				<li><a href="javascript:;" class="tit">お問い合わせ</a></li>
				<li><a href="javascript:;" class="tit">ID管理</a></li>
			</ul>
			<div class="jq_cont tab_cont">
				<div class="cont"></div>
				<div class="cont">
					<div class="jq_tabonoff comm_tab2">
						<ul class="jq_tab tab_menu">
							<li><a
								href="<%=request.getContextPath()%>/checkContractInfo/checkContractInfo?${parameter }"
								class="tit">契約情報照会</a></li>
							<li><a
								href="<%=request.getContextPath()%>/checkContactInfo/checkContactInfo?${parameter}"
								class="tit">連絡先照会</a></li>
						</ul>
						<div class="jq_cont tab_cont">
							<!-- 						<div class="cont">契約内容照会 > 契約情報照会</div> -->
							<!-- 						<div class="cont">契約内容照会 > 連絡先照会</div> -->
						</div>
					</div>
				</div>
				<div class="cont">
					<div class="jq_tabonoff comm_tab2">
						<ul class="jq_tab tab_menu">
							<li><a href="javascript:;" class="tit">月別リスト</a></li>
							<li><a href="javascript:;" class="tit">年別リスト</a></li>
							<li><a href="/electricRates/bill?type=1" class="tit">請求書</a></li>
							<li><a href="/electricRates/billDownload?${parameter}" class="tit">請求書ダウンロード</a></li>
						</ul>
						<!-- 					<div class="jq_cont tab_cont"> -->
						<!-- 						<div class="cont"></div> -->
						<!-- 						<div class="cont"></div> -->
						<!-- 						<div class="cont"></div> -->
						<!-- 					</div> -->
					</div>
				</div>
				<div class="cont">
					<div class="jq_tabonoff comm_tab2">
						<ul class="jq_tab tab_menu">
							<li><a href="<%=request.getContextPath()%>/hvUsedAmount/hvUsedAmountHalfHour.action?${parameter }" class="tit">30分ごと電力使用量</a></li>
							<li><a href="<%=request.getContextPath()%>/hvUsedAmount/hvUsedAmountDay.action?${parameter }" class="tit">日ごと電力使用量</a></li>
							<li><a href="javascript:;" class="tit">月ごと電力使用量</a></li>
							<li><a href="<%=request.getContextPath()%>/test" class="tit">最大電力検索</a></li>
							<li><a href="<%=request.getContextPath()%>/excel/excelViewForm?${parameter}"
								class="tit">月次電力量（計量日誌）ダウンロード</a></li>
						</ul>

					</div>
				</div>
				<div class="cont">
					<div class="jq_tabonoff comm_tab2">
						<ul class="jq_tab tab_menu">
							<li><a href="javascript:location.href='view.action';"
								class="tit">連絡先</a></li>
							<li><a href="javascript:;" class="tit">工事関連</a></li>
							<li><a href="javascript:;" class="tit">連絡船</a></li>
							<li><a href="<%=request.getContextPath()%>/modify/View"
								class="tit">工事関連</a></li>
							<li><a href="<%=request.getContextPath()%>/userPayment/paymentInfo" class="tit">お支払い方法</a></li>
							<li><a href="<%=request.getContextPath()%>/user/admin" class="tit">登録情報変更（管理者）</a></li>
							<li><a href="<%=request.getContextPath()%>/user/User" class="tit">登録情報変更（ユーザー）</a></li>
							<li><a href="javascript:;" class="tit">パスワード変更</a></li>
						</ul>
						<!-- 					<div class="jq_cont tab_cont"> -->
						<!-- 						<div class="cont"></div> -->
						<!-- 						<div class="cont"></div> -->
						<!-- 						<div class="cont"></div> -->
						<!-- 					</div> -->
					</div>
				</div>
			</div>
		</div>
	</div>
</div>