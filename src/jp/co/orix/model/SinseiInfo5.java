package jp.co.orix.model;

public class SinseiInfo5 {

	private String tantoInfo_YubinBango;
	private String tantoInfo_Jusho;
	private String tantoInfo_HojinName;
	private String tantoInfo_BushoName;
	private String tantoInfo_Tanto1;
	private String tantoInfo_Tel1;
	private String tantoInfo_Tel2;
	private String tantoInfo_Tel3;
	private String tantoInfo_MailAddress1;
	private String tantoInfo_Memo;

	public String getTantoInfo_YubinBango() {
		return tantoInfo_YubinBango;
	}

	public void setTantoInfo_YubinBango(String tantoInfo_YubinBango) {
		this.tantoInfo_YubinBango = tantoInfo_YubinBango;
	}

	public String getTantoInfo_Jusho() {
		return tantoInfo_Jusho;
	}

	public void setTantoInfo_Jusho(String tantoInfo_Jusho) {
		this.tantoInfo_Jusho = tantoInfo_Jusho;
	}

	public String getTantoInfo_HojinName() {
		return tantoInfo_HojinName;
	}

	public void setTantoInfo_HojinName(String tantoInfo_HojinName) {
		this.tantoInfo_HojinName = tantoInfo_HojinName;
	}

	public String getTantoInfo_BushoName() {
		return tantoInfo_BushoName;
	}

	public void setTantoInfo_BushoName(String tantoInfo_BushoName) {
		this.tantoInfo_BushoName = tantoInfo_BushoName;
	}

	public String getTantoInfo_Tanto1() {
		return tantoInfo_Tanto1;
	}

	public void setTantoInfo_Tanto1(String tantoInfo_Tanto1) {
		this.tantoInfo_Tanto1 = tantoInfo_Tanto1;
	}

	public String getTantoInfo_Tel1() {
		return tantoInfo_Tel1;
	}

	public void setTantoInfo_Tel1(String tantoInfo_Tel1) {
		this.tantoInfo_Tel1 = tantoInfo_Tel1;
	}

	public String getTantoInfo_Tel2() {
		return tantoInfo_Tel2;
	}

	public void setTantoInfo_Tel2(String tantoInfo_Tel2) {
		this.tantoInfo_Tel2 = tantoInfo_Tel2;
	}

	public String getTantoInfo_Tel3() {
		return tantoInfo_Tel3;
	}

	public void setTantoInfo_Tel3(String tantoInfo_Tel3) {
		this.tantoInfo_Tel3 = tantoInfo_Tel3;
	}

	public String getTantoInfo_MailAddress1() {
		return tantoInfo_MailAddress1;
	}

	public void setTantoInfo_MailAddress1(String tantoInfo_MailAddress1) {
		this.tantoInfo_MailAddress1 = tantoInfo_MailAddress1;
	}

	public String getTantoInfo_Memo() {
		return tantoInfo_Memo;
	}

	public void setTantoInfo_Memo(String tantoInfo_Memo) {
		this.tantoInfo_Memo = tantoInfo_Memo;
	}

}
