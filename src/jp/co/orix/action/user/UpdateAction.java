package jp.co.orix.action.user;

import java.sql.Date;
import java.util.HashMap;

import jp.co.orix.dao.UserDao;
import jp.co.orix.model.SendMailVo;
import jp.co.orix.model.User;
import jp.co.orix.model.UserDetail;
import jp.co.orix.model.UserMail;
import jp.co.orix.util.SendMail;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class UpdateAction extends ActionSupport implements ModelDriven, Preparable {

	private User user;
	private UserMail mail;
	private UserDetail detail;
	private UserDao userDao;
	private CommApi_131 commApi;
	
	
	private SendMailVo sendMailVo = new SendMailVo();
	private SendMail sendMail = new SendMail();
	
	private String newM0201;
	private String newM0200;
	private String M0201;
	private int M0100;
	private Date M0400;
	private String M0301;
	
	
	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		mail = new UserMail();
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return mail;
	}

	public String updateAdmin() throws Exception {
			int nameUpdate;
			int mailUpdate;
			int ualdUpdate;
			M0100 = 10001;
			M0201 = "admin";
			System.out.println("newM0201 : "+newM0201);
			
			user = userDao.select(M0201);
			M0301 = user.getM0301();
			System.out.println("M0301 = " +M0301);
			
			if(newM0200.equals(user.getMail().getM0200())){
				System.out.println("newM0200 : " + newM0200);
				System.out.println("User.getMail : " +user.getMail().getM0200());
				nameUpdate = userDao.nameUpdate(M0100, newM0201);
				user = userDao.limitDaysCheck(M0100);
				System.out.println("AdminUpdateUser : "+nameUpdate);
			} else {
				nameUpdate = userDao.nameUpdate(M0100, newM0201);
				System.out.println("newM0200 : "+newM0200);
				System.out.println("userNumber : " + user.getM0100());
//				2017.02.14 메일발송 작업부분. 내일 조져라.
				sendMailVo.setRecipient(newM0200);
				sendMailVo.setSubject("メール変更申込です。");
				sendMailVo.setBody("http://localhost:8080/Orix_koautu/user/changeMail?newM0200="+newM0200+"&M0100="+user.getM0100()+"&M0301="+M0301);
				
				
				System.out.println("Mail Test = "+sendMailVo);
				
				System.out.println("Mail Test 전송 됨? ");
				ualdUpdate = userDao.getUrlAccessLimitDays(M0100);
				sendMail.sendMail(sendMailVo);
				
				user = userDao.limitDaysCheck(M0100);
				System.out.println("UserCheckTime = "+user);
				System.out.println("UserCheckTimeMail = "+user.getMail());
			}
			return SUCCESS;
	}
	
	public String updateUser() throws Exception {
		int detailUpdate;
		int mailUpdate;
		int ualdUpdate;
		M0100 = 10000;
		M0201 = "test";
		user = userDao.userSelect(M0201);
		M0301 = user.getM0301();
		System.out.println("M0301 = " +M0301);
		
		if(newM0200.equals(user.getMail().getM0200())){
			
			System.out.println("User.getMail : " +user.getMail().getM0200());
			detailUpdate = userDao.detailUpdate(M0100, detail);
			System.out.println("UserdetailUpdate : " + detailUpdate);
			user = userDao.userlimitDaysCheck(M0100);
			System.out.println("UserCheckTime = "+user);
			
		} else {
			
			detailUpdate = userDao.detailUpdate(M0100, detail);
			System.out.println("UserdetailUpdate : " + detailUpdate);
			
			sendMailVo.setRecipient(newM0200);
			sendMailVo.setSubject("メール変更申込です。");
			sendMailVo.setBody("http://localhost:8080/Orix_koautu/user/changeMail?newM0200="+newM0200+"&M0100="+user.getM0100()+"&M0301="+M0301);
			
			
			System.out.println("Mail Test = "+sendMailVo);
			
			System.out.println("Mail Test 전송 됨? ");
			ualdUpdate = userDao.getUrlAccessLimitDays(M0100);
			sendMail.sendMail(sendMailVo);
			
			user = userDao.userlimitDaysCheck(M0100);
			System.out.println("UserCheckTime = "+user);
			System.out.println("UserCheckTimeMail = "+user.getMail());
		}
		return SUCCESS;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserMail getMail() {
		return mail;
	}

	public void setMail(UserMail mail) {
		this.mail = mail;
	}

	public UserDetail getDetail() {
		return detail;
	}

	public void setDetail(UserDetail detail) {
		this.detail = detail;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public String getNewM0201() {
		return newM0201;
	}

	public void setNewM0201(String newM0201) {
		this.newM0201 = newM0201;
	}

	public int getM0100() {
		return M0100;
	}

	public void setM0100(int m0100) {
		M0100 = m0100;
	}

	public String getNewM0200() {
		return newM0200;
	}

	public void setNewM0200(String newM0200) {
		this.newM0200 = newM0200;
	}

	public Date getM0400() {
		return M0400;
	}

	public void setM0400(Date m0400) {
		M0400 = m0400;
	}

	public String getM0201() {
		return M0201;
	}

	public void setM0201(String m0201) {
		M0201 = m0201;
	}

	public SendMailVo getSendMailVo() {
		return sendMailVo;
	}

	public void setSendMailVo(SendMailVo sendMailVo) {
		this.sendMailVo = sendMailVo;
	}

	public SendMail getSendMail() {
		return sendMail;
	}

	public void setSendMail(SendMail sendMail) {
		this.sendMail = sendMail;
	}

	public String getM0301() {
		return M0301;
	}

	public void setM0301(String m0301) {
		M0301 = m0301;
	}
	
}
