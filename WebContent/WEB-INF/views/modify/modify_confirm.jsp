<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<div class="modify_confirm">
	<div class="input">
		<form action="Insert">
			<table>
				<tr>
					<th colspan="3" align="left">工事関連</th>
				</tr>
				<tr>
					<th>設備の新設⋅更新①</th>
					<td><input type="text" readonly id="read"
						value="<s:property value="modifyvo.Equipment1"/>"></td>
					<td><input type="text" readonly id="read"
						value="<s:property value="modifyvo.EquipmentKubun1"/>"></td>
				</tr>
				<tr>
					<th>設備の新設⋅更新②</th>
					<td><input type="text" readonly id="read"
						value="<s:property value="modifyvo.Equipment2"/>"></td>
					<td><input type="text" readonly id="read"
						value="<s:property value="modifyvo.EquipmentKubun2"/>"></td>
				</tr>
				<tr>
					<th>受電容量の変更</th>
					<td class="td_left"><input type="text" readonly id="read"
						value="<s:property value="modifyvo.ChangeKubun"/>"></td>
					<td align="left"></td>
				</tr>
			</table>
			<br />
			<table>
				<tr>
					<td colspan="3" align="left">その他を選択した場合、補足事項がある場合はこちらにご記入ください。</td>
				</tr>
				<tr>
					<th>補足事項</th>
					<td colspan="2"><textarea id="myDIV" rows="4" cols="83"><s:property
								value="modifyvo.Supplement" />
							</textarea></td>
				</tr>
			</table>
			<br />
			<table>
				<tr>
					<th id="th_left" colspan="2">工事事業者</th>
				</tr>
				<tr>
					<th>事業者名</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.WorkCompanyName" />"></td>
				</tr>
				<tr>
					<th>担当</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.WorkTantoName" />"></td>
				</tr>
				<tr>
					<th>連絡先</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.WorkTel1" />-<s:property value="modifyvo.WorkTel2" />-<s:property value="modifyvo.WorkTel3" />"></td>
				</tr>
			</table>
			<br />
			<table>
				<tr>
					<th id="th_left" colspan="2">主任技術者</th>
				</tr>
				<tr>
					<th>社名</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.MainEngineerCompanyName" />"></td>
				</tr>
				<tr>
					<th>氏名</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.MainEngineerName"/>"></td>
				</tr>
				<tr>
					<td>&nbsp;</td>
				<tr />
				<tr>
					<th>初回連絡先</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.MainEngineerTel1"  />-<s:property value="modifyvo.MainEngineerTel2" />-<s:property value="modifyvo.MainEngineerTel3" />"></td>
				</tr>
			</table>
			<br />
			<table>
				<tr>
					<th id="th_left" colspan="2">申込担当者情報</th>
				</tr>
				<tr>
					<th>郵便番号</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.YubinBango" />"></td>
				</tr>
				<tr>
					<th>住所</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.Jusho"/>"></td>
				</tr>
				<tr>
					<th>会社名</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.HojinName" />"></td>
				</tr>
				<tr>
					<th>部署名</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.BushoName" />"></td>
				</tr>
				<tr>
					<th>ご担当者</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.TantoName" />"></td>
				</tr>
				<tr>
					<th>電話番号</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.Tel1" />-<s:property value="modifyvo.Tel2"/>-<s:property value="modifyvo.Tel3"/>"></td>
				</tr>
				<tr>
					<th>メールアドレス</th>
					<td><input type="text" id="read" readonly
						value="<s:property value="modifyvo.MailAddress"/>"></td>
				</tr>
			</table>
			<br />
			<table>
				<tr>
					<td colspan="3" align="left">その他を選択した場合、補足事項がある場合はこちらにご記入ください。</td>
				</tr>
				<tr>
					<th>自由記入欄</th>
					<td colspan="2"><textarea id="myDIV" rows="4" cols="83"><s:property
								value="modifyvo.Memo" />
							</textarea></td>
				</tr>
			</table>
			<br /> <input type="button" value="戻る" onclick="history.go(-1)">&nbsp;<input
				type="submit" value="申込" style="width:100px"> <br />
			<p>お申し込みされた内容が⎾契約情報照会画面⏌に反映されるめでは、お申し込み前に情報が表示されますのでご了承ください。</p>
		</form>
	</div>

</div>