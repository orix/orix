package jp.co.orix.action.bill;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

import jp.co.orix.model.Bill;
import jp.co.orix.model.BillMonth;
import jp.co.orix.util.Util;

public class BillHtmlForm {
	
	static public String htmlForm(Bill bill, Integer type, String date, ArrayList<BillMonth> list) throws NumberFormatException, ParseException{
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM");
		SimpleDateFormat format3 = new SimpleDateFormat("yyyy年M月dd日");
		SimpleDateFormat format4 = new SimpleDateFormat("MM");
		
		boolean isSummer = true;
		
		String japanYear = Util.japanYearMonthCircle(format.parse(date));
		String japanYearMonth = Util.japanYearMonthDayCircle(format.parse(date));

		Integer setMonth = Integer.parseInt(format4.format(format.parse(date)));
		if( setMonth >= 5 && setMonth <= 8) isSummer = true;
		
		String body = "";
		//타이틀
		body+="<table border='0' cellpadding='0' cellspacing='0' align='center'  class='main-title'>";
		body+= "<tr style='height: 40px; font-size: 20px'>";
		body+="<td>ご 請 求 内 容";
		body+="<div style='width:100%; height:1px; background: black '></div>";
		body+="</td></tr></table>";
		//空白
		body+="<div style='height: 20px'></div>";
		
		body+="<table border='0' width='70%' align='center'> ";
		body+="<tr style='font-size: 14px'>";
		body+="<th class='title-center' width='50%'>"+japanYear+"</th>";
		body+="<td  width='50%'>"+Util.toNumberComma(bill.getH0604())+"円</td>";
		body+="</tr></table>";
		
		body+="<div style='height: 20px'></div>";
		//使用場所
		body+="<table border='0'  width='55%'>";
		body+="<tr><th colspan='2' class='title' > 1.ご使用場所</th></tr>";
		body+="<tr><th width='50%' class='title-center'>住所</th>";
		body+="<td width='50%' style='text-align: left'>"+bill.getH0605()+"</td></tr>";
		body+="<tr><th class='title-center'>名称</th>";
		body+="<td style='text-align: left' >"+bill.getH0606()+"</td></tr>";
		body+="</table>";
		//空白
		body+="<div style='height: 10px'></div>";
		//異様期間
		body+="<table border='0' width='55%'>";
		body+="<tr><th width='50%' class='title' > 2.ご使用期間</th>";
		body+="<td width='50%' class='center'>";
		
		if (type == 1) body+=japanYearMonth;
				
		body+= "</td></tr>";
		body+="</table>";
		//空白
		body+="<div style='height: 20px'></div>";
		//お引き落とし
		body+="<table border='0' width='55%'>";
		body+="<tr><th colspan='2' class='title' > 3.お引き落とし</th></tr>";
		body+="<tr><th width='50%' class='title-center'>銀行</th>";
		body+="<td width='50%' style='text-align: left;'>";
				if (type != 1) body+=bill.getH0609();
		body+= "</td></tr>";
		body+="<tr><th class='title-center'>口座番号</th>";
		body+="<td style='text-align: left;'>";
				if (type != 1) body+=bill.getH0610();
		body+= "</td></tr>";
		body+="<tr><th class='title-center'>口座名義</th>";
		body+="<td style='text-align: left;'>";
				if (type != 1) body+=bill.getH0610();
		body+= "</td></tr>";
		body+="</table>";
		//空白
		body+="<div style='height: 10px'></div>";
		//お引き落とし予定日
		body+="<table border='0' width='55%'>";
		body+="<tr><th width='50%' class='title' >";
		if (type != 1) 
			body+=" 4.お引き落とし予定日";
		body+="</th>";
		body+="<td width='50%' class='center'>";
		body+=format3.format(format.parse(date));
		body+="</td></tr>";
		body+="</table>";
		//空白
		body+="<div style='height: 10px'></div>";
		//電力量等内駅
		body+="<table border='0' width='100%'>";
		body+="<tr><th colspan='6' class='title' > 5.電力量等内駅</th></tr>";
		body+="<tr style='font-size:11px'>";
		body+="<th width='16%'>使用電力量</th>"
				+ "<td width='16%'>"+Util.toNumberComma(bill.getH0613())+"kWh</td>"
				+ "<th width='16%'>当月最大需要電力</th>"
				+ "<td width='16%'>"+Util.toNumberComma(bill.getH0616())+"kWh</td>"
				+ "<td colspan='2' width='36%'></td>";	
		body+= "</tr>";
		body+="<tr style='font-size:11px'>";
		body+="<th  style='font-size:14px'>乗率</th><td>"+bill.getH0614()+"</td>"
				+ "<th>有効電力量当月分</th><td>"+Util.toNumberComma(bill.getH0633())+"kWh</td>"
				+ "<th width='20%'>有効電力量指示数当月分</th><td width='16%'>"+Util.toNumberComma(bill.getH0617())+"kWh</td>";	
		body+= "</tr>";
		body+="<tr style='font-size:11px'>";
		body+="<th>当月力率</th><td>"+bill.getH0615()+"%</td>"
				+ "<th>無効電力量当月分</th><td>"+Util.toNumberComma(bill.getH0634())+"kVart</td>"
				+ "<th>無効電力量指示数当月分</th><td>"+Util.toNumberComma(bill.getH0618())+"kVart</td>";	
		body+= "</tr>";
		body+="</table>";
		//空白
		body+="<div style='height: 10px'></div>";
		//オリックス株式会社
		// tab : type = 1 
		// move : type = 2
		body+="<table border='0' width='100%'>";
		body+="<tr><th colspan='6' class='title' > 6.オリックス株式会社　主契約料金</th></tr>";
		
		body+="<tr style='font-size:12px'><th rowspan='2' colspan='2' width='25%'>基本料金</th>"
				  +"<th width='17%'>i.契約電力(本体kW)</th>"
				  +"<th width='17%'>ii.単価(円/kW)</th>"
				  +"<th width='17%'>iii.力率(%)</th>"
				  +"<th width='24%'>基本料金</th>"
				 + "</tr>"
				 + "<tr style='font-size:12px'>";
				 if (type == 1){
					 body+= "<td>"+Util.toNumberComma(bill.getH0619())+"kW</td>"
							 + "<td class='center'>-</td>"
							 + "<td class='center'>-</td>";
				 } else{
					 body+= "<td>"+Util.toNumberComma(bill.getH0619())+"kW</td>"
							 + "<td>"+Util.toNumberCommaAndPoint(bill.getH0620())+"円/kW</td>"
							 + "<td>"+Util.toNumberComma(bill.getH0621())+"</td>";
				 }
		body+= "<td>"+Util.toNumberCommaAndPoint(bill.getH0622())+"円</td>"
				 + "</tr>";
		//type = 1 合計 
		if (type == 1) {
			body+="<tr style='font-size:12px'><th rowspan='2' colspan='2' width='25%'>電力量料金</th>"
				  	 + "<th>使用電力量</th>"
					 + "<th>単価</th>"
					 + "<th>燃料費調整</th>"
					 + "<th>電力量料金</th>"
				 	 + "</tr>"
				 	 + "<tr style='font-size:12px'>"
				 	 + "<td>"+Util.toNumberComma(bill.getH0623())+"kW</td>"
					 + "<td class='center'>-</td>"
					 + "<td class='center'>-</td>"
					 + "<td>"+Util.toNumberCommaAndPoint(bill.getH0626())+"円</td>"
					 + "</tr>";
			body+="<tr style='font-size:12px'>"
					 +"<th  colspan='2' width='25%' style='font-size:10px'>再生可能エネルギー発電促進賦課金等"
					 + "＠東電</th>"
					 + "<td>"+Util.toNumberComma(bill.getH0627())+"kW</td>"
					 + "<td class='center'> - </td>"
					 + "<td class='blank'></td>"
					 + "<td>"+Util.toNumberComma(bill.getH0629())+"円</td>"
					 + "</tr>";
		}else if (type == 2) {
			body+="<tr style='font-size:12px'><th  colspan='2' width='25%'>電力量料金</th>"
				  	 + "<th>使用電力量</th>"
					 + "<th>単価</th>"
					 + "<th>燃料費調整</th>"
					 + "<th>電力量料金</th>"
				 	 + "</tr>"
				 	 + "<tr style='font-size:12px'>"
				 	 + "<th rowspan='2' >単価</th>";
			
					if (isSummer) {
					   body+= "<th>夏季</th>"
							   	 + "<td>"+Util.toNumberComma(bill.getH0623())+"kWh</td>"
								 + "<td>"+Util.toNumberCommaAndPoint(bill.getH0624())+"円/kWh</td>"
								 + "<td>"+Util.toNumberCommaAndPoint(bill.getH0625())+"円/kWh</td>"
								 + "<td>"+Util.toNumberCommaAndPoint(bill.getH0626())+"円</td>"
								 + "</tr>"
								 + "<tr style='font-size:12px'>"
							 	 + "<th>他季</th>"
							     + "<td class='center'>-</td>"
								 + "<td class='center'>-</td>"
								 + "<td class='center'>-</td>"
								 + "<td class='center'>-</td>";
					}else{
						body+= "<th>夏季</th>"
							 	 + "<td class='center'>-</td>"
								 + "<td class='center'>-</td>"
								 + "<td class='center'>-</td>"
								 + "<td class='center'>-</td>"
								 + "</tr>"
								 + "<tr style='font-size:12px'>"
							 	 + "<th>他季</th>"
							 	 + "<td>"+Util.toNumberComma(bill.getH0623())+"kWh</td>"
								 + "<td>"+Util.toNumberCommaAndPoint(bill.getH0624())+"円/kWh</td>"
								 + "<td>"+Util.toNumberCommaAndPoint(bill.getH0625())+"円/kWh</td>"
								 + "<td>"+Util.toNumberCommaAndPoint(bill.getH0626())+"円</td>";
					}
			body+="</tr><tr style='font-size:12px'>"
					 +"<th  colspan='2' width='25%' style='font-size:10px'>再生可能エネルギー発電促進賦課金等"
					 + "＠東電</th>"
					 + "<td>"+Util.toNumberComma(bill.getH0627())+"kWh</td>"
					 + "<td>"+Util.toNumberCommaAndPoint(bill.getH0628())+"円/kWh</td>" //소숫점 문제
					 + "<td class='blank'></td>"
					 + "<td>"+Util.toNumberComma(bill.getH0629())+"円</td>"
					 + "</tr>";
		}
		//type = 2
		body+="<tr style='font-size:12px'>"
				 +"<th  colspan='2' width='25%'>合　　　計</th>"
				 + "<td colspan='3' class='blank'></td>"
				 + "<td>"+Util.toNumberComma(bill.getH0631())+"円</td>"
				 + "</tr>";
		 body+="<tr style='font-size:12px'>"
				 +"<th  colspan='2' width='25%'>他季1</th>"
				 + "<td colspan='3' class='blank'></td>"
				 + "<td>"+Util.toNumberComma(bill.getH0632())+"円</td>"
				 + "</tr>";
		 body+="<tr style='font-size:12px'>"
				 +"<th  colspan='2' width='25%'>(うち消費税相当額)</th>"
				 + "<td colspan='3' class='blank'></td>"
				 + "<td>"+Util.toNumberComma(bill.getH0633())+"円</td>"
				 + "</tr>";
		body+="</table>";
		
		
		//空白
		body+="<div style='height: 10px'></div>";
		//最大需要電力の履歴
		body+="<table border='0' width='100%'>";
		body+="<tr><th colspan='6' class='title' > 7.電力量等内駅</th></tr>";
		/**
		 *  終わり → [11][10][ 9 ][ 8 ][ 7 ][ 6 ] 
		 *  				[ 5 ][ 4 ][ 3 ][ 2 ][ 1 ][ 0 ] ←始め  
 		 */
		//row1
		body+="<tr style='font-size:12px'>";
		body+="<th>"+list.get(11).getMonth()+"</th>"
				+ "<th>"+list.get(10).getMonth()+"</th>"
				+ "<th>"+list.get(9).getMonth()+"</th>"
				+ "<th>"+list.get(8).getMonth()+"</th>"
				+ "<th>"+list.get(7).getMonth()+"</th>"
				+ "<th>"+list.get(6).getMonth()+"</th>";
		body+= "</tr>";
		//row2
		body+="<tr style='font-size:12px'>";
		body+="<td>"+list.get(11).getKw()+"kW</td>"
				+ "<td>"+list.get(10).getKw()+"kW</td>"
				+ "<td>"+list.get(9).getKw()+"kW</td>"
				+ "<td>"+list.get(8).getKw()+"kW</td>"
				+ "<td>"+list.get(7).getKw()+"kW</td>"
				+ "<td>"+list.get(6).getKw()+"kW</td>";
		body+= "</tr>";
		//row1
		body+="<tr style='font-size:12px'>";
		body+="<th>"+list.get(5).getMonth()+"</th>"
				+ "<th>"+list.get(4).getMonth()+"</th>"
				+ "<th>"+list.get(3).getMonth()+"</th>"
				+ "<th>"+list.get(2).getMonth()+"</th>"
				+ "<th>"+list.get(1).getMonth()+"</th>"
				+ "<th>"+list.get(0).getMonth()+"</th>";
		body+= "</tr>";
		//row2
		body+="<tr style='font-size:12px'>";
		body+="<td>"+list.get(5).getKw()+"kW</td>"
				+ "<td>"+list.get(4).getKw()+"kW</td>"
				+ "<td>"+list.get(3).getKw()+"kW</td>"
				+ "<td>"+list.get(2).getKw()+"kW</td>"
				+ "<td>"+list.get(1).getKw()+"kW</td>"
				+ "<td>"+list.get(0).getKw()+"kW</td>";
		body+= "</tr>";
		body+="</table>";
		
		return body;
	}
	
}
