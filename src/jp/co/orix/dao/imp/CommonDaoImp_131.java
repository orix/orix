package jp.co.orix.dao.imp;

import java.util.ArrayList;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.CommonDao_131;
import jp.co.orix.dao.GetGroupAndPlaceDao;
import jp.co.orix.model.UniversalInfoITHNT000;

public class CommonDaoImp_131 implements CommonDao_131 {

	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	private List<UniversalInfoITHNT000> universalInfoList = new ArrayList<UniversalInfoITHNT000>();
	
	//getSystemPortalClass
	@Override
	public List<UniversalInfoITHNT000> getSystemPortalClass(String T0100,
			String T0200) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		CommonDao_131 commApi = session.getMapper(CommonDao_131.class);

		try {
			universalInfoList = commApi.getSystemPortalClass(T0100, T0200);
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}

		return universalInfoList;
	}

	public SqlSessionFactory getSqlSession() {
		return sqlSession;
	}

	public void setSqlSession(SqlSessionFactory sqlSession) {
		this.sqlSession = sqlSession;
	}

	public List<UniversalInfoITHNT000> getUniversalInfoList() {
		return universalInfoList;
	}

	public void setUniversalInfoList(List<UniversalInfoITHNT000> universalInfoList) {
		this.universalInfoList = universalInfoList;
	}
	
}
