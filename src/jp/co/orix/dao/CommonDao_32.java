package jp.co.orix.dao;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public interface CommonDao_32 {
	public String selectMessageByCond(HashMap<String, String> cond) throws SQLException;
	public List<Map<String, Double>> selectByHalfhour(Map<String, String> cond) throws SQLException;
	public List<Map<String, Double>> selectByDay(Map<String, String> cond) throws SQLException;
	public Map<String, Double> selectByMonth(Map<String, String> cond) throws SQLException;
}
