package jp.co.orix.bdconn;

import java.io.InputStream;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class SqlSessionFactoryService
{
  private static SqlSessionFactory sessionFactory;
  
  static
  {
    try
    {
      InputStream in = Resources.getResourceAsStream("/SqlMapConfig.xml");
      sessionFactory = new SqlSessionFactoryBuilder().build(in);
    }
    catch (Exception e)
    {
      System.err.println("sql error : " + e.getMessage());
    }
  }
  
  public static SqlSessionFactory getsqlFactoeyService()
  {
    return sessionFactory;
  }
}
