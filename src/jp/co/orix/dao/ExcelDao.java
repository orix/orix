package jp.co.orix.dao;

import jp.co.orix.model.Excel;

import org.apache.ibatis.annotations.Param;

public interface ExcelDao {
//	public List<Excel> excelDown(@Param("excel") String excel);
	public Excel excelDown(@Param("excel") String excel);

}
