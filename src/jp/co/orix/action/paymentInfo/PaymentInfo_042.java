package jp.co.orix.action.paymentInfo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import jp.co.orix.dao.GetGroupAndPlaceDao;
import jp.co.orix.dao.ModifyDao_040;
import jp.co.orix.dao.PaymentInfoDao;
import jp.co.orix.model.IMACP000;
import jp.co.orix.model.UniversalInfoITHNT000;
import jp.co.orix.model.UsePlaceCombobox;
import jp.co.orix.model.UserPaymentInfo;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class PaymentInfo_042 extends ActionSupport implements ServletRequestAware {

	private UserPaymentInfo userPaymentInfo;   // model
	private UserPaymentInfo inputUserPaymentInfo;   // model
	private PaymentInfoDao paymentInfoDao;     // DAO
	private IMACP000 imacp000;

	private Map paymentMethodInfoSet = new HashMap<>();
	private Map paymentMethodInfoResult = new HashMap<>();

	private Map valueToUpdate = new HashMap<>();

	private String status;
	private String changeScope;
	private ArrayList locationList = new ArrayList<>(); 

	public HttpServletRequest request;
	private String parameter;	
/*
	ModifyDao_040 modifyDao;	
	ArrayList<UsePlaceCombobox> usePlaceComboboxList;*/

	private GetGroupAndPlaceDao getGroupAndPlaceDao;
	private List<UniversalInfoITHNT000> universalInfoList; 

	
	
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;	
	}

	public String getUserInfo() {
		
		if (paymentInfoDao.getUserInfo(userPaymentInfo.getHV011_location()) == null) {
			return ERROR;
		} else {
			userPaymentInfo = paymentInfoDao.getUserInfo(userPaymentInfo.getHV011_location());
		}

		paymentMethodInfoSet.put("HV011_location", userPaymentInfo.getHV011_location());
		paymentMethodInfoSet.put("paymentID", 20001);
		paymentMethodInfoResult = paymentInfoDao.getKubunName(paymentMethodInfoSet);

		userPaymentInfo.setHV011_paymentMethodID(Integer.parseInt((String)paymentMethodInfoResult.get("T0200")));
		userPaymentInfo.setHV011_paymentMethod((String)paymentMethodInfoResult.get("T0401"));

		ActionContext context = ActionContext.getContext();
		Map session = (Map)context.getSession();

		session.put("userPaymentInfo", userPaymentInfo);
		context.setSession(session);

		return SUCCESS;
	}

	public String toPaymentInfo() throws Exception {

		universalInfoList = getGroupAndPlaceDao.getAllUsePlaceComboboxList();
	
		
		if (status !=null) {
			ActionContext context = ActionContext.getContext();
			Map session = (Map)context.getSession();
			session.remove("userPaymentInfo");
		} 
		return SUCCESS;
	}

	public String checkOfContent() {

		ActionContext context = ActionContext.getContext();
		Map session = (Map)context.getSession();

		inputUserPaymentInfo = (UserPaymentInfo)session.get("userPaymentInfo");
		try {
			userPaymentInfo.setHV011_paymentMethodID(inputUserPaymentInfo.getHV011_paymentMethodID());
			userPaymentInfo.setHV011_location(inputUserPaymentInfo.getHV011_location());

			session.put("changeScope", changeScope);

		} catch (Exception e) {
			// TODO: handle exception
		}
		return SUCCESS;
	}

	public String changePaymentMethod() {

		ActionContext context = ActionContext.getContext();
		Map session = (Map)context.getSession();

		userPaymentInfo = (UserPaymentInfo)session.get("userPaymentInfo");

		return SUCCESS;
	}

	public String accountInfoChange() {

		ActionContext context = ActionContext.getContext();
		Map session = (Map)context.getSession();

		userPaymentInfo = (UserPaymentInfo)session.get("userPaymentInfo");

		return SUCCESS;
	}

	public String toHvChangePaymentConfirm() throws Exception {

		ActionContext context = ActionContext.getContext();
		Map session = (Map)context.getSession();

		inputUserPaymentInfo = (UserPaymentInfo)session.get("userPaymentInfo");


		inputUserPaymentInfo.setCM004_financialInstitutionName(userPaymentInfo.getCM004_financialInstitutionName());
		inputUserPaymentInfo.setCM004_accountNameAfter(userPaymentInfo.getCM004_accountNameAfter());
		inputUserPaymentInfo.setHV011_zipCode(userPaymentInfo.getHV011_zipCode());
		inputUserPaymentInfo.setHV011_address(userPaymentInfo.getHV011_address());
		inputUserPaymentInfo.setHV011_companyName(userPaymentInfo.getHV011_companyName());
		inputUserPaymentInfo.setHV011_departmentName(userPaymentInfo.getHV011_departmentName());
		inputUserPaymentInfo.setHV011_personInChargeName(userPaymentInfo.getHV011_personInChargeName());
		inputUserPaymentInfo.setHV011_areaCode(userPaymentInfo.getHV011_areaCode());
		inputUserPaymentInfo.setIDUMS000_mailAddress(userPaymentInfo.getIDUMS000_mailAddress());
		inputUserPaymentInfo.setHV011_entry(userPaymentInfo.getHV011_entry());

		if (paymentInfoDao.countRecordOfIDUMS000() == 0) {
			inputUserPaymentInfo.setIDUMS000_pK(1);
		} else {
			inputUserPaymentInfo.setIDUMS000_pK(paymentInfoDao.countRecordOfIDUMS000() + 1);
		}

		imacp000 = new IMACP000();
		if(paymentInfoDao.countRecordOfIMACP000() == 0) {
			imacp000.setAcceptNumber("1");
		} else {
			imacp000.setAcceptNumber((paymentInfoDao.countRecordOfIMACP000()+ ""));
			System.out.println(imacp000.getAcceptNumber());
		}

		System.out.println("그럼한번찍어보자 : " + imacp000.getAcceptNumber());

		imacp000.setChangeDivision("03");

		imacp000.setPotalName("1");
		imacp000.setP1("p1");
		imacp000.setP1_name("木村");
		imacp000.setP1_Number("p1");
		imacp000.setP1_Number("1");
		imacp000.setGroupName("group1");

		imacp000.setLocation(inputUserPaymentInfo.getHV011_location());

		if (changeScope.equalsIgnoreCase("0")) {

			paymentInfoDao.changeUserPaymentInfo_HV011(inputUserPaymentInfo);
			paymentInfoDao.changeUserPaymentInfo_CM004(inputUserPaymentInfo);
			paymentInfoDao.changeUserPaymentInfo_IDUMS000(inputUserPaymentInfo);
			paymentInfoDao.getInsertIMACP000SQL(imacp000);

		} else {

			paymentInfoDao.changeUserPaymentInfo_All_HV011(inputUserPaymentInfo);
			paymentInfoDao.changeUserPaymentInfo_All_CM004(inputUserPaymentInfo);
			paymentInfoDao.changeUserPaymentInfo_All_IDUMS000(inputUserPaymentInfo);
			paymentInfoDao.getInsertIMACP000SQL_All(imacp000);

		}

		return SUCCESS;

	}


	public UserPaymentInfo getUserPaymentInfo() {
		return userPaymentInfo;
	}

	public void setUserPaymentInfo(UserPaymentInfo userPaymentInfo) {
		this.userPaymentInfo = userPaymentInfo;
	}

	public PaymentInfoDao getPaymentInfoDao() {
		return paymentInfoDao;
	}

	public void setPaymentInfoDao(PaymentInfoDao paymentInfoDao) {
		this.paymentInfoDao = paymentInfoDao;
	}

	public UserPaymentInfo getInputUserPaymentInfo() {
		return inputUserPaymentInfo;
	}

	public void setInputUserPaymentInfo(UserPaymentInfo inputUserPaymentInfo) {
		this.inputUserPaymentInfo = inputUserPaymentInfo;
	}

	public IMACP000 getImacp000() {
		return imacp000;
	}

	public void setImacp000(IMACP000 imacp000) {
		this.imacp000 = imacp000;
	}

	public Map getPaymentMethodInfoSet() {
		return paymentMethodInfoSet;
	}

	public void setPaymentMethodInfoSet(Map paymentMethodInfoSet) {
		this.paymentMethodInfoSet = paymentMethodInfoSet;
	}

	public Map getPaymentMethodInfoResult() {
		return paymentMethodInfoResult;
	}

	public void setPaymentMethodInfoResult(Map paymentMethodInfoResult) {
		this.paymentMethodInfoResult = paymentMethodInfoResult;
	}

	public Map getValueToUpdate() {
		return valueToUpdate;
	}

	public void setValueToUpdate(Map valueToUpdate) {
		this.valueToUpdate = valueToUpdate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getChangeScope() {
		return changeScope;
	}

	public void setChangeScope(String changeScope) {
		this.changeScope = changeScope;
	}
/*
	public ModifyDao_040 getModifyDao() {
		return modifyDao;
	}

	public void setModifyDao(ModifyDao_040 modifyDao) {
		this.modifyDao = modifyDao;
	}

	public ArrayList<UsePlaceCombobox> getUsePlaceComboboxList() {
		return usePlaceComboboxList;
	}

	public void setUsePlaceComboboxList(
			ArrayList<UsePlaceCombobox> usePlaceComboboxList) {
		this.usePlaceComboboxList = usePlaceComboboxList;
	}*/

	public HttpServletRequest getRequest() {
		return request;
	}

	public void setRequest(HttpServletRequest request) {
		this.request = request;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public GetGroupAndPlaceDao getGetGroupAndPlaceDao() {
		return getGroupAndPlaceDao;
	}

	public void setGetGroupAndPlaceDao(GetGroupAndPlaceDao getGroupAndPlaceDao) {
		this.getGroupAndPlaceDao = getGroupAndPlaceDao;
	}

	public List<UniversalInfoITHNT000> getUniversalInfoList() {
		return universalInfoList;
	}

	public void setUniversalInfoList(List<UniversalInfoITHNT000> universalInfoList) {
		this.universalInfoList = universalInfoList;
	}


}
