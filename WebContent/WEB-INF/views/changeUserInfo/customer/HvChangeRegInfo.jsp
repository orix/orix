<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<title>登録情報変更</title>

<style>
table {
	border-collapse: collapse;
	align: center;
	width: 50%;
}

th {
	background-color: #4F81BD;
	padding: 10px;
	color: white;
	border-radius: 8px;
	border-color: #000000;
	border: 2px solid white;
	font-size: 20px;
}

td {
	background-color: #FFFFFF;
	color: black;
	border: 2px white;
	border-radius: 5px;
	font-size: 20px;
}

input[type=text] {
	width: 100%;
	padding: 10px;
	margin: 0 0;
	box-sizing: border-box;
	border: 2px solid #1E4F8B;
	border-radius: 8px;
	font-size: 20px;
}

input[type=button] {
   background-color: #4F81BD;
    box-shadow: 0 4px 16px 0 rgba(0,0,0,0.2), 0 4px 10px 0 rgba(0,0,0,0.19);
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 500px;
    cursor: pointer;
    text-align: center;
    border-radius: 8px;
    font-size: 20px;
}

input[type=submit] {
   background-color: #4F81BD;
    box-shadow: 0 4px 16px 0 rgba(0,0,0,0.2), 0 4px 10px 0 rgba(0,0,0,0.19);
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 500px;
    cursor: pointer;
    text-align: center;
    border-radius: 8px;
    font-size: 20px;
}

</style>
<script src="<%=request.getContextPath() %>/resource/js/common_131.js"></script>
<script type="text/javascript">
	function onSubmit() {
		
		var H1807 = document.getElementsByName("H1807")[0].value;//우편번호
		var H1809 = document.getElementsByName("H1809")[0].value;//주소
		var H1811 = document.getElementsByName("H1811")[0].value;//건물명
		var H1813 = document.getElementsByName("H1813")[0].value;//회사명
		var H1815 = document.getElementsByName("H1815")[0].value;//부서명
		var H1817 = document.getElementsByName("H1817")[0].value;//담당자
		
		var newM0200 = document.getElementsByName("newM0200")[0].value;
		
		if(H1807 == '') {
			alert('郵便番号が入力されていません。');
			return false;
		}
		
		if(!IsValidateStringValue(H1807)) {
			alert('半角で入力させてください。');
			return false;
		}
		
// 【IsPostalNo】　郵便番号
		if(!IsPostalNo(H1807)) {
			alert('郵便番号に無効な値が入力されています。');
			return false;
		}
// 　=======================================
		if(H1809 == '') {
			alert('住所が入力されていません。');
			return false;
		}
// 【IsValidateWideStringValue】　住所
		if(!IsValidateWideStringValue(H1809)) {
			alert('全角で入力させてください。');
			return false;
		}
// 		======================================
// 		【IsValidateWideStringValue】ビル・マンション名
		if(!IsValidateWideStringValue(H1811)) {
			alert('全角で入力させてください。');
			return false;
		}
// ==================================
		if(H1813 == '') {
			alert('会社名が入力されていません。');
			return false;
		}
// 		【IsValidateWideStringValue】
		if(!IsValidateWideStringValue(H1813)) {
			alert('全角で入力させてください。');
			return false;
		}
// ======================================
// 		【IsValidateWideStringValue】部署
		if(!IsValidateWideStringValue(H1815)) {
			alert('全角で入力させてください。');
			return false;
		}
// ====================================
		if(H1817 == '') {
			alert('ご担当者が入力されていません。');
			return false;
		}
// 		【IsValidateWideStringValue】
		if(!IsValidateWideStringValue(H1817)) {
			alert('全角で入力させてください。');
			return false;
		}
// =======================================
		if(!IsValidateStringValue(newM0200)) {
			alert('半角で入力させてください。');
			return false;
		}
// 		【IsMailAddress】
		if (!IsMailAddress(newM0200)) {
			alert('メールアドレスに無効な値が入力されています。');
			return false;
		}

		document.forms["detail"].action = "checkUser";
		document.forms["detail"].submit();

	}
</script>

</head>
<body>

	<div align="center" style="color: #245A82">
		<h2>変更内容入力</h2>
		<br />
		<h2>※「＊」の付いた項目は必ず入力してください。</h2>
		<br/>
	</div>

	<div align="center">
		<form method="post" name="detail" action="checkUser">
			<table>
				<tr>
					<th colspan="3" align="left">送付先情報</th>
				</tr>

				<tr>
					<td></td>
					<th align="center">変更前</th>
					<th align="center">変更後</th>
				</tr>

				<tr>
					<th align="center">*郵便番号</th>
					<td><input type="text" name="H1806" value="${user.detail.h1806}" disabled></td>
					<td><input type="text" name="H1807" value="${user.detail.h1806}"></td>
				</tr>

				<tr>
					<th align="center">*住所</th>
					<td><input type="text" name="H1808" value="${user.detail.h1808}" disabled></td>
					<td><input type="text" name="H1809" value="${user.detail.h1808}"></td>
				</tr>
				
				<tr>
					<th align="center">ビル・マンション名</th>
					<td><input type="text" name="H1810" value="${user.detail.h1810}" disabled></td>
					<td><input type="text" name="H1811" value="${user.detail.h1810}"></td>
				</tr>
				
				<tr>
					<th align="center">*会社名</th>
					<td><input type="text" name="H1812" value="${user.detail.h1812}" disabled></td>
					<td><input type="text" name="H1813" value="${user.detail.h1812}"></td>
				</tr>
				
				<tr>
					<th align="center">部署名</th>
					<td><input type="text" name="H1814" value="${user.detail.h1814}" disabled></td>
					<td><input type="text" name="H1815" value="${user.detail.h1814}"></td>
				</tr>
				
				<tr>
					<th align="center">*ご担当者</th>
					<td><input type="text" name="H1816" value="${user.detail.h1816}" disabled></td>
					<td><input type="text" name="H1817" value="${user.detail.h1816}"></td>
				</tr>
				
				<tr>
					<td><br/></td>
				</tr>
				
				<tr>
					<th colspan="3" align="left">ユーザーID情報</th>
				</tr>
				
				<tr>
					<td></td>
					<th align="center">変更前</th>
					<th align="center">変更後</th>
				</tr>
				
				<tr>
					<th align="center">*メールアドレス</th>
					<td><input type="text" name="M0200" value="${user.mail.m0200}" disabled></td>
					<td><input type="text" name="newM0200" value="${user.mail.m0200}"></td>
				</tr>
			
				<tr align="center">
					<td colspan="3"><br/>
					<input type="button" value="内容確認" onClick="onSubmit()"/></td>
				</tr>
			</table>
		</form>
	</div>