package jp.co.orix.dao.imp;
/**
 * @内容　「GetGroupComboboxList」と「GetUsePlaceComboboxList」のＡＰＩに関するＤＡＯＩＭＰＬ
 * @author Jongmoo Kim
 */
import java.util.ArrayList;
import java.util.Map;
import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.GetGroupAndPlaceDao;
import jp.co.orix.model.UniversalInfoITHNT000;

public class GetGroupAndPlaceDaoImpl implements GetGroupAndPlaceDao {

	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	private List<UniversalInfoITHNT000> universalInfoList = new ArrayList<UniversalInfoITHNT000>();

	@Override
	public List<UniversalInfoITHNT000> getGroupComboboxList(String valueOfcolum) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		GetGroupAndPlaceDao getGroupAndPlaceDao = session.getMapper(GetGroupAndPlaceDao.class);

		try {
			universalInfoList = getGroupAndPlaceDao.getGroupComboboxList(valueOfcolum);
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}

		return universalInfoList;
	}

	@Override
	public List<UniversalInfoITHNT000> getUsePlaceComboboxList(
			Map<String, String> condition) {
		// TODO Auto-generated method stub
		SqlSession session =sqlSession.openSession();
		GetGroupAndPlaceDao getGroupAndPlaceDao = session.getMapper(GetGroupAndPlaceDao.class);

		try {
			universalInfoList = getGroupAndPlaceDao.getUsePlaceComboboxList(condition);
		} catch(Exception e) {
			System.err.println(e.getMessage()); 
		} finally {
			session.close();
		}

		return universalInfoList;
	}

	@Override
	public List<UniversalInfoITHNT000> getAllUsePlaceComboboxList() {
		// TODO Auto-generated method stub
		SqlSession session =sqlSession.openSession();
		GetGroupAndPlaceDao getGroupAndPlaceDao = session.getMapper(GetGroupAndPlaceDao.class);

		try {
			universalInfoList = getGroupAndPlaceDao.getAllUsePlaceComboboxList();
		} catch(Exception e) {
			System.err.println(e.getMessage()); 
		} finally {
			session.close();
		}

		return universalInfoList;
	}



}
