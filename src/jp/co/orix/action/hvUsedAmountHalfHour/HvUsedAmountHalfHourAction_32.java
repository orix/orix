package jp.co.orix.action.hvUsedAmountHalfHour;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import jp.co.orix.dao.CheckContractInfoDao;
import jp.co.orix.dao.GetCustomerContractNumberDao;
import jp.co.orix.util.CommApi_32;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

public class HvUsedAmountHalfHourAction_32 extends ActionSupport implements ServletRequestAware {
	
	private String groupCombobox;				//グループコンボボックス
	private String usePlaceCombobox;			//ご使用場所
	private String customerContractNumber; 		//お客様契約番号
	
	private GetCustomerContractNumberDao getCustomerContractNumberDao;	//Dao for GetControlNumbers method
	//
	private String adminMessage;				//コメント管理情報
	private String specDate;					//指定日				
	private String compDate;					//比較日				
	private String compChk;						//比較表示				
	
	private Map<String, ArrayList<String>> usePowerTableMap;	// 使用量情報をモデル
	private String usePowerGraphMappingModel;					// グラフ作成する情報クラス」モデル
	private String graphCreateInfo;								// グラフのマッピング用モデルリスト
	
	public HttpServletRequest request;
	private String parameter;									// ロードパラメータ
	@Override
	public String execute() throws Exception{
		//try-catch for setting error
		try{
			//1-3. コントロール初期化	
			//1)	画面間の引き継ぎ情報を設定する。
			CommApi_32 api = new CommApi_32();
			api.setFormParamator(request, "HvUsedAmountHalfHour");
			HashMap<String, Object> param = api.getFormParamator("HvUsedAmountHalfHour");
			parameter = (String)param.get("P1");
			
			//2)	コンボボックス設定
			//①	グループコンボボックス, ②	ご使用場所コンボボックスはJSPで設定します
			//set fromDate to System Time
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = cal.getTime();
			//③	指定日設定
			if(specDate== null || specDate.isEmpty()){
				specDate = timeFormat.format(date);
			}
			//④	比較日設定
			if(compDate == null || compDate.isEmpty()){
				//set toDate to System time - 1 year
				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 1);
				date = cal.getTime();
				compDate = timeFormat.format(date);
			}
			
			//3)	管理者コメント設定
			//get adminMessage from DB
			adminMessage = CommApi_32.GetComment("1", "000", "1");
			
			//4)以下のように各コントロールのプロパティを設定する。
			if(compChk == null)
				compChk = "on";
			else if(compChk.equals("off")){
				compChk = null;
			}else{
				compChk = "on";
			}
			
			//3-1.　表示処理
			//1)~4)はJSPでします
			
			//5)	共通サービス【GetControlNumbers】を使用し、管理番号を取得する。
			
			if(groupCombobox == null){
				groupCombobox = "";
			}
			if(usePlaceCombobox == null){
				usePlaceCombobox = "";
			}
			
			
			//3-1.　表示処理
			//5)	共通サービス【GetControlNumbers】を使用し、管理番号を取得する。
			if(groupCombobox.length() != 0 && usePlaceCombobox.length() != 0){
				Map<String, String> condition = new HashMap<String, String>();
				condition.put("groupCombobox", groupCombobox);
				condition.put("usePlaceCombobox", usePlaceCombobox);
			
				customerContractNumber = getCustomerContractNumberDao.getCustomerContractNumber(condition);
			}else{
				customerContractNumber = "00000";
			}
			
			//6)	共通サービス【GetCustomerContractNumber】を使用し、お客様契約番号を取得する
			//test value setting(manageNumber) // 担当者が来ない。。。
			String manageNumber = "1";				//use GetControlNumbers() to get manageNumber after test.
			
			//7) 30分別データを取得する。
				//①	指定日の使用量情報を取得する。
			List<Map<String, Double>> usePowerMapLi = CommApi_32.getUsedPowerAmountInfo(manageNumber, "2", specDate, specDate, customerContractNumber);
			Map<String, Double> usePowerMap = usePowerMapLi.get(0);
				//②	比較表示の値が’ON’の場合、比較日の使用量情報を取得する。
			List<Map<String, Double>> useCompPowerMapLi = null;
			Map<String, Double> useCompPowerMap = null;
			if(compChk != null ){
				useCompPowerMapLi = CommApi_32.getUsedPowerAmountInfo(manageNumber, "2", compDate, compDate, customerContractNumber);
				useCompPowerMap = useCompPowerMapLi.get(0);
			}
				//③	使用量情報をモデルに設定する。
			//set table data map(usePowerTableMap)
			//1. create map to get four String arrays (column names, use power amounts when start date, use power amounts when compared date) 
			usePowerTableMap = new HashMap<String, ArrayList<String>>();
			//2. create and set columnNames
			ArrayList<String> columnNames = getColumnNameList();
			usePowerTableMap.put("columnNames", columnNames);
			//3. create and set usePowers
			ArrayList<String> usePowers = getUsePowerList(usePowerMap);
			usePowerTableMap.put("usePowers", usePowers);
			//4. create and set useCompPowers 
			ArrayList<String> useCompPowers = null;
			if(compChk != null){
				useCompPowers = getUsePowerList(useCompPowerMap);
				usePowerTableMap.put("useCompPowers", useCompPowers);
			}
			//5. create and set useDifPowers 
			ArrayList<String> useDifPowers = null;
			if(compChk != null && useCompPowerMapLi != null && useCompPowerMapLi.size() != 0){
				useDifPowers = getUseDifPowerList(usePowerMap, useCompPowerMap);
				usePowerTableMap.put("useDifPowers", useDifPowers);
			}
				//④下記の情報を「グラフ作成する情報クラス」モデルに設定する。
			GraphCreateInfo graphCreateInfo = new GraphCreateInfo();
			//(additional)set x-axis label
			setGraphLabels(graphCreateInfo.getLabels());
			//set datasets
			//1. set dataset for usePowers 
			//h.　比較表示フラグが’ON’かつ比較データが存在する場合、比較日の時間別使用量を設定する。
					if(compChk != null && useCompPowerMapLi != null && useCompPowerMapLi.size() != 0){
						graphCreateInfo.getDatasets().add(
								getGraphLineDataset("比較日"));
					}
			//2. set dataset for usePowers 
			//b.　各配色をWeb.cconfigのPowerBorderColorより取得し、設定する。(not provided by customer)
			//c.　昼間凡例を設定する。
			//d.　夜間凡例を設定する。
			graphCreateInfo.getDatasets().add(
					getGraphBarDataset("昼間時間(8:00~22:00)","rgba(244,129,86,1)"));
			graphCreateInfo.getDatasets().add(
					getGraphBarDataset("夜間(昼間時間以外)","rgba(245,195,115,1)"));
			//2. set dataset for usePowers 
			//b.　各配色をWeb.cconfigのPowerBorderColorより取得し、設定する。(not provided by customer)
			
			//f.　指定日の時間別使用量を設定する。ｱ.　使用量設定値 = 使用量 * 10000M
			graphCreateInfo.setxScaleLabel("時間");
			graphCreateInfo.setyScaleLabel("kWh");
			//f.　指定日の時間別使用量を設定する。ｲ.　取得した時間別使用量が昼間の場合、昼間リストに設定し、　それ以外の場合、夜間リストに設定する。 (can not divide datas into day-time set and night-time set)
			Map<String, Object> usePowerGraphMap = new HashMap<String, Object>();
			
			
			usePowerGraphMap.put("useDayTimePowers", getDayTimeUsePowerList(usePowers));
			usePowerGraphMap.put("useNightTimePowers", getNightTimeUsePowerList(usePowers));
			//h.　比較表示フラグが’ON’かつ比較データが存在する場合、比較日の時間別使用量を設定する。
			if(compChk != null && useCompPowerMapLi != null && useCompPowerMapLi.size() != 0) {
				usePowerGraphMap.put("useCompPowers", useCompPowers);
			}
				//⑤共通サービス【GetGraphUsedPower】を使用し、使用量グラフ作成を行う。 -グラフのマッピング用モデルリスト
			Gson gson = new Gson();
			usePowerGraphMappingModel = gson.toJson(usePowerGraphMap);
			this.graphCreateInfo = gson.toJson(graphCreateInfo);
			
			//8)	6)で取得データが存在しない場合、『共通メッセージ(E0005)』を表示する。 //JSPでします。

			
			return SUCCESS;
		}catch(Exception e){
			return "error";
		}
	}
	
	private Map<String,Object> getGraphBarDataset(String label, String color){
		Map<String,Object> dataset = new HashMap<String,Object>();
		dataset.put("type", "bar");
		dataset.put("label", label);
		dataset.put("backgroundColor", color);
		return dataset;
	}
	
	private Map<String,Object> getGraphLineDataset(String label){
		Map<String,Object> dataset = new HashMap<String,Object>();
		dataset.put("type", "line");
		dataset.put("label", label);
		dataset.put("backgroundColor", "rgba(107,156,213,1)");
		dataset.put("tension", 0);
		dataset.put("borderWidth", 2);
		dataset.put("radius	", 2);

		dataset.put("borderColor","rgba(107,156,213,1)");
		
		dataset.put("fill", false);
		dataset.put("data", "");
		return dataset;
	}
	//set Labels in List for creating Graph
	private void setGraphLabels(List<String> labelLi) throws Exception{
		for(int i = 0; i < 24; i++){
			labelLi.add(i+"");
			labelLi.add("");
		}
		labelLi.add("");
	}
	
	private ArrayList<String> getUsePowerList(Map<String, Double> usePowerMap) throws Exception{
		ArrayList<String> usePowerList = new ArrayList<String>();
		for(int i = 1; i <= usePowerMap.size(); i++){
			usePowerList.add(String.format("%.4f", usePowerMap.get(i+"")));
		}
		return usePowerList;
	}
	
	private ArrayList<String> getUseDifPowerList(Map<String, Double> usePowerMap, Map<String, Double> useCompPowerMap) throws Exception{
		ArrayList<String> useDifPowers = new ArrayList<String>();
		for(int i = 1; i <= usePowerMap.size(); i++){
			if(usePowerMap.get(i+"") != null && useCompPowerMap.get(i+"") != null)
			useDifPowers.add(String.format("%.4f", (usePowerMap.get(i+"") - useCompPowerMap.get(i+""))));
		}
		return useDifPowers;
	}
	
	private ArrayList<String> getColumnNameList(){
		ArrayList<String> nameList = new ArrayList<String>();
		
		nameList.add("00:00-00:29");
		nameList.add("00:30-00:59");
		nameList.add("01:00-01:29");
		nameList.add("01:30-01:59");
		nameList.add("02:00-02:29");
		nameList.add("02:30-02:59");
		nameList.add("03:00-03:29");
		nameList.add("03:30-03:59");
		nameList.add("04:00-04:29");
		nameList.add("04:30-04:59");
		nameList.add("05:00-05:29");
		nameList.add("05:30-05:59");
		nameList.add("06:00-06:29");
		nameList.add("06:30-06:59");
		nameList.add("07:00-07:29");
		nameList.add("07:30-07:59");
		nameList.add("08:00-08:29");
		nameList.add("08:30-08:59");
		nameList.add("09:00-09:29");
		nameList.add("09:30-09:59");
		nameList.add("10:00-10:29");
		nameList.add("10:30-10:59");
		nameList.add("11:00-11:29");
		nameList.add("11:30-11:59");
		nameList.add("12:00-12:29");
		nameList.add("12:30-12:59");
		nameList.add("13:00-13:29");
		nameList.add("13:30-13:59");
		nameList.add("14:00-14:29");
		nameList.add("14:30-14:59");
		nameList.add("15:00-15:29");
		nameList.add("15:30-15:59");
		nameList.add("16:00-16:29");
		nameList.add("16:30-16:59");
		nameList.add("17:00-17:29");
		nameList.add("17:30-17:59");
		nameList.add("18:00-18:29");
		nameList.add("18:30-18:59");
		nameList.add("19:00-19:29");
		nameList.add("19:30-19:59");
		nameList.add("20:00-20:29");
		nameList.add("20:30-20:59");
		nameList.add("21:00-21:29");
		nameList.add("21:30-21:59");
		nameList.add("22:00-22:29");
		nameList.add("22:30-22:59");
		nameList.add("23:00-23:29");
		nameList.add("23:30-23:59");
		
		return nameList;
	}

	private ArrayList<String> getNightTimeUsePowerList(List<String> usePowerLi) throws Exception{
		ArrayList<String> usePowerList = new ArrayList<String>();
		for (int i = 0; i < 16; i++) {
			usePowerList.add(usePowerLi.get(i));
		}
		for(int i=16; i < 43 ; i++){
			usePowerList.add("0");
		}
		for (int i = 43; i < usePowerLi.size(); i++) {
			usePowerList.add(usePowerLi.get(i));
		}
		return usePowerList;
	}
	
	private ArrayList<String> getDayTimeUsePowerList(List<String> usePowerLi) throws Exception{
		ArrayList<String> usePowerList = new ArrayList<String>();
		for (int i = 0; i < 16; i++) {
			usePowerList.add("0");
		}
		for(int i=16; i < 43 ; i++){
			usePowerList.add(usePowerLi.get(i));
		}
		for (int i = 43; i < usePowerLi.size(); i++) {
			usePowerList.add("0");
		}
		return usePowerList;
	}
	

	public String getAdminMessage() {
		return adminMessage;
	}

	public void setAdminMessage(String adminMessage) {
		this.adminMessage = adminMessage;
	}

	public String getSpecDate() {
		return specDate;
	}

	public void setSpecDate(String specDate) {
		this.specDate = specDate;
	}

	public String getCompDate() {
		return compDate;
	}

	public void setCompDate(String compDate) {
		this.compDate = compDate;
	}

	public String getCompChk() {
		return compChk;
	}

	public void setCompChk(String compChk) {
		this.compChk = compChk;
	}

	public Map<String, ArrayList<String>> getUsePowerTableMap() {
		return usePowerTableMap;
	}

	public void setUsePowerTableMap(Map<String, ArrayList<String>> usePowerTableMap) {
		this.usePowerTableMap = usePowerTableMap;
	}

	public String getUsePowerGraphMappingModel() {
		return usePowerGraphMappingModel;
	}

	public void setUsePowerGraphMappingModel(String usePowerGraphMappingModel) {
		this.usePowerGraphMappingModel = usePowerGraphMappingModel;
	}

	public String getGraphCreateInfo() {
		return graphCreateInfo;
	}

	public void setGraphCreateInfo(String graphCreateInfo) {
		this.graphCreateInfo = graphCreateInfo;
	}

	public String getGroupCombobox() {
		return groupCombobox;
	}

	public void setGroupCombobox(String groupCombobox) {
		this.groupCombobox = groupCombobox;
	}

	public String getUsePlaceCombobox() {
		return usePlaceCombobox;
	}

	public void setUsePlaceCombobox(String usePlaceCombobox) {
		this.usePlaceCombobox = usePlaceCombobox;
	}

	public String getCustomerContractNumber() {
		return customerContractNumber;
	}

	public void setCustomerContractNumber(String customerContractNumber) {
		this.customerContractNumber = customerContractNumber;
	}



	public GetCustomerContractNumberDao getGetCustomerContractNumberDao() {
		return getCustomerContractNumberDao;
	}

	public void setGetCustomerContractNumberDao(
			GetCustomerContractNumberDao getCustomerContractNumberDao) {
		this.getCustomerContractNumberDao = getCustomerContractNumberDao;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	
	

}
