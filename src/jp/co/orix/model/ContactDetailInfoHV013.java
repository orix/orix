package jp.co.orix.model;

import java.util.Date;

public class ContactDetailInfoHV013 {

	private int h1301; 		//H1301		高圧連絡先明細情報ID	number			NOT NULL
	private String h1302;	//H1302		受付区分				varchar2(2)		NOT NULL
	private String h1303;	//H1303		郵便番号_変更前		varchar2(100)	NOT NULL
	private String h1304;	//H1304		郵便番号_変更後		varchar2(100)	
	private String h1305;	//H1305		都道府県名			varchar2(100)	
	private String h1306;	//H1306		市区町村名			varchar2(100)	
	private String h1307;	//H1307		町名_変更前			varchar2(100)	
	private String h1308;	//H1308		町名_変更後			varchar2(100)	
	private String h1309;	//H1309		番地_変更前			varchar2(100)	
	private String h1310;	//H1310		番地_変更後			varchar2(100)	
	private String h1311;	//H1311		ビル・マンション名_変更前	varchar2(100)	
	private String h1312;	//H1312		ビル・マンション名_変更後	varchar2(100)	
	private String h1313;	//H1313		部屋番号_変更前		varchar2(10)	
	private String h1314;	//H1314		部屋番号_変更後		varchar2(10)	
	private String h1315;	//H1315		屋号_変更前			varchar2(10)	
	private String h1316;	//H1316		屋号_変更後			varchar2(10)	
	private String h1317;	//H1317		法人名_変更前			varchar2(100)	
	private String h1318;	//H1318		法人名_変更後			varchar2(100)	
	private String h1319;	//H1319		部署名_変更前			varchar2(20)	
	private String h1320;	//H1320		部署名_変更後			varchar2(20)	
	private String h1321;	//H1321		担当者1_変更前		varchar2(20)	
	private String h1322;	//H1322		担当者1_変更後		varchar2(20)	
	private String h1323;	//H1323		担当者2_変更前		varchar2(20)	
	private String h1324;	//H1324		担当者2_変更後		varchar2(20)	
	private String h1325;	//H1325		メールアドレス1_変更前	varchar2(50)	
	private String h1326;	//H1326		メールアドレス1_変更後	varchar2(50)	
	private String h1327;	//H1327		メールアドレス2_変更前	varchar2(50)	
	private String h1328;	//H1328		メールアドレス2_変更後	varchar2(50)	
	private String h1329;	//H1329		市外局番-変更前		varchar2(５)	
	private String h1330;	//H1330		市内局番-変更前		varchar2(４)	
	private String h1331;	//H1331		加入者番号-変更前		varchar2(４)	
	private String h1332;	//H1332		市外局番-変更後		varchar2(５)	
	private String h1333;	//H1333		市内局番-変更後		varchar2(４)	
	private String h1334;	//H1334		加入者番号-変更後		varchar2(４)	
	private String h1335;	//H1335		FAX番号_変更前		varchar2(15)	
	private String h1336;	//H1336		FAX番号_変更後		varchar2(15)	
	private String h1337;	//H1337		内容確認				varchar2(300)	
	private int h1338;		//H1338		高圧連絡先情報ID		number			NOT NULL
	private Date h1339;		//H1339		登録日				date	
	private Date h1340;		//H1340		更新日				date	
	
	public int getH1301() {
		return h1301;
	}
	public void setH1301(int h1301) {
		this.h1301 = h1301;
	}
	public String getH1302() {
		return h1302;
	}
	public void setH1302(String h1302) {
		this.h1302 = h1302;
	}
	public String getH1303() {
		return h1303;
	}
	public void setH1303(String h1303) {
		this.h1303 = h1303;
	}
	public String getH1304() {
		return h1304;
	}
	public void setH1304(String h1304) {
		this.h1304 = h1304;
	}
	public String getH1305() {
		return h1305;
	}
	public void setH1305(String h1305) {
		this.h1305 = h1305;
	}
	public String getH1306() {
		return h1306;
	}
	public void setH1306(String h1306) {
		this.h1306 = h1306;
	}
	public String getH1307() {
		return h1307;
	}
	public void setH1307(String h1307) {
		this.h1307 = h1307;
	}
	public String getH1308() {
		return h1308;
	}
	public void setH1308(String h1308) {
		this.h1308 = h1308;
	}
	public String getH1309() {
		return h1309;
	}
	public void setH1309(String h1309) {
		this.h1309 = h1309;
	}
	public String getH1310() {
		return h1310;
	}
	public void setH1310(String h1310) {
		this.h1310 = h1310;
	}
	public String getH1311() {
		return h1311;
	}
	public void setH1311(String h1311) {
		this.h1311 = h1311;
	}
	public String getH1312() {
		return h1312;
	}
	public void setH1312(String h1312) {
		this.h1312 = h1312;
	}
	public String getH1313() {
		return h1313;
	}
	public void setH1313(String h1313) {
		this.h1313 = h1313;
	}
	public String getH1314() {
		return h1314;
	}
	public void setH1314(String h1314) {
		this.h1314 = h1314;
	}
	public String getH1315() {
		return h1315;
	}
	public void setH1315(String h1315) {
		this.h1315 = h1315;
	}
	public String getH1316() {
		return h1316;
	}
	public void setH1316(String h1316) {
		this.h1316 = h1316;
	}
	public String getH1317() {
		return h1317;
	}
	public void setH1317(String h1317) {
		this.h1317 = h1317;
	}
	public String getH1318() {
		return h1318;
	}
	public void setH1318(String h1318) {
		this.h1318 = h1318;
	}
	public String getH1319() {
		return h1319;
	}
	public void setH1319(String h1319) {
		this.h1319 = h1319;
	}
	public String getH1320() {
		return h1320;
	}
	public void setH1320(String h1320) {
		this.h1320 = h1320;
	}
	public String getH1321() {
		return h1321;
	}
	public void setH1321(String h1321) {
		this.h1321 = h1321;
	}
	public String getH1322() {
		return h1322;
	}
	public void setH1322(String h1322) {
		this.h1322 = h1322;
	}
	public String getH1323() {
		return h1323;
	}
	public void setH1323(String h1323) {
		this.h1323 = h1323;
	}
	public String getH1324() {
		return h1324;
	}
	public void setH1324(String h1324) {
		this.h1324 = h1324;
	}
	public String getH1325() {
		return h1325;
	}
	public void setH1325(String h1325) {
		this.h1325 = h1325;
	}
	public String getH1326() {
		return h1326;
	}
	public void setH1326(String h1326) {
		this.h1326 = h1326;
	}
	public String getH1327() {
		return h1327;
	}
	public void setH1327(String h1327) {
		this.h1327 = h1327;
	}
	public String getH1328() {
		return h1328;
	}
	public void setH1328(String h1328) {
		this.h1328 = h1328;
	}
	public String getH1329() {
		return h1329;
	}
	public void setH1329(String h1329) {
		this.h1329 = h1329;
	}
	public String getH1330() {
		return h1330;
	}
	public void setH1330(String h1330) {
		this.h1330 = h1330;
	}
	public String getH1331() {
		return h1331;
	}
	public void setH1331(String h1331) {
		this.h1331 = h1331;
	}
	public String getH1332() {
		return h1332;
	}
	public void setH1332(String h1332) {
		this.h1332 = h1332;
	}
	public String getH1333() {
		return h1333;
	}
	public void setH1333(String h1333) {
		this.h1333 = h1333;
	}
	public String getH1334() {
		return h1334;
	}
	public void setH1334(String h1334) {
		this.h1334 = h1334;
	}
	public String getH1335() {
		return h1335;
	}
	public void setH1335(String h1335) {
		this.h1335 = h1335;
	}
	public String getH1336() {
		return h1336;
	}
	public void setH1336(String h1336) {
		this.h1336 = h1336;
	}
	public String getH1337() {
		return h1337;
	}
	public void setH1337(String h1337) {
		this.h1337 = h1337;
	}
	public int getH1338() {
		return h1338;
	}
	public void setH1338(int h1338) {
		this.h1338 = h1338;
	}
	public Date getH1339() {
		return h1339;
	}
	public void setH1339(Date h1339) {
		this.h1339 = h1339;
	}
	public Date getH1340() {
		return h1340;
	}
	public void setH1340(Date h1340) {
		this.h1340 = h1340;
	}
	
	@Override
	public String toString() {
		return "ContactDetailInfoHV013 [h1301=" + h1301 + ", h1302=" + h1302
				+ ", h1303=" + h1303 + ", h1304=" + h1304 + ", h1305=" + h1305
				+ ", h1306=" + h1306 + ", h1307=" + h1307 + ", h1308=" + h1308
				+ ", h1309=" + h1309 + ", h1310=" + h1310 + ", h1311=" + h1311
				+ ", h1312=" + h1312 + ", h1313=" + h1313 + ", h1314=" + h1314
				+ ", h1315=" + h1315 + ", h1316=" + h1316 + ", h1317=" + h1317
				+ ", h1318=" + h1318 + ", h1319=" + h1319 + ", h1320=" + h1320
				+ ", h1321=" + h1321 + ", h1322=" + h1322 + ", h1323=" + h1323
				+ ", h1324=" + h1324 + ", h1325=" + h1325 + ", h1326=" + h1326
				+ ", h1327=" + h1327 + ", h1328=" + h1328 + ", h1329=" + h1329
				+ ", h1330=" + h1330 + ", h1331=" + h1331 + ", h1332=" + h1332
				+ ", h1333=" + h1333 + ", h1334=" + h1334 + ", h1335=" + h1335
				+ ", h1336=" + h1336 + ", h1337=" + h1337 + ", h1338=" + h1338
				+ ", h1339=" + h1339 + ", h1340=" + h1340 + "]";
	}
	
	
}
