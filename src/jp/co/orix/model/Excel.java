package jp.co.orix.model;

public class Excel {
	private int M0401;		// 년월
	private String M0620;		// 파일명
	private String M0621;		// 경로
	
	
	public Excel(){}
	public int getM0401() {
		return M0401;
	}
	public void setM0401(int m0401) {
		M0401 = m0401;
	}
	public String getM0620() {
		return M0620;
	}
	public void setM0620(String m0620) {
		M0620 = m0620;
	}
	public String getM0621() {
		return M0621;
	}
	public void setM0621(String m0621) {
		M0621 = m0621;
	}
	@Override
	public String toString() {
		return "Excel [M0401=" + M0401 + ", M0620=" + M0620 + ", M0621="
				+ M0621 + "]";
	}


	
}
