package jp.co.orix.model;

import java.sql.Date;

public class UserMail {
	//ユーザー識別番号
	private int M0101;
	//メール登録日時
	private Date M0102;
	//SEQ
	private int M0103;
	//メールアドレス
	private String M0200;
	//有効
	private String M0301;
	//最終メール通知日時
	private Date M0400;
	//登録セッションＩＤ
	private String M9902;
	//削除日時
	private Date M9991;
	//削除セッションＩＤ
	private String M9992;
	
	public UserMail() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public UserMail(int m0101, Date m0102, int m0103, String m0200,
			String m0301, Date m0400, String m9902, Date m9991, String m9992) {
		super();
		M0101 = m0101;
		M0102 = m0102;
		M0103 = m0103;
		M0200 = m0200;
		M0301 = m0301;
		M0400 = m0400;
		M9902 = m9902;
		M9991 = m9991;
		M9992 = m9992;
	}

	public int getM0101() {
		return M0101;
	}

	public void setM0101(int m0101) {
		M0101 = m0101;
	}

	public Date getM0102() {
		return M0102;
	}

	public void setM0102(Date m0102) {
		M0102 = m0102;
	}

	public int getM0103() {
		return M0103;
	}

	public void setM0103(int m0103) {
		M0103 = m0103;
	}

	public String getM0200() {
		return M0200;
	}

	public void setM0200(String m0200) {
		M0200 = m0200;
	}

	public String getM0301() {
		return M0301;
	}

	public void setM0301(String m0301) {
		M0301 = m0301;
	}

	public Date getM0400() {
		return M0400;
	}

	public void setM0400(Date m0400) {
		M0400 = m0400;
	}

	public String getM9902() {
		return M9902;
	}

	public void setM9902(String m9902) {
		M9902 = m9902;
	}

	public Date getM9991() {
		return M9991;
	}

	public void setM9991(Date m9991) {
		M9991 = m9991;
	}

	public String getM9992() {
		return M9992;
	}

	public void setM9992(String m9992) {
		M9992 = m9992;
	}

	@Override
	public String toString() {
		return "UserMail [M0101=" + M0101 + ", M0102=" + M0102 + ", M0103="
				+ M0103 + ", M0200=" + M0200 + ", M0301=" + M0301 + ", M0400="
				+ M0400 + ", M9902=" + M9902 + ", M9991=" + M9991 + ", M9992="
				+ M9992 + "]";
	}

	
}
