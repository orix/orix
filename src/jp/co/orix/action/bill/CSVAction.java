package jp.co.orix.action.bill;

import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import jp.co.orix.dao.imp.BillDaoImp;
import jp.co.orix.model.Bill;
import jp.co.orix.util.CSVForm;
import jp.co.orix.util.CommApi_32;
import jp.co.orix.util.Util;

import org.apache.struts2.ServletActionContext;
import org.apache.struts2.interceptor.ServletRequestAware;
import org.apache.struts2.interceptor.ServletResponseAware;
import org.apache.struts2.util.ServletContextAware;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class CSVAction  extends ActionSupport  implements ServletContextAware{
	
	private HashMap<String, Object> data = new HashMap<String, Object>();
	private BillDaoImp daoImp = new BillDaoImp();
	public Util util = new Util();
	private ServletContext context;
	private String date;
	
	public String csvDownload()  throws Exception {
		// TODO Auto-generated method stub
	
		SimpleDateFormat format1 = new SimpleDateFormat("yyyy/MM");
		SimpleDateFormat format2 = new SimpleDateFormat("yyyyMM");
		HttpServletRequest request = ServletActionContext.getRequest();
		
		String savePath  = context.getRealPath("/").concat("resource/csv/");
		
		 //파일 확장자 정보 계약번호_yyyyMM_정구서정보.csv
		/*
		 * 	Integer billId, 
		 *  String group, 
		 *  String area, 
		 *  String uesDate) {
		 * */
		
		CommApi_32 api = new CommApi_32();
		api.setFormParamator(request, "bill");
		HashMap<String, Object> param = api.getFormParamator("bill");
				
//		Integer billNumber = Integer.parseInt((String)hashmap.get("billNumber"));
		String group= (String)param.get("group");
		String place =  (String)param.get("place");
		Integer type = Integer.parseInt((String)param.get("type"));
		date = (String)param.get("useDate");
		
		Bill bill = null;
		
		if (type == 1) {
			bill = daoImp.findByBillAll(format1.parse(date));
		}else{
			bill = daoImp.findByBillInfo(group, place, format1.parse(date), null);
		}
		
		
		
		if (bill != null) {
			String fileName = bill.getH0601() + "_" +format2.format(format1.parse(date))+"_" +  "請求書情報.csv";
			
			//CSV download
			try {
				
				FileWriter writer = new FileWriter(savePath+fileName);
				
		        CSVForm.writeLine(writer, Arrays.asList(bill.csvString()));
		        
		        writer.flush();
		        writer.close();
		        
				data.put("fileName", fileName);
				data.put("success", true);
			
				
			} catch (Exception e) {
				// TODO: handle exception
				System.err.println("ERROR : " + e.getMessage());
				data.put("success", false);
			}
			
		}else{
			data.put("success", false);
			data.put("message", "");
		}
													
		
		
		return SUCCESS;
	}
	
	

	public HashMap<String, Object> getData() {
		return data;
	}


	public void setData(HashMap<String, Object> data) {
		this.data = data;
	}
	
//	@Override
//	public void setServletRequest(HttpServletRequest request) {
//		// TODO Auto-generated method stub
//		this.request = request;
//	}
//
//
//	@Override
//	public void setServletResponse(HttpServletResponse response) {
//		// TODO Auto-generated method stub
//		this.response = response; 
//	}

	@Override
	public void setServletContext(ServletContext context) {
		// TODO Auto-generated method stub
		this.context = context;
	}

	public String getDate() {
		return date;
	}

	public void setDate(String date) {
		this.date = date;
	}



	
}
