package jp.co.orix.action.modify;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import jp.co.orix.dao.ModifyDao_040;
import jp.co.orix.model.KubunCombobox;
import jp.co.orix.model.ModifyVo;
import jp.co.orix.model.UsePlace;
import jp.co.orix.model.UsePlaceCombobox;

import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

public class Modify_040 extends ActionSupport {
	
	String H0804;
	ModifyDao_040 modifyDao;	
	ArrayList<UsePlaceCombobox> usePlaceComboboxList;
	ArrayList<KubunCombobox> kubunComboboxList;
	UsePlace usePlace;
	Map<String, Integer> KubunComboParam;
	ModifyVo modifyvo;

	public ModifyDao_040 getModifyDao() {		return modifyDao;	}
	public void setModifyDao(ModifyDao_040 modifyDao) {		this.modifyDao = modifyDao;	}
	public ArrayList<UsePlaceCombobox> getUsePlaceComboboxList() {		return usePlaceComboboxList;	}
	public void setUsePlaceComboboxList(ArrayList<UsePlaceCombobox> usePlaceComboboxList) {	this.usePlaceComboboxList = usePlaceComboboxList;}
	public String getH0804() {		return H0804;	}
	public void setH0804(String h0804) {		H0804 = h0804;	}
	public UsePlace getUsePlace() {		return usePlace;	}
	public void setUsePlace(UsePlace usePlace) {		this.usePlace = usePlace;	}
	public ArrayList<KubunCombobox> getKubunComboboxList() {		return kubunComboboxList;}
	public void setKubunComboboxList(ArrayList<KubunCombobox> kubunComboboxList) {		this.kubunComboboxList = kubunComboboxList;}
	
	public Map<String, Integer> getKubunComboParam() {		return KubunComboParam;	}
	public void setKubunComboParam(Map<String, Integer> kubunComboParam) {		KubunComboParam = kubunComboParam;	}
	
	
	public ModifyVo getModifyvo() {		return modifyvo;	}
	public void setModifyvo(ModifyVo modifyvo) {		this.modifyvo = modifyvo;	}
	
	@Override
	public String execute() throws Exception {
		// TODO Auto-generated method stub
		return SUCCESS;
	}

	
	
	public String view() throws Exception {
		
		usePlaceComboboxList = new ArrayList<UsePlaceCombobox>();
		usePlaceComboboxList = modifyDao.GetUsePlaceComboboxList();
		
		kubunComboboxList = getKubunCombo();
		
		return SUCCESS;
	}
	
	public String search() throws Exception{
		
		usePlaceComboboxList = new ArrayList<UsePlaceCombobox>();
		usePlaceComboboxList = modifyDao.GetUsePlaceComboboxList();
		
		usePlace = modifyDao.GetUsePlaceUsingH0804(H0804);
		
		kubunComboboxList = getKubunCombo();
		
				
		return SUCCESS;
	}
	
	public ArrayList<KubunCombobox> getKubunCombo() throws Exception{
		
		KubunComboParam = new HashMap<String, Integer>();
		KubunComboParam.put("Equipment1",50002);
		KubunComboParam.put("Equipment2", 50003);
		KubunComboParam.put("ChangeKubun", 50004);
		
		kubunComboboxList = modifyDao.GetKubunComboboxList(KubunComboParam);
		
		return kubunComboboxList;
	}
	
	public String Confirm() throws Exception{
		
		ActionContext context = ActionContext.getContext();
		Map session = (Map)context.getSession();
		
		session.put("modifyvo", modifyvo);
		context.setSession(session);
		
		return SUCCESS;
	}
	
	public String Insert() throws Exception{
		
		
		ActionContext context = ActionContext.getContext();
		Map session = (Map)context.getSession();
		
		modifyvo = (ModifyVo) session.get("modifyvo");
		
		modifyDao.ModifyPlace(modifyvo);
		
		return SUCCESS;
	}

	
}
