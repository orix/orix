$(document).ready(function() {
	$('#groupCombobox').change(function(event) {
		var groupComboboxCode = $("select#groupCombobox").val();
		//alert(groupComboboxCode);
		$.getJSON('getUsePlaceComboboxList', {
			groupComboboxCode : groupComboboxCode
		}, function(jsonResponse) {
			/* $('#ajaxResponse').text(jsonResponse.dummyMsg); */
			var select = $('#usePlaceCombobox');
			//alert(select);
			select.find('option').remove();
			$.each(jsonResponse.usePlaceMap, function(key, value) {
				$('<option>').val(key).text(value).appendTo(select);
			});
		});
	});
});