package jp.co.orix.dao;
/**
 * @内容　「GetGroupComboboxList」と「GetUsePlaceComboboxList」のＡＰＩに関するＤＡＯ
 * @author Jongmoo Kim
 */
import java.util.List;
import java.util.Map;

import jp.co.orix.model.UniversalInfoITHNT000;

public interface GetGroupAndPlaceDao {

	public List<UniversalInfoITHNT000> getGroupComboboxList(String valueOfcolum);
	public List<UniversalInfoITHNT000> getUsePlaceComboboxList(
			Map<String, String> condition);
	public List<UniversalInfoITHNT000> getAllUsePlaceComboboxList();
	
}
