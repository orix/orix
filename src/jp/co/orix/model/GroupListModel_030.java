package jp.co.orix.model;

import java.util.Date;

public class GroupListModel_030 {
	
	private int h0601;
	private String h0602;
	private String h0603;
	private int h0604;
	private String h0605;
	private String h0606;
	private Date h0607;
	private int h0608;
	private String h0609;
	private String h0610;
	private String h0611;
	private Date h0612;
	private int h0613;
	private int h0614;
	private int h0615;
	private int h0616;
	private int h0617;
	private int h0618;
	private int h0619;
	private int h0620;
	private int h0621;
	private int h0622;
	private int h0623;
	private int h0624;
	private int h0625;
	private int h0626;
	private int h0627;
	private int h0628;
	private int h0629;
	private int h0630;
	private int h0631;
	private int h0632;
	private int h0633;
	private int h0634;
	private int h0635;
	private int h0636;
	private int h0637;
	private int h0638;
	private int h0639;
	private int h0640;
	private int h0641;
	private int h0642;
	private int h0643;
	private int h0644;
	private Date h0645;
	private Date h0646;
	private String h0647;
	private String h0648;
	
	public GroupListModel_030() {
	}
	
	

	public GroupListModel_030(int h0601, String h0602, String h0603, int h0604,
			String h0605, String h0606, Date h0607, int h0608, String h0609,
			String h0610, String h0611, Date h0612, int h0613, int h0614,
			int h0615, int h0616, int h0617, int h0618, int h0619, int h0620,
			int h0621, int h0622, int h0623, int h0624, int h0625, int h0626,
			int h0627, int h0628, int h0629, int h0630, int h0631, int h0632,
			int h0633, int h0634, int h0635, int h0636, int h0637, int h0638,
			int h0639, int h0640, int h0641, int h0642, int h0643, int h0644,
			Date h0645, Date h0646, String h0647, String h0648) {
		this.h0601 = h0601;
		this.h0602 = h0602;
		this.h0603 = h0603;
		this.h0604 = h0604;
		this.h0605 = h0605;
		this.h0606 = h0606;
		this.h0607 = h0607;
		this.h0608 = h0608;
		this.h0609 = h0609;
		this.h0610 = h0610;
		this.h0611 = h0611;
		this.h0612 = h0612;
		this.h0613 = h0613;
		this.h0614 = h0614;
		this.h0615 = h0615;
		this.h0616 = h0616;
		this.h0617 = h0617;
		this.h0618 = h0618;
		this.h0619 = h0619;
		this.h0620 = h0620;
		this.h0621 = h0621;
		this.h0622 = h0622;
		this.h0623 = h0623;
		this.h0624 = h0624;
		this.h0625 = h0625;
		this.h0626 = h0626;
		this.h0627 = h0627;
		this.h0628 = h0628;
		this.h0629 = h0629;
		this.h0630 = h0630;
		this.h0631 = h0631;
		this.h0632 = h0632;
		this.h0633 = h0633;
		this.h0634 = h0634;
		this.h0635 = h0635;
		this.h0636 = h0636;
		this.h0637 = h0637;
		this.h0638 = h0638;
		this.h0639 = h0639;
		this.h0640 = h0640;
		this.h0641 = h0641;
		this.h0642 = h0642;
		this.h0643 = h0643;
		this.h0644 = h0644;
		this.h0645 = h0645;
		this.h0646 = h0646;
		this.h0647 = h0647;
		this.h0648 = h0648;
	}


	public int getH0601() {
		return h0601;
	}

	public void setH0601(int h0601) {
		this.h0601 = h0601;
	}

	public String getH0602() {
		return h0602;
	}

	public void setH0602(String h0602) {
		this.h0602 = h0602;
	}

	public String getH0603() {
		return h0603;
	}

	public void setH0603(String h0603) {
		this.h0603 = h0603;
	}

	public int getH0604() {
		return h0604;
	}

	public void setH0604(int h0604) {
		this.h0604 = h0604;
	}

	public String getH0605() {
		return h0605;
	}

	public void setH0605(String h0605) {
		this.h0605 = h0605;
	}

	public String getH0606() {
		return h0606;
	}

	public void setH0606(String h0606) {
		this.h0606 = h0606;
	}

	public Date getH0607() {
		return h0607;
	}

	public void setH0607(Date h0607) {
		this.h0607 = h0607;
	}

	public int getH0608() {
		return h0608;
	}

	public void setH0608(int h0608) {
		this.h0608 = h0608;
	}

	public String getH0609() {
		return h0609;
	}

	public void setH0609(String h0609) {
		this.h0609 = h0609;
	}

	public String getH0610() {
		return h0610;
	}

	public void setH0610(String h0610) {
		this.h0610 = h0610;
	}

	public String getH0611() {
		return h0611;
	}

	public void setH0611(String h0611) {
		this.h0611 = h0611;
	}

	public Date getH0612() {
		return h0612;
	}

	public void setH0612(Date h0612) {
		this.h0612 = h0612;
	}

	public int getH0613() {
		return h0613;
	}

	public void setH0613(int h0613) {
		this.h0613 = h0613;
	}

	public int getH0614() {
		return h0614;
	}

	public void setH0614(int h0614) {
		this.h0614 = h0614;
	}

	public int getH0615() {
		return h0615;
	}

	public void setH0615(int h0615) {
		this.h0615 = h0615;
	}

	public int getH0616() {
		return h0616;
	}

	public void setH0616(int h0616) {
		this.h0616 = h0616;
	}

	public int getH0617() {
		return h0617;
	}

	public void setH0617(int h0617) {
		this.h0617 = h0617;
	}

	public int getH0618() {
		return h0618;
	}

	public void setH0618(int h0618) {
		this.h0618 = h0618;
	}

	public int getH0619() {
		return h0619;
	}

	public void setH0619(int h0619) {
		this.h0619 = h0619;
	}

	public int getH0620() {
		return h0620;
	}

	public void setH0620(int h0620) {
		this.h0620 = h0620;
	}

	public int getH0621() {
		return h0621;
	}

	public void setH0621(int h0621) {
		this.h0621 = h0621;
	}

	public int getH0622() {
		return h0622;
	}

	public void setH0622(int h0622) {
		this.h0622 = h0622;
	}

	public int getH0623() {
		return h0623;
	}

	public void setH0623(int h0623) {
		this.h0623 = h0623;
	}

	public int getH0624() {
		return h0624;
	}

	public void setH0624(int h0624) {
		this.h0624 = h0624;
	}

	public int getH0625() {
		return h0625;
	}

	public void setH0625(int h0625) {
		this.h0625 = h0625;
	}

	public int getH0626() {
		return h0626;
	}

	public void setH0626(int h0626) {
		this.h0626 = h0626;
	}

	public int getH0627() {
		return h0627;
	}

	public void setH0627(int h0627) {
		this.h0627 = h0627;
	}

	public int getH0628() {
		return h0628;
	}

	public void setH0628(int h0628) {
		this.h0628 = h0628;
	}

	public int getH0629() {
		return h0629;
	}

	public void setH0629(int h0629) {
		this.h0629 = h0629;
	}

	public int getH0630() {
		return h0630;
	}

	public void setH0630(int h0630) {
		this.h0630 = h0630;
	}

	public int getH0631() {
		return h0631;
	}

	public void setH0631(int h0631) {
		this.h0631 = h0631;
	}

	public int getH0632() {
		return h0632;
	}

	public void setH0632(int h0632) {
		this.h0632 = h0632;
	}

	public int getH0633() {
		return h0633;
	}

	public void setH0633(int h0633) {
		this.h0633 = h0633;
	}

	public int getH0634() {
		return h0634;
	}

	public void setH0634(int h0634) {
		this.h0634 = h0634;
	}

	public int getH0635() {
		return h0635;
	}

	public void setH0635(int h0635) {
		this.h0635 = h0635;
	}

	public int getH0636() {
		return h0636;
	}

	public void setH0636(int h0636) {
		this.h0636 = h0636;
	}

	public int getH0637() {
		return h0637;
	}

	public void setH0637(int h0637) {
		this.h0637 = h0637;
	}

	public int getH0638() {
		return h0638;
	}

	public void setH0638(int h0638) {
		this.h0638 = h0638;
	}

	public int getH0639() {
		return h0639;
	}

	public void setH0639(int h0639) {
		this.h0639 = h0639;
	}

	public int getH0640() {
		return h0640;
	}

	public void setH0640(int h0640) {
		this.h0640 = h0640;
	}

	public int getH0641() {
		return h0641;
	}

	public void setH0641(int h0641) {
		this.h0641 = h0641;
	}

	public int getH0642() {
		return h0642;
	}

	public void setH0642(int h0642) {
		this.h0642 = h0642;
	}

	public int getH0643() {
		return h0643;
	}

	public void setH0643(int h0643) {
		this.h0643 = h0643;
	}

	public int getH0644() {
		return h0644;
	}

	public void setH0644(int h0644) {
		this.h0644 = h0644;
	}

	public Date getH0645() {
		return h0645;
	}

	public void setH0645(Date h0645) {
		this.h0645 = h0645;
	}

	public Date getH0646() {
		return h0646;
	}

	public void setH0646(Date h0646) {
		this.h0646 = h0646;
	}

	public String getH0647() {
		return h0647;
	}

	public void setH0647(String h0647) {
		this.h0647 = h0647;
	}

	public String getH0648() {
		return h0648;
	}

	public void setH0648(String h0648) {
		this.h0648 = h0648;
	}

	@Override
	public String toString() {
		return "GroupListModel_023 [h0601=" + h0601 + ", h0602=" + h0602
				+ ", h0603=" + h0603 + ", h0607=" + h0607 + "]";
	}
	
	
}
