package jp.co.orix.model;
/**
 * @汎用テーブルに関するＶＯ
 * @author Jongmoo Kim
 */
import java.util.Date;

public class UniversalInfo {

	private String t0000;	//T0000 取消サイン
	private String t0100;	//T0100 区分
	private String t0200;	//T0200 コード
	private Date t0300;		//T0300 作成日・変更日
	private int	t0400;		//T0400 桁数
	private String t0401;	//T0401 値
	private String t0402;	//T0402 略称値
	private String t0500;	//T0500 親コード
	private Date t9971;		//T9971 外部Ｉ／Ｆ日時
	private String t9972;	//T9972 外部Ｉ／ＦセッションＩＤ
	
	public String getT0000() {
		return t0000;
	}
	public void setT0000(String t0000) {
		this.t0000 = t0000;
	}
	public String getT0100() {
		return t0100;
	}
	public void setT0100(String t0100) {
		this.t0100 = t0100;
	}
	public String getT0200() {
		return t0200;
	}
	public void setT0200(String t0200) {
		this.t0200 = t0200;
	}
	public Date getT0300() {
		return t0300;
	}
	public void setT0300(Date t0300) {
		this.t0300 = t0300;
	}
	public int getT0400() {
		return t0400;
	}
	public void setT0400(int t0400) {
		this.t0400 = t0400;
	}
	public String getT0401() {
		return t0401;
	}
	public void setT0401(String t0401) {
		this.t0401 = t0401;
	}
	public String getT0402() {
		return t0402;
	}
	public void setT0402(String t0402) {
		this.t0402 = t0402;
	}
	public String getT0500() {
		return t0500;
	}
	public void setT0500(String t0500) {
		this.t0500 = t0500;
	}
	public Date getT9971() {
		return t9971;
	}
	public void setT9971(Date t9971) {
		this.t9971 = t9971;
	}
	public String getT9972() {
		return t9972;
	}
	public void setT9972(String t9972) {
		this.t9972 = t9972;
	}

	@Override
	public String toString() {
		return "UniversalInfo [t0000=" + t0000 + ", t0100=" + t0100
				+ ", t0200=" + t0200 + ", t0300=" + t0300 + ", t0400=" + t0400
				+ ", t0401=" + t0401 + ", t0402=" + t0402 + ", t0500=" + t0500
				+ ", t9971=" + t9971 + ", t9972=" + t9972 + "]";
	}

}
