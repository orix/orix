package jp.co.orix.dao.imp;

import java.util.HashMap;
import java.util.Map;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.PaymentInfoDao;
/*import jp.co.orix.dbconn.SqlSessionFactoryService;*/
import jp.co.orix.model.IMACP000;
import jp.co.orix.model.UserPaymentInfo;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

public class PaymentInfoDaoImp implements PaymentInfoDao {
	
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	
	private UserPaymentInfo userPaymentInfo;
	private Map paymentMethodInfoResult = new HashMap<>();
	
	private int countRecordOfIDUMS000;
	private int countRecordOfIMACP000;

	@Override
	public UserPaymentInfo getUserInfo(String HV011_location) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			userPaymentInfo = dao.getUserInfo(HV011_location);  
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
		return userPaymentInfo;
		
	}
	
	@Override
	public Map getKubunName(Map paymentMethodInfoSet) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			paymentMethodInfoResult = dao.getKubunName(paymentMethodInfoSet); 
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
		return paymentMethodInfoResult;
	}
	
	@Override
	public void changeUserPaymentInfo_HV011(UserPaymentInfo userPaymentInfo) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			dao.changeUserPaymentInfo_HV011(userPaymentInfo); 
			session.commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
	}
	
	@Override
	public void changeUserPaymentInfo_CM004(UserPaymentInfo userPaymentInfo) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			dao.changeUserPaymentInfo_CM004(userPaymentInfo);  
			session.commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
	}
	
	@Override
	public int countRecordOfIDUMS000() {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			countRecordOfIDUMS000 = dao.countRecordOfIDUMS000();  
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		return countRecordOfIDUMS000;
	}
	
	@Override
	public int countRecordOfIMACP000() {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			countRecordOfIMACP000 = dao.countRecordOfIMACP000();  
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		return countRecordOfIMACP000;
	}
	
	


	@Override
	public void changeUserPaymentInfo_IDUMS000(UserPaymentInfo userPaymentInfo) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			dao.changeUserPaymentInfo_IDUMS000(userPaymentInfo);  
			session.commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
	}
	
	@Override
	public void getInsertIMACP000SQL(IMACP000 imacp000) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			dao.getInsertIMACP000SQL(imacp000);  
			session.commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
			
		}finally{
			session.close();
		}
	}
	
	// All
	@Override
	public void changeUserPaymentInfo_All_HV011(UserPaymentInfo userPaymentInfo) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			dao.changeUserPaymentInfo_All_HV011(userPaymentInfo);  
			session.commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
		
	}

	@Override
	public void changeUserPaymentInfo_All_CM004(UserPaymentInfo userPaymentInfo) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			dao.changeUserPaymentInfo_All_CM004(userPaymentInfo); 
			session.commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
	}

	@Override
	public void changeUserPaymentInfo_All_IDUMS000(
			UserPaymentInfo userPaymentInfo) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			dao.changeUserPaymentInfo_All_IDUMS000(userPaymentInfo); 
			session.commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
	}

	@Override
	public void getInsertIMACP000SQL_All(IMACP000 imacp000) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		PaymentInfoDao dao = session.getMapper(PaymentInfoDao.class);
		
		try {
			dao.getInsertIMACP000SQL_All(imacp000); 
			session.commit();
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
			
		}finally{
			session.close();
		}
		
	}


	public UserPaymentInfo getUserPaymentInfo() {
		
		
		
		return userPaymentInfo;
	}

	public void setUserPaymentInfo(UserPaymentInfo userPaymentInfo) {
		this.userPaymentInfo = userPaymentInfo;
	}

	public Map getPaymentMethodInfoResult() {
		return paymentMethodInfoResult;
	}

	public void setPaymentMethodInfoResult(Map paymentMethodInfoResult) {
		this.paymentMethodInfoResult = paymentMethodInfoResult;
	}

	public int getCountRecordOfIDUMS000() {
		return countRecordOfIDUMS000;
	}

	public void setCountRecordOfIDUMS000(int countRecordOfIDUMS000) {
		this.countRecordOfIDUMS000 = countRecordOfIDUMS000;
	}

	public int getCountRecordOfIMACP000() {
		return countRecordOfIMACP000;
	}

	public void setCountRecordOfIMACP000(int countRecordOfIMACP000) {
		this.countRecordOfIMACP000 = countRecordOfIMACP000;
	}

}
