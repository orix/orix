package jp.co.orix.commons;

import org.apache.ibatis.annotations.Param;

public interface CommonAPIDao_030 {
	
	public void getUpdateUserNameSql(@Param("oldUserName") String oldUserName,
			                         @Param("newUserName") String newUserName,	
									 @Param("userNumber") int userNumber,
									 @Param("lastSessionId") String lastSessionId);
	
	public int getLocationData();
	
	public String getContractNumber(@Param("usePlace") String usePlace);
	
	/*public Object getFormParamenter();
	public Object getGroupComboboxList();
	public Object getUsePlaceComboboxList();
	
	public Object isValidCombobox();
	public Object getAllFileResult();*/
};
