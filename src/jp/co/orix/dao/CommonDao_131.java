package jp.co.orix.dao;

import java.util.List;

import jp.co.orix.model.UniversalInfoITHNT000;

import org.apache.ibatis.annotations.Param;

public interface CommonDao_131 {

	public List<UniversalInfoITHNT000> getSystemPortalClass(@Param("T0100") String T0100, @Param("T0200") String T0200); 
	
}
