package jp.co.orix.model;

public class SinseiInfo2 {

	private String jukyuKanriInfo_HojinName;
	private String jukyuKanriInfo_BushoName;
	private String jukyuKanriInfo_Tanto1;
	private String jukyuKanriInfo_Tanto2;
	private String jukyuKanriInfo_Tel1;
	private String jukyuKanriInfo_Tel2;
	private String jukyuKanriInfo_Tel3;
	private String jukyuKanriInfo_Fax;
	private String jukyuKanriInfo_MailAddress1;
	private String jukyuKanriInfo_MailAddress2;
	public String getJukyuKanriInfo_HojinName() {
		return jukyuKanriInfo_HojinName;
	}
	public void setJukyuKanriInfo_HojinName(String jukyuKanriInfo_HojinName) {
		this.jukyuKanriInfo_HojinName = jukyuKanriInfo_HojinName;
	}
	public String getJukyuKanriInfo_BushoName() {
		return jukyuKanriInfo_BushoName;
	}
	public void setJukyuKanriInfo_BushoName(String jukyuKanriInfo_BushoName) {
		this.jukyuKanriInfo_BushoName = jukyuKanriInfo_BushoName;
	}
	public String getJukyuKanriInfo_Tanto1() {
		return jukyuKanriInfo_Tanto1;
	}
	public void setJukyuKanriInfo_Tanto1(String jukyuKanriInfo_Tanto1) {
		this.jukyuKanriInfo_Tanto1 = jukyuKanriInfo_Tanto1;
	}
	public String getJukyuKanriInfo_Tanto2() {
		return jukyuKanriInfo_Tanto2;
	}
	public void setJukyuKanriInfo_Tanto2(String jukyuKanriInfo_Tanto2) {
		this.jukyuKanriInfo_Tanto2 = jukyuKanriInfo_Tanto2;
	}
	public String getJukyuKanriInfo_Tel1() {
		return jukyuKanriInfo_Tel1;
	}
	public void setJukyuKanriInfo_Tel1(String jukyuKanriInfo_Tel1) {
		this.jukyuKanriInfo_Tel1 = jukyuKanriInfo_Tel1;
	}
	public String getJukyuKanriInfo_Tel2() {
		return jukyuKanriInfo_Tel2;
	}
	public void setJukyuKanriInfo_Tel2(String jukyuKanriInfo_Tel2) {
		this.jukyuKanriInfo_Tel2 = jukyuKanriInfo_Tel2;
	}
	public String getJukyuKanriInfo_Tel3() {
		return jukyuKanriInfo_Tel3;
	}
	public void setJukyuKanriInfo_Tel3(String jukyuKanriInfo_Tel3) {
		this.jukyuKanriInfo_Tel3 = jukyuKanriInfo_Tel3;
	}
	public String getJukyuKanriInfo_Fax() {
		return jukyuKanriInfo_Fax;
	}
	public void setJukyuKanriInfo_Fax(String jukyuKanriInfo_Fax) {
		this.jukyuKanriInfo_Fax = jukyuKanriInfo_Fax;
	}
	public String getJukyuKanriInfo_MailAddress1() {
		return jukyuKanriInfo_MailAddress1;
	}
	public void setJukyuKanriInfo_MailAddress1(String jukyuKanriInfo_MailAddress1) {
		this.jukyuKanriInfo_MailAddress1 = jukyuKanriInfo_MailAddress1;
	}
	public String getJukyuKanriInfo_MailAddress2() {
		return jukyuKanriInfo_MailAddress2;
	}
	public void setJukyuKanriInfo_MailAddress2(String jukyuKanriInfo_MailAddress2) {
		this.jukyuKanriInfo_MailAddress2 = jukyuKanriInfo_MailAddress2;
	}

	
	
}
