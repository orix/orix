package jp.co.orix.model;

public class SinseiInfo4 {

	private String keiyakuInfo_HojinName;
	private String keiyakuInfo_BushoName;
	private String keiyakuInfo_Tanto1;
	private String keiyakuInfo_Tanto2;
	private String keiyakuInfo_Tel1;
	private String keiyakuInfo_Tel2;
	private String keiyakuInfo_Tel3;
	private String keiyakuInfo_Fax;
	private String keiyakuInfo_MailAddress1;
	private String keiyakuInfo_MailAddress2;
	public String getKeiyakuInfo_HojinName() {
		return keiyakuInfo_HojinName;
	}
	public void setKeiyakuInfo_HojinName(String keiyakuInfo_HojinName) {
		this.keiyakuInfo_HojinName = keiyakuInfo_HojinName;
	}
	public String getKeiyakuInfo_BushoName() {
		return keiyakuInfo_BushoName;
	}
	public void setKeiyakuInfo_BushoName(String keiyakuInfo_BushoName) {
		this.keiyakuInfo_BushoName = keiyakuInfo_BushoName;
	}
	public String getKeiyakuInfo_Tanto1() {
		return keiyakuInfo_Tanto1;
	}
	public void setKeiyakuInfo_Tanto1(String keiyakuInfo_Tanto1) {
		this.keiyakuInfo_Tanto1 = keiyakuInfo_Tanto1;
	}
	public String getKeiyakuInfo_Tanto2() {
		return keiyakuInfo_Tanto2;
	}
	public void setKeiyakuInfo_Tanto2(String keiyakuInfo_Tanto2) {
		this.keiyakuInfo_Tanto2 = keiyakuInfo_Tanto2;
	}
	public String getKeiyakuInfo_Tel1() {
		return keiyakuInfo_Tel1;
	}
	public void setKeiyakuInfo_Tel1(String keiyakuInfo_Tel1) {
		this.keiyakuInfo_Tel1 = keiyakuInfo_Tel1;
	}
	public String getKeiyakuInfo_Tel2() {
		return keiyakuInfo_Tel2;
	}
	public void setKeiyakuInfo_Tel2(String keiyakuInfo_Tel2) {
		this.keiyakuInfo_Tel2 = keiyakuInfo_Tel2;
	}
	public String getKeiyakuInfo_Tel3() {
		return keiyakuInfo_Tel3;
	}
	public void setKeiyakuInfo_Tel3(String keiyakuInfo_Tel3) {
		this.keiyakuInfo_Tel3 = keiyakuInfo_Tel3;
	}
	public String getKeiyakuInfo_Fax() {
		return keiyakuInfo_Fax;
	}
	public void setKeiyakuInfo_Fax(String keiyakuInfo_Fax) {
		this.keiyakuInfo_Fax = keiyakuInfo_Fax;
	}
	public String getKeiyakuInfo_MailAddress1() {
		return keiyakuInfo_MailAddress1;
	}
	public void setKeiyakuInfo_MailAddress1(String keiyakuInfo_MailAddress1) {
		this.keiyakuInfo_MailAddress1 = keiyakuInfo_MailAddress1;
	}
	public String getKeiyakuInfo_MailAddress2() {
		return keiyakuInfo_MailAddress2;
	}
	public void setKeiyakuInfo_MailAddress2(String keiyakuInfo_MailAddress2) {
		this.keiyakuInfo_MailAddress2 = keiyakuInfo_MailAddress2;
	}
	
	
}
