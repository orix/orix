package jp.co.orix.model;

public class UserPaymentInfo {
	
	private int HV011_pK;   // HV011 primaryKey(고압결제정보ID)
	private String HV011_contractNumber;   // 계약번호
	private String HV011_contractName;   // 계약자명의인
	private int HV011_paymentMethodID;   // 지급방법 구분(숫자)
	private String HV011_paymentMethod;   // 결제 방법
	private String HV011_userID;   // 사용자ID
	private String HV011_zipCode;   // 우편번호
	private String HV011_address;   // 주소
	private String HV011_companyName;   // 회사명
	private String HV011_departmentName;   // 부서명
	private String HV011_personInChargeName;   // 담당자명
	private String HV011_areaCode;   // 지역번호
	private String HV011_downtownCode;   // 시내국번
	private String HV011_memberCode;   // 가입자번호
	private String HV011_entry;   // 자유기입
	private String HV011_financialInstitutionID;   // 금융기관 정보 ID	 
	private String HV011_registrationDate;   // 등록일
	private String HV011_updateDate;   // 업데이트 날짜
	private String HV011_administrationNumber;   // 관리번호
	private String HV011_location;   // 사용장소
	private String HV011_phoneNumber;   // 전화번호

	private String CM004_financialInstitutionID;   // CM004 primaryKey(금융기관정보 ID)	
	private String CM004_financialInstitutionName;   // 금융기관명
	private String CM004_branchName;   // 지점명
	private String CM004_accountSubjectName;   // 계좌과목명
	private String CM004_accountNumber;   // 계좌번호
	private String CM004_accountNameBefore;   // 계좌명의인(변경전)
	private String CM004_accountNameAfter;   // 계좌명의인(변경후)
	private String CM004_registrationDate;   // 등록일
	private String CM004_updateDate;   // 업데이트 날짜
	
	private int IDUMS000_pK;   // IDUMS000 primaryKey
	private String IDUMS000_sendMailTime; 
	private String IDUMS000_mailAddress;   // 메일주소

	
	
	public int getHV011_pK() {
		return HV011_pK;
	}
	public void setHV011_pK(int hV011_pK) {
		HV011_pK = hV011_pK;
	}
	public String getHV011_contractNumber() {
		return HV011_contractNumber;
	}
	public void setHV011_contractNumber(String hV011_contractNumber) {
		HV011_contractNumber = hV011_contractNumber;
	}
	public String getHV011_contractName() {
		return HV011_contractName;
	}
	public void setHV011_contractName(String hV011_contractName) {
		HV011_contractName = hV011_contractName;
	}
	public int getHV011_paymentMethodID() {
		return HV011_paymentMethodID;
	}
	public void setHV011_paymentMethodID(int hV011_paymentMethodID) {
		HV011_paymentMethodID = hV011_paymentMethodID;
	}
	public String getHV011_paymentMethod() {
		return HV011_paymentMethod;
	}
	public void setHV011_paymentMethod(String hV011_paymentMethod) {
		HV011_paymentMethod = hV011_paymentMethod;
	}
	public String getHV011_userID() {
		return HV011_userID;
	}
	public void setHV011_userID(String hV011_userID) {
		HV011_userID = hV011_userID;
	}
	public String getHV011_zipCode() {
		return HV011_zipCode;
	}
	public void setHV011_zipCode(String hV011_zipCode) {
		HV011_zipCode = hV011_zipCode;
	}
	public String getHV011_address() {
		return HV011_address;
	}
	public void setHV011_address(String hV011_address) {
		HV011_address = hV011_address;
	}
	public String getHV011_companyName() {
		return HV011_companyName;
	}
	public void setHV011_companyName(String hV011_companyName) {
		HV011_companyName = hV011_companyName;
	}
	public String getHV011_departmentName() {
		return HV011_departmentName;
	}
	public void setHV011_departmentName(String hV011_departmentName) {
		HV011_departmentName = hV011_departmentName;
	}
	public String getHV011_personInChargeName() {
		return HV011_personInChargeName;
	}
	public void setHV011_personInChargeName(String hV011_personInChargeName) {
		HV011_personInChargeName = hV011_personInChargeName;
	}
	public String getHV011_areaCode() {
		return HV011_areaCode;
	}
	public void setHV011_areaCode(String hV011_areaCode) {
		HV011_areaCode = hV011_areaCode;
	}
	public String getHV011_downtownCode() {
		return HV011_downtownCode;
	}
	public void setHV011_downtownCode(String hV011_downtownCode) {
		HV011_downtownCode = hV011_downtownCode;
	}
	public String getHV011_memberCode() {
		return HV011_memberCode;
	}
	public void setHV011_memberCode(String hV011_memberCode) {
		HV011_memberCode = hV011_memberCode;
	}
	public String getHV011_entry() {
		return HV011_entry;
	}
	public void setHV011_entry(String hV011_entry) {
		HV011_entry = hV011_entry;
	}
	public String getHV011_financialInstitutionID() {
		return HV011_financialInstitutionID;
	}
	public void setHV011_financialInstitutionID(String hV011_financialInstitutionID) {
		HV011_financialInstitutionID = hV011_financialInstitutionID;
	}
	public String getHV011_registrationDate() {
		return HV011_registrationDate;
	}
	public void setHV011_registrationDate(String hV011_registrationDate) {
		HV011_registrationDate = hV011_registrationDate;
	}
	public String getHV011_updateDate() {
		return HV011_updateDate;
	}
	public void setHV011_updateDate(String hV011_updateDate) {
		HV011_updateDate = hV011_updateDate;
	}
	public String getHV011_administrationNumber() {
		return HV011_administrationNumber;
	}
	public void setHV011_administrationNumber(String hV011_administrationNumber) {
		HV011_administrationNumber = hV011_administrationNumber;
	}
	public String getHV011_location() {
		return HV011_location;
	}
	public void setHV011_location(String hV011_location) {
		HV011_location = hV011_location;
	}
	public String getHV011_phoneNumber() {
		return HV011_phoneNumber;
	}
	public void setHV011_phoneNumber(String hV011_phoneNumber) {
		HV011_phoneNumber = hV011_phoneNumber;
	}
	public String getCM004_financialInstitutionID() {
		return CM004_financialInstitutionID;
	}
	public void setCM004_financialInstitutionID(String cM004_financialInstitutionID) {
		CM004_financialInstitutionID = cM004_financialInstitutionID;
	}
	public String getCM004_financialInstitutionName() {
		return CM004_financialInstitutionName;
	}
	public void setCM004_financialInstitutionName(
			String cM004_financialInstitutionName) {
		CM004_financialInstitutionName = cM004_financialInstitutionName;
	}
	public String getCM004_branchName() {
		return CM004_branchName;
	}
	public void setCM004_branchName(String cM004_branchName) {
		CM004_branchName = cM004_branchName;
	}
	public String getCM004_accountSubjectName() {
		return CM004_accountSubjectName;
	}
	public void setCM004_accountSubjectName(String cM004_accountSubjectName) {
		CM004_accountSubjectName = cM004_accountSubjectName;
	}
	public String getCM004_accountNumber() {
		return CM004_accountNumber;
	}
	public void setCM004_accountNumber(String cM004_accountNumber) {
		CM004_accountNumber = cM004_accountNumber;
	}
	public String getCM004_accountNameBefore() {
		return CM004_accountNameBefore;
	}
	public void setCM004_accountNameBefore(String cM004_accountNameBefore) {
		CM004_accountNameBefore = cM004_accountNameBefore;
	}
	public String getCM004_accountNameAfter() {
		return CM004_accountNameAfter;
	}
	public void setCM004_accountNameAfter(String cM004_accountNameAfter) {
		CM004_accountNameAfter = cM004_accountNameAfter;
	}
	public String getCM004_registrationDate() {
		return CM004_registrationDate;
	}
	public void setCM004_registrationDate(String cM004_registrationDate) {
		CM004_registrationDate = cM004_registrationDate;
	}
	public String getCM004_updateDate() {
		return CM004_updateDate;
	}
	public void setCM004_updateDate(String cM004_updateDate) {
		CM004_updateDate = cM004_updateDate;
	}
	public int getIDUMS000_pK() {
		return IDUMS000_pK;
	}
	public void setIDUMS000_pK(int iDUMS000_pK) {
		IDUMS000_pK = iDUMS000_pK;
	}
	
	public String getIDUMS000_sendMailTime() {
		return IDUMS000_sendMailTime;
	}
	public void setIDUMS000_sendMailTime(String iDUMS000_sendMailTime) {
		IDUMS000_sendMailTime = iDUMS000_sendMailTime;
	}

	public String getIDUMS000_mailAddress() {
		return IDUMS000_mailAddress;
	}
	public void setIDUMS000_mailAddress(String iDUMS000_mailAddress) {
		IDUMS000_mailAddress = iDUMS000_mailAddress;
	}
	@Override
	public String toString() {
		return "UserPaymentInfo [HV011_pK=" + HV011_pK
				+ ", HV011_contractNumber=" + HV011_contractNumber
				+ ", HV011_contractName=" + HV011_contractName
				+ ", HV011_paymentMethodID=" + HV011_paymentMethodID
				+ ", HV011_paymentMethod=" + HV011_paymentMethod
				+ ", HV011_userID=" + HV011_userID + ", HV011_zipCode="
				+ HV011_zipCode + ", HV011_address=" + HV011_address
				+ ", HV011_companyName=" + HV011_companyName
				+ ", HV011_departmentName=" + HV011_departmentName
				+ ", HV011_personInChargeName=" + HV011_personInChargeName
				+ ", HV011_areaCode=" + HV011_areaCode
				+ ", HV011_downtownCode=" + HV011_downtownCode
				+ ", HV011_memberCode=" + HV011_memberCode + ", HV011_entry="
				+ HV011_entry + ", HV011_financialInstitutionID="
				+ HV011_financialInstitutionID + ", HV011_registrationDate="
				+ HV011_registrationDate + ", HV011_updateDate="
				+ HV011_updateDate + ", HV011_administrationNumber="
				+ HV011_administrationNumber + ", HV011_location="
				+ HV011_location + ", HV011_phoneNumber=" + HV011_phoneNumber
				+ ", CM004_financialInstitutionID="
				+ CM004_financialInstitutionID
				+ ", CM004_financialInstitutionName="
				+ CM004_financialInstitutionName + ", CM004_branchName="
				+ CM004_branchName + ", CM004_accountSubjectName="
				+ CM004_accountSubjectName + ", CM004_accountNumber="
				+ CM004_accountNumber + ", CM004_accountNameBefore="
				+ CM004_accountNameBefore + ", CM004_accountNameAfter="
				+ CM004_accountNameAfter + ", CM004_registrationDate="
				+ CM004_registrationDate + ", CM004_updateDate="
				+ CM004_updateDate + ", IDUMS000_pK=" + IDUMS000_pK
				+ ", IDUMS000_mailAddress=" + IDUMS000_mailAddress + "]";
	}
	
	
}
