<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<body>
<script>
function openMenu(subMenu) {
    var i;
    var x = document.getElementsByClassName("popUp");
    for (i = 0; i < x.length; i++) {
       x[i].style.display = "none";  
    }
    document.getElementById(subMenu).style.display = "block";  
}
</script>

<div id="bottom">	
	<h1>ORIX ほかにはないアンサーを。 低圧ボータルはこちら 高圧</h1><br> <br /> 請求書ダウンロード

	<div id="table">
		<ul>
			<li><a href="javascript:void(0)" onclick="openMenu('notice')">お知らせ</a></li>
			<li><a href="javascript:void(0)" onclick="openMenu('contract')">契約内容照会</a></li>
			<li><a href="javascript:void(0)" onclick="openMenu('electricity')">電気料金照会</a></li>
			<li><a href="javascript:void(0)" onclick="openMenu('result')">ご使用実績照会</a></li>
			<li><a href="javascript:void(0)" onclick="openMenu('renewal')">変更申込</a></li>
			<li><a href="javascript:void(0)" onclick="openMenu('QandA')">Q&A</a></li>
			<li><a href="javascript:void(0)" onclick="openMenu('appointment')">お問い合わせ</a></li>
			<li><a href="javascript:void(0)" onclick="openMenu('manageOfId')">ID管理</a></li>
		</ul>
	</div>
	<br /> <br />
	
	<div id="electricity" class="popUp" style="display: none">
		<ul>
			<li><a href="javascript:void(0)" onclick="#">月リスト</a></li>
			<li><a href="javascript:void(0)" onclick="#">年間リスト</a></li>
			<li><a href="javascript:void(0)" onclick="#">請求書</a></li>
			<li><a href="createPDF.action">請求書ダウンロード</a></li>
			<li><a href="javascript:void(0)" onclick="#">ログイン</a></li>
		</ul>
		<h2>electricity</h2>
	</div>
	
	<div id="notice" class="popUp" style="display: none">
		<h2>notice</h2>
	</div>

	<div id=contract class="popUp" style="display: none">
		<h2> contract</h2>
	</div>

	<div id=result class="popUp" style="display: none">
		<h2> result</h2>
	</div>
	
	<div id=renewal class="popUp" style="display: none">
		<h2>renewal</h2>
	</div>
	
	<div id=QandA class="popUp" style="display: none">
		<h2> QandA</h2>
	</div>
	
	<div id=appointment class="popUp" style="display: none">
		<h2> appointment</h2>
	</div>
	
	<div id=manageOfId class="popUp" style="display: none">
		<h2> manageOfId</h2>
	</div>
	
	<div id=download class="popUp" style="display: none">
		<h2> download</h2>
	</div>

	
</div>

<div id="electricity" class="submenubar">
			<ul>
				<li><a href="javascript:void(0)" onclick="#">月リスト</a></li>
				<li><a href="javascript:void(0)" onclick="#">年間リスト</a></li>
				<li><a href="javascript:void(0)" onclick="#">請求書</a></li>
				<li><a href="createPDF.action">請求書ダウンロード</a></li>
				<li><a href="javascript:void(0)" onclick="#">ログイン</a></li>
			</ul>
		</div>

</body>