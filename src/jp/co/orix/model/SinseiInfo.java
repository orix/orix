package jp.co.orix.model;

public class SinseiInfo {

	private String usePlace;
	private int placeOption;
	private String shiyojusho;
	private String shiyoName;
	private String keiyakuNo;
	private String keiyakuMeibinin;

	private String seikyushoSofuinfo_YubinBango;
	private String seikyushoSofuinfo_Jusho1;
	private String seikyushoSofuinfo_Jusho2;
	private String seikyushoSofuinfo_Jusho3;
	private String seikyushoSofuinfo_Jusho4;
	private String seikyushoSofuinfo_BiruName;
	private String seikyushoSofuinfo_RoomNo;
	private String seikyushoSofuinfo_Yago;
	private String seikyushoSofuinfo_HojinName;
	private String seikyushoSofuinfo_BushoName;
	private String seikyushoSofuinfo_Tanto1;
	private String seikyushoSofuinfo_Tanto2;
	private String seikyushoSofuinfo_MailAddress;
	private String seikyushoSofuinfo_MailAddress2;
	public String getUsePlace() {
		return usePlace;
	}
	public void setUsePlace(String usePlace) {
		this.usePlace = usePlace;
	}
	
	public int getPlaceOption() {
		return placeOption;
	}
	public void setPlaceOption(int placeOption) {
		this.placeOption = placeOption;
	}
	public String getShiyojusho() {
		return shiyojusho;
	}
	public void setShiyojusho(String shiyojusho) {
		this.shiyojusho = shiyojusho;
	}
	public String getShiyoName() {
		return shiyoName;
	}
	public void setShiyoName(String shiyoName) {
		this.shiyoName = shiyoName;
	}
	public String getKeiyakuNo() {
		return keiyakuNo;
	}
	public void setKeiyakuNo(String keiyakuNo) {
		this.keiyakuNo = keiyakuNo;
	}
	public String getKeiyakuMeibinin() {
		return keiyakuMeibinin;
	}
	public void setKeiyakuMeibinin(String keiyakuMeibinin) {
		this.keiyakuMeibinin = keiyakuMeibinin;
	}
	public String getSeikyushoSofuinfo_YubinBango() {
		return seikyushoSofuinfo_YubinBango;
	}
	public void setSeikyushoSofuinfo_YubinBango(String seikyushoSofuinfo_YubinBango) {
		this.seikyushoSofuinfo_YubinBango = seikyushoSofuinfo_YubinBango;
	}
	public String getSeikyushoSofuinfo_Jusho1() {
		return seikyushoSofuinfo_Jusho1;
	}
	public void setSeikyushoSofuinfo_Jusho1(String seikyushoSofuinfo_Jusho1) {
		this.seikyushoSofuinfo_Jusho1 = seikyushoSofuinfo_Jusho1;
	}
	public String getSeikyushoSofuinfo_Jusho2() {
		return seikyushoSofuinfo_Jusho2;
	}
	public void setSeikyushoSofuinfo_Jusho2(String seikyushoSofuinfo_Jusho2) {
		this.seikyushoSofuinfo_Jusho2 = seikyushoSofuinfo_Jusho2;
	}
	public String getSeikyushoSofuinfo_Jusho3() {
		return seikyushoSofuinfo_Jusho3;
	}
	public void setSeikyushoSofuinfo_Jusho3(String seikyushoSofuinfo_Jusho3) {
		this.seikyushoSofuinfo_Jusho3 = seikyushoSofuinfo_Jusho3;
	}
	public String getSeikyushoSofuinfo_Jusho4() {
		return seikyushoSofuinfo_Jusho4;
	}
	public void setSeikyushoSofuinfo_Jusho4(String seikyushoSofuinfo_Jusho4) {
		this.seikyushoSofuinfo_Jusho4 = seikyushoSofuinfo_Jusho4;
	}
	public String getSeikyushoSofuinfo_BiruName() {
		return seikyushoSofuinfo_BiruName;
	}
	public void setSeikyushoSofuinfo_BiruName(String seikyushoSofuinfo_BiruName) {
		this.seikyushoSofuinfo_BiruName = seikyushoSofuinfo_BiruName;
	}
	public String getSeikyushoSofuinfo_RoomNo() {
		return seikyushoSofuinfo_RoomNo;
	}
	public void setSeikyushoSofuinfo_RoomNo(String seikyushoSofuinfo_RoomNo) {
		this.seikyushoSofuinfo_RoomNo = seikyushoSofuinfo_RoomNo;
	}
	public String getSeikyushoSofuinfo_Yago() {
		return seikyushoSofuinfo_Yago;
	}
	public void setSeikyushoSofuinfo_Yago(String seikyushoSofuinfo_Yago) {
		this.seikyushoSofuinfo_Yago = seikyushoSofuinfo_Yago;
	}
	public String getSeikyushoSofuinfo_HojinName() {
		return seikyushoSofuinfo_HojinName;
	}
	public void setSeikyushoSofuinfo_HojinName(String seikyushoSofuinfo_HojinName) {
		this.seikyushoSofuinfo_HojinName = seikyushoSofuinfo_HojinName;
	}
	public String getSeikyushoSofuinfo_BushoName() {
		return seikyushoSofuinfo_BushoName;
	}
	public void setSeikyushoSofuinfo_BushoName(String seikyushoSofuinfo_BushoName) {
		this.seikyushoSofuinfo_BushoName = seikyushoSofuinfo_BushoName;
	}
	public String getSeikyushoSofuinfo_Tanto1() {
		return seikyushoSofuinfo_Tanto1;
	}
	public void setSeikyushoSofuinfo_Tanto1(String seikyushoSofuinfo_Tanto1) {
		this.seikyushoSofuinfo_Tanto1 = seikyushoSofuinfo_Tanto1;
	}
	public String getSeikyushoSofuinfo_Tanto2() {
		return seikyushoSofuinfo_Tanto2;
	}
	public void setSeikyushoSofuinfo_Tanto2(String seikyushoSofuinfo_Tanto2) {
		this.seikyushoSofuinfo_Tanto2 = seikyushoSofuinfo_Tanto2;
	}
	public String getSeikyushoSofuinfo_MailAddress() {
		return seikyushoSofuinfo_MailAddress;
	}
	public void setSeikyushoSofuinfo_MailAddress(
			String seikyushoSofuinfo_MailAddress) {
		this.seikyushoSofuinfo_MailAddress = seikyushoSofuinfo_MailAddress;
	}
	public String getSeikyushoSofuinfo_MailAddress2() {
		return seikyushoSofuinfo_MailAddress2;
	}
	public void setSeikyushoSofuinfo_MailAddress2(
			String seikyushoSofuinfo_MailAddress2) {
		this.seikyushoSofuinfo_MailAddress2 = seikyushoSofuinfo_MailAddress2;
	}

//	private String jukyuKanriInfo_HojinName;
//	private String jukyuKanriInfo_BushoName;
//	private String jukyuKanriInfo_Tanto1;
//	private String jukyuKanriInfo_Tanto2;
//	private String jukyuKanriInfo_Tel1;
//	private String jukyuKanriInfo_Tel2;
//	private String jukyuKanriInfo_Tel3;
//	private String jukyuKanriInfo_Fax;
//	private String jukyuKanriInfo_MailAddress1;
//	private String jukyuKanriInfo_MailAddress2;

//	private String seikyuInfo_HojinName;
//	private String seikyuInfo_BushoName;
//	private String seikyuInfo_Tanto1;
//	private String seikyuInfo_Tanto2;
//	private String seikyuInfo_Tel1;
//	private String seikyuInfo_Tel2;
//	private String seikyuInfo_Tel3;
//	private String seikyuInfo_Fax;
//	private String seikyuInfo_MailAddress1;
//	private String seikyuInfo_MailAddress2;

//	private String keiyakuInfo_HojinName;
//	private String keiyakuInfo_BushoName;
//	private String keiyakuInfo_Tanto1;
//	private String keiyakuInfo_Tanto2;
//	private String keiyakuInfo_Tel1;
//	private String keiyakuInfo_Tel2;
//	private String keiyakuInfo_Tel3;
//	private String keiyakuInfo_Fax;
//	private String keiyakuInfo_MailAddress1;
//	private String keiyakuInfo_MailAddress2;

//	private String tantoInfo_YubinBango;
//	private String tantoInfo_Jusho;
//	private String tantoInfo_HojinName;
//	private String tantoInfo_BushoName;
//	private String tantoInfo_Tanto1;
//	private String tantoInfo_Tel1;
//	private String tantoInfo_Tel2;
//	private String tantoInfo_Tel3;
//	private String tantoInfo_MailAddress1;
//	private String tantoInfo_Memo;



}
