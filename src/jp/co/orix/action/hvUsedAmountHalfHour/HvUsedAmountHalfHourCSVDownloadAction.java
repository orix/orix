package jp.co.orix.action.hvUsedAmountHalfHour;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.orix.dao.CheckContractInfoDao;
import jp.co.orix.dao.GetCustomerContractNumberDao;
import jp.co.orix.util.CommApi_32;

import com.opensymphony.xwork2.ActionSupport;

public class HvUsedAmountHalfHourCSVDownloadAction extends ActionSupport{
	private String customerContractNumber; 
	
	private GetCustomerContractNumberDao getCustomerContractNumberDao;
	
	private String specDate;									//	
	private String compDate;									//
	private String compChk;										//
	
	private InputStream fileInputStream;
	private String contentDisposition;
	@Override
	public String execute() throws Exception{
		try{
			//3-2. CSVダウンロード処理
					if(compChk == null)
						compChk = "on";
					else if(compChk.equals("off")){
						compChk = null;
					}else{
						compChk = "on";
					}
					
	
					//test value setting(manageNumber)
					String manageNumber = "1";				//use GetControlNumbers() to get manageNumber after test.
					
					
					List<Map<String, Double>> usePowerMapLi = CommApi_32.getUsedPowerAmountInfo(manageNumber, "2", specDate, specDate, customerContractNumber);
					Map<String, Double> usePowerMap = usePowerMapLi.get(0);
					Map<String, Double> useCompPowerMap = null;
					if(compChk != null){
						List<Map<String, Double>> useCompPowerMapLi = CommApi_32.getUsedPowerAmountInfo(manageNumber, "2", compDate, compDate, customerContractNumber);
						useCompPowerMap = useCompPowerMapLi.get(0);
					}
					
					//5. create and set useDifPowers 
					ArrayList<String> useDifPowers = null;
					if(compChk != null && useCompPowerMap != null && useCompPowerMap.size() != 0){
						useDifPowers = getUseDifPowerList(usePowerMap, useCompPowerMap);
					}
			
			//9)CSVダウンロードデータ作成を行う。
					Map<String, Object> csvDownLoadDataMap = new HashMap<String, Object>();
					
					
					SimpleDateFormat viewTimeFormat = new SimpleDateFormat("yyyy/MM/dd");
					SimpleDateFormat csvTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
					SimpleDateFormat csv2TimeFormat = new SimpleDateFormat("yyyy年MM月dd日");
					//①ファイル名を設定する。
					setContentDisposition("attachment;filename="+URLEncoder.encode("30分毎電力使用量_" +csv2TimeFormat.format(viewTimeFormat.parse(specDate))+ "_" + csvTimeFormat.format(Calendar.getInstance().getTime()) + ".csv", "UTF-8"));
					
					//②	見出しを設定する。				
					List<String> csvColumn = new ArrayList<String>();
					//見出し項目：”No”、”指定日”、”比較日”、”時間”、”指定日使用量(kWh)”、”比較日使用量(kWh)”、”差分使用量(kWh)”
					csvColumn.add("No");
					csvColumn.add("指定日");
					csvColumn.add("比較日");
					csvColumn.add("時間");
					csvColumn.add("指定日使用量(kWh)");
					csvColumn.add("比較日使用量(kWh)");
					csvColumn.add("差分使用量(kWh)");
					csvDownLoadDataMap.put("columns", csvColumn);
					
					//③	データ設定を行う。
					/*
					 * 	a.　設定データ：No、指定日、比較日、時間、指定日使用量、比較日使用量、差分使用量																					
					 *	b.　比較表示フラグが’ON’の場合、比較日付を設定する。																					
					 *	c.　比較日使用量が存在する場合、データを設定する。																					
					 *	d.　比較表示フラグが’ON’かつ比較使用量データが存在し、差分使用量が存在する場合、																					
			　		 *		差分使用量を設定する。																					
					 */
					//a.　設定データ：No、指定日、比較日、時間、指定日使用量、比較日使用量、差分使用量	
					List<List<String>> csvData = new ArrayList<List<String>>();
					
					List<String> csvDataNo = new ArrayList<String>();
					List<String> csvDataSpecDate = new ArrayList<String>();
					List<String> csvDataCompDate = new ArrayList<String>();
					
					List<String> csvDataTime = getColumnNameList();	
					List<String> csvDataUsePowers =	getUsePowerList(usePowerMap);
					List<String> csvDataUseCompPowers = getUsePowerList(useCompPowerMap);
					
					
					List<String> csvDataUseDifPowers =  useDifPowers;
					
					String specDateStrMonth = csv2TimeFormat.format(viewTimeFormat.parse(specDate));
					String compDateStrMonth = csv2TimeFormat.format(viewTimeFormat.parse(compDate));
					
					if(compChk != null && useCompPowerMap != null && useCompPowerMap.size() != 0){
						for(int i=1; i<=48; i++ ){
						csvDataCompDate.add(compDateStrMonth);
						}
					}
					for(int i=1; i<=48; i++ ){
						csvDataNo.add(i+"");
						csvDataSpecDate.add(specDateStrMonth);
						
					}
					
					csvData.add(csvDataNo);
					csvData.add(csvDataSpecDate);
					csvData.add(csvDataCompDate);
					csvData.add(csvDataTime);
					csvData.add(csvDataUsePowers);
					csvData.add(csvDataUseCompPowers);
					csvData.add(csvDataUseDifPowers);
					
					csvDownLoadDataMap.put("column",csvColumn);
					csvDownLoadDataMap.put("data",csvData);
					
					StringBuffer csvBuffer = CommApi_32.GetCsvFileResult(csvDownLoadDataMap);
					
					setFileInputStream(new ByteArrayInputStream(csvBuffer.toString().getBytes("UTF-8")));
			
					return SUCCESS;
			
		}catch(Exception e){
			return "error";
		}
		
	}
	
	private ArrayList<String> getColumnNameList(){
		ArrayList<String> nameList = new ArrayList<String>();
		
		nameList.add("00:00-00:29");
		nameList.add("00:30-00:59");
		nameList.add("01:00-01:29");
		nameList.add("01:30-01:59");
		nameList.add("02:00-02:29");
		nameList.add("02:30-02:59");
		nameList.add("03:00-03:29");
		nameList.add("03:30-03:59");
		nameList.add("04:00-04:29");
		nameList.add("04:30-04:59");
		nameList.add("05:00-05:29");
		nameList.add("05:30-05:59");
		nameList.add("06:00-06:29");
		nameList.add("06:30-06:59");
		nameList.add("07:00-07:29");
		nameList.add("07:30-07:59");
		nameList.add("08:00-08:29");
		nameList.add("08:30-08:59");
		nameList.add("09:00-09:29");
		nameList.add("09:30-09:59");
		nameList.add("10:00-10:29");
		nameList.add("10:30-10:59");
		nameList.add("11:00-11:29");
		nameList.add("11:30-11:59");
		nameList.add("12:00-12:29");
		nameList.add("12:30-12:59");
		nameList.add("13:00-13:29");
		nameList.add("13:30-13:59");
		nameList.add("14:00-14:29");
		nameList.add("14:30-14:59");
		nameList.add("15:00-15:29");
		nameList.add("15:30-15:59");
		nameList.add("16:00-16:29");
		nameList.add("16:30-16:59");
		nameList.add("17:00-17:29");
		nameList.add("17:30-17:59");
		nameList.add("18:00-18:29");
		nameList.add("18:30-18:59");
		nameList.add("19:00-19:29");
		nameList.add("19:30-19:59");
		nameList.add("20:00-20:29");
		nameList.add("20:30-20:59");
		nameList.add("21:00-21:29");
		nameList.add("21:30-21:59");
		nameList.add("22:00-22:29");
		nameList.add("22:30-22:59");
		nameList.add("23:00-23:29");
		nameList.add("23:30-23:59");
		
		return nameList;
	}
	
	private ArrayList<String> getUsePowerList(Map<String, Double> usePowerMap) throws Exception{
		ArrayList<String> usePowerList = new ArrayList<String>();
		if(usePowerMap != null)
		for(int i = 1; i <= usePowerMap.size(); i++){
			usePowerList.add(String.format("%.4f", usePowerMap.get(i+"")));
		}
		return usePowerList;
	}
	
	private ArrayList<String> getUseDifPowerList(Map<String, Double> usePowerMap, Map<String, Double> useCompPowerMap) throws Exception{
		ArrayList<String> useDifPowers = new ArrayList<String>();
		if(usePowerMap != null && useCompPowerMap != null)
		for(int i = 1; i <= useCompPowerMap.size(); i++){
			useDifPowers.add(String.format("%.4f", (usePowerMap.get(i+"") - useCompPowerMap.get(i+""))));
		}
		return useDifPowers;
	}

	public String getSpecDate() {
		return specDate;
	}

	public void setSpecDate(String specDate) {
		this.specDate = specDate;
	}

	public String getCompDate() {
		return compDate;
	}

	public void setCompDate(String compDate) {
		this.compDate = compDate;
	}

	public String getCompChk() {
		return compChk;
	}

	public void setCompChk(String compChk) {
		this.compChk = compChk;
	}

	public String getContentDisposition() {
		return contentDisposition;
	}

	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

	public String getCustomerContractNumber() {
		return customerContractNumber;
	}

	public void setCustomerContractNumber(String customerContractNumber) {
		this.customerContractNumber = customerContractNumber;
	}

	public GetCustomerContractNumberDao getGetCustomerContractNumberDao() {
		return getCustomerContractNumberDao;
	}

	public void setGetCustomerContractNumberDao(
			GetCustomerContractNumberDao getCustomerContractNumberDao) {
		this.getCustomerContractNumberDao = getCustomerContractNumberDao;
	}
	
}
