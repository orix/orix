package jp.co.orix.action.contactInfo;
/**
 * @内容		「連絡先照会」画面の動きと関するコントロールクラス
 * @author Jongmoo Kim
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import jp.co.orix.dao.CheckContactInfoDao;
import jp.co.orix.dao.GetControlNumberDao;
import jp.co.orix.dao.GetCustomerContractNumberDao;
import jp.co.orix.dao.GetGroupAndPlaceDao;
import jp.co.orix.model.ContactInfoHV003;
import jp.co.orix.model.UniversalInfoITHNT000;
import jp.co.orix.util.CommApi_32;

import com.opensymphony.xwork2.ActionSupport;

public class CheckContactInfoAction extends ActionSupport implements ServletRequestAware {

	private static final String T0100 = "50002";
	private static final String VALUE1_OF_HV013_IN_HV003 = "01";
	private static final String VALUE2_OF_HV013_IN_HV003 = "02";
	private static final String VALUE3_OF_HV013_IN_HV003 = "03";
	private static final String VALUE4_OF_HV013_IN_HV003 = "04";

	private String groupCombobox;
	private String usePlaceCombobox;
	private String useDateCombobox;

	private String contractNumber;
	private String controlNumber;

	private GetControlNumberDao getControlNumberDao;
	private GetCustomerContractNumberDao getCustomerContractNumberDao;
	private CheckContactInfoDao checkContactInfoDao;
	private GetGroupAndPlaceDao getGroupAndPlaceDao;

	private List<UniversalInfoITHNT000> universalInfoList;
	private List<ContactInfoHV003> contactInfoList;

	public HttpServletRequest request;
	private String parameter;

	public String viewCheckContactInfoForm() throws Exception {
		
		System.out.println("024_Check");
		System.out.println("parameter : " + parameter);
		System.out.println("groupCombobox : " + groupCombobox);
		System.out.println("usePlaceCombobox : " + usePlaceCombobox);

		// parameter가 Null로 넘어 왔을 경우(전 페이지에서 검색값 없이 바로 넘어 왔을 경우)
		if(groupCombobox == null && usePlaceCombobox == null) {
			universalInfoList = getGroupAndPlaceDao.getGroupComboboxList(T0100);
			//グループの値がない場合、出力する「エラーメッセージ」
			if(universalInfoList.isEmpty()) {
				return ERROR;
			}
			return SUCCESS;
		}


		Map<String, String> condition = new HashMap<String, String>();
		condition.put("groupCombobox", groupCombobox);
		condition.put("usePlaceCombobox", usePlaceCombobox);

		controlNumber = getControlNumberDao.getControlNumber(condition);
		contractNumber = getCustomerContractNumberDao.getCustomerContractNumber(condition);

		condition.put("controlNumber", controlNumber);
		condition.put("contractNumber", contractNumber);
		condition.put("VALUE1_OF_HV013_IN_HV003", VALUE1_OF_HV013_IN_HV003);
		condition.put("VALUE2_OF_HV013_IN_HV003", VALUE2_OF_HV013_IN_HV003);
		condition.put("VALUE3_OF_HV013_IN_HV003", VALUE3_OF_HV013_IN_HV003);
		condition.put("VALUE4_OF_HV013_IN_HV003", VALUE4_OF_HV013_IN_HV003);

		contactInfoList = checkContactInfoDao.getDestinationOfBillBy(condition);
		contactInfoList.addAll(checkContactInfoDao.getPowerManagementEntriesBy(condition));
		contactInfoList.addAll(checkContactInfoDao.getInfoOfEnquiriesBy(condition));
		contactInfoList.addAll(checkContactInfoDao.getInfoOfCooperationBy(condition));

		//高圧連絡先照会データが取得できなかった場合、『共通メッセージ（E0005）』を出力する。
		//jsp페이지에서 처리
		/*if(contactInfoList.size() == 0) {
			return INPUT;
		}*/
		
		try {
			System.out.println("param_1");
			CommApi_32 api = new CommApi_32();
			api.setFormParamator(request, "checkContractInfo");
			HashMap<String, Object> param = api.getFormParamator("checkContractInfo");
			parameter = (String)param.get("param");
		} catch(Exception e) {
			System.out.println("param_2");
			System.out.println("contactInfoList.size() : " + contactInfoList.size());	
		}

		System.out.println("parameter : " + parameter);
		System.out.println("024_End");
		System.out.println("========================");

		return SUCCESS;
	}

	
	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getGroupCombobox() {
		return groupCombobox;
	}
	public void setGroupCombobox(String groupCombobox) {
		this.groupCombobox = groupCombobox;
	}
	public String getUsePlaceCombobox() {
		return usePlaceCombobox;
	}
	public void setUsePlaceCombobox(String usePlaceCombobox) {
		this.usePlaceCombobox = usePlaceCombobox;
	}
	public GetCustomerContractNumberDao getGetCustomerContractNumberDao() {
		return getCustomerContractNumberDao;
	}
	public void setGetCustomerContractNumberDao(
			GetCustomerContractNumberDao getCustomerContractNumberDao) {
		this.getCustomerContractNumberDao = getCustomerContractNumberDao;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getControlNumber() {
		return controlNumber;
	}
	public void setControlNumber(String controlNumber) {
		this.controlNumber = controlNumber;
	}
	public GetControlNumberDao getGetControlNumberDao() {
		return getControlNumberDao;
	}
	public void setGetControlNumberDao(GetControlNumberDao getControlNumberDao) {
		this.getControlNumberDao = getControlNumberDao;
	}
	public CheckContactInfoDao getCheckContactInfoDao() {
		return checkContactInfoDao;
	}
	public void setCheckContactInfoDao(CheckContactInfoDao checkContactInfoDao) {
		this.checkContactInfoDao = checkContactInfoDao;
	}
	public List<ContactInfoHV003> getContactInfoList() {
		return contactInfoList;
	}
	public void setContactInfoList(List<ContactInfoHV003> contactInfoList) {
		this.contactInfoList = contactInfoList;
	}
	public GetGroupAndPlaceDao getGetGroupAndPlaceDao() {
		return getGroupAndPlaceDao;
	}
	public void setGetGroupAndPlaceDao(GetGroupAndPlaceDao getGroupAndPlaceDao) {
		this.getGroupAndPlaceDao = getGroupAndPlaceDao;
	}
	public List<UniversalInfoITHNT000> getUniversalInfoList() {
		return universalInfoList;
	}
	public void setUniversalInfoList(List<UniversalInfoITHNT000> universalInfoList) {
		this.universalInfoList = universalInfoList;
	}
	public String getUseDateCombobox() {
		return useDateCombobox;
	}
	public void setUseDateCombobox(String useDateCombobox) {
		this.useDateCombobox = useDateCombobox;
	}


}
