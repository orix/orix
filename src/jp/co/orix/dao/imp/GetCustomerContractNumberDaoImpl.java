package jp.co.orix.dao.imp;

import java.util.Map;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.GetCustomerContractNumberDao;


public class GetCustomerContractNumberDaoImpl implements GetCustomerContractNumberDao {

	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	private String contractNumber;
		
	@Override
	public String getCustomerContractNumber(Map<String, String> condition) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		GetCustomerContractNumberDao getCustomerContractNumberDao = session.getMapper(GetCustomerContractNumberDao.class);
		
		try {
			contractNumber = getCustomerContractNumberDao.getCustomerContractNumber(condition);
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		
		return contractNumber;
		
	}
	
}