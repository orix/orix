package jp.co.orix.util;
/**
 * @author Jongmoo Kim
 */

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import jp.co.orix.dao.GetGroupAndPlaceDao;
import jp.co.orix.model.UniversalInfoITHNT000;

import com.opensymphony.xwork2.ActionSupport;

public class GetGroupAndUsePlaceComboboxList extends ActionSupport {

	private static final String T0100 = "50002";
	private static final String T0200 = "50003";

	private UniversalInfoITHNT000 universalInfo;
	private List<UniversalInfoITHNT000> universalInfoList;
	private GetGroupAndPlaceDao getGroupAndPlaceDao;

	private String groupComboboxCode;
	private Map groupComboboxMap = new LinkedHashMap();
	private Map usePlaceMap = new LinkedHashMap();

	/* @GetGroupComboboxList */
	public String getGroupComboboxList() throws Exception {
		universalInfoList = getGroupAndPlaceDao.getGroupComboboxList(T0100);

		for(int i = 0; i < universalInfoList.size(); i++) {
			groupComboboxMap.put(universalInfoList.get(i).getT0200(), universalInfoList.get(i).getT0401());	
		}

		return SUCCESS;
	}

	/* @GetUsePlaceComboboxList	*/
	public String getUsePlaceComboboxList() throws Exception {
		Map<String, String> condition = new HashMap<String, String>();
		condition.put("T0100", T0100);
		condition.put("T0200", T0200);
		condition.put("groupComboboxCode", groupComboboxCode);

		universalInfoList = getGroupAndPlaceDao.getUsePlaceComboboxList(condition);

		if (universalInfoList.size() > 0) {
			for(int i = 0; i < universalInfoList.size(); i++) {
				usePlaceMap.put(universalInfoList.get(i).getT0200(), universalInfoList.get(i).getT0401());
			}
		} else {
			usePlaceMap.put("success", false);
			usePlaceMap.put("message", "ご使用場所に無効な値が選択されています。");
		}

		return SUCCESS;
	}

	/* ここからはｇｅｔｔｅｒ＆ｓｅｔｔｅｒ　*/
	public UniversalInfoITHNT000 getUniversalInfo() {
		return universalInfo;
	}
	public void setUniversalInfo(UniversalInfoITHNT000 universalInfo) {
		this.universalInfo = universalInfo;
	}
	public List<UniversalInfoITHNT000> getUniversalInfoList() {
		return universalInfoList;
	}
	public void setUniversalInfoList(List<UniversalInfoITHNT000> universalInfoList) {
		this.universalInfoList = universalInfoList;
	}
	public GetGroupAndPlaceDao getGetGroupAndPlaceDao() {
		return getGroupAndPlaceDao;
	}
	public void setGetGroupAndPlaceDao(GetGroupAndPlaceDao getGroupAndPlaceDao) {
		this.getGroupAndPlaceDao = getGroupAndPlaceDao;
	}
	public String getGroupComboboxCode() {
		return groupComboboxCode;
	}
	public void setGroupComboboxCode(String groupComboboxCode) {
		this.groupComboboxCode = groupComboboxCode;
	}
	public Map getUsePlaceMap() {
		return usePlaceMap;
	}
	public void setUsePlaceMap(Map usePlaceMap) {
		this.usePlaceMap = usePlaceMap;
	}
	public Map getGroupComboboxMap() {
		return groupComboboxMap;
	}
	public void setGroupComboboxMap(Map groupComboboxMap) {
		this.groupComboboxMap = groupComboboxMap;
	}


}
