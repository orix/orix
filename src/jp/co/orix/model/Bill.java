package jp.co.orix.model;

import java.util.Date;

public class Bill {
	/*
	Table : HV006
	Culumns : H0601 - NUMBER	Not null // 高圧請求書情報ID (고압청구서 ID)
					H0602 - VARCHAR2(10)	Not null //グループID (group ID)
					H0603 - VARCHAR2(10)	Not null // 사용장소
					H0604 - NUMBER // 년월분 세부청구금액
					H0605 - VARCHAR2(100)	Not null //장소
					H0606 - VARCHAR2(100)	Not null //명칭
					H0607 - DATE	 Not null // 사용기간 (5~8여름 / 그외 )
					H0608 - NUMBER	Not null // 금융기간ID
					H0609 - VARCHAR2(100)	Not null // 금융기간명칭
					H0610 - VARCHAR2(20)	Not null // 계좌번호
					H0611 - VARCHAR2(20)	Not null // 계좌명칭
					H0612 - DATE	Not null // 지불기간
					H0613 - NUMBER // 사용전력량
					H0614 - NUMBER //승률
					H0615 - NUMBER // 당월력률
					H0616 - NUMBER	Not null // 당월최대수요전력
					H0617 - NUMBER // 유효 전력량 지시 수 당월 분
					H0618 - NUMBER // 무효 전력량 지시 수 당월 분
					H0619 - NUMBER // 계약전력
					H0620 - NUMBER(10,2) //단가 
					H0621 - NUMBER // 력율
					H0622 - NUMBER(10,2) // 기본요금
					H0623 - NUMBER // 표준사용전력량
					H0624 - NUMBER(10,2) // 표준단가
					H0625 - NUMBER(10,2) //표준 연료비조정
					H0626 - NUMBER(10,2) //표준전력량요금
					
					H0627 - NUMBER // 재생에너지 발전촉진 부과금(사용전력량)
					H0628 - NUMBER(10,2) // 재생에너지 발전촉진 부과금(단가)
					H0629 - NUMBER  // 재생에너지 발전촉진 부과금(전력량요금)
					
					H0630 - NUMBER(10,2) // 별지1 전력량 요금
					H0631 - NUMBER //합계 전력량 요금
					H0632 - NUMBER // (중 소비세 상당액) _ 전력량 요금
					H0633 - NUMBER  // 유효전력량 당월분
					H0634 - NUMBER  // 무효전력량 당월분
					
					
					H0635 - NUMBER  // 계절시간대별 _ 사용 전력량
					H0636 - NUMBER(10,2) // 계절시간대별 _ 단가
					H0637 - NUMBER(10,2) // 계절시간대별 _ 연료조정비
					H0638 - NUMBER(10,2) // 계절시간대별 _ 전력량요금
					H0639 - NUMBER // 휴일고부하 _ 사용전력량
					H0640 - NUMBER(10,2) // 휴일고부하 _ 단가
					H0641 - NUMBER(10,2) // 휴일고부하 _ 연료비조정
					H0642 - NUMBER(10,2) // 휴일고부하 _ 전력량요금
					
					H0643 - NUMBER  // 인출 예정일
					H0644 - NUMBER //관리자코멘트
					H0645 - DATE	Not null // 등록일
					H0646 - DATE	Not null // 수정일
					H0647 - VARCHAR2(10)	Not null
					H0648 - VARCHAR2(10)	Not null
	*/
	
	
	private Integer 	H0601;
	private String 	H0602;
	private String 	H0603;
	private Integer 	H0604;
	private String 	H0605;
	private String 	H0606;	
	private Date 		H0607;
	private Integer 	H0608;
	private String 	H0609;
	private String 	H0610;
	private String 	H0611;
	private Date 		H0612;
	private Integer 	H0613;
	private Integer 	H0614;
	private Integer 	H0615;
	private Integer 	H0616;
	private Integer 	H0617;
	private Integer 	H0618;
	private Integer 	H0619;
	private Double 	H0620;
	private Integer 	H0621;
	private Double 	H0622;
	private Integer 	H0623;
	private Double 	H0624;
	private Double 	H0625;
	private Double 	H0626;
	private Integer 	H0627;
	private Double 	H0628;
	private Integer 	H0629;
	private Double 	H0630;
	private Integer 	H0631;
	private Integer 	H0632;
	private Integer 	H0633;
	private Integer 	H0634;
	private Integer 	H0635;
	private Double 	H0636;
	private Double 	H0637;
	private Double 	H0638;
	private Integer 	H0639;
	private Double 	H0640;
	private Double 	H0641;
	private Double 	H0642;
	private Integer 	H0643;
	private Integer 	H0644;
	private Date 		H0645;
	private Date 		H0646;
	private Double 	H0647;
	private Double 	H0648;
	public Integer getH0601() {
		return H0601;
	}
	public void setH0601(Integer h0601) {
		H0601 = h0601;
	}
	public String getH0602() {
		return H0602;
	}
	public void setH0602(String h0602) {
		H0602 = h0602;
	}
	public String getH0603() {
		return H0603;
	}
	public void setH0603(String h0603) {
		H0603 = h0603;
	}
	public Integer getH0604() {
		return H0604;
	}
	public void setH0604(Integer h0604) {
		H0604 = h0604;
	}
	public String getH0605() {
		return H0605;
	}
	public void setH0605(String h0605) {
		H0605 = h0605;
	}
	public String getH0606() {
		return H0606;
	}
	public void setH0606(String h0606) {
		H0606 = h0606;
	}
	public Date getH0607() {
		return H0607;
	}
	public void setH0607(Date h0607) {
		H0607 = h0607;
	}
	public Integer getH0608() {
		return H0608;
	}
	public void setH0608(Integer h0608) {
		H0608 = h0608;
	}
	public String getH0609() {
		return H0609;
	}
	public void setH0609(String h0609) {
		H0609 = h0609;
	}
	public String getH0610() {
		return H0610;
	}
	public void setH0610(String h0610) {
		H0610 = h0610;
	}
	public String getH0611() {
		return H0611;
	}
	public void setH0611(String h0611) {
		H0611 = h0611;
	}
	public Date getH0612() {
		return H0612;
	}
	public void setH0612(Date h0612) {
		H0612 = h0612;
	}
	public Integer getH0613() {
		return H0613;
	}
	public void setH0613(Integer h0613) {
		H0613 = h0613;
	}
	public Integer getH0614() {
		return H0614;
	}
	public void setH0614(Integer h0614) {
		H0614 = h0614;
	}
	public Integer getH0615() {
		return H0615;
	}
	public void setH0615(Integer h0615) {
		H0615 = h0615;
	}
	public Integer getH0616() {
		return H0616;
	}
	public void setH0616(Integer h0616) {
		H0616 = h0616;
	}
	public Integer getH0617() {
		return H0617;
	}
	public void setH0617(Integer h0617) {
		H0617 = h0617;
	}
	public Integer getH0618() {
		return H0618;
	}
	public void setH0618(Integer h0618) {
		H0618 = h0618;
	}
	public Integer getH0619() {
		return H0619;
	}
	public void setH0619(Integer h0619) {
		H0619 = h0619;
	}
	public Double getH0620() {
		return H0620;
	}
	public void setH0620(Double h0620) {
		H0620 = h0620;
	}
	public Integer getH0621() {
		return H0621;
	}
	public void setH0621(Integer h0621) {
		H0621 = h0621;
	}
	public Double getH0622() {
		return H0622;
	}
	public void setH0622(Double h0622) {
		H0622 = h0622;
	}
	public Integer getH0623() {
		return H0623;
	}
	public void setH0623(Integer h0623) {
		H0623 = h0623;
	}
	public Double getH0624() {
		return H0624;
	}
	public void setH0624(Double h0624) {
		H0624 = h0624;
	}
	public Double getH0625() {
		return H0625;
	}
	public void setH0625(Double h0625) {
		H0625 = h0625;
	}
	public Double getH0626() {
		return H0626;
	}
	public void setH0626(Double h0626) {
		H0626 = h0626;
	}
	public Integer getH0627() {
		return H0627;
	}
	public void setH0627(Integer h0627) {
		H0627 = h0627;
	}
	public Double getH0628() {
		return H0628;
	}
	public void setH0628(Double h0628) {
		H0628 = h0628;
	}
	public Integer getH0629() {
		return H0629;
	}
	public void setH0629(Integer h0629) {
		H0629 = h0629;
	}
	public Double getH0630() {
		return H0630;
	}
	public void setH0630(Double h0630) {
		H0630 = h0630;
	}
	public Integer getH0631() {
		return H0631;
	}
	public void setH0631(Integer h0631) {
		H0631 = h0631;
	}
	public Integer getH0632() {
		return H0632;
	}
	public void setH0632(Integer h0632) {
		H0632 = h0632;
	}
	public Integer getH0633() {
		return H0633;
	}
	public void setH0633(Integer h0633) {
		H0633 = h0633;
	}
	public Integer getH0634() {
		return H0634;
	}
	public void setH0634(Integer h0634) {
		H0634 = h0634;
	}
	public Integer getH0635() {
		return H0635;
	}
	public void setH0635(Integer h0635) {
		H0635 = h0635;
	}
	public Double getH0636() {
		return H0636;
	}
	public void setH0636(Double h0636) {
		H0636 = h0636;
	}
	public Double getH0637() {
		return H0637;
	}
	public void setH0637(Double h0637) {
		H0637 = h0637;
	}
	public Double getH0638() {
		return H0638;
	}
	public void setH0638(Double h0638) {
		H0638 = h0638;
	}
	public Integer getH0639() {
		return H0639;
	}
	public void setH0639(Integer h0639) {
		H0639 = h0639;
	}
	public Double getH0640() {
		return H0640;
	}
	public void setH0640(Double h0640) {
		H0640 = h0640;
	}
	public Double getH0641() {
		return H0641;
	}
	public void setH0641(Double h0641) {
		H0641 = h0641;
	}
	public Double getH0642() {
		return H0642;
	}
	public void setH0642(Double h0642) {
		H0642 = h0642;
	}
	public Integer getH0643() {
		return H0643;
	}
	public void setH0643(Integer h0643) {
		H0643 = h0643;
	}
	public Integer getH0644() {
		return H0644;
	}
	public void setH0644(Integer h0644) {
		H0644 = h0644;
	}
	public Date getH0645() {
		return H0645;
	}
	public void setH0645(Date h0645) {
		H0645 = h0645;
	}
	public Date getH0646() {
		return H0646;
	}
	public void setH0646(Date h0646) {
		H0646 = h0646;
	}
	public Double getH0647() {
		return H0647;
	}
	public void setH0647(Double h0647) {
		H0647 = h0647;
	}
	public Double getH0648() {
		return H0648;
	}
	public void setH0648(Double h0648) {
		H0648 = h0648;
	}
	@Override
	public String toString() {
		return "Bill [H0601=" + H0601 + ", H0602=" + H0602 + ", H0603=" + H0603
				+ ", H0604=" + H0604 + ", H0605=" + H0605 + ", H0606=" + H0606
				+ ", H0607=" + H0607 + ", H0608=" + H0608 + ", H0609=" + H0609
				+ ", H0610=" + H0610 + ", H0611=" + H0611 + ", H0612=" + H0612
				+ ", H0613=" + H0613 + ", H0614=" + H0614 + ", H0615=" + H0615
				+ ", H0616=" + H0616 + ", H0617=" + H0617 + ", H0618=" + H0618
				+ ", H0619=" + H0619 + ", H0620=" + H0620 + ", H0621=" + H0621
				+ ", H0622=" + H0622 + ", H0623=" + H0623 + ", H0624=" + H0624
				+ ", H0625=" + H0625 + ", H0626=" + H0626 + ", H0627=" + H0627
				+ ", H0628=" + H0628 + ", H0629=" + H0629 + ", H0630=" + H0630
				+ ", H0631=" + H0631 + ", H0632=" + H0632 + ", H0633=" + H0633
				+ ", H0634=" + H0634 + ", H0635=" + H0635 + ", H0636=" + H0636
				+ ", H0637=" + H0637 + ", H0638=" + H0638 + ", H0639=" + H0639
				+ ", H0640=" + H0640 + ", H0641=" + H0641 + ", H0642=" + H0642
				+ ", H0643=" + H0643 + ", H0644=" + H0644 + ", H0645=" + H0645
				+ ", H0646=" + H0646 + ", H0647=" + H0647 + ", H0648=" + H0648
				+ "]";
	}
	
	public String csvString() {
		StringBuffer str = new StringBuffer();
		str.append(H0601).append(",");
		str.append(H0602).append(",");
		str.append(H0603).append(",");
		str.append(H0604).append(",");
		str.append(H0605).append(",");
		str.append(H0606).append(",");
		str.append(H0607).append(",");
		str.append(H0608).append(",");
		str.append(H0609).append(",");
		str.append(H0610).append(",");
		str.append(H0611).append(",");
		str.append(H0612).append(",");
		str.append(H0613).append(",");
		str.append(H0614).append(",");
		str.append(H0615).append(",");
		str.append(H0616).append(",");
		str.append(H0617).append(",");
		str.append(H0618).append(",");
		str.append(H0619).append(",");
		str.append(H0620).append(",");
		str.append(H0621).append(",");
		str.append(H0622).append(",");
		str.append(H0623).append(",");
		str.append(H0624).append(",");
		str.append(H0625).append(",");
		str.append(H0626).append(",");
		str.append(H0627).append(",");
		str.append(H0628).append(",");
		str.append(H0629).append(",");
		str.append(H0630).append(",");
		str.append(H0631).append(",");
		str.append(H0632).append(",");
		str.append(H0633).append(",");
		str.append(H0634).append(",");
		str.append(H0635).append(",");
		str.append(H0636).append(",");
		str.append(H0637).append(",");
		str.append(H0638).append(",");
		str.append(H0639).append(",");
		str.append(H0640).append(",");
		str.append(H0641).append(",");
		str.append(H0642).append(",");
		str.append(H0643).append(",");
		str.append(H0644).append(",");
		str.append(H0645).append(",");
		str.append(H0646).append(",");
		str.append(H0647).append(",");
		str.append(H0648).append(",");
		
		
		
		return str.toString();
	}
}
