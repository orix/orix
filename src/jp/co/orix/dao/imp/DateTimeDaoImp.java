package jp.co.orix.dao.imp;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.DateTimeDao;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class DateTimeDaoImp implements DateTimeDao {
	@Autowired
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	@Override
	public String dateTime() {

		String dateTime = "";
		SqlSession session = sqlSession.openSession();
		System.out.println(session);
		DateTimeDao dao = session.getMapper(DateTimeDao.class);
		
		try {
			dateTime = dao.dateTime();
			System.out.println(dateTime);
		
		
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		return dateTime;
	}
	public SqlSessionFactory getSqlSession() {
		return sqlSession;
	}
	public void setSqlSession(SqlSessionFactory sqlSession) {
		this.sqlSession = sqlSession;
	}

	
}
