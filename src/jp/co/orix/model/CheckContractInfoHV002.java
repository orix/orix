package jp.co.orix.model;

import java.util.Date;

public class CheckContractInfoHV002 {

	private int contractInformationId;		//H0201	高圧契約情報照会ID	number
	private int supplyPointNumber;			//H0202	供給地点特定番号		number
	private String groupId;					//H0203	グループID				varchar2(10)
	private String addressId;				//H0204	使用場所ID			varchar2(10)
	private String address;					//H0205	住所					varchar2(100)
	private String contractNumber;			//H0206	契約番号				varchar2(10)
	private String contractName;			//H0207	契約者名義人			varchar2(20)
	private String payment;					//H0208	お支払方法			varchar2(2)
	private String mainContract;			//H0209	主契約				varchar2(20)
	private String standbyPowerContact;		//H0210	予備電源契約			varchar2(20)
	private String standbyLineContact;		//H0211	予備線契約			varchar2(20)
	private int financialInsitutionId;		//H0212	金融機関ID			number
	private String financialInsitutionName;	//H0213	金融機関名			varchar2(20)
	private int branchId;					//H0214	支店ID				number
	private String branch;					//H0215	支店名				varchar2(20)
	private int accountId;					//H0216	口座科目ID			number
	private String accountName;				//H0217	口座科目名			varchar2(5)
	private String accountNumber;			//H0218	口座番号				varchar2(20)
	private String accountHolder;			//H0219	口座名義人			varchar2(20)
	private Date dateOfRecord;				//H0220	登録日				date
	private Date dateOfModification;		//H0221	更新日				date
	
	private UniversalInfoITHNT000 universalInfo;
	
	public int getContractInformationId() {
		return contractInformationId;
	}
	public void setContractInformationId(int contractInformationId) {
		this.contractInformationId = contractInformationId;
	}
	public int getSupplyPointNumber() {
		return supplyPointNumber;
	}
	public void setSupplyPointNumber(int supplyPointNumber) {
		this.supplyPointNumber = supplyPointNumber;
	}
	public String getGroupId() {
		return groupId;
	}
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}
	public String getAddressId() {
		return addressId;
	}
	public void setAddressId(String addressId) {
		this.addressId = addressId;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public String getContractName() {
		return contractName;
	}
	public void setContractName(String contractName) {
		this.contractName = contractName;
	}
	public String getPayment() {
		return payment;
	}
	public void setPayment(String payment) {
		this.payment = payment;
	}
	public String getMainContract() {
		return mainContract;
	}
	public void setMainContract(String mainContract) {
		this.mainContract = mainContract;
	}
	public String getStandbyPowerContact() {
		return standbyPowerContact;
	}
	public void setStandbyPowerContact(String standbyPowerContact) {
		this.standbyPowerContact = standbyPowerContact;
	}
	public String getStandbyLineContact() {
		return standbyLineContact;
	}
	public void setStandbyLineContact(String standbyLineContact) {
		this.standbyLineContact = standbyLineContact;
	}
	public int getFinancialInsitutionId() {
		return financialInsitutionId;
	}
	public void setFinancialInsitutionId(int financialInsitutionId) {
		this.financialInsitutionId = financialInsitutionId;
	}
	public String getFinancialInsitutionName() {
		return financialInsitutionName;
	}
	public void setFinancialInsitutionName(String financialInsitutionName) {
		this.financialInsitutionName = financialInsitutionName;
	}
	public int getBranchId() {
		return branchId;
	}
	public void setBranchId(int branchId) {
		this.branchId = branchId;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public int getAccountId() {
		return accountId;
	}
	public void setAccountId(int accountId) {
		this.accountId = accountId;
	}
	public String getAccountName() {
		return accountName;
	}
	public void setAccountName(String accountName) {
		this.accountName = accountName;
	}
	public String getAccountNumber() {
		return accountNumber;
	}
	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}
	public String getAccountHolder() {
		return accountHolder;
	}
	public void setAccountHolder(String accountHolder) {
		this.accountHolder = accountHolder;
	}
	public Date getDateOfRecord() {
		return dateOfRecord;
	}
	public void setDateOfRecord(Date dateOfRecord) {
		this.dateOfRecord = dateOfRecord;
	}
	public Date getDateOfModification() {
		return dateOfModification;
	}
	public void setDateOfModification(Date dateOfModification) {
		this.dateOfModification = dateOfModification;
	}
	public UniversalInfoITHNT000 getUniversalInfo() {
		return universalInfo;
	}
	public void setUniversalInfo(UniversalInfoITHNT000 universalInfo) {
		this.universalInfo = universalInfo;
	}
	
	@Override
	public String toString() {
		return "CheckContractInfoHV002 [contractInformationId="
				+ contractInformationId + ", supplyPointNumber="
				+ supplyPointNumber + ", groupId=" + groupId + ", addressId="
				+ addressId + ", address=" + address + ", contractNumber="
				+ contractNumber + ", contractName=" + contractName
				+ ", payment=" + payment + ", mainContract=" + mainContract
				+ ", standbyPowerContact=" + standbyPowerContact
				+ ", standbyLineContact=" + standbyLineContact
				+ ", financialInsitutionId=" + financialInsitutionId
				+ ", financialInsitutionName=" + financialInsitutionName
				+ ", branchId=" + branchId + ", branch=" + branch
				+ ", accountId=" + accountId + ", accountName=" + accountName
				+ ", accountNumber=" + accountNumber + ", accountHolder="
				+ accountHolder + ", dateOfRecord=" + dateOfRecord
				+ ", dateOfModification=" + dateOfModification + "]";
	}
		
	
}
