package jp.co.orix.dao;

import jp.co.orix.model.User;
import jp.co.orix.model.UserDetail;

import org.apache.ibatis.annotations.Param;

public interface UserDao {
	public User getUserData(@Param("identificationNumber") String identificationNumber) throws Exception;
	public User select(@Param("id") String id); 
//	public User checkAdmin(@Param("type") Integer type); , @Param("type") Integer type
//	public User checkUser(@Param("type") Integer type);
	public int nameUpdate(@Param("type") Integer type,  @Param("newM0201") String newM0201);
	public int mailUpdate(@Param("type") Integer type, @Param("newM0200") String newM0200);
	public int afterUpdate(@Param("type") Integer type, @Param("detail") UserDetail detail);
	public int detailUpdate(@Param("type") Integer type, @Param("detail") UserDetail detail);
	public User userSelect(@Param("id") String id); //, @Param("type") Integer type
	public User afterCheck(@Param("id") String id);
	public int getUrlAccessLimitDays(@Param("type") Integer type);
	public User limitDaysCheck(@Param("type") Integer type);
	public User userlimitDaysCheck(@Param("type") Integer type);
//	public getSystemPortalClass()
	public String dateTime();
	
}
