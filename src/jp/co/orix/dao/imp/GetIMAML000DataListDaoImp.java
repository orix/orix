package jp.co.orix.dao.imp;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.DateTimeDao;
import jp.co.orix.dao.GetIMAML000DataListDao;

public class GetIMAML000DataListDaoImp implements GetIMAML000DataListDao{
	@Autowired
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	@Override
	public String getIMAML000DataList() {
		String GetIMAML000DataList = "";
		SqlSession session = sqlSession.openSession();
		GetIMAML000DataListDao dao = session.getMapper(GetIMAML000DataListDao.class);
		
		try {
			GetIMAML000DataList = dao.getIMAML000DataList();
		
		
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		return GetIMAML000DataList;
	}
	
}
