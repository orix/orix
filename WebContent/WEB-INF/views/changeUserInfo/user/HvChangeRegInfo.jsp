<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<title>登録情報変更</title>

<style>
table {
	border-collapse: collapse;
	align: center;
	width: 50%;
}

th {
	background-color: #4F81BD;
	padding: 10px;
	color: white;
	border-radius: 8px;
	border-color: #000000;
	border: 2px solid white;
	font-size: 20px;
}

td {
	background-color: #FFFFFF;
	color: black;
	border: 2px white;
	border-radius: 5px;
	font-size: 20px;
}

input[type=text] {
	width: 100%;
	padding: 10px;
	margin: 0 0;
	box-sizing: border-box;
	border: 2px solid #1E4F8B;
	border-radius: 8px;
	font-size: 20px;
}

input[type=button] {
   background-color: #4F81BD;
    box-shadow: 0 4px 16px 0 rgba(0,0,0,0.2), 0 4px 10px 0 rgba(0,0,0,0.19);
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 500px;
    cursor: pointer;
    text-align: center;
    border-radius: 8px;
    font-size: 20px;
}

input[type=submit] {
   background-color: #4F81BD;
    box-shadow: 0 4px 16px 0 rgba(0,0,0,0.2), 0 4px 10px 0 rgba(0,0,0,0.19);
    border: none;
    color: white;
    padding: 16px 32px;
    text-decoration: none;
    margin: 4px 500px;
    cursor: pointer;
    text-align: center;
    border-radius: 8px;
    font-size: 20px;
}

</style>
<script src="<%=request.getContextPath() %>/resource/js/common_131.js"></script>
<script type="text/javascript">
	function onSubmit() {
		var newM0201 = document.getElementsByName("newM0201")[0].value;
		var newM0200 = document.getElementsByName("newM0200")[0].value;
		
		if(newM0201 == '') {
			alert('ユーザー名が入力されていません。');
			return false;
		}
		
		if(newM0200 == '') {
			alert('メールアドレスが入力されていません。');
			return false;
		}
		
		if(!IsValidateStringValue(newM0200)) {
			alert('半角で入力させてください。');
			return false;
		}
// 		【IsMailAddress】
		if (!IsMailAddress(newM0200)) {
			alert('メールアドレスに無効な値が入力されています。');
			return false;
		}

		document.forms["user"].action = "checkAdmin";
		document.forms["user"].submit();

	}
</script>
</head>
<body>

	<div align="center" style="color: #245A82">
		<h2>変更内容入力</h2>
		<br />	
		<h2>※「＊」の付いた項目は必ず入力してください。</h2>
	</div>

	<div align="center">
		<form name="user" method="post"  action="checkAdmin">
			<table>
				<tr>
					<th colspan="3" align="left">ユーザーID情報</th>
				</tr>

				<tr>
					<td></td>
					<th align="center">変更前</th>
					<th align="center">変更後</th>
				</tr>

				<tr>
					<th align="center">*ユーザー名</th>
					<td><input type="text" name="M0201" value="${M0201}" disabled/></td>
					<td><input type="text" name="newM0201" value="${M0201}"/></td>
				</tr>
				
				<tr>
					<th align="center">*メールアドレス</th>
					<td><input type="text" name="M0200" value="${user.mail.m0200 }" disabled></td>
					<td><input type="text" name="newM0200" value="${user.mail.m0200 }"></td>
				</tr>
			
				<tr align="center">
					<td colspan="3"><br/>
					<input type="button" value="内容確認" onClick="onSubmit()"/></td>
				</tr>
			</table>
		</form>
	</div>