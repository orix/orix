package jp.co.orix.util;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import jp.co.orix.model.SendMailVo;

public class SendMail{

	private SendMailVo sendMailVo;
	
	public void sendMail(SendMailVo sendMailVo) throws MessagingException{

		
		String host = "smtp.gmail.com";
		String username = "backsan211@gmail.com";	
		String password = "qortksinfo";

		// 메일 내용
		String recipient = sendMailVo.getRecipient();
		String subject = sendMailVo.getSubject();
		String body = sendMailVo.getBody();

		System.out.println("SendMail recipient = "+ recipient);
		System.out.println("SendMail subject = "+ subject);
		System.out.println("SendMail body = "+ body);
		
		
		//properties 설정
		Properties props = new Properties();
		props.put("mail.smtps.auth", "true");
		// 메일 세션
		Session session = Session.getDefaultInstance(props);
		MimeMessage msg = new MimeMessage(session);

		// 메일 관련
		msg.setSubject(subject);
		msg.setText(body);
		msg.setFrom(new InternetAddress(username));
		msg.addRecipient(Message.RecipientType.TO, new InternetAddress(recipient));

		// 발송 처리
		Transport transport = session.getTransport("smtps");
		transport.connect(host, username, password);
		transport.sendMessage(msg, msg.getAllRecipients());
		transport.close();     
	}

	public SendMailVo getSendMailVo() {
		return sendMailVo;
	}

	public void setSendMailVo(SendMailVo sendMailVo) {
		this.sendMailVo = sendMailVo;
	}
	
}