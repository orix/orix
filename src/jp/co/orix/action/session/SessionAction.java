package jp.co.orix.action.session;

import java.util.Map;

import jp.co.orix.dao.UserDao;
import jp.co.orix.model.User;
import jp.co.orix.model.UserDetail;
import jp.co.orix.model.UserMail;

import org.apache.struts2.dispatcher.SessionMap;
import org.apache.struts2.interceptor.SessionAware;

import com.opensymphony.xwork2.ActionSupport;

public class SessionAction extends ActionSupport implements SessionAware {

	private User user;
	private UserMail mail;
	private UserDetail detail;
	private UserDao userDao;
	
	private SessionMap<String, Object> session;
	
	private String  M0201;
	private int M0100;
	
	private int H1801;
	
	@Override
	public void setSession(Map<String, Object> session) {
		// TODO Auto-generated method stub
		this.session = (SessionMap<String, Object>) session;
	}

	public String getLoginInfoAdmin() throws Exception {
		M0201 = "admin";
//	    M0100 = 10001;
		session.put("M0201", M0201);
//		session.put("M0100", M0100); , M0100
		
		user =  userDao.select(M0201);
		System.out.println("result : " + user);
		System.out.println("result : " + user.getMail());
		System.out.println("user.getM0201 : " +user.getM0201());
		System.out.println("user.getM0100 : " + user.getM0100());
		return SUCCESS;
	}
	
	public String getLoginInfoUser() throws Exception {
		M0201 = "test";
//		M0100 = 10000;
		session.put("M0201", M0201);
//		session.put("M0100", M0100); , M0100
		
		user = userDao.userSelect(M0201);
		System.out.println("User : " + user);
		System.out.println("Mail : " + user.getMail());
		System.out.println("Detail : " + user.getDetail());
		System.out.println("user.getM0201 : " +user.getM0201());
		System.out.println("user.getM0100 : " + user.getM0100());
		return SUCCESS;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserMail getMail() {
		return mail;
	}

	public void setMail(UserMail mail) {
		this.mail = mail;
	}

	public UserDetail getDetail() {
		return detail;
	}

	public void setDetail(UserDetail detail) {
		this.detail = detail;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public SessionMap<String, Object> getSession() {
		return session;
	}

	public void setSession(SessionMap<String, Object> session) {
		this.session = session;
	}

	public String getM0201() {
		return M0201;
	}

	public void setM0201(String m0201) {
		M0201 = m0201;
	}

	public int getM0100() {
		return M0100;
	}

	public void setM0100(int m0100) {
		M0100 = m0100;
	}

	public int getH1801() {
		return H1801;
	}

	public void setH1801(int h1801) {
		H1801 = h1801;
	}
	
}
