package jp.co.orix.model;

import java.sql.Date;

public class UserDetail {

	//�����o�^���ύX�\���擾���ID
	private int H1801;
	//���[�U�[ID
	private String H1802;
	//��t�ԍ�
	private String H1803;
	//�ύX�t���O
	private String H1804;
	//�X�e�[�^�X
	private String H1805;
	//�X�֔ԍ�_�ύX�O
	private String H1806;
	//�X�֔ԍ�_�ύX��
	private String H1807;
	//�Z��_�ύX�O
	private String H1808;
	//�Z��_�ύX��
	private String H1809;
	//�r���E�}���V������_�ύX�O
	private String H1810;
	//�r���E�}���V������_�ύX��
	private String H1811;
	//��Ж�_�ύX�O
	private String H1812;
	//��Ж�_�ύX��
	private String H1813;
	//������_�ύX�O
	private String H1814;
	//������_�ύX��
	private String H1815;
	//�S����_�ύX�O
	private String H1816;
	//�S����_�ύX��
	private String H1817;
	//�o�^��
	private Date H1818;
	//�X�V��
	private Date H1819;
	
	private User user;
	
	public UserDetail() {
		super();
		// TODO Auto-generated constructor stub
	}
	
	public UserDetail(int h1801, String h1802, String h1803, String h1804,
			String h1805, String h1806, String h1807, String h1808,
			String h1809, String h1810, String h1811, String h1812,
			String h1813, String h1814, String h1815, String h1816,
			String h1817, Date h1818, Date h1819) {
		super();
		H1801 = h1801;
		H1802 = h1802;
		H1803 = h1803;
		H1804 = h1804;
		H1805 = h1805;
		H1806 = h1806;
		H1807 = h1807;
		H1808 = h1808;
		H1809 = h1809;
		H1810 = h1810;
		H1811 = h1811;
		H1812 = h1812;
		H1813 = h1813;
		H1814 = h1814;
		H1815 = h1815;
		H1816 = h1816;
		H1817 = h1817;
		H1818 = h1818;
		H1819 = h1819;
	}

	public int getH1801() {
		return H1801;
	}

	public void setH1801(int h1801) {
		H1801 = h1801;
	}

	public String getH1802() {
		return H1802;
	}

	public void setH1802(String h1802) {
		H1802 = h1802;
	}

	public String getH1803() {
		return H1803;
	}

	public void setH1803(String h1803) {
		H1803 = h1803;
	}

	public String getH1804() {
		return H1804;
	}

	public void setH1804(String h1804) {
		H1804 = h1804;
	}

	public String getH1805() {
		return H1805;
	}

	public void setH1805(String h1805) {
		H1805 = h1805;
	}

	public String getH1806() {
		return H1806;
	}

	public void setH1806(String h1806) {
		H1806 = h1806;
	}

	public String getH1807() {
		return H1807;
	}

	public void setH1807(String h1807) {
		H1807 = h1807;
	}

	public String getH1808() {
		return H1808;
	}

	public void setH1808(String h1808) {
		H1808 = h1808;
	}

	public String getH1809() {
		return H1809;
	}

	public void setH1809(String h1809) {
		H1809 = h1809;
	}

	public String getH1810() {
		return H1810;
	}

	public void setH1810(String h1810) {
		H1810 = h1810;
	}

	public String getH1811() {
		return H1811;
	}

	public void setH1811(String h1811) {
		H1811 = h1811;
	}

	public String getH1812() {
		return H1812;
	}

	public void setH1812(String h1812) {
		H1812 = h1812;
	}

	public String getH1813() {
		return H1813;
	}

	public void setH1813(String h1813) {
		H1813 = h1813;
	}

	public String getH1814() {
		return H1814;
	}

	public void setH1814(String h1814) {
		H1814 = h1814;
	}

	public String getH1815() {
		return H1815;
	}

	public void setH1815(String h1815) {
		H1815 = h1815;
	}

	public String getH1816() {
		return H1816;
	}

	public void setH1816(String h1816) {
		H1816 = h1816;
	}

	public String getH1817() {
		return H1817;
	}

	public void setH1817(String h1817) {
		H1817 = h1817;
	}

	public Date getH1818() {
		return H1818;
	}

	public void setH1818(Date h1818) {
		H1818 = h1818;
	}

	public Date getH1819() {
		return H1819;
	}

	public void setH1819(Date h1819) {
		H1819 = h1819;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	@Override
	public String toString() {
		return "UserDetail [H1801=" + H1801 + ", H1802=" + H1802 + ", H1803="
				+ H1803 + ", H1804=" + H1804 + ", H1805=" + H1805 + ", H1806="
				+ H1806 + ", H1807=" + H1807 + ", H1808=" + H1808 + ", H1809="
				+ H1809 + ", H1810=" + H1810 + ", H1811=" + H1811 + ", H1812="
				+ H1812 + ", H1813=" + H1813 + ", H1814=" + H1814 + ", H1815="
				+ H1815 + ", H1816=" + H1816 + ", H1817=" + H1817 + ", H1818="
				+ H1818 + ", H1819=" + H1819 + "]";
	}
	
}
