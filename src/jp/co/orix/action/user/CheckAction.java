package jp.co.orix.action.user;

import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.orix.dao.UserDao;
import jp.co.orix.model.User;
import jp.co.orix.model.UserDetail;
import jp.co.orix.model.UserMail;

import com.opensymphony.xwork2.ActionSupport;
import com.opensymphony.xwork2.ModelDriven;
import com.opensymphony.xwork2.Preparable;

public class CheckAction extends ActionSupport implements Preparable, ModelDriven {

	private User user;
	private UserMail mail;
	private UserDetail detail;
	private UserDao userDao;
	
	private int count;
	private String newM0201;
	private String newM0200;
	private String H1802;
	private int M0100;
	
	private int H1801;
	private String H1807;
	private String H1809;
	private String H1811;
	private String H1813;
	private String H1815;
	private String H1817;
	private Date H1819;

	@Override
	public void prepare() throws Exception {
		// TODO Auto-generated method stub
		detail = new UserDetail();
	}

	@Override
	public Object getModel() {
		// TODO Auto-generated method stub
		return detail;
	}

	public String checkAdmin() throws Exception {

		return SUCCESS;
	}
	
	public String checkUser() throws Exception {
		int afterUpdate;
		M0100 = 10000;
		H1802 = "test";
		System.out.println("H1801 = " +M0100);
		System.out.println("before : " +detail);
		afterUpdate = userDao.afterUpdate(M0100, detail);
		user = userDao.afterCheck(H1802);
		System.out.println("afterUpdate : " + afterUpdate);
		return SUCCESS;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserMail getMail() {
		return mail;
	}

	public void setMail(UserMail mail) {
		this.mail = mail;
	}

	public UserDetail getDetail() {
		return detail;
	}

	public void setDetail(UserDetail detail) {
		this.detail = detail;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public String getNewM0201() {
		return newM0201;
	}

	public void setNewM0201(String newM0201) {
		this.newM0201 = newM0201;
	}

	public String getNewM0200() {
		return newM0200;
	}

	public void setNewM0200(String newM0200) {
		this.newM0200 = newM0200;
	}

	public int getM0100() {
		return M0100;
	}

	public void setM0100(int m0100) {
		M0100 = m0100;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
	
	

	public String getH1802() {
		return H1802;
	}

	public void setH1802(String h1802) {
		H1802 = h1802;
	}

	public String getH1807() {
		return H1807;
	}

	public void setH1807(String h1807) {
		H1807 = h1807;
	}

	public String getH1809() {
		return H1809;
	}

	public void setH1809(String h1809) {
		H1809 = h1809;
	}

	public String getH1811() {
		return H1811;
	}

	public void setH1811(String h1811) {
		H1811 = h1811;
	}

	public String getH1813() {
		return H1813;
	}

	public void setH1813(String h1813) {
		H1813 = h1813;
	}

	public String getH1815() {
		return H1815;
	}

	public void setH1815(String h1815) {
		H1815 = h1815;
	}

	public String getH1817() {
		return H1817;
	}

	public void setH1817(String h1817) {
		H1817 = h1817;
	}

	public Date getH1819() {
		return H1819;
	}

	public void setH1819(Date h1819) {
		H1819 = h1819;
	}

	public int getH1801() {
		return H1801;
	}

	public void setH1801(int h1801) {
		H1801 = h1801;
	}

	
	
}
