package jp.co.orix.commons;

import com.opensymphony.xwork2.ActionSupport;

public class CommonAPIAction_030 extends ActionSupport{

	public CommonAPIDao_030 commonAPIDao_030;
	public int countLocationData;
	public String contractNumber;

	public String getUpdateUserNameSql() throws Exception {
		
		System.out.println("before");
		commonAPIDao_030.getUpdateUserNameSql("new", "changed29", 1, "lastSessionID2");
		System.out.println("after");
		return SUCCESS;
	}
	
	public String getLocationData() throws Exception {
		
		System.out.println("countLocationData : " + commonAPIDao_030.getLocationData());
		
		countLocationData = commonAPIDao_030.getLocationData();
		
		if (countLocationData == 7) {
			System.out.println("{0}出来ません。");
			return ERROR;
		} 
		
		return SUCCESS;
	}
	
	public String getContractNumber() throws Exception {
		
		System.out.println("contractNumber : " + commonAPIDao_030.getContractNumber("c0000"));
		contractNumber = commonAPIDao_030.getContractNumber("c0000");
		
		return SUCCESS;
	}
	
	@Override
	public String execute() throws Exception {
		return SUCCESS;
	}

	public CommonAPIDao_030 getCommonAPIDao_030() {
		return commonAPIDao_030;
	}

	public void setCommonAPIDao_030(CommonAPIDao_030 commonAPIDao_030) {
		this.commonAPIDao_030 = commonAPIDao_030;
	}

	public int getCountLocationData() {
		return countLocationData;
	}

	public void setCountLocationData(int countLocationData) {
		this.countLocationData = countLocationData;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	
	
	
}
