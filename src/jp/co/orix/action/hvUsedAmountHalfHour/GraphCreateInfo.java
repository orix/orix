package jp.co.orix.action.hvUsedAmountHalfHour;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class GraphCreateInfo {
	//c.　昼間凡例を設定する。
	private List<String> labels;
	//a.　凡例名に”比較日”を設定する。b.　各配色をWeb.cconfigのPowerBorderColorより取得し、設定する。c.　昼間凡例を設定する。 d.　夜間凡例を設定する。e.　各項目設定を行う。f.　指定日の時間別使用量を設定する。ｲ.　取得した時間別使用量が昼間の場合、昼間リストに設定し、h.　比較表示フラグが’ON’かつ比較データが存在する場合、比較日の時間別使用量を設定する。
	private List<Map<String,Object>> datasets;
	//f.　指定日の時間別使用量を設定する。ｱ.　使用量設定値 = 使用量 * 10000M 
	private String xScaleLabel;
	private String yScaleLabel;
	
	public GraphCreateInfo() {
		labels = new ArrayList<String>();
		datasets = new ArrayList<Map<String,Object>>();
		xScaleLabel = "" ;
		yScaleLabel = "" ;
	}
	
	public List<String> getLabels() {
		return labels;
	}
	public void setLabels(List<String> labels) {
		this.labels = labels;
	}
	public List<Map<String, Object>> getDatasets() {
		return datasets;
	}
	public void setDatasets(List<Map<String, Object>> datasets) {
		this.datasets = datasets;
	}

	public String getxScaleLabel() {
		return xScaleLabel;
	}

	public void setxScaleLabel(String xScaleLabel) {
		this.xScaleLabel = xScaleLabel;
	}

	public String getyScaleLabel() {
		return yScaleLabel;
	}

	public void setyScaleLabel(String yScaleLabel) {
		this.yScaleLabel = yScaleLabel;
	}
	
	

}
