package jp.co.orix.dao.imp;

import java.util.ArrayList;
import java.util.Map;

import jp.co.orix.dao.ModifyDao_040;
import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.model.KubunCombobox;
import jp.co.orix.model.MailMessageData;
import jp.co.orix.model.ModifyVo;
import jp.co.orix.model.UsePlace;
import jp.co.orix.model.UsePlaceCombobox;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class ModifyDaoImp_040 implements ModifyDao_040 {
		
	ArrayList<UsePlaceCombobox> usePlaceCombobox;
	UsePlace usePlace;
	ArrayList<KubunCombobox> kubunCombobox;
	MailMessageData mailMessageDate;
	
		
	public ArrayList<UsePlaceCombobox> getUsePlaceCombobox() {		return usePlaceCombobox;	}
	public void setUsePlaceCombobox(ArrayList<UsePlaceCombobox> usePlaceCombobox) {		this.usePlaceCombobox = usePlaceCombobox;	}
	
	@Autowired
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	
	public ArrayList<UsePlaceCombobox> GetUsePlaceComboboxList() throws Exception {
		
		SqlSession session = sqlSession.openSession();
		ModifyDao_040 query = session.getMapper(ModifyDao_040.class);
		
		try {
			usePlaceCombobox = query.GetUsePlaceComboboxList();
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			session.close();
		}
				
		return usePlaceCombobox;
	}
	@Override
	public UsePlace GetUsePlaceUsingH0804(String H0804) throws Exception {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		ModifyDao_040 query = session.getMapper(ModifyDao_040.class);
		
		try {
			usePlace = query.GetUsePlaceUsingH0804(H0804);
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			session.close();
		}
				
		return usePlace;
		
	}
	
	@Override
	public ArrayList<KubunCombobox> GetKubunComboboxList(Map<String,Integer> condition) throws Exception {
		SqlSession session = sqlSession.openSession();
		ModifyDao_040 query = session.getMapper(ModifyDao_040.class);
		
		try {
			kubunCombobox = query.GetKubunComboboxList(condition);
		} catch (Exception e) {
			// TODO: handle exception
		}finally{
			session.close();
		}
				
		return kubunCombobox;
	}
	
	@Override
	public void ModifyPlace(ModifyVo modifyvo) {

		SqlSession session = sqlSession.openSession();
		ModifyDao_040 query = session.getMapper(ModifyDao_040.class);
		try {
			query.ModifyPlace(modifyvo);
			session.commit();

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
	}
	
	@Override
	public MailMessageData GetMailMessageData(String Type) throws Exception {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		ModifyDao_040 query = session.getMapper(ModifyDao_040.class);
		
		try{
			mailMessageDate = query.GetMailMessageData(Type);
		}catch(Exception e){
			
		}finally{
			session.close();
		}
		
		//return MailMessageData;
		return mailMessageDate;
	}
}
