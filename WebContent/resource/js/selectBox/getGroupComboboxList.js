$(document).ready(function() {
	$.getJSON('getUsePlaceComboboxList', function(jsonResponse) {
		/* $('#ajaxResponse').text(jsonResponse.dummyMsg); */
		var select = $('#groupCombobox');
		//alert(select);
		select.find('option').remove();
		$.each(jsonResponse.groupComboboxMap, function(key, value) {
			$('<option>').val(key).text(value).appendTo(select);
		});
	});
});