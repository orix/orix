<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="s" uri="/struts-tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<style>
.font{
	color : #245A82;
	font-weight:bold;
}

table {
	border-collapse: collapse;
	align: center;
	 margin: auto;
}
th {
	background-color: #4F81BD;
	padding: 6px;
	color: white;
	border-radius: 7px;
	border-color: #000000;
	border-top: 2px solid white;
	border-left: 2px solid white;
	border-right: 2px solid white;
}
.button1{
	background-color: #4F81BD;
	padding: 7px 20px;
	color: white;
    border-bottom: 2px solid #323C73;
	border-right: 2px solid #323C73;
    border-radius: 7px;
    width: 80px;
    font-weight:bold;
}
.button2{
	background-color: #4F81BD;
	padding: 7px 20px;
	color: white;
    border-bottom: 2px solid #323C73;
	border-right: 2px solid #323C73;
    border-radius: 7px;
    width: 120px;
    font-weight:bold;
    
}
.button3{
	background-color: #4F81BD;
	padding: 7px 20px;
	color: white;
    border-bottom: 2px solid #323C73;
	border-right: 2px solid #323C73;
    border-radius: 7px;
    width: 150px;
    position:absolute;  
    bottom:100px;
    display:inline-block;
	font-weight:bold;
}
.td {
	background-color: #FFFFFF;
	padding: 6px;
	color: black;
 	border-radius: 7px; 
	border-color: #4F81BD;
	border: 1px solid #4F81BD;
}

</style>
<script type="text/javascript">
	$(function(){
		$("#goSearchExcel").click(function(){
			var useDate = $("#useDate").val();
			var path = "/excel/excelViewForm?useDate="+ useDate;
					
			var group = $("#groupCombobox").val();
			if(group.length!=0)
				path += "&groupCombobox=" + group;
			var place = $("#usePlaceCombobox").val();
			if(place.length!=0)
				path += "&usePlaceCombobox=" + place;
			
			console.log(path);
			
			window.location = path;
		});
	});
	
	function excelDown(){
		   var check_obj = document.getElementsByName("M0621");
		   var check_leng = check_obj.length;
		   var checked = 0;
		   
		   for(i=0;i<check_leng;i++){
			   if(check_obj[i].checked == true){
				   checked += 1;
			   }
		   }
		   if(checked == 0){
			   alert("ダウンロードを行うご使用場所を選択してください。");
			   history.go(0);
		   }else{
				var retVal = confirm("ファイルのダウンロードを行います。よろしいですか？");
				   if(retVal == true ){
					document.forms["excel"].action = "excela";
					document.forms["excel"].submit();
// 					   window.location = "/excela";
				   }else{
				      alert("취소되었습니다.");
				      history.go(-1);
				   }
		   }
	}
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/jquery/1.9.0/jquery.js"></script>

<script type="text/javascript"
	src="../../resource/js/selectBox/getGroupAndUsePlaceComboboxList2.js"></script>

<script>
function checkboxSelectQue(n,obj) {
    var i;
    var chk = document.getElementsByName(obj);
    var tot = chk.length;
    for (i = 0; i < tot; i++) {
        if (n == 1) chk[i].checked = true;
        if (n == 2) chk[i].checked = false;
        if (n == 3) chk[i].checked = !chk[i].checked;
    }
}

</script>

<div id="content" style=" padding-top:10px; padding-bottom:10px; text-align:center; align:center;">
		<input type="hidden" name="parameter" id="parameter"
		value="${parameter }">
	<table style="width:100%; border-bottom: 3px solid #4F81BD;">
		<tr>
			<td width="30%"  class="font">
			グループ
				<select
 			id="groupCombobox" name="groupCombobox" style="width:250px;height:30px;" > 
			<option value=""></option>
 		</select>
			</td>
			<td width="50%"  class="font">
			ご使用場所
				<select
 			id="usePlaceCombobox" name="usePlaceCombobox" style="width:500px;height:30px;" ><option 
 				value=""></option></select>&nbsp;&nbsp;&nbsp;&nbsp; 
 		
</td>

			
			<td width="20%"  class="font">
			出力年月
				<select id="useDate" style="width:120px;height:30px;">
			<s:iterator value="sixMonth" status="st">
				<option>${sixMonth[st.index]}</option>
			</s:iterator>
				</select>
			</td>
			<td width="10%">	
<!-- 				<input type="button" class="button1" value="検索" id="goSearchExcel" /> -->
				<button id="goSearchExcel" class="button1" value="">検索</button>
<!-- 				onClick="search(month.value)" -->
			</td>
		</tr>
	</table>
<br/>
		<h3 align="center" class="font" >計量日誌ファイルダウンロード</h3>
		<form name="excel" method="post">
	<table style="width:85%;">
	<tr align="left">
	<td colspan="5"><input type="button" class="button2" value="全選択" style="width: 120px;" onClick="checkboxSelectQue(1,'M0621')"/>&nbsp;&nbsp;&nbsp;
	<input type="button" class="button2" value="全選択解除" onClick="checkboxSelectQue(2,'M0621')"/></td>
	</tr>
	<tr>
	<th width="5%">No</th>
	<th width="10%">選択</th>
	<th width="30%">ご使用場所</th>
	<th width="10%">対象年月</th>
	<th width="45%">ファイル</th>
	</tr>
		<s:iterator value="listSearch">
						<tr class="td">
	<td class="td" width="5%"><s:property value="%{M0100}"/></td>
	<td class="td" width="10%"><input type="checkbox" name="M0621" value="<s:property value="%{M0621}"/>"/></td>
	<td class="td" width="30%"><s:property value="%{M0402}"/></td>
	<td class="td" width="10%"><s:property value="%{M0401}"/></td>
	<td class="td" width="45%"><s:property value="%{M0620}"/></td>
						</tr>
		</s:iterator>
	</table>
		<div style="display:inline-block; margin:auto;"><input type="submit" class="button3" value="ダウンロード" onClick="excelDown()"/></div>
		</form>
</div>