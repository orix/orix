package jp.co.orix.dao;

import java.util.List;

import jp.co.orix.model.MonthList;

import org.apache.ibatis.annotations.Param;

public interface MonthListDao {

//	public List<String> excelDown(@Param("type") String type);
	public List<MonthList> selectEx(@Param("searchDate") int searchDate,@Param("m0402") String m0402);
//	public List<MonthList> selectEx(int searchDate, String m0402);
	public List<MonthList> getMonth(@Param("getMonth") int getMonth);
//	public void test2();
}
