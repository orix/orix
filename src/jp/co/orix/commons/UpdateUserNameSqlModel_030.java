package jp.co.orix.commons;

import java.util.Date;

public class UpdateUserNameSqlModel_030 {

	private String M0100;
	private String M0202;
	private Date M9951;
	private String M9952;
	
	public UpdateUserNameSqlModel_030() {
	}

	public UpdateUserNameSqlModel_030(String m0100, String m0202, Date m9951,
			String m9952) {
		M0100 = m0100;
		M0202 = m0202;
		M9951 = m9951;
		M9952 = m9952;
	}

	public String getM0100() {
		return M0100;
	}

	public void setM0100(String m0100) {
		M0100 = m0100;
	}

	public String getM0202() {
		return M0202;
	}

	public void setM0202(String m0202) {
		M0202 = m0202;
	}

	public Date getM9951() {
		return M9951;
	}

	public void setM9951(Date m9951) {
		M9951 = m9951;
	}

	public String getM9952() {
		return M9952;
	}

	public void setM9952(String m9952) {
		M9952 = m9952;
	}

	@Override
	public String toString() {
		return "UpdateUserNameSqlModel [M0100=" + M0100 + ", M0202=" + M0202
				+ ", M9951=" + M9951 + ", M9952=" + M9952 + "]";
	}
	
}
