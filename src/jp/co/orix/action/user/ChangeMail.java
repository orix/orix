package jp.co.orix.action.user;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import jp.co.orix.dao.UserDao;
import jp.co.orix.model.User;
import jp.co.orix.model.UserDetail;
import jp.co.orix.model.UserMail;

import com.opensymphony.xwork2.ActionSupport;

public class ChangeMail extends ActionSupport {

	private User user;
	private UserMail mail;
	private UserDetail detail;
	private UserDao userDao;

	private String newM0200;
	private int M0100;
	private String M0201;
	private String M0301;
	
	private String today;
	private Date limitDay;
	private SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
	private Date sysdate;
	
	
	public String changeMail() throws Exception {
		int mailUpdate;
		
		System.out.println("CMail M0301 = " + M0301);

		
		if (M0301.equals("1")) {

			M0201 = "admin";

			user = userDao.select(M0201);
			user = userDao.limitDaysCheck(M0100);
			
			limitDay = user.getMail().getM0400();
			today = userDao.dateTime();
			sysdate =  fmt.parse(today);
			
			int compare = sysdate.compareTo(limitDay);
			
			if(compare <= 0) {
				
				mailUpdate = userDao.mailUpdate(M0100, newM0200);
				
			} else if (compare > 0) {
				
				return ERROR;
			}

		} else {

			M0201 = "test";

			user = userDao.select(M0201);
			user = userDao.userlimitDaysCheck(M0100);
			
			limitDay = user.getMail().getM0400();
			today = userDao.dateTime();
			sysdate =  fmt.parse(today);
			int compare = sysdate.compareTo(limitDay);
			
			if(compare <= 0) {
				
				mailUpdate = userDao.mailUpdate(M0100, newM0200);
				
			} else if (compare > 0) {
				
				return ERROR;
			}
			
		}

		return SUCCESS;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public UserMail getMail() {
		return mail;
	}

	public void setMail(UserMail mail) {
		this.mail = mail;
	}

	public UserDetail getDetail() {
		return detail;
	}

	public void setDetail(UserDetail detail) {
		this.detail = detail;
	}

	public UserDao getUserDao() {
		return userDao;
	}

	public void setUserDao(UserDao userDao) {
		this.userDao = userDao;
	}

	public String getNewM0200() {
		return newM0200;
	}

	public void setNewM0200(String newM0200) {
		this.newM0200 = newM0200;
	}

	public int getM0100() {
		return M0100;
	}

	public void setM0100(int m0100) {
		M0100 = m0100;
	}

	public String getM0201() {
		return M0201;
	}

	public void setM0201(String m0201) {
		M0201 = m0201;
	}

	public String getM0301() {
		return M0301;
	}

	public void setM0301(String m0301) {
		M0301 = m0301;
	}

	public String getToday() {
		return today;
	}

	public void setToday(String today) {
		this.today = today;
	}

	public Date getLimitDay() {
		return limitDay;
	}

	public void setLimitDay(Date limitDay) {
		this.limitDay = limitDay;
	}

	public DateFormat getFmt() {
		return fmt;
	}

	public void setFmt(SimpleDateFormat fmt) {
		this.fmt = fmt;
	}

	public Date getSysdate() {
		return sysdate;
	}

	public void setSysdate(Date sysdate) {
		this.sysdate = sysdate;
	}

}
