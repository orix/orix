<%@page import="jp.co.orix.model.KubunCombobox"%>
<%@page import="jp.co.orix.model.UsePlace"%>
<%@page import="jp.co.orix.model.UsePlaceCombobox"%>
<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%
	ArrayList<UsePlaceCombobox> usePlaceComboboxList;	
	usePlaceComboboxList = new ArrayList();
	
	if(request.getAttribute("usePlaceComboboxList")!=null){
		usePlaceComboboxList = (ArrayList<UsePlaceCombobox>)request.getAttribute("usePlaceComboboxList");
	}
	
	UsePlace usePlace = new UsePlace();
	
	if(request.getAttribute("usePlace")!=null){
		usePlace = (UsePlace)request.getAttribute("usePlace");
	}else{
		
	}
	
	ArrayList<KubunCombobox> kubunComboboxList;
	kubunComboboxList = new ArrayList();
	
	if(request.getAttribute("kubunComboboxList")!=null){
		kubunComboboxList = (ArrayList<KubunCombobox>)request.getAttribute("kubunComboboxList");
	}
	int kubunsize = kubunComboboxList.size();
%>
<script type="text/javascript">
	function equalKubun1() {
		document.getElementById("select2").selectedIndex = document
				.getElementById("select1").selectedIndex;
	}
	function equalKubun2() {
		document.getElementById("select1").selectedIndex = document
				.getElementById("select2").selectedIndex;
	}
	function equalKubun3() {
		document.getElementById("select4").selectedIndex = document
				.getElementById("select3").selectedIndex;

	}
	function equalKubun4() {
		document.getElementById("select3").selectedIndex = document
				.getElementById("select4").selectedIndex;
	}
	
	function Remove_Error(str){
		document.getElementById(str).innerHTML = ""
	}
	
	function IsValidateStringValue(str) {
		var letters = /^[A-Za-z]+$/;
		if (!str.value.match(letters)) {
			return false;
		} else {
			return true;
		}
	}
	function IsValidCombobox(str1,str2){
		if(str1 == str2 && str1!=0){
			return true;
		}else{
			return false;
		}
	}
	
	function IsValidateWideStringValue(str){
		
		for(var i=0;i<str.length;i++){
		 var c = str.charCodeAt(i);
		 if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
			 	return false;
			}else{
				return true;
			}
		}
	}
	
	function IsMailAddress(str){
		var regex = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;

        if(!regex.test(str)) {
            return false;
            }
        else{
        	return true;
        }
	}
	
	function IsPostalNo(str) {
		var regPostcode = /^([0-9]){3}[-]([0-9]){4}$/;
		if(!regPostcode.test(str)){
			return false;
		}
		else{
			return true;
		}
	}
	
	function IsBlank(str){
		if(str.length == 0 || !str.trim()){
			return true;
		}else{
			return false;
		}
		
	}
	
	function IsNumber(n) {
		  return !isNaN(parseFloat(n)) && isFinite(n);
	}
	
	function IsBango1(n){
		return n.length<2 || n.length>=6;
	}
	
	function IsBango2(n){
		return n.length>=5;
	}
	
	function IsBango3(n){
		return n.length<4 || n.length>=5;
	}
	
	function IsBangoLength(n1,n2,n3){
		return n1.length + n2.length + n3.length < 10 || n1.length + n2.length + n3.length >= 12;
	}


	function Modify_Validation() {
		
		var error_cnt = 0;
		
		/* 1)設備１チェック */
		var Equipment1 = document.getElementsByName("modifyvo.Equipment1")[0].value;
		var EquipmentKubun1 = document.getElementsByName("modifyvo.EquipmentKubun1")[0].value;
		var Equipment1_index = document.getElementsByName("modifyvo.Equipment1")[0].selectedIndex;
		var EquipmentKubun1_index = document.getElementsByName("modifyvo.EquipmentKubun1")[0].selectedIndex;
		
		if(!IsValidCombobox(Equipment1_index,EquipmentKubun1_index)){
			document.getElementById("Equipment1_error").innerHTML = "設備1に無効な値が選択されています。"
			error_cnt++;
		}else{
			Remove_Error("Equipment1_error");
		}
		if(!IsBlank(Equipment1)&&IsBlank(EquipmentKubun1)){
			document.getElementById("EquipmentKubun1_error").innerHTML = "区分1を選択して下さい。"
				error_cnt++;
		}else{
			Remove_Error("EquipmentKubun1_error");
		}
		
		/* 2)設備2チェック */
		var Equipment2 = document.getElementsByName("modifyvo.Equipment2")[0].value;
		var Equipment2_index = document.getElementsByName("modifyvo.Equipment2")[0].selectedIndex;
		var EquipmentKubun2 = document.getElementsByName("modifyvo.EquipmentKubun2")[0].value;
		var EquipmentKubun2_index = document.getElementsByName("modifyvo.EquipmentKubun2")[0].selectedIndex;
				
		if(!IsBlank(Equipment2)){
			
			if(!IsValidCombobox(Equipment2_index,EquipmentKubun2_index)){
				document.getElementById("Equipment2_error").innerHTML = "設備1に無効な値が選択されています。"
					error_cnt++;
			}else{
				Remove_Error("Equipment2_error");
			}
			
			if(!IsBlank(Equipment2) && IsBlank(EquipmentKubun2)){
				document.getElementById("EquipmentKubun2_error").innerHTML = "区分1を選択して下さい。"
					error_cnt++;
			}else{
				Remove_Error("EquipmentKubun2_error");
			} 
			
			
		}
		
		/* 3)補足事項チェック */
		var Supplement = document.getElementsByName("modifyvo.Supplement")[0].value;
		
		if(!IsValidateWideStringValue(Supplement)){
			document.getElementById("Supplement_error").innerHTML = "補足事項は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("Supplement_error");
		}
		
		/* 4)事業者名チェック */
		var WorkCompanyName = document.getElementsByName("modifyvo.WorkCompanyName")[0].value;
		
		if(!IsValidateWideStringValue(WorkCompanyName)){
			document.getElementById("WorkCompanyName_error").innerHTML = "事業者名は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("WorkCompanyName_error");
		}
		
		/* 5)工事事業者担当チェック */
		var WorkTantoName = document.getElementsByName("modifyvo.WorkTantoName")[0].value;
		
		if(!IsValidateWideStringValue(WorkTantoName)){
			document.getElementById("WorkTantoName_error").innerHTML = "担当は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("WorkTantoName_error");
		}
		
		/* 6)工事事業者連絡先詳細チェック */
		var WorkTel1 = document.getElementsByName("modifyvo.WorkTel1")[0].value;
		var WorkTel2 = document.getElementsByName("modifyvo.WorkTel2")[0].value;
		var WorkTel3 = document.getElementsByName("modifyvo.WorkTel3")[0].value;
		
		if(IsBlank(WorkTel1) || !IsNumber(WorkTel1) || IsBango1(WorkTel1)){			
			document.getElementById("WorkTel1_error").innerHTML = "市外局番を整数2桁以上、5桁以内で入力してください。"
				error_cnt++;
		}else{
			Remove_Error("WorkTel1_error");
		}
		if(IsBlank(WorkTel2) || !IsNumber(WorkTel2) || IsBango2(WorkTel2)){			
			document.getElementById("WorkTel2_error").innerHTML = "市内局番を整1桁以上、4桁以内で入力してください。"
				error_cnt++;
		}else{
			Remove_Error("WorkTel2_error");
		}
		if(IsBlank(WorkTel3) || !IsNumber(WorkTel3) || IsBango3(WorkTel3)){			
			document.getElementById("WorkTel3_error").innerHTML = "加入者番号を整数4桁で入力してください。"
				error_cnt++;
		}else{
			Remove_Error("WorkTel3_error");
		}
		if(
				document.getElementById("WorkTel1_error").innerHTML=="" && 
				document.getElementById("WorkTel2_error").innerHTML=="" && 
				document.getElementById("WorkTel3_error").innerHTML=="" && 
				IsBangoLength(WorkTel1,WorkTel2,WorkTel2)){
				document.getElementById("WorkTel_error").innerHTML = "電話番号を正しく入力してください。"
				error_cnt++;
		}else{
		/* 	document.getElementById("modifyvo.WorkTel").value = WorkTel1 +'-'+ WorkTel2 +'-'+ WorkTel3; */
			Remove_Error("WorkTel_error");
		}
		
		/* 7)主任技術者の社名チェック */
		var MainEngineerCompanyName = document.getElementsByName("modifyvo.MainEngineerCompanyName")[0].value;
		
		if(!IsValidateWideStringValue(MainEngineerCompanyName)){
			document.getElementById("MainEngineerCompanyName_error").innerHTML = "は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("MainEngineerCompanyName_error");
		}
		/* 8)氏名チェック */
		var MainEngineerName = document.getElementsByName("modifyvo.MainEngineerName")[0].value;
		
		if(!IsValidateWideStringValue(MainEngineerName)){
			document.getElementById("MainEngineerName_error").innerHTML = "社名は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("MainEngineerName_error");
		}
		/* 9)初回連絡先チェック */
		var EngineerTel1 = document.getElementsByName("modifyvo.MainEngineerTel1")[0].value;
		var EngineerTel2 = document.getElementsByName("modifyvo.MainEngineerTel2")[0].value;
		var EngineerTel3 = document.getElementsByName("modifyvo.MainEngineerTel3")[0].value;
		
		if(IsBlank(EngineerTel1) || !IsNumber(EngineerTel1) || IsBango1(EngineerTel1)){			
			document.getElementById("EngineerTel1_error").innerHTML = "市外局番を整数2桁以上、5桁以内で入力してください。"
				error_cnt++;
		}else{
			Remove_Error("EngineerTel1_error");
		}
		if(IsBlank(EngineerTel2) || !IsNumber(EngineerTel2) || IsBango2(EngineerTel2)){			
			document.getElementById("EngineerTel2_error").innerHTML = "市内局番を整1桁以上、4桁以内で入力してください。"
				error_cnt++;
		}else{
			Remove_Error("EngineerTel2_error");
		}
		if(IsBlank(EngineerTel3) || !IsNumber(EngineerTel3) || IsBango3(EngineerTel3)){			
			document.getElementById("EngineerTel3_error").innerHTML = "加入者番号を整数4桁で入力してください。"
				error_cnt++;
		}else{
			Remove_Error("EngineerTel3_error");
		}
		if(
				document.getElementById("EngineerTel1_error").innerHTML=="" && 
				document.getElementById("EngineerTel2_error").innerHTML=="" && 
				document.getElementById("EngineerTel3_error").innerHTML=="" &&
				IsBangoLength(EngineerTel1,EngineerTel2,EngineerTel3)){
			document.getElementById("EngineerTel_error").innerHTML = "電話番号を正しく入力してください。"
				error_cnt++;
		}else{
			/* document.getElementById("modifyvo.MainEngineerTel").value = EngineerTel1 +'-'+ EngineerTel2 +'-'+ EngineerTel3; */
			Remove_Error("EngineerTel_error");
		}
		/* 10)申込担当者情報の郵便番号チェック */
		var YubinBango = document.getElementsByName("modifyvo.YubinBango")[0].value;
		
		if(IsBlank(YubinBango)){
			document.getElementById("YubinBango_error").innerHTML = "郵便番号に無効な値が選択されています。"
				error_cnt++;

		}else{
			Remove_Error("YubinBango_error");
		}
		
		if(!IsBlank(YubinBango) && !IsPostalNo(YubinBango)){
			document.getElementById("YubinBango_error").innerHTML = "郵便番号に無効な値が選択されています。"
				error_cnt++;
		}
		
		/* 11)申込担当者情報の住所チェック*/
		var Jusho = document.getElementsByName("modifyvo.Jusho")[0].value;
		
		if(!IsValidateWideStringValue(Jusho)){
			document.getElementById("Jusho_error").innerHTML = "は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("Jusho_error");
		}
		
		/* 12)申込担当者情報の会社名チェック */
		var HojinName = document.getElementsByName("modifyvo.HojinName")[0].value;
		
		if(!IsValidateWideStringValue(HojinName)){
			document.getElementById("HojinName_error").innerHTML = "住所は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("HojinName_error");
		}
		
		/* 13)申込担当者情報の部署名チェック  ???????????????????????*/
		/* 14)申込担当者情報の部署名チェック */
		var BushoName = document.getElementsByName("modifyvo.BushoName")[0].value;
		
		if(!IsValidateWideStringValue(BushoName)){
			document.getElementById("BushoName_error").innerHTML = "部署名は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("BushoName_error");
		}		
		
		/* 15)申込担当者情報のご担当者チェック */
		var TantoName = document.getElementsByName("modifyvo.TantoName")[0].value;
		
		if(!IsValidateWideStringValue(TantoName)){
			document.getElementById("TantoName_error").innerHTML = "ご担当者は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("TantoName_error");
		}
		/* 16)申込担当者情報の電話番号チェック */
		var Tel1 = document.getElementsByName("modifyvo.Tel1")[0].value;
		var Tel2 = document.getElementsByName("modifyvo.Tel2")[0].value;
		var Tel3 = document.getElementsByName("modifyvo.Tel3")[0].value;
		
		if(IsBlank(Tel1) || !IsNumber(Tel1) || IsBango1(Tel1)){			
			document.getElementById("Tel1_error").innerHTML = "市外局番を整数2桁以上、5桁以内で入力してください。"
				error_cnt++;
		}else{
			Remove_Error("Tel1_error");
		}
		if(IsBlank(Tel2) || !IsNumber(Tel2) || IsBango2(Tel2)){			
			document.getElementById("Tel2_error").innerHTML = "市内局番を整1桁以上、4桁以内で入力してください。"
				error_cnt++;
		}else{
			Remove_Error("Tel2_error");
		}
		if(IsBlank(Tel3) || !IsNumber(Tel3) || IsBango3(Tel3)){			
			document.getElementById("Tel3_error").innerHTML = "加入者番号を整数4桁で入力してください。"
				error_cnt++;
		}else{
			Remove_Error("Tel3_error");
		}
		if(	
				document.getElementById("Tel1_error").innerHTML=="" && 
				document.getElementById("Tel2_error").innerHTML=="" && 
				document.getElementById("Tel3_error").innerHTML=="" &&				
				IsBangoLength(Tel1,Tel2,Tel3)){
			document.getElementById("Tel_error").innerHTML = "電話番号を正しく入力してください。"
				error_cnt++;
		}else{
			/* document.getElementById("modifyvo.Tel").value = Tel1 +'-'+ Tel2 +'-'+ Tel3; */
			Remove_Error("Tel_error");
		}
		/* 17)申込担当者情報のメールアドレスチェック */
			var MailAddress = document.getElementsByName("modifyvo.MailAddress")[0].value;
		
		if(IsBlank(MailAddress)){
			document.getElementById("MailAddress_error").innerHTML = "メールアドレスに無効な値が選択されています。"
				error_cnt++;

		}else{
			Remove_Error("MailAddress_error");
		}
		
		if(!IsBlank(MailAddress) && !IsMailAddress(MailAddress)){
			document.getElementById("MailAddress_error").innerHTML = "メールアドレスに無効な値が選択されています。"
				error_cnt++;
		}
		
		/* 18)自由記入欄チェック */
		var Memo = document.getElementsByName("modifyvo.Memo")[0].value;
		
		if(!IsValidateWideStringValue(Memo)){
			document.getElementById("Memo_error").innerHTML = "自由記入欄は全角文字を入力してください。"
				error_cnt++;

		}else{
			Remove_Error("Memo_error");
		}
		
		
		return error_cnt==0;
	}

</script>

<div class="modify">

	<div class="search">
		<form action="Search">
			ご使用場所<input type="text" list="usePlaceComboboxList" name="H0804"
				value="${H0804}" />
			<datalist id="usePlaceComboboxList">
				<%
					for (int i = 0; i < usePlaceComboboxList.size(); i++) {
				%>
				<option value="<%=usePlaceComboboxList.get(i).getH0804()%>">
					<%
						}
					%>
				
			</datalist>
			<input type="submit" value="検索">
		</form>
	</div>
	<hr>
	<br />
	<div class="result">
		<%
			if (request.getAttribute("usePlace") != null) {
		%>
		<table>
			<tr>
				<th>住所</th>

				<td><input type="text" value="<%=usePlace.getH0802()%>"
					readonly></td>
			</tr>
			<tr>
				<th>ご使用場所</th>
				<td><input type="text" value="<%=usePlace.getH0804()%>"
					readonly></td>
			</tr>
			<tr>
				<th>契約番号</th>
				<td><input type="text" value="<%=usePlace.getH0805()%>"
					readonly></td>
			</tr>
			<tr>
				<th>契約者名義人</th>
				<td><input type="text" value="<%=usePlace.getH0806()%>"
					readonly></td>
			</tr>
		</table>
		<%
			} else {
		%>
		<table>
			<tr>
				<th>住所</th>

				<td><input type="text" readonly></td>
			</tr>
			<tr>
				<th>ご使用場所</th>
				<td><input type="text" readonly></td>
			</tr>
			<tr>
				<th>契約番号</th>
				<td><input type="text" readonly></td>
			</tr>
			<tr>
				<th>契約者名義人</th>
				<td><input type="text" readonly></td>
			</tr>
		</table>
		<%
			}
		%>

	</div>
	<br />

	<div class="input">
		<form action="Confirm" onsubmit="return Modify_Validation()">
			<table>
				<tr>
					<td colspan="3">※ ⎾*⏌の付いた項目は必ず入力してください。</td>
				</tr>
				<tr>
					<th colspan="3" align="left">工事関連</th>
				</tr>
				<tr>
					<th>設備の新設⋅更新①</th>
					<td><select id="select1" onchange="equalKubun1()"
						name="modifyvo.Equipment1">
							<option disabled selected></option>
							<%
								for (int i = 0; i < kubunComboboxList.size(); i++) {
							%>
							<%
								if (kubunComboboxList.get(i).getT0100() == 50003) {
							%>
							<option><%=kubunComboboxList.get(i).getT0200()%></option>
							<%
								}
								}
							%>
					</select></td>
					<td><select id="select2" onchange="equalKubun2()"
						name="modifyvo.EquipmentKubun1">
							<option disabled selected></option>
							<%
								for (int i = 0; i < kubunComboboxList.size(); i++) {
							%>
							<%
								if (kubunComboboxList.get(i).getT0100() == 50003) {
							%>
							<option><%=kubunComboboxList.get(i).getT0401()%></option>
							<%
								}
								}
							%>
					</select></td>
				</tr>
				<tr>
					<th>設備の新設⋅更新②</th>
					<td><select id="select3" onchange="equalKubun3()"
						name="modifyvo.Equipment2">
							<option disabled selected></option>
							<%
								for (int i = 0; i < kubunComboboxList.size(); i++) {
							%>
							<%
								if (kubunComboboxList.get(i).getT0100() == 50003) {
							%>
							<option><%=kubunComboboxList.get(i).getT0200()%></option>
							<%
								}
								}
							%>
					</select></td>
					<td><select id="select4" onchange="equalKubun4()"
						name="modifyvo.EquipmentKubun2">
							<option disabled selected></option>
							<%
								for (int i = 0; i < kubunComboboxList.size(); i++) {
							%>
							<%
								if (kubunComboboxList.get(i).getT0100() == 50003) {
							%>
							<option><%=kubunComboboxList.get(i).getT0401()%></option>
							<%
								}
								}
							%>
					</select></td>
				</tr>
				<tr>
					<th>受電容量の変更</th>
					<td class="td_left"><select name="modifyvo.ChangeKubun">
							<option disabled selected></option>

							<%
								for (int i = 0; i < kubunComboboxList.size(); i++) {
							%>
							<%
								if (kubunComboboxList.get(i).getT0100() == 50004) {
							%>
							<option><%=kubunComboboxList.get(i).getT0200()%>_<%=kubunComboboxList.get(i).getT0401()%></option>
							<%
								}
								}
							%>
					</select></td>
					<td align="left">(該当する場合、選択)</td>
				</tr>
			</table>
			<p id="Equipment1_error"></p>
			<p id="EquipmentKubun1_error"></p>
			<p id="Equipment2_error"></p>
			<p id="EquipmentKubun2_error"></p>

			<br />
			<table>
				<tr>
					<td colspan="3" align="left">その他を選択した場合、補足事項がある場合はこちらにご記入ください。</td>
				</tr>
				<tr>
					<th>補足事項</th>
					<td colspan="2"><textarea id="myDIV" rows="4" cols="83"
							name="modifyvo.Supplement"></textarea></td>
				</tr>
			</table>
			<p id="Supplement_error"></p>
			<br />
			<%
				if (request.getAttribute("usePlace") != null) {
			%>
			<input type="hidden" value="<%=usePlace.getH0801()%>"name="modifyvo.KeiyakuNo">
			<table>
				<tr>
					<th id="th_left" colspan="3">工事事業者</th>
				</tr>
				<tr>
					<th>事業者名</th>
					<td><input type="text" id="read"
						value="<%=usePlace.getH0807()%>" readonly></td>
					<td><input type="text" name="modifyvo.WorkCompanyName"></td>
				</tr>
				<tr>
					<th>担当</th>
					<td><input type="text" id="read"
						value="<%=usePlace.getH0808()%>" readonly></td>
					<td><input type="text" name="modifyvo.WorkTantoName"></td>
				</tr>
				<tr>
					<th>連絡先</th>
					<td><input type="text" id="read-mini"
						value="<%=usePlace.getH0809()%>" readonly>-<input
						type="text" id="read-mini" value="<%=usePlace.getH0810()%>"
						readonly>-<input type="text" id="read-mini"
						value="<%=usePlace.getH0811()%>" readonly></td>
					<td><input type="text" id="mini" name="modifyvo.WorkTel1">-<input
						type="text" id="mini" name="modifyvo.WorkTel2">-<input type="text"
						id="mini" name="modifyvo.WorkTel3"><!-- <input type="hidden" id="modifyvo.WorkTel" name="modifyvo.WorkTel"> --></td>
						
				</tr>
			</table>
			<%
				} else {
			%>
			<table>
				<tr>
					<th id="th_left" colspan="3">工事事業者</th>
				</tr>
				<tr>
					<th>事業者名</th>
					<td><input type="text" id="read" readonly></td>
					<td><input type="text" name="modifyvo.WorkCompanyName"></td>
				</tr>
				<tr>
					<th>担当</th>
					<td><input type="text" id="read" readonly></td>
					<td><input type="text" name="modifyvo.WorkTantoName"></td>
				</tr>
				<tr>
					<th>連絡先</th>
					<td><input type="text" id="read-mini" readonly>-<input
						type="text" id="read-mini" readonly>-<input type="text"
						id="read-mini" readonly></td>
					<td><input type="text" id="mini" name="modifyvo.WorkTel1">-<input
						type="text" id="mini" name="modifyvo.WorkTel2">-<input type="text"
						id="mini" name="modifyvo.WorkTel3"><!-- <input type="hidden" id="modifyvo.WorkTel" name="modifyvo.WorkTel"> --></td>
				</tr>
			</table>
			<%
				}
			%>
			
			<p id="WorkCompanyName_error"></p>
			<p id="WorkTantoName_error"></p>
			<p id="WorkTel1_error"></p>
			<p id="WorkTel2_error"></p>
			<p id="WorkTel3_error"></p>
			<p id="WorkTel_error"></p>
			
			<br />
			<%
				if (request.getAttribute("usePlace") != null) {
			%>
			<table>
				<tr>
					<th id="th_left" colspan="3">主任技術者</th>
				</tr>
				<tr>
					<td></td>
					<th>変更前</th>
					<th>変更後</th>
				</tr>
				<tr>
					<th>社名</th>
					<td><input type="text" id="read"
						value="<%=usePlace.getH0812()%>" readonly></td>
					<td><input type="text" name="modifyvo.MainEngineerCompanyName"></td>
				</tr>
				<tr>
					<th>氏名</th>
					<td><input type="text" id="read"
						value="<%=usePlace.getH0813()%>" readonly></td>
					<td><input type="text" name="modifyvo.MainEngineerName"></td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<th>初回連絡先</th>

					<td><input type="text" value="<%=usePlace.getH0814()%>"
						id="read-mini" readonly>-<input type="text" id="read-mini"
						value="<%=usePlace.getH0815()%>" readonly>-<input
						type="text" value="<%=usePlace.getH0816()%>" id="read-mini"
						readonly></td>
					<td><input type="text" id="mini" name="modifyvo.MainEngineerTel1">-<input
						type="text" id="mini" name="modifyvo.MainEngineerTel2">-<input
						type="text" id="mini" name="modifyvo.MainEngineerTel3"><!-- <input type="hidden" id="modifyvo.MainEngineerTel" name="modifyvo.MainEngineerTel"> --></td>

				</tr>
			</table>
			<%
				} else {
			%>
			<table>
				<tr>
					<th id="th_left" colspan="3">主任技術者</th>
				</tr>
				<tr>
					<td></td>
					<th>変更前</th>
					<th>変更後</th>
				</tr>
				<tr>
					<th>社名</th>
					<td><input type="text" id="read" readonly></td>
					<td><input type="text" name="modifyvo.MainEngineerCompanyName"></td>
				</tr>
				<tr>
					<th>氏名</th>
					<td><input type="text" id="read" readonly></td>
					<td><input type="text" name="modifyvo.MainEngineerName"></td>
				</tr>
				<tr>
					<td colspan="3">&nbsp;</td>
				</tr>
				<tr>
					<th>初回連絡先</th>

					<td><input type="text" id="read-mini">-<input
						type="text" id="read-mini">-<input type="text"
						id="read-mini"></td>
					<td><input type="text" id="mini" name="modifyvo.MainEngineerTel1">-<input
						type="text" id="mini" name="modifyvo.MainEngineerTel2">-<input
						type="text" id="mini" name="modifyvo.MainEngineerTel3"><!-- <input type="hidden" id="modifyvo.MainEngineerTel" name="modifyvo.MainEngineerTel"> --></td>

				</tr>
			</table>
			<%
				}
			%>
			<p id="MainEngineerCompanyName_error"></p>
			<p id="MainEngineerName_error"></p>
			<p id="EngineerTel1_error"></p>
			<p id="EngineerTel2_error"></p>
			<p id="EngineerTel3_error"></p>
			<p id="EngineerTel_error"></p>
			
			<br />
			<table>
				<tr>
					<th id="th_left" colspan="3">申込担当者情報</th>
				</tr>
				<tr>
					<th id="id_fixed">*郵便番号</th>
					<td id="td_fixed"><input type="text"
						name="modifyvo.YubinBango" id="zip"></td>
					<td align="left"><input type="button" value="郵便番号検索"></td>
				</tr>
				<tr>
					<th>住所</th>
					<td><input type="text" name="modifyvo.Jusho" id="address"></td>
					<td></td>
				</tr>
				<tr>
					<th>*会社名</th>
					<td><input type="text" name="modifyvo.HojinName"></td>
					<td></td>
				</tr>
				<tr>
					<th>部署名</th>
					<td><input type="text" name="modifyvo.BushoName"></td>
					<td></td>
				</tr>
				<tr>
					<th>*ご担当者</th>
					<td><input type="text" name="modifyvo.TantoName"></td>
					<td></td>
				</tr>
				<tr>
					<th>*電話番号</th>
					<td><input type="text" id="mini" name="modifyvo.Tel1">-<input
						type="text" id="mini" name="modifyvo.Tel2">-<input type="text"
						id="mini" name="modifyvo.Tel3"><!-- <input type="hidden" id="modifyvo.Tel" name="modifyvo.Tel"> --></td>
					<td></td>
				</tr>
				<tr>
					<th>*メールアドレス</th>
					<td><input type="text" name="modifyvo.MailAddress"></td>
					<td></td>
				</tr>
			</table>
			
			<p id="YubinBango_error"></p>
			<p id="Jusho_error"></p>
			<p id="HojinName_error"></p>
			<p id="BushoName_error"></p>
			<p id="TantoName_error"></p>
			<p id="Tel1_error"></p>
			<p id="Tel2_error"></p>
			<p id="Tel3_error"></p>
			<p id="Tel_error"></p>
			
			<p id="MailAddress_error"></p>
			
			
			<br />
			<table>
				<tr>
					<th>自由記入欄</th>
					<td colspan="2"><textarea id="myDIV" rows="4" cols="83"
							name="modifyvo.Memo"></textarea></td>
				</tr>
			</table>
			<p id="Memo_error"></p>
			
			<br /> <input type="submit" value="内容確認"> <br /> <br />
			<p>お申し込みされた内容が⎾契約情報照会画面⏌に反映されるめでは、お申し込み前に情報が表示されますのでご了承ください。</p>
		</form>

	</div>
</div>
