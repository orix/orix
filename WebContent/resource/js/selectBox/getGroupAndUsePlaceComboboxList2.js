$(function(){
	$.ajax({url:"/getUsePlaceComboboxList",
		dataType:'json',
		success: function(data) {
			var json = JSON.parse(JSON.stringify(data));
			var select = $('#groupCombobox');
			var groupCombobox = GetQueryStringParameters("groupCombobox");

			select.find('option').remove();
			$('<option>').val("").text("全権").appendTo(select);
			$.each(json.groupComboboxMap, function(key, value) {
				if(typeof(groupCombobox) != "undefined"  && groupCombobox == key) {
					$('<option>').val(key).text(value).appendTo(select).attr('selected','selected');	
				} else {
					$('<option>').val(key).text(value).appendTo(select);	
				}
			});
			placeComboboxList();
		}});
	$('#groupCombobox').change(function(event) {
		placeComboboxList();
	});

	function placeComboboxList(){
		var groupComboboxCode = $("select#groupCombobox").val();
		var select = $('#usePlaceCombobox');
		var usePlaceCombobox = GetQueryStringParameters("usePlaceCombobox");

		if (groupComboboxCode != 0)	{
			$.getJSON('/getUsePlaceComboboxList', {
				groupComboboxCode : groupComboboxCode
			}, function(jsonResponse) {
				select.find('option').remove();
				if(!jsonResponse.usePlaceMap.success && typeof jsonResponse.usePlaceMap.success != "undefined") {
					alert(jsonResponse.usePlaceMap.message);
				} else {
					$.each(jsonResponse.usePlaceMap, function(key, value) {
						if(usePlaceCombobox == key && typeof(usePlaceCombobox) != "undefined") {
							$('<option>').val(key).text(value).appendTo(select).attr('selected','selected');
						} else {
							$('<option>').val(key).text(value).appendTo(select);
						}
					});
				}
			});
		}else{
			select.find('option').remove();
			$('<option>').val("").text("全権").appendTo(select);
		}
	}
});

function GetQueryStringParameters(sParam) {

	var sPageURL = window.location.search.substring(1);
	var sURLVariables = sPageURL.split('&');

	for (var i = 0; i < sURLVariables.length; i++) {
		var sParameterName = sURLVariables[i].split('=');
		if (sParameterName[0] == sParam) {
			return sParameterName[1];
		}
	}
}