package jp.co.orix.model;

public class Info3 {
	private String billingCompanyName;
	private String billingDivisionName;
	private String billingName1;
	private String billingName2;
	private String billingPhone1;
	private String billingPhone2;
	private String billingPhone3;
	private String billingFax;
	private String billingMail1;
	private String billingMail2;

	public String getBillingCompanyName() {
		return billingCompanyName;
	}

	public void setBillingCompanyName(String billingCompanyName) {
		this.billingCompanyName = billingCompanyName;
	}

	public String getBillingDivisionName() {
		return billingDivisionName;
	}

	public void setBillingDivisionName(String billingDivisionName) {
		this.billingDivisionName = billingDivisionName;
	}

	public String getBillingName1() {
		return billingName1;
	}

	public void setBillingName1(String billingName1) {
		this.billingName1 = billingName1;
	}

	public String getBillingName2() {
		return billingName2;
	}

	public void setBillingName2(String billingName2) {
		this.billingName2 = billingName2;
	}



	public String getBillingFax() {
		return billingFax;
	}

	public void setBillingFax(String billingFax) {
		this.billingFax = billingFax;
	}

	public String getBillingMail1() {
		return billingMail1;
	}

	public void setBillingMail1(String billingMail1) {
		this.billingMail1 = billingMail1;
	}

	public String getBillingMail2() {
		return billingMail2;
	}

	public void setBillingMail2(String billingMail2) {
		this.billingMail2 = billingMail2;
	}

	public String getBillingPhone1() {
		return billingPhone1;
	}

	public void setBillingPhone1(String billingPhone1) {
		this.billingPhone1 = billingPhone1;
	}

	public String getBillingPhone2() {
		return billingPhone2;
	}

	public void setBillingPhone2(String billingPhone2) {
		this.billingPhone2 = billingPhone2;
	}

	public String getBillingPhone3() {
		return billingPhone3;
	}

	public void setBillingPhone3(String billingPhone3) {
		this.billingPhone3 = billingPhone3;
	}

}
