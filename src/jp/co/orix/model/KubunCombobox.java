package jp.co.orix.model;

public class KubunCombobox {
	private int T0100;
	private String T0200;
	private String T0401;
	
	public int getT0100() {
		return T0100;
	}
	public void setT0100(int t0100) {
		T0100 = t0100;
	}
	public String getT0200() {
		return T0200;
	}
	public void setT0200(String t0200) {
		T0200 = t0200;
	}
	public String getT0401() {
		return T0401;
	}
	public void setT0401(String t0401) {
		T0401 = t0401;
	}
	public KubunCombobox() {
		// TODO Auto-generated constructor stub
	}
	public KubunCombobox(int t0100, String t0200, String t0401) {
		T0100 = t0100;
		T0200 = t0200;
		T0401 = t0401;
	}
	@Override
	public String toString() {
		return "KubunCombo [T0100=" + T0100 + ", T0200=" + T0200 + ", T0401="
				+ T0401 + "]";
	}
	
}
