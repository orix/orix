package jp.co.orix.model;

public class Info2 {
	private String managementCorporateName;
	private String managementDivisionName;
	private String managementName1;
	private String managementName2;
	private String managementPhone1;
	private String managementPhone2;
	private String managementPhone3;
	private String managementFax;
	private String managementMail1;
	private String managementMail2;

	public String getManagementCorporateName() {
		return managementCorporateName;
	}

	public void setManagementCorporateName(String managementCorporateName) {
		this.managementCorporateName = managementCorporateName;
	}

	public String getManagementDivisionName() {
		return managementDivisionName;
	}

	public void setManagementDivisionName(String managementDivisionName) {
		this.managementDivisionName = managementDivisionName;
	}

	public String getManagementName1() {
		return managementName1;
	}

	public void setManagementName1(String managementName1) {
		this.managementName1 = managementName1;
	}

	public String getManagementName2() {
		return managementName2;
	}

	public void setManagementName2(String managementName2) {
		this.managementName2 = managementName2;
	}

	

	public String getManagementFax() {
		return managementFax;
	}

	public void setManagementFax(String managementFax) {
		this.managementFax = managementFax;
	}

	public String getManagementMail1() {
		return managementMail1;
	}

	public void setManagementMail1(String managementMail1) {
		this.managementMail1 = managementMail1;
	}

	public String getManagementMail2() {
		return managementMail2;
	}

	public void setManagementMail2(String managementMail2) {
		this.managementMail2 = managementMail2;
	}

	public String getManagementPhone1() {
		return managementPhone1;
	}

	public void setManagementPhone1(String managementPhone1) {
		this.managementPhone1 = managementPhone1;
	}

	public String getManagementPhone2() {
		return managementPhone2;
	}

	public void setManagementPhone2(String managementPhone2) {
		this.managementPhone2 = managementPhone2;
	}

	public String getManagementPhone3() {
		return managementPhone3;
	}

	public void setManagementPhone3(String managementPhone3) {
		this.managementPhone3 = managementPhone3;
	}

}
