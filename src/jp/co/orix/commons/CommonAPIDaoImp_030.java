package jp.co.orix.commons;

import java.util.ArrayList;
import java.util.Date;

import jp.co.orix.bdconn.SqlSessionFactoryService;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class CommonAPIDaoImp_030 implements CommonAPIDao_030 {
	
	public ArrayList<UpdateUserNameSqlModel_030> arrayList;
	public int countLocationData;
	public String contractNumber;

	@Autowired
	 private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();

	
	@Override
	public String getContractNumber(String usePlace) {
		
		SqlSession session = sqlSession.openSession();
		CommonAPIDao_030 dao = session.getMapper(CommonAPIDao_030.class);
		
		try {
			contractNumber = dao.getContractNumber(usePlace);
			
			session.commit();
		} catch (Exception e){
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
	
		return contractNumber;
	}
	
	@Override
	public int getLocationData() {
		
		SqlSession session = sqlSession.openSession();
		CommonAPIDao_030 dao = session.getMapper(CommonAPIDao_030.class);
		
		try {
			
			countLocationData = dao.getLocationData();
			
			session.commit();
		
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		
		
		return countLocationData;
	}
	
	@Override
	public void getUpdateUserNameSql(
			String oldUserName, String newUserName, int userNumber,
			String lastSessionId) {
		
		SqlSession session = sqlSession.openSession();
		CommonAPIDao_030 dao = session.getMapper(CommonAPIDao_030.class);
		
		try {
			
			
			dao.getUpdateUserNameSql(oldUserName, newUserName, userNumber, lastSessionId);
			
			session.commit();
			
			System.out.println("oldUserName " + oldUserName);
			System.out.println("newUserName" + newUserName);
			System.out.println("userNumber" + userNumber);
			System.out.println("lastSessionId" + lastSessionId);
		
		} catch (Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		
	}

	public SqlSessionFactory getSqlSession() {
		return sqlSession;
	}

	public void setSqlSession(SqlSessionFactory sqlSession) {
		this.sqlSession = sqlSession;
	}

	public ArrayList<UpdateUserNameSqlModel_030> getArrayList() {
		return arrayList;
	}

	public void setArrayList(ArrayList<UpdateUserNameSqlModel_030> arrayList) {
		this.arrayList = arrayList;
	}

	public int getCountLocationData() {
		return countLocationData;
	}

	public void setCountLocationData(int countLocationData) {
		this.countLocationData = countLocationData;
	}

	public String getContractNumber() {
		return contractNumber;
	}

	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}

	

	
	
	
}
