<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="s" uri="/struts-tags"%>

<script type="text/javascript">
	function getUserInfo() {
		var location = document.getElementById("choiceLocation").value;
		
		if (location == '') {
			alert("ご使用場所を選択してください。")
			return false;
		} else {
		window.location = "getUserInfo.action?userPaymentInfo.HV011_location="
				+ location;
		return true;
		}
	}

	function getCustomerInfo() {
		var paymentMethod = document.forms["getUserInfoForm"]["userPaymentInfo.HV011_paymentMethodID"].value;
		if (paymentMethod == 1 || paymentMethod == 2 || paymentMethod == 3) {
			document.getElementById("getUserInfoFormID").action = "accountInfoChange.action";
		} else {
			document.getElementById("getUserInfoFormID").action = "changePaymentMethod.action";
		}
	}
</script>

<form method="post" name="getUserInfoForm" id="getUserInfoFormID"
	onsubmit="getCustomerInfo()">
	<br>
	<div align="center">
		ご使用場所 
		<input id="choiceLocation" type="text" list="universalInfoList" class="selectLocation" />
		<datalist id="universalInfoList">
			<c:forEach var="obj" items="${universalInfoList}">
				<option value="${obj.t0200}">${obj.t0401}</option>
			</c:forEach>
		</datalist>
		<input type="button" value="検索" onclick="return getUserInfo()"
			class="button"> <br> <br>
	</div>
	<hr class="hr_top">
	<br>
	<div align="center" class="getUserInfoTable">
		<table>
			<tr>
				<td id="title">住所</td>
				<td><input type=text value="${userPaymentInfo.HV011_address}"
					readonly="readonly" id="outputTextField"></td>
			</tr>
			<tr>
				<td id="title">ご使用場所</td>
				<td><input type=text value="${userPaymentInfo.HV011_location}"
					readonly="readonly" id="outputTextField"></td>
			</tr>
			<tr>
				<td id="title">契約番号</td>
				<td><input type=text
					value="${userPaymentInfo.HV011_contractNumber}" readonly="readonly"
					id="outputTextField"></td>
			</tr>
			<tr>
				<td id="title">契約名義人</td>
				<td><input type=text
					value="${userPaymentInfo.HV011_contractName}" readonly="readonly"
					id="outputTextField"></td>
			</tr>
		</table>
		<br> <br>
	</div>

	<div align="center">
		現在のお支払方法 <br> <input type="text"
			value="${userPaymentInfo.HV011_paymentMethod}" readonly="readonly"
			name="userPaymentInfo.HV011_paymentMethod" id="outputTextField">
	</div>
	<br> <br> <input type="hidden"
		value="${userPaymentInfo.HV011_paymentMethodID}"
		name="userPaymentInfo.HV011_paymentMethodID">

	<s:set name="HV011_paymentMethodID"
		value="userPaymentInfo.HV011_paymentMethodID" />

	<s:if test="%{#HV011_paymentMethodID == 1}">
		<div align="center">
			現在の口座情報を変更する場合は<br>下記のボタンを押してください。<br> <br>
		</div>
		<div align="center">
			<input type="submit" value="口座情報を変更する" class="submit">
		</div>
	</s:if>


	<s:elseif test="%{#HV011_paymentMethodID == 2}">
		<div align="center">
			現在の口座情報を変更する場合は<br>下記のボタンを押してください。<br> <br>
		</div>
		<div align="center">
			<input type="submit" value="口座情報を変更する" class="submit">
		</div>
	</s:elseif>

	<s:elseif test="%{#HV011_paymentMethodID == 3}">
		<div align="center">
			現在の口座情報を変更する場合は<br>下記のボタンを押してください。<br> <br>
		</div>
		<div align="center">
			<input type="submit" value="口座情報を変更する" class="submit">
		</div>
	</s:elseif>

	<s:elseif test="%{#HV011_paymentMethodID == 4}">
		<div align="center">
			お支払方法を口座振替に変更される場合は、<br>下記のボタンを押してください。<br> <br>
		</div>
		<div align="center">
			<input type="submit" value="支払方法を口座振替に変更する" class="submit">
		</div>
	</s:elseif>

	<s:elseif test="%{#HV011_paymentMethodID == 8}">
		<div align="center">
			お支払方法を口座振替に変更される場合は、<br>下記のボタンを押してください。<br> <br>
		</div>
		<div align="center">
			<input type="submit" value="支払方法を口座振替に変更する" class="submit">
		</div>
	</s:elseif>

</form>
