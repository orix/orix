package jp.co.orix.dao;

import java.util.Map;

import jp.co.orix.model.Info;
import jp.co.orix.model.Info2;
import jp.co.orix.model.Info3;
import jp.co.orix.model.Info4;

import org.apache.ibatis.annotations.Param;

public interface ChangeAddressDao_121 {
	// @Param("id") Integer idNum,
	public Info infoSearch01(@Param("location") String location);

	public Info2 infoSearch02(@Param("location") String location);

	public Info3 infoSearch03(@Param("location") String location);

	public Info4 infoSearch04(@Param("location") String location);

	public void updateInfo01(Map map);

	public void updateInfo02(Map map);

	public void updateInfo03(Map map);

	public void updateInfo04(Map map);

	public void updateInfo05(Map map);
	
	public int maxValueHV013();

//	public int insertIMACP000(Map map);
}
