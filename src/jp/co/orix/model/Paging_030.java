package jp.co.orix.model;

public class Paging_030 {

	private int startRow;
	private int endRow;
	
	public Paging_030() {
	}
	
	public Paging_030(int startRow, int endRow) {
		this.startRow = startRow;
		this.endRow = endRow;
	}

	public int getStartRow() {
		return startRow;
	}
	public void setStartRow(int startRow) {
		this.startRow = startRow;
	}
	public int getEndRow() {
		return endRow;
	}
	public void setEndRow(int endRow) {
		this.endRow = endRow;
	}
}
