package jp.co.orix.dao.imp;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.CommonDao_32;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

public class CommonDaoImpl_32 implements CommonDao_32 {

	@Autowired
	private SqlSessionFactory sqlSessionFactory= SqlSessionFactoryService.getsqlFactoeyService();
	
	@Override
	public String selectMessageByCond(HashMap<String, String> cond) throws SQLException {
		// TODO Auto-generated method stub
		SqlSession session = sqlSessionFactory.openSession();
		CommonDao_32 dao = session.getMapper(CommonDao_32.class);
		
		String adminMessage = "";
		
		try {
			adminMessage = dao.selectMessageByCond(cond);
			
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
		
		}
		return adminMessage;
	}
	
	@Override
	public List<Map<String, Double>> selectByHalfhour(Map<String, String> cond) throws SQLException{
		SqlSession session = sqlSessionFactory.openSession();
		CommonDao_32 dao = session.getMapper(CommonDao_32.class);
		List<Map<String, Double>> mapLi = null;
		
		try {
			mapLi = dao.selectByHalfhour(cond);
//			mapLi = session.selectList("selectByHalfhour", cond);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
		
		}
		return mapLi;
	}
	
	@Override
	public List<Map<String, Double>> selectByDay(Map<String, String> cond)
			throws SQLException {
		SqlSession session = sqlSessionFactory.openSession();
		CommonDao_32 dao = session.getMapper(CommonDao_32.class);

		List<Map<String, Double>> mapLi = null;

		try {
			mapLi = dao.selectByDay(cond);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {

		}
		return mapLi;
	}

	@Override
	public Map<String, Double> selectByMonth(Map<String, String> cond)
			throws SQLException {
		// TODO Auto-generated method stub
		return null;
	}
	
	public SqlSessionFactory getSqlSessionFactory() {
		return sqlSessionFactory;
	}

	public void setSqlSessionFactory(SqlSessionFactory sqlSessionFactory) {
		this.sqlSessionFactory = sqlSessionFactory;
	}

	
}
