package jp.co.orix.dao.imp;

import java.util.Map;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.ChangeAddressDao_121;
import jp.co.orix.model.Info;
import jp.co.orix.model.Info2;
import jp.co.orix.model.Info3;
import jp.co.orix.model.Info4;
import jp.co.orix.model.SinseiInfo;
import jp.co.orix.model.SinseiInfo2;
import jp.co.orix.model.SinseiInfo3;
import jp.co.orix.model.SinseiInfo4;
import jp.co.orix.model.SinseiInfo5;

import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.sun.corba.se.pept.transport.ContactInfo;

public class ChangeAddressDaoImp_121 implements ChangeAddressDao_121 {
	private Info info = new Info();
	private Info2 info2 = new Info2();
	private Info3 info3 = new Info3();
	private Info4 info4 = new Info4();
	@Autowired
	private SqlSessionFactory sqlSession = SqlSessionFactoryService
			.getsqlFactoeyService();

	@Override
	public Info infoSearch01(@Param("location") String location) {

		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {

			info = dao.infoSearch01(location);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		return info;

	}

	@Override
	public Info2 infoSearch02(@Param("location") String location) {

		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {

			info2 = dao.infoSearch02(location);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		return info2;

	}

	@Override
	public Info3 infoSearch03(@Param("location") String location) {

		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {

			info3 = dao.infoSearch03(location);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		return info3;

	}

	@Override
	public Info4 infoSearch04(@Param("location") String location) {

		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {

			info4 = dao.infoSearch04(location);

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		return info4;

	}

	// @Override
	// public int insertIMACP000(Map<String, String> map) {
	//
	// SqlSession session = sqlSession.openSession();
	// ChangeAddressDao_121 dao = session
	// .getMapper(ChangeAddressDao_121.class);
	//
	// try {
	// int check = dao.insertIMACP000(map);
	//
	// return check;
	// } catch (Exception e) {
	// // TODO: handle exception
	// System.err.println(e.getMessage());
	// } finally {
	// session.commit();
	// session.close();
	// }
	//
	// return 0;
	// }

	@Override
	public void updateInfo01(Map map) {

		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {
			dao.updateInfo01(map);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public void updateInfo02(Map map) {
		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {
			dao.updateInfo02(map);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public void updateInfo03(Map map) {
		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {
			dao.updateInfo03(map);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public void updateInfo04(Map map) {
		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {
			dao.updateInfo04(map);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public void updateInfo05(Map map) {
		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {
			dao.updateInfo05(map);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.commit();
			session.close();
		}

	}

	@Override
	public int maxValueHV013() {
		SqlSession session = sqlSession.openSession();
		ChangeAddressDao_121 dao = session
				.getMapper(ChangeAddressDao_121.class);

		try {
			int num = dao.maxValueHV013();
			return num;

		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
		return 0;
	}

}
