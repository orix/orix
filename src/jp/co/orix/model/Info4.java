package jp.co.orix.model;

public class Info4 {
	private String contactCorporateName;
	private String contactDivisionName;
	private String contactName1;
	private String contactName2;
	private String contactPhone1;
	private String contactPhone2;
	private String contactPhone3;
	private String contactFax;
	private String contactMail1;
	private String contactMail2;

	public String getContactCorporateName() {
		return contactCorporateName;
	}

	public void setContactCorporateName(String contactCorporateName) {
		this.contactCorporateName = contactCorporateName;
	}

	public String getContactDivisionName() {
		return contactDivisionName;
	}

	public void setContactDivisionName(String contactDivisionName) {
		this.contactDivisionName = contactDivisionName;
	}

	public String getContactName1() {
		return contactName1;
	}

	public void setContactName1(String contactName1) {
		this.contactName1 = contactName1;
	}

	public String getContactName2() {
		return contactName2;
	}

	public void setContactName2(String contactName2) {
		this.contactName2 = contactName2;
	}



	public String getContactFax() {
		return contactFax;
	}

	public void setContactFax(String contactFax) {
		this.contactFax = contactFax;
	}

	public String getContactMail1() {
		return contactMail1;
	}

	public void setContactMail1(String contactMail1) {
		this.contactMail1 = contactMail1;
	}

	public String getContactMail2() {
		return contactMail2;
	}

	public void setContactMail2(String contactMail2) {
		this.contactMail2 = contactMail2;
	}

	public String getContactPhone1() {
		return contactPhone1;
	}

	public void setContactPhone1(String contactPhone1) {
		this.contactPhone1 = contactPhone1;
	}

	public String getContactPhone2() {
		return contactPhone2;
	}

	public void setContactPhone2(String contactPhone2) {
		this.contactPhone2 = contactPhone2;
	}

	public String getContactPhone3() {
		return contactPhone3;
	}

	public void setContactPhone3(String contactPhone3) {
		this.contactPhone3 = contactPhone3;
	}

}
