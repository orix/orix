package jp.co.orix.dao;

import jp.co.orix.model.CheckContractInfoHV002;

public interface CheckContractInfoDao {

	public CheckContractInfoHV002 getContractInfoBy(String customerContractNumber);

}
