package jp.co.orix.util;

import java.io.UnsupportedEncodingException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import jp.co.orix.dao.CommonDao_32;
import jp.co.orix.dao.imp.CommonDaoImpl_32;

	
/**														
 * @author Noh Seong-Jun
 * 
 * @description 
 * IsValidateStringValue() : Check that Input String is Half-Character[UTF-8 based] (Yes : return true, No : return false) 
 *
 */

public class CommApi_32 {
	
	private String parameter;
	private HashMap<String, Object> parameterMap = new HashMap<>();
	//Check that Input String is Half-Character[UTF-8 based] (Yes : return true, No : return false) 
	public static boolean IsValidateStringValue(String str){
		char[] chars = str.toCharArray();
		for (int i = 0; i < chars.length; i++) {
			 char c = chars[i];
			 if (!((c <= '\u007e') || // 闍ｱ謨ｰ蟄�
				(c == '\u00a5') || // \險伜捷
				(c == '\u203e') || // ~險伜捷
				(c >= '\uff61' && c <= '\uff9f') // 蜊願ｧ偵き繝�
			)) {
				 return false;
			 }
		}
		return true;
	}
	
	public static String GetComment(String commentClass, String commentType, String showState) throws Exception{
		CommonDao_32 dao = new CommonDaoImpl_32();
		HashMap<String, String> cond = new HashMap<String, String>();
		cond.put("commentClass", commentClass);
		cond.put("commentType", commentType);
		cond.put("showState", showState);
		return dao.selectMessageByCond(cond);
	}
	//make CsvFile 
	public static StringBuffer GetCsvFileResult(Map<String, Object> csvDownLoadDataMap) throws UnsupportedEncodingException{
		StringBuffer writer = new StringBuffer();
		writer.append('\uFEFF'); //add BOM
		
		List<String> column = (List<String>)csvDownLoadDataMap.get("column");
		List<List<String>> data = (List<List<String>>)csvDownLoadDataMap.get("data");
		
		for(int i=0; i< column.size(); i++){
			writer.append(column.get(i));
			if(i != column.size()-1)
				writer.append(',');
		}
		writer.append('\n');
		
		for(int i=0; i< data.get(0).size(); i++){
			for(int j=0; j<column.size(); j++){
//				System.out.println(writer);
				if(data.get(j) != null && data.get(j).size() !=0){
				writer.append(data.get(j).get(i));
				}else{
					writer.append("");
				}
				if(j != column.size()-1)
					writer.append(',');
			}
			writer.append('\n');
		}
		
		return writer;
	}
	
	
	static public  Date ConvertStringToDate(String strDate) throws ParseException {
		//String => date convert
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
		Date date = dateFormat.parse(strDate);
		
		return date;
	}
		
	
	//Web Service
	public static List<Map<String, Double>> getUsedPowerAmountInfo(String manageNumber,
			String dataClass, String fromDate, String toDate,
			String customerContractNumber) throws Exception {
		
		CommonDao_32 dao = new CommonDaoImpl_32();
		HashMap<String, String> cond = new HashMap<String, String>();
		
		//test code
		SimpleDateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd");
		long difDays = (timeFormat.parse(toDate).getTime() - timeFormat.parse(fromDate).getTime())/(24*60*60*1000);
		
		cond.put("manageNumber", manageNumber);
		cond.put("customerContractNumber", customerContractNumber);
		
		cond.put("fromSearchDate", fromDate);
		cond.put("toSearchDate", toDate);
		
		if(difDays == 0){
			cond.put("timeType", "03");
			List<Map<String, Double>> mapLi = dao.selectByHalfhour(cond);
			List<Map<String, Double>> usePowerMapLi = new ArrayList<Map<String, Double>>();
			usePowerMapLi.add(new HashMap<String, Double>());
			
			for(int j = 1 ; j <= 48 ; j++){
				double usePower = 0;
				for(int i = 0; i < mapLi.size() ; i++){
					usePower += mapLi.get(i).get(j + "");
				}
				usePowerMapLi.get(0).put(j + "", usePower);
			}
			return usePowerMapLi;
		}else if(difDays >= 365){
			cond.put("timeType", "01");
//			return testDao.selectByMonth(cond);
		}else{
			cond.put("timeType", "02");
			
			List<Map<String, Double>> mapLi = dao.selectByDay(cond);			//point 1
			List<Map<String, Double>> usePowerMapLi = new ArrayList<Map<String, Double>>();
			
			
			for(int i = 0; i < mapLi.size(); i++){
				int day = Integer.parseInt((mapLi.get(i).get("day"))+"");
				if(usePowerMapLi.isEmpty() || usePowerMapLi.size() != day ){
					usePowerMapLi.add(new HashMap<String, Double>());
					mapLi.get(i).remove("day");
					usePowerMapLi.get(day-1).putAll(mapLi.get(i));
				}
					for(int j = 1; j <= 24; j++){
						double usePower = usePowerMapLi.get(day-1).get(j + "");
						double usePower2 = mapLi.get(i).get(j + "");
						usePowerMapLi.get(day-1).put(j + "", usePower + usePower2);
					}
				
			}
			return usePowerMapLi;
		}
		return null;
	}
	
	public void setFormParamator(HttpServletRequest req, String key){
		String parameters = "";
		Enumeration enu= req.getParameterNames();
		String name = "";
		StringBuffer buffer = new StringBuffer();
		HashMap<String, Object> map = new HashMap<>();
		
		if (enu.hasMoreElements()) {
			while(enu.hasMoreElements()){
				
				name = (String)enu.nextElement();
				String[] value = req.getParameterValues(name);
				int temp = value.length;
				if(temp == 1){ //배열이 아닌것
					for(int i=0; i<temp; i++){
						if (!value[i].equals("undefined")){
							if (value[i].length() != 0) {
								//String type
								buffer.append("&"+name+"=" + value[i]);
								
								//Map type
								map.put(name, value[i]);
							}
						}
					}
				}
			}
			parameters = buffer.toString();
			if (parameters.length() != 0) 
				parameters = parameters.substring(1);
			
			map.put("param", parameters);
			HashMap<String, Object> setMap = new HashMap<>();
			setMap.put(key, map);
			setParameterMap(setMap);
		}else{
			setParameterMap(null);
		}
		
	}
	
	public boolean isValidateAlphanumericJisLevel2(String str){
		byte[] byteArray = str.getBytes();
		for (int i = 0; i < byteArray.length; i++) {
			if ((byteArray[i] >= (byte)0x81 && byteArray[i] <= (byte)0x9f) || 
					(byteArray[i] >= (byte)0xe0 && byteArray[i] <= (byte)0xef)){
				if ((byteArray[i+1] >= (byte)0x40 && byteArray[i+1] <= (byte)0x7e) ||  
						(byteArray[i+1] >= (byte)0x80 && byteArray[i+1] <= (byte)0xfc)){
					return true;
				}
			}
		}
		return false;
	}
	
	public HashMap<String, Object> getFormParamator(String key) {
		
		if (getParameterMap() != null) 
			return (HashMap<String, Object>) getParameterMap().get(key);
		else
			return null;
		
		
	}

	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}

	public HashMap<String, Object> getParameterMap() {
		return parameterMap;
	}

	public void setParameterMap(HashMap<String, Object> parameterMap) {
		this.parameterMap = parameterMap;
	}
	
	
}
