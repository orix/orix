package jp.co.orix.model;

import java.util.Date;

public class SearchModel_030 {

	private String h0602;
	private String h0603;
	private Date h0607;
	
	public SearchModel_030() {
	}

	public SearchModel_030(String h0602, String h0603, Date h0607) {
		this.h0602 = h0602;
		this.h0603 = h0603;
		this.h0607 = h0607;
	}

	public String getH0602() {
		return h0602;
	}

	public void setH0602(String h0602) {
		this.h0602 = h0602;
	}

	public String getH0603() {
		return h0603;
	}

	public void setH0603(String h0603) {
		this.h0603 = h0603;
	}

	public Date getH0607() {
		return h0607;
	}

	public void setH0607(Date h0607) {
		this.h0607 = h0607;
	}

	@Override
	public String toString() {
		return "SearchModel_030 [h0602=" + h0602 + ", h0603=" + h0603
				+ ", h0607=" + h0607 + "]";
	}
	
	
}
