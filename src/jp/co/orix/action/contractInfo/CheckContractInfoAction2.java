package jp.co.orix.action.contractInfo;
/**
 * @内容		「契約情報照会」画面の動きと関するコントロールクラス
 * @author Jongmoo Kim
 */
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import jp.co.orix.dao.CheckContractInfoDao;
import jp.co.orix.dao.GetCustomerContractNumberDao;
import jp.co.orix.dao.GetGroupAndPlaceDao;
import jp.co.orix.model.CheckContractInfoHV002;
import jp.co.orix.model.UniversalInfoITHNT000;
import jp.co.orix.util.CommApi_32;

import com.opensymphony.xwork2.ActionSupport;

public class CheckContractInfoAction2 extends ActionSupport implements ServletRequestAware {

	private static final String T0100 = "50002";

	private String groupCombobox;
	private String usePlaceCombobox;
	private String contractNumber; 

	private CheckContractInfoHV002 checkContractInfoHV002;

	private GetCustomerContractNumberDao getCustomerContractNumberDao;
	private CheckContractInfoDao checkContractInfoDao;

	private GetGroupAndPlaceDao getGroupAndPlaceDao;
	private List<UniversalInfoITHNT000> universalInfoList;

	public HttpServletRequest request;
	private String parameter;


	/* 画面表示  */
	public String viewCheckContractInfoForm() throws Exception {
		System.out.println("023start");
		try {
			System.out.println("1");
			CommApi_32 api = new CommApi_32();
			api.setFormParamator(request, "searchCheckContractInfoBy");
			HashMap<String, Object> param = api.getFormParamator("searchCheckContractInfoBy");
			parameter = (String)param.get("param");
			this.parameter = parameter;

			return NONE;
		} catch(Exception e) {
			// param이 없다면
			System.out.println("없음?");
		}


		//グループの値がない場合、出力する「エラーメッセージ」
		universalInfoList = getGroupAndPlaceDao.getGroupComboboxList(T0100);

		if(universalInfoList.isEmpty()) {
			return ERROR;
		} else {
			return SUCCESS;	
		}
	}

	/* 検索  */
	public String searchCheckContractInfoBy() throws Exception {

		System.out.println("023parameter : " + parameter);
		if(parameter == null) {
			//get&setFormParam Test
			CommApi_32 api = new CommApi_32();
			api.setFormParamator(request, "searchCheckContactInfoBy");
			System.out.println(api.getParameterMap());
			HashMap<String, Object> param = api.getFormParamator("searchCheckContactInfoBy");
			parameter = (String)param.get("param");
			System.out.println("parameter : " + parameter);	
		}
		
		
		
		/*//get&setFormParam Test
		CommApi_32 api = new CommApi_32();
		api.setFormParamator(request, "searchCheckContractInfoBy");
		//System.out.println(api.getParameterMap());
		HashMap<String, Object> param = api.getFormParamator("searchCheckContractInfoBy");
		parameter = (String)param.get("param");
		//System.out.println("parameter : " + parameter);
*/

		Map<String, String> condition = new HashMap<String, String>();
		condition.put("groupCombobox", groupCombobox);
		condition.put("usePlaceCombobox", usePlaceCombobox);

		contractNumber = getCustomerContractNumberDao.getCustomerContractNumber(condition);
		checkContractInfoHV002 = checkContractInfoDao.getContractInfoBy(contractNumber);

		if(checkContractInfoHV002 == null) {
			//高圧契約情報が取得できなかった場合「エラーメッセージ」を出力
			return ERROR;
		} else {
			//高圧契約情報が取得する場合、ディービーから抽出したデータを画面に表示
			
			return SUCCESS;
		}

	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	public String getParameter() {
		return parameter;
	}
	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	public String getGroupCombobox() {
		return groupCombobox;
	}
	public void setGroupCombobox(String groupCombobox) {
		this.groupCombobox = groupCombobox;
	}
	public String getUsePlaceCombobox() {
		return usePlaceCombobox;
	}
	public void setUsePlaceCombobox(String usePlaceCombobox) {
		this.usePlaceCombobox = usePlaceCombobox;
	}
	public GetCustomerContractNumberDao getGetCustomerContractNumberDao() {
		return getCustomerContractNumberDao;
	}
	public void setGetCustomerContractNumberDao(
			GetCustomerContractNumberDao getCustomerContractNumberDao) {
		this.getCustomerContractNumberDao = getCustomerContractNumberDao;
	}
	public String getContractNumber() {
		return contractNumber;
	}
	public void setContractNumber(String contractNumber) {
		this.contractNumber = contractNumber;
	}
	public CheckContractInfoHV002 getCheckContractInfoHV002() {
		return checkContractInfoHV002;
	}
	public void setCheckContractInfoHV002(
			CheckContractInfoHV002 checkContractInfoHV002) {
		this.checkContractInfoHV002 = checkContractInfoHV002;
	}
	public CheckContractInfoDao getCheckContractInfoDao() {
		return checkContractInfoDao;
	}
	public void setCheckContractInfoDao(CheckContractInfoDao checkContractInfoDao) {
		this.checkContractInfoDao = checkContractInfoDao;
	}
	public GetGroupAndPlaceDao getGetGroupAndPlaceDao() {
		return getGroupAndPlaceDao;
	}
	public void setGetGroupAndPlaceDao(GetGroupAndPlaceDao getGroupAndPlaceDao) {
		this.getGroupAndPlaceDao = getGroupAndPlaceDao;
	}
	public List<UniversalInfoITHNT000> getUniversalInfoList() {
		return universalInfoList;
	}
	public void setUniversalInfoList(List<UniversalInfoITHNT000> universalInfoList) {
		this.universalInfoList = universalInfoList;
	}




}