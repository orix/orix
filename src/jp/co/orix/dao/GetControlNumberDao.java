package jp.co.orix.dao;

import java.util.Map;

public interface GetControlNumberDao {

	public String getControlNumber(Map<String, String> condition);

}
