package jp.co.orix.dao.imp;

import java.util.ArrayList;
import java.util.Date;

import javax.jws.soap.SOAPBinding.Use;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.springframework.beans.factory.annotation.Autowired;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.BillDao;
import jp.co.orix.dao.UserDao;
import jp.co.orix.model.Bill;
import jp.co.orix.model.BillMonth;

public class BillDaoImp implements BillDao {

	
//	 private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	@Autowired
	 private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	
	

	@Override
	public ArrayList<Bill> findAll() {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		BillDao dao = session.getMapper(BillDao.class);
		
		ArrayList<Bill> list = new ArrayList<Bill>();
		try {
			list = dao.findAll();
		
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
		return list;
	}

	@Override
	public Bill findByBillInfo(String group, String area, Date uesDate, Integer num) {
		// TODO Auto-generated method stub
		
		SqlSession session = sqlSession.openSession();
		BillDao dao = session.getMapper(BillDao.class);
		
		Bill bill = new Bill();
		
		try {
			bill = dao.findByBillInfo(group, area, uesDate, num);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
		return bill;
	}

	

	@Override
	public ArrayList<BillMonth> findByBillYearList(String endDate) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		BillDao dao = session.getMapper(BillDao.class);
		
		ArrayList<BillMonth> list = new ArrayList<>();
		
		try {
			
			list = dao.findByBillYearList(endDate);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
		return list;
	}

	@Override
	public Bill findByBillAll(Date uesDate) {
		// TODO Auto-generated method stub
		SqlSession session = sqlSession.openSession();
		BillDao dao = session.getMapper(BillDao.class);
		
		Bill bill = new Bill();
		
		try {
			bill = dao.findByBillAll(uesDate);
		} catch (Exception e) {
			// TODO: handle exception
			System.err.println(e.getMessage());
		}finally{
			session.close();
		}
		
		return bill;
	}

	
}
