package jp.co.orix.util;

import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;

import com.itextpdf.text.pdf.PdfStructTreeController.returnType;

public class Util {
	
	/*
	 * 이번달 포함해서 출력 
	 * month 에서 -1을 빼줌
	 * ex)이번달 포함출력이면 12 - 1 = 11
	 * 
	 * */
	static public ArrayList<String>  yearList(Calendar end, Integer month){
		
		SimpleDateFormat format = new SimpleDateFormat("yyyy/MM");
		ArrayList<String> array = new ArrayList<>();
		
		Calendar start  = Calendar.getInstance();
		start.setTime(end.getTime());
		start.add(Calendar.MONTH, (month * -1));
		while (start.compareTo(end) != 1) {			
			array.add(format.format(end.getTime()));
			end.add(Calendar.MONTH, -1);
		}
		
		return array;
	}
	
	
	static public String japanYearMonthCircle(Date date) {
			
			SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
			SimpleDateFormat formatMonth = new SimpleDateFormat("M");
			
			Integer year = Integer.parseInt(formatYear.format(date));
			Integer month = Integer.parseInt(formatMonth.format(date));
			
			Integer start = 0;
			String renko = "";
			if(year >= 2019){
				start = year - 2019;
				renko = "";
			}else if (year >=  1989  && year < 2019) {
				start = year -1989;
				renko = "平成";
			}else if (year < 1989) {
				start = year -1926;
				renko = "昭和";
			}
			start += 1;
			String title = renko + start + "年" + month +"月分ご請求金額"; 
			return title;
	}
	
	static public String japanYearMonthDayCircle(Date date) {
		
		SimpleDateFormat formatYear = new SimpleDateFormat("yyyy");
		SimpleDateFormat formatMonth = new SimpleDateFormat("M");
		SimpleDateFormat formatDay = new SimpleDateFormat("dd");
		
		Integer year = Integer.parseInt(formatYear.format(date));
		Integer month = Integer.parseInt(formatMonth.format(date));
		Integer day = Integer.parseInt(formatDay.format(date));
		
		Integer start = 0;
		String renko = "";
		if(year >= 2019){
			start = year - 2019;
			renko = "";
		}else if (year >=  1989  && year < 2019) {
			start = year -1989;
			renko = "平成";
		}else if (year < 1989) {
			start = year -1926;
			renko = "昭和";
		}
		start += 1;
		String title = renko + start + "年" + month +"月"+day+"日"; 
		return title;
	}
	
	static public String toNumberComma(Integer number)
	{
		DecimalFormat df = new DecimalFormat("#,###");
		return df.format(number);
	}
	
	static public String toNumberCommaAndPoint(double number)
	{
		DecimalFormat df = new DecimalFormat("#,##0.00");
		return df.format(number);
	}
	
}
