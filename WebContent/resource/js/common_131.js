/**
 * 
 */

function IsValidateStringValue(str) {
	for (var i = 0; i < str.length; i++) {
		var c = str.charCodeAt(i);
		if (!((c >= 0x0 && c < 0x81) || (c == 0xf8f0)
				|| (c >= 0xff61 && c < 0xffa0) || (c >= 0xf8f1 && c < 0xf8f4))) {

			return false;
		}
	}
	return true;
}

function IsValidateWideStringValue(str) {

	for (var i = 0; i < str.length; i++) {
		var c = str.charCodeAt(i);
		if (c < 256 || (c >= 0xff61 && c <= 0xff9f)) {
			return false;
		} else {
			return true;
		}
	}
}

function IsMailAddress(str) {
	var regex = /^([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)@([0-9a-zA-Z]([-_\\.]*[0-9a-zA-Z]+)*)[\\.]([a-zA-Z]{2,9})$/;

	if (!regex.test(str)) {
		return false;
	} else {
		return true;
	}
}

function IsPostalNo(str) {
	var regPostcode = /^([0-9]){7}$/;
	if (!regPostcode.test(str)) {
		return false;
	} else {
		return true;
	}
}