package jp.co.orix.dao.imp;

import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;

import jp.co.orix.bdconn.SqlSessionFactoryService;
import jp.co.orix.dao.CheckContractInfoDao;
import jp.co.orix.model.CheckContractInfoHV002;

public class CheckContractInfoDaoImpl implements CheckContractInfoDao {

	private CheckContractInfoHV002 checkContractInfoHV002;
	private SqlSessionFactory sqlSession = SqlSessionFactoryService.getsqlFactoeyService();
	
	@Override
	public CheckContractInfoHV002 getContractInfoBy(
			String customerContractNumber) {
		// TODO Auto-generated method stub
		SqlSession  session = sqlSession.openSession();
		CheckContractInfoDao checkContractInfoDao = session.getMapper(CheckContractInfoDao.class);
		
		try {
			checkContractInfoHV002 = checkContractInfoDao.getContractInfoBy(customerContractNumber);
		} catch(Exception e) {
			System.err.println(e.getMessage());
		} finally {
			session.close();
		}
	
		return checkContractInfoHV002;
	}

}