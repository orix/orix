package jp.co.orix.action.HvUsedAmountDay;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.net.URLEncoder;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import jp.co.orix.dao.GetCustomerContractNumberDao;
import jp.co.orix.util.CommApi_32;

import com.opensymphony.xwork2.ActionSupport;

public class HvUsedAmountDayCSVDownloadAction extends ActionSupport{
//	private String groupCombobox;
//	private String usePlaceCombobox;
	private String customerContractNumber; 
	
	private GetCustomerContractNumberDao getCustomerContractNumberDao;
	
	
	private String specDate;									//	
	private String compDate;									//
	private String compChk;										//
	
	private InputStream fileInputStream;
	private String contentDisposition;
	@Override
	public String execute() throws Exception{
		try{
			System.out.println("asdfasdf"+customerContractNumber);
		
		//3-2. CSVダウンロード処理
				if(compChk == null)
					compChk = "on";
				else if(compChk.equals("off")){
					compChk = null;
				}else{
					compChk = "on";
				}
				Calendar cal = Calendar.getInstance();
				Date date = null;
				SimpleDateFormat timeMonthFormat = new SimpleDateFormat("yyyy/MM");
				SimpleDateFormat timeDayFormat = new SimpleDateFormat("yyyy/MM/dd");
				//type cast : String to Date
				//set from date and to date for searching used Amount of power
				Date fromSpecDate = timeMonthFormat.parse(specDate);
				cal.setTime(fromSpecDate);
				cal.set(Calendar.DAY_OF_MONTH, 1);
				date = cal.getTime();
				String fromSpecDateStr = timeDayFormat.format(date);
				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1);
				cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)-1);
				date = cal.getTime();
				String toSpecDateStr = timeDayFormat.format(date);
				
				Date fromCompDate = timeMonthFormat.parse(compDate);
				cal.setTime(fromCompDate);
				cal.set(Calendar.DAY_OF_MONTH, 1);
				date = cal.getTime();
				String fromCompDateStr = timeDayFormat.format(date);
				cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1);
				cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)-1);
				date = cal.getTime();
				String toCompDateStr = timeDayFormat.format(date);
				
				
//				Map<String, String> condition = new HashMap<String, String>();
//				condition.put("groupCombobox", groupCombobox);
//				condition.put("usePlaceCombobox", usePlaceCombobox);
//				
//				customerContractNumber = getCustomerContractNumberDao.getCustomerContractNumber(condition);
				
				//test value setting(manageNumber)
				String manageNumber = "1";				//use GetControlNumbers() to get manageNumber after test.
				
				
				//get usePowerMap(for halfhour)
				List<Map<String, Double>> usePowerMapLi = CommApi_32.getUsedPowerAmountInfo(manageNumber, "2", fromSpecDateStr, toSpecDateStr, customerContractNumber);
				//get usePowerMap(for halfhour)
				List<Map<String, Double>> useCompPowerMapLi = null;
				if(compChk != null){
					useCompPowerMapLi = CommApi_32.getUsedPowerAmountInfo(manageNumber, "2", fromCompDateStr, toCompDateStr, customerContractNumber);
				}
				
				int dayRange = usePowerMapLi.size();
				ArrayList<String> columnNames = getColumnNameList(dayRange);
				//3. create and set usePowers
				ArrayList<String> usePowers = getUsePowerList(usePowerMapLi, dayRange);
				//4. create and set useCompPowers 
				ArrayList<String> useCompPowers = null;
				
				//5. create and set useDifPowers 
				ArrayList<String> useDifPowers = null;
				if(compChk != null && useCompPowerMapLi != null && useCompPowerMapLi.size() != 0){
					useCompPowers = getUsePowerList(useCompPowerMapLi, dayRange);
					useDifPowers = getUseDifPowerList(usePowers, useCompPowers);
				}
		
		//9)CSVダウンロードデータ作成を行う。
				Map<String, Object> csvDownLoadDataMap = new HashMap<String, Object>();
				
				//①ファイル名を設定する。
				SimpleDateFormat viewTimeFormat = new SimpleDateFormat("yyyy年MM月");
				SimpleDateFormat csvTimeFormat = new SimpleDateFormat("yyyyMMddHHmmss");
				Date time = timeMonthFormat.parse(specDate);
				
				//②	見出しを設定する。				
				List<String> csvColumn = new ArrayList<String>();
				//見出し項目：”No”、”指定日”、”比較日”、”時間”、”指定日使用量(kWh)”、”比較日使用量(kWh)”、”差分使用量(kWh)”
				csvColumn.add("No");
				csvColumn.add("指定月");
				csvColumn.add("比較月");
				csvColumn.add("日");
				csvColumn.add("指定月使用量(kWh)");
				csvColumn.add("比較月使用量(kWh)");
				csvColumn.add("差分使用量(kWh)");
				csvDownLoadDataMap.put("columns", csvColumn);
				
				//③	データ設定を行う。
				/*
				 * 	a.　設定データ：No、指定日、比較日、時間、指定日使用量、比較日使用量、差分使用量																					
				 *	b.　比較表示フラグが’ON’の場合、比較日付を設定する。																					
				 *	c.　比較日使用量が存在する場合、データを設定する。																					
				 *	d.　比較表示フラグが’ON’かつ比較使用量データが存在し、差分使用量が存在する場合、																					
		　		 *		差分使用量を設定する。																					
				 */
				//a.　設定データ：No、指定日、比較日、時間、指定日使用量、比較日使用量、差分使用量	
				List<List<String>> csvData = new ArrayList<List<String>>();
				
				List<String> csvDataNo = new ArrayList<String>();
				List<String> csvDataSpecDate = new ArrayList<String>();
				List<String> csvDataCompDate = new ArrayList<String>();
				
				List<String> csvDataTime = columnNames;	
				List<String> csvDataUsePowers =	usePowers;
				List<String> csvDataUseCompPowers = new ArrayList<String>();
				List<String> csvDataUseDifPowers = new ArrayList<String>();
				
				String specDateStrMonth = viewTimeFormat.format(timeMonthFormat.parse(specDate));
				
				for(int i=1; i<= dayRange; i++ ){
					csvDataNo.add(i+"");
					csvDataSpecDate.add(specDateStrMonth);
				}
				
				if(compChk != null && useCompPowerMapLi != null && useCompPowerMapLi.size() != 0){
					csvDataUseCompPowers = useCompPowers;
					csvDataUseDifPowers = useDifPowers;
				
					String compDateStrMonth = viewTimeFormat.format(timeMonthFormat.parse(compDate));
					for(int i=1; i<= dayRange; i++ ){
						csvDataCompDate.add(compDateStrMonth);
					}
				}
				
				csvData.add(csvDataNo);
				csvData.add(csvDataSpecDate);
				csvData.add(csvDataCompDate);
				csvData.add(csvDataTime);
				csvData.add(csvDataUsePowers);
				csvData.add(csvDataUseCompPowers);
				csvData.add(csvDataUseDifPowers);
				
				String fileName ="日毎電力使用量_" + viewTimeFormat.format(time)+ "_" + csvTimeFormat.format(Calendar.getInstance().getTime()) + ".csv";
				csvDownLoadDataMap.put("column",csvColumn);
				csvDownLoadDataMap.put("data",csvData);
				
				StringBuffer csvBuffer = CommApi_32.GetCsvFileResult(csvDownLoadDataMap);
				setContentDisposition("attachment;filename="+URLEncoder.encode(fileName, "UTF-8"));
				setFileInputStream(new ByteArrayInputStream(csvBuffer.toString().getBytes("UTF-8")));
		
				return SUCCESS;
		}catch(Exception e){
			e.printStackTrace();
			return "error";
		}
	}
	
	private ArrayList<String> getColumnNameList(int range) throws Exception{
		ArrayList<String> columNameList = new ArrayList<String>();
		for(int i=1; i <= range; i++){
			columNameList.add(i +"");
		}
		return columNameList;
	}
	
	private ArrayList<String> getUsePowerList(List<Map<String, Double>> usePowerMapLi, int dayRange) throws Exception{
		ArrayList<String> usePowerList = new ArrayList<String>();
		for(int i = 0; i < usePowerMapLi.size() &&  i < dayRange; i++){
			Iterator<Double> it = usePowerMapLi.get(i).values().iterator();
			double useAmount =0.0;
			while(it.hasNext()){
				useAmount = useAmount + it.next();
			}
			usePowerList.add(String.format("%.4f", useAmount));
		}
		return usePowerList;
	}
	
	private ArrayList<String> getUseDifPowerList(ArrayList<String> usePowers, ArrayList<String> useCompPowers) throws Exception{
		ArrayList<String> useDifPowers = new ArrayList<String>();
		for(int i = 0; i < usePowers.size(); i++){
			useDifPowers.add(String.format("%.4f", (Double.parseDouble(usePowers.get(i)) - Double.parseDouble(useCompPowers.get(i)))));
		}
		return useDifPowers;
	}

	public String getSpecDate() {
		return specDate;
	}

	public void setSpecDate(String specDate) {
		this.specDate = specDate;
	}

	public String getCompDate() {
		return compDate;
	}

	public void setCompDate(String compDate) {
		this.compDate = compDate;
	}

	public String getCompChk() {
		return compChk;
	}

	public void setCompChk(String compChk) {
		this.compChk = compChk;
	}

	public String getContentDisposition() {
		return contentDisposition;
	}

	public void setContentDisposition(String contentDisposition) {
		this.contentDisposition = contentDisposition;
	}

	public InputStream getFileInputStream() {
		return fileInputStream;
	}

	public void setFileInputStream(InputStream fileInputStream) {
		this.fileInputStream = fileInputStream;
	}

//	public String getGroupCombobox() {
//		return groupCombobox;
//	}
//
//	public void setGroupCombobox(String groupCombobox) {
//		this.groupCombobox = groupCombobox;
//	}
//
//	public String getUsePlaceCombobox() {
//		return usePlaceCombobox;
//	}
//
//	public void setUsePlaceCombobox(String usePlaceCombobox) {
//		this.usePlaceCombobox = usePlaceCombobox;
//	}

	public String getCustomerContractNumber() {
		return customerContractNumber;
	}

	public void setCustomerContractNumber(String customerContractNumber) {
		this.customerContractNumber = customerContractNumber;
	}

	public GetCustomerContractNumberDao getGetCustomerContractNumberDao() {
		return getCustomerContractNumberDao;
	}

	public void setGetCustomerContractNumberDao(
			GetCustomerContractNumberDao getCustomerContractNumberDao) {
		this.getCustomerContractNumberDao = getCustomerContractNumberDao;
	}
}
