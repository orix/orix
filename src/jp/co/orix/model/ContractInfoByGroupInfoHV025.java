package jp.co.orix.model;

import java.util.Date;

public class ContractInfoByGroupInfoHV025 {

	private int h2501;		//H2501	グループ別契約情報ID	number			NOT NULL
	private String h2502;	//H2502	ユーザーID				varchar2(10)	NOT NULL
	private String h2503;	//H2503	グループID				varchar2(10)	NOT NULL
	private String h2504;	//H2504	使用場所ID			varchar2(10)	NOT NULL
	private String h2505;	//H2505	契約番号				varchar2(10)	NOT NULL
	private String h2506;	//H2506	管理番号				varchar2(10)	NOT NULL
	private Date h2507;		//H2507	表示範囲開始			date	
	private Date h2508;		//H2508	表示範囲終了			date
	
	public int getH2501() {
		return h2501;
	}
	public void setH2501(int h2501) {
		this.h2501 = h2501;
	}
	public String getH2502() {
		return h2502;
	}
	public void setH2502(String h2502) {
		this.h2502 = h2502;
	}
	public String getH2503() {
		return h2503;
	}
	public void setH2503(String h2503) {
		this.h2503 = h2503;
	}
	public String getH2504() {
		return h2504;
	}
	public void setH2504(String h2504) {
		this.h2504 = h2504;
	}
	public String getH2505() {
		return h2505;
	}
	public void setH2505(String h2505) {
		this.h2505 = h2505;
	}
	public String getH2506() {
		return h2506;
	}
	public void setH2506(String h2506) {
		this.h2506 = h2506;
	}
	public Date getH2507() {
		return h2507;
	}
	public void setH2507(Date h2507) {
		this.h2507 = h2507;
	}
	public Date getH2508() {
		return h2508;
	}
	public void setH2508(Date h2508) {
		this.h2508 = h2508;
	}
	
	@Override
	public String toString() {
		return "ContractInfoByGroupInfoHV025 [h2501=" + h2501 + ", h2502="
				+ h2502 + ", h2503=" + h2503 + ", h2504=" + h2504 + ", h2505="
				+ h2505 + ", h2506=" + h2506 + ", h2507=" + h2507 + ", h2508="
				+ h2508 + "]";
	}
	
	
}
