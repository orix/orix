package jp.co.orix.model;

import java.util.Date;

public class ContactInfoHV003 {

	private int h0301; 		//H0301	高圧連絡先情報ID	number			NOT NULL
	private String h0302;	//H0302	 処理区分			varchar2(2)		NOT NULL
	private String h0303;	//H0303	グループID			varchar2(10)	NOT NULL
	private String h0304; 	//H0304	使用場所ID		varchar2(10)	NOT NULL
	private String h0305;	//H0305	住所				varchar2(100)	NOT NULL
	private String h0306;	//H0306	契約番号			varchar2(10)	NOT NULL
	private String h0307;	//H0307	契約者名義人		varchar2(20)	NOT NULL
	private String h0308;	//H0308	登録日			date			NOT NULL
	private Date h0309;		//H0309	更新日			date			NOT NULL
	private int h0310;		//H0310 ユーザー識別番号	number(15)		NOT NULL
	private String h0311;	//H0311 管理番号			varchar2(10)	NOT NULL
	private ContactDetailInfoHV013 contactDetailInfo;

	public int getH0301() {
		return h0301;
	}
	public void setH0301(int h0301) {
		this.h0301 = h0301;
	}
	public String getH0302() {
		return h0302;
	}
	public void setH0302(String h0302) {
		this.h0302 = h0302;
	}
	public String getH0303() {
		return h0303;
	}
	public void setH0303(String h0303) {
		this.h0303 = h0303;
	}
	public String getH0304() {
		return h0304;
	}
	public void setH0304(String h0304) {
		this.h0304 = h0304;
	}
	public String getH0305() {
		return h0305;
	}
	public void setH0305(String h0305) {
		this.h0305 = h0305;
	}
	public String getH0306() {
		return h0306;
	}
	public void setH0306(String h0306) {
		this.h0306 = h0306;
	}
	public String getH0307() {
		return h0307;
	}
	public void setH0307(String h0307) {
		this.h0307 = h0307;
	}
	public String getH0308() {
		return h0308;
	}
	public void setH0308(String h0308) {
		this.h0308 = h0308;
	}
	public Date getH0309() {
		return h0309;
	}
	public void setH0309(Date h0309) {
		this.h0309 = h0309;
	}
	public int getH0310() {
		return h0310;
	}
	public void setH0310(int h0310) {
		this.h0310 = h0310;
	}
	public String getH0311() {
		return h0311;
	}
	public void setH0311(String h0311) {
		this.h0311 = h0311;
	}
	public ContactDetailInfoHV013 getContactDetailInfo() {
		return contactDetailInfo;
	}
	public void setContactDetailInfo(ContactDetailInfoHV013 contactDetailInfo) {
		this.contactDetailInfo = contactDetailInfo;
	}
	
	@Override
	public String toString() {
		return "ContactInfoHV003 [h0301=" + h0301 + ", h0302=" + h0302
				+ ", h0303=" + h0303 + ", h0304=" + h0304 + ", h0305=" + h0305
				+ ", h0306=" + h0306 + ", h0307=" + h0307 + ", h0308=" + h0308
				+ ", h0309=" + h0309 + ", h0310=" + h0310 + ", h0311=" + h0311
				+ ", contactDetailInfo=" + contactDetailInfo + "]";
	}
	
	
}
