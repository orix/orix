package jp.co.orix.model;

public class UsePlace {
	
	private int H0801;
	private String H0802;
	private String H0804;
	private int H0805;
	private String H0806;
	private String H0807;
	private String H0808;
	private String H0809;
	private String H0810;
	private String H0811;
	private String H0812;
	private String H0813;
	private int H0814;
	private int H0815;
	private int H0816;
	
	public int getH0801() {
		return H0801;
	}
	public void setH0801(int h0801) {
		H0801 = h0801;
	}
	public String getH0802() {
		return H0802;
	}
	public void setH0802(String h0802) {
		H0802 = h0802;
	}
	public String getH0804() {
		return H0804;
	}
	public void setH0804(String h0804) {
		H0804 = h0804;
	}
	public int getH0805() {
		return H0805;
	}
	public void setH0805(int h0805) {
		H0805 = h0805;
	}
	public String getH0806() {
		return H0806;
	}
	public void setH0806(String h0806) {
		H0806 = h0806;
	}
	public String getH0807() {
		return H0807;
	}
	public void setH0807(String h0807) {
		H0807 = h0807;
	}
	public String getH0808() {
		return H0808;
	}
	public void setH0808(String h0808) {
		H0808 = h0808;
	}
	public String getH0809() {
		return H0809;
	}
	public void setH0809(String h0809) {
		H0809 = h0809;
	}
	public String getH0810() {
		return H0810;
	}
	public void setH0810(String h0810) {
		H0810 = h0810;
	}
	public String getH0811() {
		return H0811;
	}
	public void setH0811(String h0811) {
		H0811 = h0811;
	}	
	public String getH0812() {
		return H0812;
	}
	public void setH0812(String h0812) {
		H0812 = h0812;
	}
	public String getH0813() {
		return H0813;
	}
	public void setH0813(String h0813) {
		H0813 = h0813;
	}
	public int getH0814() {
		return H0814;
	}
	public void setH0814(int h0814) {
		H0814 = h0814;
	}
	public int getH0815() {
		return H0815;
	}
	public void setH0815(int h0815) {
		H0815 = h0815;
	}
	public int getH0816() {
		return H0816;
	}
	public void setH0816(int h0816) {
		H0816 = h0816;
	}
		
	@Override
	public String toString() {
		return "UsePlace [H0801=" + H0801 + ", H0802=" + H0802 + ", H0804="
				+ H0804 + ", H0805=" + H0805 + ", H0806=" + H0806 + ", H0807="
				+ H0807 + ", H0808=" + H0808 + ", H0809=" + H0809 + ", H0810="
				+ H0810 + ", H0811=" + H0811 + ", H0812=" + H0812 + ", H0813="
				+ H0813 + ", H0814=" + H0814 + ", H0815=" + H0815 + ", H0816="
				+ H0816 + "]";
	}
	
	public UsePlace(int h0801, String h0802, String h0804, int h0805,
			String h0806, String h0807, String h0808, String h0809,
			String h0810, String h0811, String h0812, String h0813, int h0814,
			int h0815, int h0816) {
		super();
		H0801 = h0801;
		H0802 = h0802;
		H0804 = h0804;
		H0805 = h0805;
		H0806 = h0806;
		H0807 = h0807;
		H0808 = h0808;
		H0809 = h0809;
		H0810 = h0810;
		H0811 = h0811;
		H0812 = h0812;
		H0813 = h0813;
		H0814 = h0814;
		H0815 = h0815;
		H0816 = h0816;
	}
	
	public UsePlace() {
		// TODO Auto-generated constructor stub
	}
	
}
