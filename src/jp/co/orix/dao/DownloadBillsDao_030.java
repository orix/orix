package jp.co.orix.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import jp.co.orix.model.GroupListModel_030;

import org.apache.ibatis.annotations.Param;

public interface DownloadBillsDao_030 {
	
	public ArrayList<GroupListModel_030> getGroupComboboxList();
	public ArrayList<GroupListModel_030> getSearchList(@Param("groupId") String groupId,
													   @Param("placeId") String placeId,
													   @Param("selectedDate") Date selectedDate);
	
	public ArrayList<String> getNengetsuComboboxList();
	
	public int getRowCount(Map condition);
	
	public ArrayList<GroupListModel_030> getPageInfo(Map<String, Object> condition);

	/*public Object getFormParamenter();
	public Object getGroupComboboxList();
	public Object getUsePlaceComboboxList();
	
	public Object isValidCombobox();
	public Object getControlNumber();
	public Object getCustomerContractNumber();
	public Object getAllFileResult();*/
};
