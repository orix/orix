/**
 * 
 */
function getSearchPostalNo2() {
	var zip2 = $('#zip2').val();

	// ここでzipのバリデーションを行ってください

	$.ajax({
		type : 'get',
		url : 'https://maps.googleapis.com/maps/api/geocode/json',
		crossDomain : true,
		dataType : 'json',
		data : {
			address : zip2,
			language : 'ja',
			sensor : false
		},
		success : function(resp) {
			if (resp.status == "OK") {
				// APIのレスポンスから住所情報を取得
				var obj = resp.results[0].address_components;
				if (obj.length < 5) {
					alert('正しい郵便番号を入力してください');
					return false;
				}
				var address1 = obj[3]['long_name'];
				var address2 = obj[2]['long_name'];
				var address3 = obj[1]['long_name'];
				//          var fullAddress = state+city+address1;

				$('#address1').val(address1); // 都道府県
				$('#address2').val(address2); // 市区町村
				$('#address3').val(address3); // 番地
				//          $('#address4').val(fullAddress);
			} else {
				alert('住所情報が取得できませんでした');
				return false;
			}
		}
	});
}

function getSearchPostalNo() {
	var zip = $('#zip').val();

	// ここでzipのバリデーションを行ってください
	$.ajax({
		type : 'get',
		url : 'https://maps.googleapis.com/maps/api/geocode/json',
		crossDomain : true,
		dataType : 'json',
		data : {
			address : zip,
			language : 'ja',
			sensor : false
		},
		success : function(resp) {
			if (resp.status == "OK") {
				// APIのレスポンスから住所情報を取得
				var obj = resp.results[0].address_components;
				if (obj.length < 5) {
					alert('正しい郵便番号を入力してください');
					return false;
				}
				
				var address1 = obj[3]['long_name'];
				var address2 = obj[2]['long_name'];
				var address3 = obj[1]['long_name'];
				var fullAddress = address1 + address2 + address3;
				$('#address').val(fullAddress);
//				$('#address11').val(address1); // 都道府県
//				$('#address22').val(address2); // 市区町村
//				$('#address33').val(address3); // 番地

			} else {
				alert('住所情報が取得できませんでした');
				return false;
			}
		}
	});
}

function test() {
	$('#address4').val("test");
}

function GetQueryStringParams(sParam)
{
    var sPageURL = window.location.search.substring(1);
    var sURLVariables = sPageURL.split('&');

    for (var i = 0; i < sURLVariables.length; i++)
    {
        var sParameterName = sURLVariables[i].split('=');
        if (sParameterName[0] == sParam)
        {
            return sParameterName[1];
        }
    }
}