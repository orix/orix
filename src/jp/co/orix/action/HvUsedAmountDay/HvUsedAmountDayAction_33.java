package jp.co.orix.action.HvUsedAmountDay;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.struts2.interceptor.ServletRequestAware;

import jp.co.orix.action.hvUsedAmountHalfHour.GraphCreateInfo;
import jp.co.orix.dao.CheckContractInfoDao;
import jp.co.orix.dao.GetCustomerContractNumberDao;
import jp.co.orix.util.CommApi_32;

import com.google.gson.Gson;
import com.opensymphony.xwork2.ActionSupport;

public class HvUsedAmountDayAction_33 extends ActionSupport implements ServletRequestAware {
	private String groupCombobox;			//グループコンボボックス
	private String usePlaceCombobox;		//ご使用場所
	private String customerContractNumber; 	//お客様契約番号
	
	private GetCustomerContractNumberDao getCustomerContractNumberDao;	//Dao for GetControlNumbers method
	
	private String adminMessage;								//コメント管理情報
	private String specDate;									//指定日			
	private String compDate;									//比較日	
	private String compChk;										//比較表示	
	private List<String> comboDateRange;						//月の範囲
	
	private Map<String, ArrayList<String>> usePowerTableMap;	// 使用量情報をモデル 
	private String usePowerGraphMappingModel;					// グラフ作成する情報クラス」モデル
	private String graphCreateInfo;								// グラフのマッピング用モデルリスト
	
	public HttpServletRequest request;
	private String parameter;									// ロードパラメータ
	@Override
	public String execute() throws Exception{
		//try-catch for setting error
		try{
			//1-3. コントロール初期化	
			//1)	画面間の引き継ぎ情報を設定する。
			CommApi_32 api = new CommApi_32();
			api.setFormParamator(request, "HvUsedAmountDay");
			HashMap<String, Object> param = api.getFormParamator("HvUsedAmountDay");
			parameter = (String)param.get("P1");
			
			//2)	コンボボックス設定
			Calendar cal = Calendar.getInstance();
			SimpleDateFormat timeMonthFormat = new SimpleDateFormat("yyyy/MM");
			SimpleDateFormat timeDayFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = cal.getTime();
			//③	指定月設定
			//set fromDate to System Time
			if(specDate== null || specDate.isEmpty()){
				specDate = timeMonthFormat.format(date);
			}
			//④	比較月設定

			if(compDate == null || compDate.isEmpty()){
				//set toDate to System time - 1 year
				cal.set(Calendar.YEAR, cal.get(Calendar.YEAR) - 1);
				date = cal.getTime();
				compDate = timeMonthFormat.format(date);
			}
			//月の範囲
			comboDateRange = setDateRangeForComboBox(timeMonthFormat);
			
			//set the date of spectation 
	//		//type cast : String to Date
			//set from date and to date for searching used Amount of power
			Date fromSpecDate = timeMonthFormat.parse(specDate);
			cal.setTime(fromSpecDate);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			date = cal.getTime();
			String fromSpecDateStr = timeDayFormat.format(date);
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1);
			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)-1);
			date = cal.getTime();
			String toSpecDateStr = timeDayFormat.format(date);
			
			//set the date of comparision 
			Date fromCompDate = timeMonthFormat.parse(compDate);
			cal.setTime(fromCompDate);
			cal.set(Calendar.DAY_OF_MONTH, 1);
			date = cal.getTime();
			String fromCompDateStr = timeDayFormat.format(date);
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)+1);
			cal.set(Calendar.DAY_OF_MONTH, cal.get(Calendar.DAY_OF_MONTH)-1);
			date = cal.getTime();
			String toCompDateStr = timeDayFormat.format(date);
			
			
			
			//3)	管理者コメント設定

			//get adminMessage from DB
			adminMessage = CommApi_32.GetComment("1", "010", "1");
			
			//4)以下のように各コントロールのプロパティを設定する。
			if(compChk == null)
				compChk = "on";
			else if(compChk.equals("off")){
				compChk = null;
			}else{
				compChk = "on";
			}
			
			if(groupCombobox == null){
				groupCombobox = "";
			}
			if(usePlaceCombobox == null){
				usePlaceCombobox = "";
			}
			
			//3-1.　表示処理
			//5)	共通サービス【GetControlNumbers】を使用し、管理番号を取得する。
			if(groupCombobox.length() != 0 && usePlaceCombobox.length() != 0){
				Map<String, String> condition = new HashMap<String, String>();
				condition.put("groupCombobox", groupCombobox);
				condition.put("usePlaceCombobox", usePlaceCombobox);
				
				customerContractNumber = getCustomerContractNumberDao.getCustomerContractNumber(condition);
				}else{
					customerContractNumber = "00000";
				}
			//6)	共通サービス【GetCustomerContractNumber】を使用し、お客様契約番号を取得する
			//test value setting(manageNumber) // 担当者が来ない。。。
			String manageNumber = "1";				//use GetControlNumbers() to get manageNumber after test.
			
			//7)	日毎別データを取得する。
				//①	指定日の使用量情報を取得する。
			List<Map<String, Double>> usePowerMapLi = CommApi_32.getUsedPowerAmountInfo(manageNumber, "2", fromSpecDateStr, toSpecDateStr, customerContractNumber);
				//②	比較表示の値が’ON’の場合、比較日の使用量情報を取得する。
			List<Map<String, Double>> useCompPowerMapLi = null;
			if(compChk != null){
				useCompPowerMapLi = CommApi_32.getUsedPowerAmountInfo(manageNumber, "2", fromCompDateStr, toCompDateStr, customerContractNumber);
			}
				//③	使用量情報をモデルに設定する。
			//set table data map(usePowerTableMap)
			//1. create map to get four String arrays (column names, use power amounts when start date, use power amounts when compared date) 
			usePowerTableMap = new HashMap<String, ArrayList<String>>();
			//2. create and set columnNames
			//added
			int dayRange = usePowerMapLi.size();
			ArrayList<String> columnNames = getColumnNameList(dayRange);
			usePowerTableMap.put("columnNames", columnNames);
			//3. create and set usePowers
			ArrayList<String> usePowers = getUsePowerList(usePowerMapLi, dayRange);
			usePowerTableMap.put("usePowers", usePowers);
			//4. create and set useCompPowers 
			ArrayList<String> useCompPowers = null;
			if(compChk != null ){
				useCompPowers = getUsePowerList(useCompPowerMapLi, dayRange);
				usePowerTableMap.put("useCompPowers", useCompPowers);
			}
			//5. create and set useDifPowers 
			ArrayList<String> useDifPowers = null;
			if(compChk != null && useCompPowerMapLi != null && useCompPowerMapLi.size() != 0){
				useDifPowers = getUseDifPowerList(usePowers, useCompPowers);
				usePowerTableMap.put("useDifPowers", useDifPowers);
			}
				//④下記の情報を「グラフ作成する情報クラス」モデルに設定する。
			GraphCreateInfo graphCreateInfo = new GraphCreateInfo();
			//(additional)set x-axis label
			graphCreateInfo.setLabels(columnNames);
			//set datasets
			//1. set dataset for usePowers 
			//h.　比較表示フラグが’ON’かつ比較データが存在する場合、比較日の時間別使用量を設定する。
			if (compChk != null && useCompPowerMapLi != null && useCompPowerMapLi.size() != 0) {
				graphCreateInfo.getDatasets().add(getGraphLineDataset("比較日"));
			}
			//2. set dataset for usePowers 
			//b.　各配色をWeb.cconfigのPowerBorderColorより取得し、設定する。(not provided by customer)
			//c.　昼間凡例を設定する。
			//d.　夜間凡例を設定する。
			graphCreateInfo.getDatasets().add(
					getGraphBarDataset("昼間時間(8:00~22:00)","rgba(244,129,86,1)"));
			graphCreateInfo.getDatasets().add(
					getGraphBarDataset("夜間(昼間時間以外)","rgba(245,195,115,1)"));
			//2. set dataset for usePowers 
			//b.　各配色をWeb.cconfigのPowerBorderColorより取得し、設定する。(not provided by customer)
			
			//f.　指定日の時間別使用量を設定する。ｱ.　使用量設定値 = 使用量 * 10000M
			graphCreateInfo.setxScaleLabel("日");
			graphCreateInfo.setyScaleLabel("kWh");
			//f.　指定日の時間別使用量を設定する。ｲ.　取得した時間別使用量が昼間の場合、昼間リストに設定し、　それ以外の場合、夜間リストに設定する。 (can not divide datas into day-time set and night-time set)
			Map<String, Object> usePowerGraphMap = new HashMap<String, Object>();
			
			
			usePowerGraphMap.put("useDayTimePowers", getDayTimeUsePowerList(usePowerMapLi,dayRange));
			usePowerGraphMap.put("useNightTimePowers", getNightTimeUsePowerList(usePowerMapLi,dayRange));
			//h.　比較表示フラグが’ON’かつ比較データが存在する場合、比較日の時間別使用量を設定する。
			if(compChk != null && useCompPowerMapLi != null && useCompPowerMapLi.size() != 0) {
				usePowerGraphMap.put("useCompPowers", useCompPowers);
			}
				//⑤共通サービス【GetGraphUsedPower】を使用し、使用量グラフ作成を行う。 -グラフのマッピング用モデルリスト
			Gson gson = new Gson();
			usePowerGraphMappingModel = gson.toJson(usePowerGraphMap);
			this.graphCreateInfo = gson.toJson(graphCreateInfo);
			
			//8)	6)で取得データが存在しない場合、『共通メッセージ(E0005)』を表示する。 //JSPでします。
			return SUCCESS;
		}catch(Exception e){
			return "error";
		}
		
	}
	
	private List<String> setDateRangeForComboBox(SimpleDateFormat format){
		List<String> comboDateRange = new ArrayList<String>();
		Calendar cal = Calendar.getInstance();
		String yearMonth = format.format(cal.getTime());
		comboDateRange.add(yearMonth);
		for(int i = 0; i < 23; i++){
			cal.set(Calendar.MONTH, cal.get(Calendar.MONTH)-1);
			yearMonth = format.format(cal.getTime());
			comboDateRange.add(yearMonth);
		}
		return comboDateRange;
	}
	
	private Map<String,Object> getGraphBarDataset(String label, String color){
		Map<String,Object> dataset = new HashMap<String,Object>();
		dataset.put("type", "bar");
		dataset.put("label", label);
		dataset.put("backgroundColor", color);
		return dataset;
	}
	
	private Map<String,Object> getGraphLineDataset(String label){
		Map<String,Object> dataset = new HashMap<String,Object>();
		dataset.put("type", "line");
		dataset.put("label", label);
		dataset.put("backgroundColor", "rgba(107,156,213,1)");
		dataset.put("tension", 0);
		dataset.put("borderWidth", 2);
		dataset.put("radius	", 2);

		dataset.put("borderColor","rgba(107,156,213,1)");
		
		dataset.put("fill", false);
		dataset.put("data", "");
		return dataset;
	}
	
	private ArrayList<String> getUsePowerList(List<Map<String, Double>> usePowerMapLi, int dayRange) throws Exception{
		ArrayList<String> usePowerList = new ArrayList<String>();
		for(int i = 0; i < usePowerMapLi.size() &&  i < dayRange; i++){
			Iterator<Double> it = usePowerMapLi.get(i).values().iterator();
			double useAmount =0.0;
			while(it.hasNext()){
				useAmount = useAmount + it.next();
			}
			usePowerList.add(String.format("%.4f", useAmount));
		}
		return usePowerList;
	}
	
	private ArrayList<String> getUseDifPowerList(ArrayList<String> usePowers, ArrayList<String> useCompPowers) throws Exception{
		ArrayList<String> useDifPowers = new ArrayList<String>();
		for(int i = 0; i < usePowers.size(); i++){
			if(usePowers.get(i) != null && useCompPowers.get(i) != null)
				useDifPowers.add(String.format("%.4f", (Double.parseDouble(usePowers.get(i)) - Double.parseDouble(useCompPowers.get(i)))));
		}
		return useDifPowers;
	}
	
	private ArrayList<String> getNightTimeUsePowerList(List<Map<String, Double>> usePowerMapLi, int dayRange) throws Exception{
		ArrayList<String> usePowerList = new ArrayList<String>();
		for(int i = 0; i < usePowerMapLi.size() &&  i < dayRange; i++){
			double useAmount = 0;
			for(int j=1; j <= 7; j++){
				useAmount += usePowerMapLi.get(i).get(j  +"");
			}
			for(int j=23; j <= usePowerMapLi.get(i).size(); j++){
				useAmount += usePowerMapLi.get(i).get(j  +"");
			}
			usePowerList.add(String.format("%.4f", useAmount));
		}
		return usePowerList;
	}
	
	private ArrayList<String> getDayTimeUsePowerList(List<Map<String, Double>> usePowerMapLi, int dayRange) throws Exception{
		ArrayList<String> usePowerList = new ArrayList<String>();
		for(int i = 0; i < usePowerMapLi.size() &&  i < dayRange; i++){
			double useAmount = 0;
			for(int j=8; j <= 22; j++){
				useAmount += usePowerMapLi.get(i).get(j  +"");
			}
			usePowerList.add(String.format("%.4f", useAmount));
		}
		return usePowerList;
	}
	
	private ArrayList<String> getColumnNameList(int range){
		ArrayList<String> columNameList = new ArrayList<String>();
		for(int i=1; i <= range; i++){
			columNameList.add(i +"");
		}
		return columNameList;
	}

	public String getAdminMessage() {
		return adminMessage;
	}

	public void setAdminMessage(String adminMessage) {
		this.adminMessage = adminMessage;
	}

	public String getSpecDate() {
		return specDate;
	}

	public void setSpecDate(String specDate) {
		this.specDate = specDate;
	}

	public String getCompDate() {
		return compDate;
	}

	public void setCompDate(String compDate) {
		this.compDate = compDate;
	}

	public String getCompChk() {
		return compChk;
	}

	public void setCompChk(String compChk) {
		this.compChk = compChk;
	}

	public Map<String, ArrayList<String>> getUsePowerTableMap() {
		return usePowerTableMap;
	}

	public void setUsePowerTableMap(Map<String, ArrayList<String>> usePowerTableMap) {
		this.usePowerTableMap = usePowerTableMap;
	}

	public String getUsePowerGraphMappingModel() {
		return usePowerGraphMappingModel;
	}

	public void setUsePowerGraphMappingModel(String usePowerGraphMappingModel) {
		this.usePowerGraphMappingModel = usePowerGraphMappingModel;
	}

	public String getGraphCreateInfo() {
		return graphCreateInfo;
	}

	public void setGraphCreateInfo(String graphCreateInfo) {
		this.graphCreateInfo = graphCreateInfo;
	}

	public List<String> getCombDateRange() {
		return comboDateRange;
	}

	public void setCombDateRange(List<String> combDateRange) {
		this.comboDateRange = combDateRange;
	}

	public List<String> getComboDateRange() {
		return comboDateRange;
	}

	public void setComboDateRange(List<String> comboDateRange) {
		this.comboDateRange = comboDateRange;
	}

	public String getGroupCombobox() {
		return groupCombobox;
	}

	public void setGroupCombobox(String groupCombobox) {
		this.groupCombobox = groupCombobox;
	}

	public String getUsePlaceCombobox() {
		return usePlaceCombobox;
	}

	public void setUsePlaceCombobox(String usePlaceCombobox) {
		this.usePlaceCombobox = usePlaceCombobox;
	}

	public String getCustomerContractNumber() {
		return customerContractNumber;
	}

	public void setCustomerContractNumber(String customerContractNumber) {
		this.customerContractNumber = customerContractNumber;
	}

	public GetCustomerContractNumberDao getGetCustomerContractNumberDao() {
		return getCustomerContractNumberDao;
	}

	public void setGetCustomerContractNumberDao(
			GetCustomerContractNumberDao getCustomerContractNumberDao) {
		this.getCustomerContractNumberDao = getCustomerContractNumberDao;
	}

	@Override
	public void setServletRequest(HttpServletRequest request) {
		// TODO Auto-generated method stub
		this.request = request;
	}
	
	public String getParameter() {
		return parameter;
	}

	public void setParameter(String parameter) {
		this.parameter = parameter;
	}
	
}
